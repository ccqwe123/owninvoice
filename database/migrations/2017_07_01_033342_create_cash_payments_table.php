<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashPaymentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cash_payments', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('sales_invoice_id')
				->unsigned()->nullable();
			$table->foreign('sales_invoice_id')
				->references('id')
				->on('sales_invoice');
			$table->decimal('amount',18,4);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cash_payments');
	}
}
