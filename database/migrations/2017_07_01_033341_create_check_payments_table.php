<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckPaymentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('check_payments', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('sales_invoice_id')
				->unsigned()->nullable();
			$table->foreign('sales_invoice_id')
				->references('id')
				->on('sales_invoice');
			$table->decimal('amount',18,4);
			$table->integer('bank_id')
				->unsigned()->nullable();
			$table->foreign('bank_id')
				->references('id')
				->on('banks');
			$table->string('account_number');
			$table->string('check_number');
			$table->date('check_date');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('check_payments');
	}
}
