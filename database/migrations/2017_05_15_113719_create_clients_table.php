<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('clients', function (Blueprint $table) {
			$table->increments('id');
			$table->string('client_name');
			$table->string('email')->nullable();
			$table->integer('vatable')->default(0);
			$table->string('tin')->nullable();
			$table->decimal('credit_limit',18,4);
			$table->integer('terms');
			$table->tinyInteger('personal')->default(0);
			$table->integer('account_manager_id')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clients');
	}
}
