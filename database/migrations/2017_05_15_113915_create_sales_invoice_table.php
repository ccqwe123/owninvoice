<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesInvoiceTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales_invoice', function (Blueprint $table) {
			$table->increments('id');
			$table->date('invoice_date');
			$table->string('si');
			$table->string('po')->unique();
			$table->enum('pay_type', ['cash', 'check']);
			$table->integer('client_id')
				->unsigned()->nullable();
			$table->foreign('client_id')
				->references('id')
				->on('clients');
			$table->integer('branch_id')
				->unsigned()->nullable();
			$table->foreign('branch_id')
				->references('id')
				->on('client_branches');
			$table->integer('account_manager_id')
				->unsigned()->nullable();
			$table->foreign('account_manager_id')
				->references('id')
				->on('account_managers');
			$table->decimal('tax_rate',3,2);
			$table->integer('terms');
			$table->enum('status', ['unpaid', 'paid','on-hold','over-due','cancelled']);
			$table->decimal('amount_total',18,4);
			$table->decimal('vat_sales',18,4);
			$table->decimal('tax_amount',18,4);
			$table->decimal('discount',18,4);
			$table->tinyInteger('archived')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales_invoice');
	}
}
