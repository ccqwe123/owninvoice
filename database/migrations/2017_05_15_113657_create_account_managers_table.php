<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountManagersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('account_managers', function (Blueprint $table) {
			$table->increments('id');
			$table->string('emp_id');
			$table->string('first_name');
			$table->string('middle_name');
			$table->string('last_name');
			$table->integer('user_id')
				->unsigned()->nullable();
			$table->foreign('user_id')
				->references('id')
				->on('users');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('account_managers');
	}
}
