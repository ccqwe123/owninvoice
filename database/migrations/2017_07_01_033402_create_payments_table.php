<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payments', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('sales_invoice_id')
				->unsigned();
			$table->foreign('sales_invoice_id')
				->references('id')
				->on('sales_invoice');

			$table->integer('cash_payment_id')
				->unsigned()->nullable();
			$table->foreign('cash_payment_id')
				->references('id')
				->on('cash_payments')
				->onDelete('cascade');

			$table->integer('check_payment_id')
				->unsigned()->nullable();
			$table->foreign('check_payment_id')
				->references('id')
				->on('check_payments')
				->onDelete('cascade');

			$table->string('or_number');
			$table->decimal('less',18,4)->default(0);
			$table->string('photo');
			$table->tinyInteger('approved')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payments');
	}
}
