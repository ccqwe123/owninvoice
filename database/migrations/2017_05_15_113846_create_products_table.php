<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function (Blueprint $table) {
			$table->increments('id');
			$table->string('prod_code',20)->unique();
			$table->string('prod_name',30);
			$table->string('prod_bar_code',30)->nullable();
			$table->tinyInteger('status')->default(1);
			$table->string('prod_description');            
			$table->decimal('prod_price', 18, 4);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}
}
