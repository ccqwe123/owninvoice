<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionItemsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transaction_items', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('sales_invoice_id')
				->unsigned()->nullable();
			$table->foreign('sales_invoice_id')
				->references('id')
				->on('sales_invoice');
			$table->integer('product_id')
				->unsigned()->nullable();
			$table->foreign('product_id')
				->references('id')
				->on('products');
			$table->integer('qty');
			$table->decimal('price',18,4);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transaction_items');
	}
}
