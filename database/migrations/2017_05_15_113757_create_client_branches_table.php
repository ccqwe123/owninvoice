<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientBranchesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('client_branches', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('client_id')->unsigned();
			$table->foreign('client_id')->references('id')->on('clients');
			$table->string('branch_name');
			$table->string('contact_person');
			$table->string('tel_no')->nullable();
			$table->string('mobile_no')->nullable();;
			$table->string('fax_no')->nullable();;
			$table->string('address');
			$table->string('email')->nullable();
			$table->tinyInteger('personal')->default(0);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('client_branches');
	}
}
