<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
	return [
		'username' => $faker->name,
		'password' => bcrypt(str_random(10)),
		'first_name' => $faker->firstName,
		'middle_name' => $faker->lastName,
		'last_name' => $faker->lastName,
		'contact' => $faker->phoneNumber,
		'photo' => 'anon.png',
	];
});

$factory->define(App\Product::class, function (Faker\Generator $faker) {
	return [
		'prod_code' => str_random(10),
		'prod_name' => $faker->word,
		'prod_description' => $faker->text,
		'prod_price' => rand(1.00,99.99),
	];
});

$factory->define(App\Client::class, function (Faker\Generator $faker) {
	return [
		'client_name' => $faker->company,
		'email' => $faker->email,
		'vatable' => 1,
		'tin' => '',
		'credit_limit' => '30000',
		'terms' => '30',
		'account_manager_id' => 1,
	];
});


$factory->define(App\ClientBranch::class, function (Faker\Generator $faker) {
	return [
		'client_id' => rand(1,5),
		'branch_name' => $faker->company,
		'contact_person' => $faker->name,
		'address' => $faker->address,
		'tel_no' => $faker->phoneNumber,
		'mobile_no' => $faker->phoneNumber,
		'fax_no' => $faker->phoneNumber,
		
	];
});

$factory->define(App\Manager::class, function (Faker\Generator $faker) {
	return [
		'emp_id' => str_random(4),
		'first_name' => $faker->firstName,
		'middle_name' => $faker->lastName,
		'last_name' => $faker->lastName,
		'user_id' => null,
		
	];
});