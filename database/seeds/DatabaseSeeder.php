<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\Client;
use App\ClientBranch;
use App\Manager;
use App\Roles;
use App\RolePrivileges;
use App\Privileges;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(SampleSiteSeeder::class);
    }
}
class SampleSiteSeeder extends Seeder 
	{
		public function run()
		{			
			$user = User::create([
				'username'	=>	'admin',
				'password'	=>	Hash::make('admin'),
				'first_name'	=>	'gabu',
				'middle_name'	=>	'gabu',
				'last_name'	=>	'gabu',
				'contact'=>	'7493345',
				]);

			DB::table('banks')->insert([
				'bank_name'	=>	'BDO Mayon',
				]);
			DB::table('banks')->insert([
				'bank_name'	=>	'BPI Mayon',
				]);

			//settings
			DB::table('var')->insert([
				'name' => 'software_name',
				'value' => 'Invoice System v1.0',
				'description' => 'Software Name',
				]);
			DB::table('var')->insert([
				'name' => 'company_name',
				'value' => 'Aguora IT Solutions',
				'description' => 'Company Name',
				]);
			DB::table('var')->insert([
				'name' => 'company_address',
				'value' => '72a Dorotea St Brgy Balingasa QC',
				'description' => 'Company Address',
				]);
			DB::table('var')->insert([
				'name' => 'company_phone',
				'value' => '7434555',
				'description' => 'Company Phone',
				]);
			DB::table('var')->insert([
				'name' => 'tax_rate',
				'value' => '0.12',
				'description' => 'Tax Rate',
				]);

			DB::table('var')->insert([
				'name' => 'decimal',
				'value' => '2',
				'description' => 'Decimal Place',
				]);

			DB::table('var')->insert([
				'name' => 'font_size',
				'value' => '11',
				'description' => 'List Font Size',
				]);


			DB::table('var')->insert([
				'name' => 'letter_head',
				'value' => '',
				'description' => 'Letter Head',
				]);

			DB::table('var')->insert([
				'name' => 'soa_footer',
				'value' => '',
				'description' => 'SOA footer',
				]);

			// DB::table('var')->insert([
			// 	'name' => 'billing_auto_email',
			// 	'value' => '0',
			// 	'description' => 'Auto Email on Billing',
			// 	]);

			DB::table('var')->insert([
				'name' => 'company_logo',
				'value' => '',
				'description' => 'Company Logo',
				]);


			$roles = Roles::create([
				'role_name' => 'superuser',
				]);

			//permissions
			$allow_product = Privileges::create([
				'name' => 'is_allow_products',
				]);
			DB::table('role_privileges')->insert([
				'role_id' => $roles->id,
				'privilege_id' => $allow_product->id,
				]);


			$allow_client = Privileges::create([
				'name' => 'is_allow_clients',
				]);
			DB::table('role_privileges')->insert([
				'role_id' => $roles->id,
				'privilege_id' => $allow_client->id,
				]);


			$is_allow_invoice = Privileges::create([
				'name' => 'is_allow_invoice',
				]);
			DB::table('role_privileges')->insert([
				'role_id' => $roles->id,
				'privilege_id' => $is_allow_invoice->id,
				]);


			$is_allow_payment = Privileges::create([
				'name' => 'is_allow_payment',
				]);
			DB::table('role_privileges')->insert([
				'role_id' => $roles->id,
				'privilege_id' => $is_allow_payment->id,
				]);

			$is_allow_user = Privileges::create([
				'name' => 'is_allow_user',
				]);
			DB::table('role_privileges')->insert([
				'role_id' => $roles->id,
				'privilege_id' => $is_allow_user->id,
				]);

			$is_allow_managers = Privileges::create([
				'name' => 'is_allow_managers',
				]);
			DB::table('role_privileges')->insert([
				'role_id' => $roles->id,
				'privilege_id' => $is_allow_managers->id,
				]);

			$is_allow_banks = Privileges::create([
				'name' => 'is_allow_banks',
				]);
			DB::table('role_privileges')->insert([
				'role_id' => $roles->id,
				'privilege_id' => $is_allow_banks->id,
				]);

			$is_allow_settings = Privileges::create([
				'name' => 'is_allow_settings',
				]);
			DB::table('role_privileges')->insert([
				'role_id' => $roles->id,
				'privilege_id' => $is_allow_settings->id,
				]);
			
			$is_allow_reports = Privileges::create([
				'name' => 'is_allow_reports',
				]);
			DB::table('role_privileges')->insert([
				'role_id' => $roles->id,
				'privilege_id' => $is_allow_reports->id,
				]);

			$is_allow_roles = Privileges::create([
				'name' => 'is_allow_roles',
				]);
			DB::table('role_privileges')->insert([
				'role_id' => $roles->id,
				'privilege_id' => $is_allow_roles->id,
				]);
			
			$is_allow_logs = Privileges::create([
				'name' => 'is_allow_logs',
				]);
			DB::table('role_privileges')->insert([
				'role_id' => $roles->id,
				'privilege_id' => $is_allow_logs->id,
				]);
			
			$is_allow_logs = Privileges::create([
				'name' => 'is_allow_payment_approval',
				]);
			DB::table('role_privileges')->insert([
				'role_id' => $roles->id,
				'privilege_id' => $is_allow_logs->id,
				]);
			

			DB::table('user_roles')->insert([
				'user_id' => $user->id,
				'role_id' => $roles->id,
				]);

			factory(Product::class,5)->create()->each(function($post){
				$post->save();
			});

			factory(Client::class,5)->create()->each(function($post){
				$post->save();
			});

			factory(ClientBranch::class,5)->create()->each(function($post){
				$post->save();
			});
			
			factory(Manager::class,2)->create()->each(function($post){
				$post->save();
			});

			$this->command->info('Seed okay!');

		}
	}