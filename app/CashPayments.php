<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashPayments extends Model
{
	//
	protected $fillable = [
		'sales_invoice_id', 'amount',
	];


	protected $table = 'cash_payments';

	public function payment_info()
	{
		return $this->belongsTo('App\Payments', 'sales_invoice_id');
	}
}
