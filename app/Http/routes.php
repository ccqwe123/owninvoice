<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
/ hi anglia, hello world
*/

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});


Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});


Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});


Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});


Route::get('/', function () {
    if(Auth::check())
	{
		return redirect('dashboard');
	}
	else
	{

		return redirect('login');
	}
});
Route::get('login', 'HomeController@login')->name('dashboarlogind');

Route::group(['middleware' => 'auth'], function() {

	Route::get('dashboard', 'HomeController@dashboard')->name('dashboard');

	Route::group(['middleware' => 'permission:is_allow_payment_approval'], function() {
		// Route::get('/payment/{id}/approve/{type}', 'PaymentController@approvePayment')->name('approve-payment');
		Route::get('/payment/{id}/approve/{type}/invoice/{invoice_id}', 'PaymentController@approvePayment')->name('approve-payment');
	});

	//invoice
	Route::group(['middleware' => 'permission:is_allow_invoice'], function() {
		Route::resource('invoice', 'InvoiceController');
		Route::get('invoice', 'InvoiceController@invoiceSearch')->name('invoice-search');
		Route::get('invoice-delete/{id}', 'InvoiceController@destroy')->name('invoice-delete');

		Route::get('invoice-archive/{id}', 'InvoiceController@archive')->name('invoice-archive');

		Route::get('invoice/import', 'InvoiceController@invoiceImport')->name('invoice-import');
		Route::post('invoice/import/csv', 'InvoiceController@invoiceImportCsv')->name('invoice-import-csv');
		Route::get('invoice/export/{type}', 'InvoiceController@invoiceExport')->name('invoice-export');

		Route::post('/invoice/transfer/', 'InvoiceController@transferInvoice')->name('transfer-invoice');
	});
	//payment
	Route::group(['middleware' => 'permission:is_allow_payment'], function() {
		Route::get('payments', 'PaymentController@index')->name('invoice-add-payment');
		Route::get('payments', 'PaymentController@paymentSearch')->name('payment-search');
		Route::get('invoice/{id}/payments', 'PaymentController@AddPayment')->name('payments-index');	
		Route::post('payment/banktobank', 'PaymentController@banktobankPayment')->name('invoice-add-banktobank-payment');
		Route::post('payment/cash', 'PaymentController@cashPayment')->name('invoice-add-cash-payment');
		Route::post('payment/check', 'PaymentController@checkPayment')->name('invoice-add-payment');
		Route::post('payment/deposit', 'PaymentController@depositPayment')->name('invoice-add-deposit-payment');
		Route::get('payments/{id}/delete/total/{total}/invoiceid/{invid}', 'PaymentController@destroy')->name('invoice-delete-payment');
		Route::get('payments/{id}/check-details', 'PaymentController@checkDetails')->name('payment-check-payment');
		Route::get('payments/{id}/cash-details', 'PaymentController@cashDetails')->name('payment-cash-payment');
		Route::get('payments/{id}/deposit-details', 'PaymentController@depositDetails')->name('payment-deposit-payment');
	});

	//products
	Route::group(['middleware' => 'permission:is_allow_products'], function() {
		Route::resource('products', 'ProductController');
		Route::get('products', 'ProductController@productSearch')->name('product-search');
		Route::get('products/{id}/active', 'ProductController@productSetActive')->name('product-set-active');
		Route::get('product-delete/{id}', 'ProductController@destroy')->name('product-delete');
		Route::get('product/import', 'ProductController@productImport')->name('product-import');
		Route::post('product/import/csv', 'ProductController@productImportCsv')->name('product-import-csv');
		Route::get('product/export/{type}', 'ProductController@productExport')->name('product-export');
	});
	

	//user
	Route::group(['middleware' => 'permission:is_allow_user'], function() {
		Route::resource('users', 'UserController');
		Route::get('users', 'UserController@userSearch')->name('user-search');
		Route::get('user-delete/{id}', 'UserController@destroy')->name('user-delete');
		Route::get('user/import', 'UserController@userImport')->name('user-import');
		Route::post('user/import/csv', 'UserController@userImportCsv')->name('user-import-csv');
		Route::get('user/export/{type}', 'UserController@userExport')->name('user-export');

		Route::get('users/{id}/role','UserController@setRole')->name('user-set-role');
		Route::post('users/{id}/update-role','UserController@updateRole')->name('user-update-role');
	});

	//roles
	Route::group(['middleware' => 'permission:is_allow_roles'], function() {
		Route::resource('roles', 'RolesController');
		// Route::get('users', 'PrivilegesController@userSearch')->name('user-search');
		Route::get('roles/{id}/privilege', 'RolesController@privilege')->name('roles-privilege');
		Route::post('roles/{id}/update/{x}', 'RolesController@updatePrivilege')->name('roles-update-privilege');
		Route::get('role-delete/{id}', 'RolesController@destroy')->name('roles-delete');
		
		
	});


	//managers
	Route::group(['middleware' => 'permission:is_allow_managers'], function() {
		Route::resource('managers', 'ManagerController');
		Route::get('managers', 'ManagerController@managerSearch')->name('manager-search');
		Route::get('manager-delete/{id}', 'ManagerController@destroy')->name('managers-delete');
		Route::get('manager/import', 'ManagerController@managerImport')->name('manager-import');
		Route::post('manager/import/csv', 'ManagerController@managerImportCsv')->name('manager-import-csv');
		Route::get('manager/export/{type}', 'ManagerController@managerExport')->name('managers-export');
	});

	//client
	Route::group(['middleware' => 'permission:is_allow_clients'], function() {
		Route::resource('clients', 'ClientController');
		Route::get('clients', 'ClientController@clientSearch')->name('client-search');
		Route::get('client-delete/{id}', 'ClientController@destroy')->name('client-delete');
		Route::get('client/import', 'ClientController@clientImport')->name('client-import');
		Route::post('client/import/csv', 'ClientController@userImportCsv')->name('client-import-csv');
		Route::get('client/export/{type}', 'ClientController@userExport')->name('client-export');
		Route::get('clients/{id}/branch/{branch_id}/bill', 'ClientController@billClient')->name('client-bill');
		

		Route::get('clients/{id}/branch/{branch_id}/bill_preview','ClientController@billPreview')->name('client-bill-preview');
		Route::get('clients/{id}/branch/{branch_id}/pdf','ClientController@pdf')->name('client-bill-pdf');

		//ClientBranch
		Route::get('clients/{id}/branch', 'BranchController@branchSearch')->name('branch-search');
		Route::resource('clients.branch', 'BranchController');

		Route::get('/client/{client_id}/branch-delete/{id}', 'BranchController@destroy')->name('branch-delete');

	});

	//banks
	Route::group(['middleware' => 'permission:is_allow_banks'], function() {
		Route::resource('banks','BankController');
		Route::get('banks', 'BankController@bankSearch')->name('bank-search');
		Route::get('bank-delete/{id}', 'BankController@destroy')->name('bank-delete');
	});
	
	//logs
	Route::group(['middleware' => 'permission:is_allow_logs'], function() {
		// Route::resource('logs','ActivityLogController');
		// Route::get('logs', 'ActivityLogController@logSearch')->name('log-search');
		Route::get('logs','ActivityLogController@logSearch');
	});


	//settings
	Route::group(['middleware' => 'permission:is_allow_settings'], function() {
		Route::resource('settings', 'SettingsController');
	});

	//reports
	Route::group(['middleware' => 'permission:is_allow_reports'], function() {
		Route::get('/reports/sales/','ReportController@reportSalesInvoiceGet');
		Route::get('/reports/sales/new','ReportController@reportSalesInvoice');
		// Route::post('/reports/sales/','ReportController@reportSalesInvoicePost');
		Route::post('/reports/sales/export/{type}','ReportController@reportSalesInvoiceExport');

		Route::get('/reports/products/','ReportController@reportProductsGet');
		Route::get('/reports/products/new','ReportController@reportProducts');
		// Route::post('/reports/products/','ReportController@reportProductsPost');
		Route::post('/reports/products/export/{type}','ReportController@reportProductsExport');

		Route::get('/reports/clients/','ReportController@reportClientsGet');
		Route::get('/reports/clients/new','ReportController@reportClients');
		// Route::post('/reports/clients/','ReportController@reportClientsPost');
		Route::post('/reports/clients/export/{type}','ReportController@reportClientsExport');

		Route::get('/reports/payments/','ReportController@reportPaymentsGet');
		Route::get('/reports/payments/new','ReportController@reportPayments');
		// Route::post('/reports/payments/','ReportController@reportPaymentsPost');
		Route::post('/reports/payments/export/{type}','ReportController@reportPaymentsExport');

		Route::get('/report/sales_invoice/print',"ReportController@printSalesInvoice");
		Route::get('/report/products/print',"ReportController@printProducts");
		Route::get('/report/clients/print',"ReportController@printClients");
		Route::get('/report/payments/print',"ReportController@printPayments");

		Route::get('/report/invoice/{id}/print/',"ReportController@printSalesInvoiceAdd");

	});


	Route::get('settings/password/change', 'SettingsController@changePassword');
	Route::post('settings/password/update', 'SettingsController@updatePassword');

	

});

//Json
Route::get('/api/fetch-clients', 'JsonController@fetchClientsAll')->name('fetch-clients');
Route::get('/api/fetch-clients/{val}', 'JsonController@fetchClients')->name('fetch-clients');
Route::get('/api/fetch-client/{id}', 'JsonController@fetchClient')->name('fetch-client');
Route::get('/api/fetch-transaction-items/{id}', 'JsonController@fetchTransactionItems')->name('fetch-transaction-items');
Route::get('/api/fetch-clients/account_manager/{id}', 'JsonController@fetchClientAccountManager')->name('fetch-clients-account_manager');
Route::get('/api/fetch-clients/balance/{id}', 'JsonController@fetchClientBalance')->name('fetch-clients-balance');
Route::get('/api/fetch-account-managers', 'JsonController@fetchAccountManagers')->name('fetch-account-managers');
Route::get('/api/fetch-products-name/{product_name}', 'JsonController@fetchProductsByName')->name('fetch-products-by-name');
Route::get('/api/fetch-products', 'JsonController@fetchProducts')->name('fetch-products');
Route::get('/api/fetch-clients/branch/{id}', 'JsonController@fetchClientBranches')->name('fetch-clients-branches');
Route::post('/api/new-client/create', 'JsonController@newClient')->name('json-new-clients');
Route::post('/api/new-item/create', 'JsonController@newItem')->name('json-new-item');
Route::post('/api/new-branch/{id}/create', 'JsonController@newBranch')->name('json-new-branch');
Route::get('/api/check/{type}/number/{num}', 'JsonController@checkIfExist')->name('json-check-number-if-exist');

//modal
Route::get('modal/client_modal','ModalController@fetchClients');
Route::get('modal/client_branches_modal','ModalController@fetchClientBranches');
Route::get('modal/products_modal','ModalController@fetchProducts');
Route::get('modal/payments_modal','ModalController@payments');
Route::get('modal/payments_modal_cash','ModalController@paymentsCash');
Route::get('modal/payments_modal_check','ModalController@paymentsCheck');
Route::get('modal/payments_modal_bankdeposit','ModalController@paymentsBankDeposit');
Route::get('modal/transfer_modal','ModalController@transfer');
Route::get('modal/new_client_modal','ModalController@newClient');
Route::get('modal/new_branch_modal','ModalController@newBranch');
Route::get('modal/new_item_modal','ModalController@newItem');
Route::get('modal/change_qty_modal','ModalController@changeQty');
Route::get('modal/change_price_modal','ModalController@changePrice');


Route::post('auth/login', 'Auth\AuthController@authenticate');
Route::get('auth/logout', 'Auth\AuthController@logout');