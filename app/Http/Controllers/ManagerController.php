<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\Manager;
use Validator;
use Session;
use Redirect;
use Response;
use Log;
use View;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
class ManagerController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
		$managers = DB::table('account_managers')
			->whereNull('deleted_at')
			->paginate(10);
		return view('manager.list_managers',['managers' => $managers]);
	}

	public function managerSearch(Request $request)
	{

		$managers = DB::table('account_managers')
			->whereNull('deleted_at')
			->Where(function ($query) use ($request) {
				$query->where('emp_id','like', '%'.$request->search.'%')
				->orWhere('first_name','like', '%'.$request->search.'%')
				->orWhere('middle_name','like', '%'.$request->search.'%')
				->orWhere('last_name','like', '%'.$request->search.'%');
			})                          
			->paginate(10);
		return view('manager.list_managers',['managers' => $managers]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$users = DB::table('users')
			->whereNull('deleted_at')
			->get();

		return view('manager.add_manager',['users' => $users]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//Log::info($request);
		$validator = Validator::make($request->all(), [
			'emp_id' => 'required|unique:account_managers|max:20|min:4',    
			'first_name' => 'required|max:30|min:2',    
			'middle_name' => 'max:30|min:1',    
			'last_name' => 'required|max:30|min:2',    
		]);

		if ($validator->fails()) {
			return redirect('/managers/create')
						->withErrors($validator)
						->withInput();
		}
		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'created manager :'.$request->first_name. " ".$request->last_name,
			'comment'  =>  '',
			'family'  =>  'insert',
			'created_at' => \Carbon\Carbon::now()
			]);


		$user_id = ($request->input('user_id') == 0 ? null : $request->input('user_id'));
		$manager = Manager::create([
			'emp_id'  =>  $request->input('emp_id'),
			'first_name'  =>  $request->input('first_name'),
			'middle_name'  =>  $request->input('middle_name'),
			'last_name'  =>  $request->input('last_name'),
			'user_id'  =>  $user_id,
		]);


		return redirect('/managers/create')->with('flash_message', 'Manager Added!!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
		$manager = DB::table('account_managers')
			->where('id','=',$id)
			->get();

		$users = DB::table('users')
			->whereNull('deleted_at')
			->get();

		return View::make('manager.edit_manager')
			->with('manager', $manager)
			->with('users', $users);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
		$validator = Validator::make($request->all(), [
			'emp_id' => 'required|max:20|min:4',    
			'first_name' => 'required|max:30|min:2',    
			'middle_name' => 'max:30|min:1',    
			'last_name' => 'required|max:30|min:2',    
		   
		]);

		if ($validator->fails()) {
			return redirect('managers/'.$id.'/edit')
						->withErrors($validator)
						->withInput();
		}

		$user_id = ($request->input('user_id') == 0 ? null : $request->input('user_id'));
		$x = Manager::find($id);
		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'edited manager :'.$x->first_name. " ".$x->last_name,
			'comment'  =>  '',
			'family'  =>  'update',
			'created_at' => \Carbon\Carbon::now()
			]);

		$x->emp_id = $request->input('emp_id');
		$x->first_name = $request->input('first_name');
		$x->middle_name = $request->input('middle_name');
		$x->last_name = $request->input('last_name');
		$x->user_id = $user_id;
		$x->save();               

		return redirect('managers')->with('flash_message', 'Manager Updated!!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
		$x = Manager::find($id);
		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'deleted manager :'.$x->first_name. " ".$x->last_name,
			'comment'  =>  '',
			'family'  =>  'delete',
			'created_at' => \Carbon\Carbon::now()
			]);

		if ($x) {
			$x->delete();
		}
		return redirect('managers')->with('flash_message', 'Manager Deleted!!');
	}
}
