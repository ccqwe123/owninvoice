<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\Roles;
use App\Privileges;
use Validator;
use Session;
use Redirect;
use Response;
use Log;
use View;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
class RolesController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
		$roles = DB::table('roles')
			->whereNull('deleted_at')
			->paginate(10);
		return view('role.list_roles',['roles' => $roles]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
			return view('role.add_role',[]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
		$validator = Validator::make($request->all(), [
			'role_name' => 'required|max:50|min:4',
		]);

		if ($validator->fails()) {
			return redirect('/roles/create')
						->withErrors($validator)
						->withInput();
		}
		DB::table('roles')->insert([
				'role_name' => $request->input('role_name'),
				]);

		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'created Roles :'.$request->role_name,
			'comment'  =>  '',
			'family'  =>  'insert',
			'created_at' => \Carbon\Carbon::now()
			]);

		return redirect('/roles/create')->with('flash_message', 'Role Added!!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
		$role = DB::table('roles')
			->where('id','=',$id)
			->get();

		return View::make('role.edit_role')
			->with('role', $role);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
		$validator = Validator::make($request->all(), [
			'role_name' => 'required|max:50|min:4',    
		]);

		if ($validator->fails()) {
			return redirect('roles/'.$id.'/edit')
						->withErrors($validator)
						->withInput();
		}

		$x = Roles::find($id);
		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'edited role :'.$request->role_name,
			'comment'  =>  '',
			'family'  =>  'update',
			'created_at' => \Carbon\Carbon::now()
			]);

		$x->role_name = $request->input('role_name');
		$x->save();

		return redirect('roles')->with('flash_message', 'Role Updated!!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
		$x = Roles::find($id);

		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'deleted role :'.$x->role_name,
			'comment'  =>  '',
			'family'  =>  'delete',
			'created_at' => \Carbon\Carbon::now()
			]);

		if ($x) {
			$x->delete();
		}
		return redirect('roles')->with('flash_message', 'Role Deleted!!');
	}

	public function privilege($id)
	{
		//
		$role = DB::table('roles')
			->where('id','=',$id)
			->get();

		$has = DB::select("SELECT role_id,privilege_id,name FROM
				role_privileges rp INNER JOIN privileges p ON p.id=rp.privilege_id
				WHERE role_id=$id;");

		$hasnot = DB::select("SELECT id privilege_id,name FROM `privileges` p WHERE id NOT IN (
			SELECT id FROM role_privileges rp 
			JOIN `privileges` p ON p.id=rp.privilege_id WHERE role_id=$id)");

		return view('role.role_privilege',[
			'has' => $has,
			'hasnot' => $hasnot,
			'role' => $role]);
	}

	public function updatePrivilege(Request $request,$id,$method)
	{
		//
		if($method=='add')
		{
			$privileges = Privileges::find($request->input('privilege_id'));
			$role = Roles::find($id);
			DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'added privilege '.$privileges->name.' to role :'.$role->role_name,
			'comment'  =>  '',
			'family'  =>  'insert',
			'created_at' => \Carbon\Carbon::now()
			]);

			DB::table('role_privileges')->insert([
				'role_id'=>$id,
				'privilege_id'=>$request->input('privilege_id')
				]);
		}
		else
		{
			$privileges = Privileges::find($request->input('privilege_id'));
			$role = Roles::find($id);
			DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'removed privilege '.$privileges->name.' to role :'.$role->role_name,
			'comment'  =>  '',
			'family'  =>  'insert',
			'created_at' => \Carbon\Carbon::now()
			]);

			DB::table('role_privileges')
				->where('role_id', '=', $id)
				->where('privilege_id', '=',$request->input('privilege_id'))
				->delete();
		}

		return redirect('roles/'.$id.'/privilege')->with('flash_message', 'Role Privilege Updated!!');
	}
}
