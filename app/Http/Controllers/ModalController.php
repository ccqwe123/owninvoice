<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Log;

class ModalController extends Controller
{
	public function fetchClients()
	{
		 return view('modal.client_modal',[]);		
	}
	public function fetchClientBranches()
	{
		 return view('modal.client_branches_modal',[]);		
	}
	public function fetchProducts()
	{
		 return view('modal.products_modal',[]);		
	}
	public function paymentsCash()
	{		
		 return view('modal.payments_modal_cash',[]);		
	}
	public function paymentsCheck()
	{		
		$banks = DB::table('banks')
			->get();
		 return view('modal.payments_modal_check',['banks'=>$banks]);		
	}
	public function paymentsBankDeposit()
	{		
		$banks = DB::table('banks')
			->get();
		 return view('modal.payments_modal_bankdeposit',['banks'=>$banks]);		
	}
	public function payments()
	{
		$banks = DB::table('banks')
			->get();
		 return view('modal.payments_modal',['banks'=>$banks]);		
	}
	public function transfer()
	{
		 return view('modal.transfer_modal',[]);		
	}
	public function newClient()
	{
		$users = DB::table('account_managers')
			->whereNull('deleted_at')
			->get();

		return view('modal.new_client_modal',['users'=>$users]);		
	}
	public function newBranch()
	{
		return view('modal.new_branch_modal',[]);		
	}
	public function newItem()
	{
		return view('modal.new_product_modal',[]);		
	}
	
	public function changeQty()
	{

		return view('modal.change_qty_modal',[]);		
	}
	public function changePrice()
	{
	
		return view('modal.change_price_modal',[]);		
	}
	
}
