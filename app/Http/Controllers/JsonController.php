<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Response;
use Auth;
use Log;
use App\Client;
use App\Product;
use App\ClientBranch;
use App\Invoice;

class JsonController extends Controller
{
	//
	public function fetchClientsAll()
	{
		if(Auth::check())
		{
			// DB::enableQueryLog();
			$clients = DB::table('clients')				
				->whereNull('clients.deleted_at')
				->limit(5)
			->get(); 
			return Response::json(['result'=>'okay','clients'=>$clients],200, array(),JSON_PRETTY_PRINT);
		}
		else
		{
			return Response::json(['result'=>'not logged in'],200, array(),JSON_PRETTY_PRINT);
		}
		
	}
public function fetchClients($val)
{
	if(Auth::check())
	{
		// DB::enableQueryLog();
		$clients = DB::table('clients')				
			->whereNull('clients.deleted_at')
			->where('clients.client_name','like', '%' . $val . '%')
			->limit(5)
		->get(); 
		return Response::json(['result'=>'okay','clients'=>$clients],200, array(),JSON_PRETTY_PRINT);
	}
	else
	{

		return Response::json(['result'=>'not logged in'],200, array(),JSON_PRETTY_PRINT);
	}
	
}
public function fetchClientAccountManager($id)
{
	if(Auth::check())
	{
		// DB::enableQueryLog();
		$account_managers = DB::table('account_managers')				
			->where('id',"=",$id)			
		->get(); 

		return Response::json(['result'=>'okay','account_managers'=>$account_managers],200, array(),JSON_PRETTY_PRINT);
	}
	else
	{

		return Response::json(['result'=>'not logged in'],200, array(),JSON_PRETTY_PRINT);
	}
	
}
public function fetchClientBalance($id)
{

	if(Auth::check())
	{
		$balance = DB::table('sales_invoice')	
			->leftJoin('cash_payments', 'cash_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('check_payments', 'check_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('deposit_payments', 'deposit_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->select(
				DB::raw("IFNULL(SUM(amount_total),0)-(IFNULL(SUM(cash_payments.amount),0))-IFNULL(SUM(check_payments.amount),0)-IFNULL(SUM(deposit_payments.amount),0) as balance"))	
			->where('client_id',"=",$id)			
		->get(); 
		return Response::json(['result'=>'okay','balance'=>$balance],200, array(),JSON_PRETTY_PRINT);
	}
	else
	{

		return Response::json(['result'=>'not logged in'],200, array(),JSON_PRETTY_PRINT);
	}
	
}

	public function fetchClient($id)
	{
		if(Auth::check())
		{
			$clients = DB::table('clients')				
				->join('client_branches', 'clients.id', '=', 'client_branches.client_id')
				->leftJoin('account_managers', 'clients.account_manager_id', '=', 'account_managers.id')
				->leftJoin('sales_invoice', 'sales_invoice.client_id', '=', 'clients.id')
					->select('clients.client_name',
					'clients.id',
					'clients.vatable',
					'clients.credit_limit',
					'clients.terms',
					'client_branches.id as branch_id',
					'client_branches.branch_name as branch_name',
					'client_branches.contact_person as contact_person',
					'client_branches.address as address',
					'account_managers.id as account_manager_id',
					'account_managers.emp_id',
					'account_managers.first_name',
					'account_managers.middle_name',
					'account_managers.last_name',
					DB::raw('(SELECT IFNULL(SUM(amount_total),0) FROM sales_invoice WHERE client_id=clients.id) AS total_invoice'),
					DB::raw('(SELECT IFNULL(SUM(amount),0) FROM sales_invoice si LEFT JOIN cash_payments cp ON cp.sales_invoice_id=si.id
							WHERE si.client_id=clients.id ) AS cash_payments'),
					DB::raw('(SELECT IFNULL(SUM(amount),0) FROM sales_invoice si LEFT JOIN check_payments chk ON chk.sales_invoice_id=si.id
							WHERE si.client_id=clients.id ) AS check_payments'),
					DB::raw('((SELECT IFNULL(SUM(amount_total),0) FROM sales_invoice WHERE client_id=clients.id)-((SELECT IFNULL(SUM(amount),0) FROM sales_invoice si LEFT JOIN cash_payments cp ON cp.sales_invoice_id=si.id
							WHERE si.client_id=clients.id )+ (SELECT IFNULL(SUM(amount),0) FROM sales_invoice si LEFT JOIN check_payments chk ON chk.sales_invoice_id=si.id
							WHERE si.client_id=clients.id ))) AS balance')
					)
				->groupBy('clients.id')
				->where('clients.id','=',$id)
				->whereNull('clients.deleted_at')
			->get(); 
			return Response::json(['result'=>'okay','clients'=>$clients],200, array(),JSON_PRETTY_PRINT);
		}
		else
		{

			return Response::json(['result'=>'not logged in'],200, array(),JSON_PRETTY_PRINT);
		}
		
	}

	public function fetchClientBranches($id)
	{
		if(Auth::check())
		{
			$client_branches = DB::table('client_branches')
				->where('client_id','=',$id)
				->whereNull('client_branches.deleted_at')
				->get(); 
			return Response::json(['result'=>'okay','client_branches'=>$client_branches],200, array(),JSON_PRETTY_PRINT);
		}
		else
		{

			return Response::json(['result'=>'not logged in'],200, array(),JSON_PRETTY_PRINT);
		}
		
	}

	public function newClient(Request $request)
	{
		if(Auth::check())
		{
			DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'created client :'.$request->client_name,
			'comment'  =>  '',
			'family'  =>  'insert',
			'created_at' => \Carbon\Carbon::now()
			]);

			$account_manager_id = ($request->input('account_manager_id') == 0 ? null : $request->input('account_manager_id'));
			$personal = ($request->input('personal') == null ? 0 : $request->input('personal'));
			$vatable = ($request->input('vatable') == null ? 0 : $request->input('vatable'));
			$address = ($request->input('address') == null ? 0 : $request->input('address'));

			$client = Client::create([
				'client_name'  =>  $request->input('client_name'),
				'email'  =>  $request->input('email'),
				'tin'  => $request->input('tin'),
				'credit_limit'  => $request->input('credit_limit'),
				'personal'  => $personal,
				'terms'  => $request->input('terms'),
				'vatable'  => $vatable,
				'account_manager_id'  => $account_manager_id,
			]);
			$branch = ($personal=='1' ? 'personal' : "main");
			$client_branch = ClientBranch::create([			
				'client_id'  =>  $client->id,
				'branch_name'  =>  $branch,
				'contact_person'  =>  $request->input('contact_person'),
				'tel_no'  =>  $request->input('tel_no'),
				'mobile_no'  =>  $request->input('mobile_no'),
				'fax_no'  =>  $request->input('fax_no'),		
				'address'  => $address,
			]);

			$clients = DB::table('clients')				
				->join('client_branches', 'clients.id', '=', 'client_branches.client_id')
				->leftJoin('account_managers', 'clients.account_manager_id', '=', 'account_managers.id')
				->leftJoin('sales_invoice', 'sales_invoice.client_id', '=', 'clients.id')
				->select('clients.*',
					'client_branches.id as branch_id',
					'client_branches.branch_name as branch_name',
					'client_branches.contact_person as contact_person',
					'client_branches.address as address',
					'account_managers.id as account_manager_id',
					'account_managers.emp_id',
					'account_managers.first_name',
					'account_managers.middle_name',
					'account_managers.last_name',
					DB::raw('(SELECT IFNULL(SUM(amount_total),0) FROM sales_invoice WHERE client_id=clients.id) AS total_invoice'),
					DB::raw('(SELECT IFNULL(SUM(amount),0) FROM sales_invoice si LEFT JOIN cash_payments cp ON cp.sales_invoice_id=si.id
							WHERE si.client_id=clients.id ) AS cash_payments'),
					DB::raw('(SELECT IFNULL(SUM(amount),0) FROM sales_invoice si LEFT JOIN check_payments chk ON chk.sales_invoice_id=si.id
							WHERE si.client_id=clients.id ) AS check_payments'),
					DB::raw('((SELECT IFNULL(SUM(amount_total),0) FROM sales_invoice WHERE client_id=clients.id)-((SELECT IFNULL(SUM(amount),0) FROM sales_invoice si LEFT JOIN cash_payments cp ON cp.sales_invoice_id=si.id
							WHERE si.client_id=clients.id )+ (SELECT IFNULL(SUM(amount),0) FROM sales_invoice si LEFT JOIN check_payments chk ON chk.sales_invoice_id=si.id
							WHERE si.client_id=clients.id ))) AS balance')
					)
				->groupBy('clients.id')
				->whereNull('clients.deleted_at')
				->where('clients.id','=',$client->id)
			->first(); 

			return Response::json(['result'=>'okay','client'=>$clients],200, array(),JSON_PRETTY_PRINT);
		}
		else
		{

			return Response::json(['result'=>'not logged in'],200, array(),JSON_PRETTY_PRINT);
		}
		
	}

	public function newItem(Request $request)
	{
		if(Auth::check())
		{
			DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'created item :'.$request->prod_name,
			'comment'  =>  '',
			'family'  =>  'insert',
			'created_at' => \Carbon\Carbon::now()
			]);

			$product = Product::create([
				'prod_code'  =>  $request->input('prod_code'),
				'prod_name'  =>  $request->input('prod_name'),
				'prod_description'  => $request->input('prod_description'),
				'prod_price'  => $request->input('prod_price'),
			]);
			

			return Response::json(['result'=>'okay'],200, array(),JSON_PRETTY_PRINT);
		}
		else
		{

			return Response::json(['result'=>'not logged in'],200, array(),JSON_PRETTY_PRINT);
		}
		
	}
	public function newBranch(Request $request,$id)
	{

		if(Auth::check())
		{
			DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'created branch :'.$request->branch_name,
			'comment'  =>  '',
			'family'  =>  'insert',
			'created_at' => \Carbon\Carbon::now()
			]);
			$personal = ($request->input('personal') == null ? 0 : $request->input('personal'));
			Log::info($personal);
			$client_branch = ClientBranch::create([			
				'client_id'  =>  $id,
				'branch_name'  => $request->input('branch_name'),
				'contact_person'  =>  $request->input('contact_person'),
				'tel_no'  =>  $request->input('tel_no'),
				'mobile_no'  =>  $request->input('mobile_no'),
				'fax_no'  =>  $request->input('fax_no'),		
				'address'  => $request->input('address'),
				'personal'  => $personal,
			]);

			return Response::json(['result'=>'okay','branch'=>$client_branch],200, array(),JSON_PRETTY_PRINT);
		}
		else
		{

			return Response::json(['result'=>'not logged in'],200, array(),JSON_PRETTY_PRINT);
		}
		
	}
	public function fetchTransactionItems($id)
	{
		if(Auth::check())
		{
			$selected_product = DB::table('transaction_items')
			->select(
				'products.id',
				'products.prod_code',
				'products.prod_name',
				DB::raw('(SELECT price FROM transaction_items WHERE sales_invoice_id='.$id.' AND transaction_items.product_id=products.id GROUP BY price) prod_price'),
				'transaction_items.qty',
				DB::raw('(price*transaction_items.qty) as total')

				)
			->join('products','products.id', '=', 'transaction_items.product_id')
			->where('sales_invoice_id','=',$id)
			->get();
			return Response::json(['result'=>'okay','selected_product'=>$selected_product],200, array(),JSON_PRETTY_PRINT);
		}
		else
		{

			return Response::json(['result'=>'not logged in'],200, array(),JSON_PRETTY_PRINT);
		}
		
	}
	public function fetchProductsByName($product_name)
	{

		if(Auth::check())
		{
			$products = DB::table('products')
				->select(
					'products.id',
					'products.prod_code',
					'products.prod_name',
					'products.prod_bar_code',
					'products.status',
					'products.prod_description',
					'products.prod_price',
					DB::raw('0 as qty')
				)
				->where('status','=',1)
				->where(function ($query) use ($product_name) {
					$query->where('prod_code','like', '%'.$product_name.'%')
							->orWhere('prod_name','like', '%'.$product_name.'%')
							->orWhere('prod_bar_code','like', '%'.$product_name.'%');	
					})
				->limit(5)
				->get(); 
			return Response::json(['result'=>'okay','products'=>$products],200, array(),JSON_PRETTY_PRINT);
		}
		else
		{

			return Response::json(['result'=>'not logged in'],200, array(),JSON_PRETTY_PRINT);
		}
		
	}
	public function fetchProducts()
	{
		if(Auth::check())
		{
			$products = DB::table('products')
				->select(
					'products.id',
					'products.prod_code',
					'products.prod_name',
					'products.prod_bar_code',
					'products.status',
					'products.prod_description',
					'products.prod_price',
					DB::raw('0 as qty')
				)
				->where('status','=',1)
				->get(); 
			return Response::json(['result'=>'okay','products'=>$products],200, array(),JSON_PRETTY_PRINT);
		}
		else
		{

			return Response::json(['result'=>'not logged in'],200, array(),JSON_PRETTY_PRINT);
		}
		
	}
	public function fetchAccountManagers()
	{
		if(Auth::check())
		{
			$account_managers = DB::table('account_managers')
				->get(); 
			return Response::json(['result'=>'okay','account_managers'=>$account_managers],200, array(),JSON_PRETTY_PRINT);
		}
		else
		{

			return Response::json(['result'=>'not logged in'],200, array(),JSON_PRETTY_PRINT);
		}
		
	}
	public function checkIfExist($type,$number)
	{
		if($type=='si')
		{
			$res = Invoice::where('si','=',$number)->get();
		}
		else
		{
			$res =  Invoice::where('po','=',$number)->get();
		}

		return Response::json(['result'=>count($res)],200, array(),JSON_PRETTY_PRINT);
	}
}
