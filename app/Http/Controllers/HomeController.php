<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Log;
class HomeController extends Controller
{
	public function login()
	{
		$logo = DB::table('var')
			->where('name','=','company_logo')
			->first();

		$company_name = DB::table('var')
			->where('name','=','company_name')
			->first();

		$software_name = DB::table('var')
			->where('name','=','software_name')
			->first();

		$software_description = DB::table('var')
			->where('name','=','software_description')
			->first();

		return view('login',[
			'logo' => $logo,
			'company_name' => $company_name,
			'software_name' => $software_name,
			'software_description' => $software_description,
			
			]);
	}

	public function help(){
		return view('help');
	}

	public function dashboard()
	{
		DB::enableQueryLog();
		$status = 'all';
		$str = $this->queryString('all','amount');
		$sales_all_query = DB::select($str);
		$sales_all_amount = $sales_all_query[0]->amount;

		$str = $this->queryString('all','count');
		$sales_all_query = DB::select($str);
		$sales_all_count = $sales_all_query[0]->count;

		// $cash_payments = DB::table('cash_payments')->count();
		$cash_payments = DB::SELECT("SELECT count(distinct(si)) AS count_number FROM payments p
			INNER JOIN cash_payments cp ON cp.id=p.cash_payment_id
			INNER JOIN sales_invoice si on si.id=p.sales_invoice_id
			WHERE p.approved=1 AND cash_payment_id IS NOT NULL AND si.archived=0 and si.deleted_at is null AND ((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id and p.approved=1)-(si.amount_total-si.discount))=0");

		$cash_payments_amount = DB::SELECT("SELECT SUM(cp.amount) AS amount FROM cash_payments cp
			INNER JOIN payments p ON p.cash_payment_id=cp.id
			INNER JOIN sales_invoice si on si.id=p.sales_invoice_id
			WHERE p.approved=1 AND cash_payment_id IS NOT NULL AND si.archived=0 and si.deleted_at is null AND ((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id and p.approved=1)-(si.amount_total-si.discount))=0");

		$deposit_payments_amount = DB::SELECT("SELECT SUM(dp.amount) AS amount FROM payments p
			INNER JOIN deposit_payments dp ON dp.id=p.deposit_payment_id
			INNER JOIN sales_invoice si on si.id=p.sales_invoice_id
			WHERE p.approved=1 AND deposit_payment_id IS NOT NULL AND si.archived=0 and si.deleted_at is null AND ((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id and p.approved=1)-(si.amount_total-si.discount))=0
");
		$check_payments = DB::SELECT("SELECT count(distinct(si)) AS count_number FROM payments p
			INNER JOIN check_payments chk ON chk.id=p.check_payment_id
			INNER JOIN sales_invoice si on si.id=p.sales_invoice_id
			WHERE p.approved=1 AND check_payment_id IS NOT NULL AND ((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id and p.approved=1)-(si.amount_total-si.discount))=0;");

		$check_payments_amount = DB::SELECT("SELECT SUM(chk.amount) AS amount FROM check_payments chk
			INNER JOIN payments p ON p.check_payment_id=chk.id
			INNER JOIN sales_invoice si on si.id=p.sales_invoice_id
			WHERE p.approved=1 AND check_payment_id IS NOT NULL AND si.archived=0 and si.deleted_at is null AND ((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id and p.approved=1)-(si.amount_total-si.discount))=0;");

		$status = 'paid';
		$str = $this->queryString('paid','amount');
		$sales_paid_query = DB::select($str);
		$sales_paid_amount = $sales_paid_query[0]->amount;

		$str = $this->queryString('paid','count');
		$sales_paid_query = DB::select($str);
		$sales_paid_count = $sales_paid_query[0]->count;

		$payments_pending = DB::table('payments')->where('approved','=',0)->count();

		$cp_amount = DB::select('SELECT SUM(cp.amount) amount FROM cash_payments cp
					LEFT JOIN payments p ON cp.id=p.cash_payment_id WHERE p.approved=0');

		$chk_amount = DB::select('SELECT SUM(chk.amount) amount FROM check_payments chk
					LEFT JOIN payments p ON chk.id=p.check_payment_id WHERE p.approved=0');

		$dp_amount = DB::select('SELECT SUM(dp.amount) amount FROM deposit_payments dp
					LEFT JOIN payments p ON dp.id=p.deposit_payment_id WHERE p.approved=0');

		$payments_pending_amount_cp = $cp_amount[0]->amount;
		$payments_pending_amount_chk = $chk_amount[0]->amount;
		$payments_pending_amount_dp = $dp_amount[0]->amount;

		$status = 'unpaid';
		$str = $this->queryString('unpaid','amount');
		$sales_unpaid_query = DB::select($str);
		$sales_unpaid_amount = $sales_unpaid_query[0]->amount;

		$str = $this->queryString('unpaid','count');
		$sales_unpaid_query = DB::select($str);
		$sales_unpaid_count = $sales_unpaid_query[0]->count;

		$sales_partially_paid = DB::select("SELECT si.id,si.amount_total,cp.amount cash_amount,chk.amount check_amount,(Select sum(less) from payments where sales_invoice_id=si.id) less_payment,
			(IFNULL(cp.amount,0)+IFNULL(chk.amount,0)+IFNULL((Select sum(less) from payments where sales_invoice_id=si.id),0)) AS paid_amount,
			CASE 
			WHEN si.amount_total = (IFNULL(cp.amount,0)+IFNULL(chk.amount,0)+IFNULL((Select sum(less) from payments where sales_invoice_id=si.id),0)) THEN 'paid'
			WHEN (IFNULL(cp.amount,0)+IFNULL(chk.amount,0)+IFNULL((Select sum(less) from payments where sales_invoice_id=si.id),0)) = 0 THEN 'not-paid'
			ELSE 'partially_paid'
			END partially_paid
			FROM sales_invoice si
			LEFT JOIN payments p on p.sales_invoice_id = si.id
			LEFT JOIN (SELECT sum(amount) amount,sales_invoice_id FROM cash_payments GROUP BY sales_invoice_id) cp on cp.sales_invoice_id=p.sales_invoice_id
			LEFT JOIN (SELECT sum(amount) amount,sales_invoice_id FROM check_payments GROUP BY sales_invoice_id) chk on chk.sales_invoice_id=p.sales_invoice_id
			WHERE (si.amount_total != (IFNULL(cp.amount,0)+IFNULL(chk.amount,0)+IFNULL((Select sum(less) from payments where sales_invoice_id=si.id),0)) AND (IFNULL(cp.amount,0)+IFNULL(chk.amount,0)+IFNULL((Select sum(less) from payments where sales_invoice_id=si.id),0)) != 0) AND p.approved=1
			GROUP BY si.id;");

		$sales_partially_paid = count($sales_partially_paid);

		$status = 'over-due';
		$str = $this->queryString('over-due','amount');
		$sales_over_due_query = DB::select($str);
		$sales_over_due_amount = $sales_over_due_query[0]->amount;

		$str = $this->queryString('over-due','count');
		$sales_over_due_query = DB::select($str);
		$sales_over_due_count = $sales_over_due_query[0]->count;

		$status = 'partially-paid';
		$str = $this->queryString('partially-paid','amount');
		$sales_partially_paid_query = DB::select($str);
		$sales_partially_paid_amount = $sales_partially_paid_query[0]->amount;

		$str = $this->queryString('partially-paid','count');
		$sales_partially_paid_query = DB::select($str);
		$sales_partially_paid_count = $sales_partially_paid_query[0]->count;

		return view('dashboard',[
			'sales_invoice_count' => $sales_all_count,
			'sales_invoice_amount' => $sales_all_amount,
			'sales_invoice_paid' => $sales_paid_count,
			'sales_invoice_paid_amount' =>$sales_paid_amount,
			'sales_partially_paid' => $sales_partially_paid_count,
			'sales_partially_paid_amount' => $sales_partially_paid_amount,
			'sales_unpaid' => $sales_unpaid_count,
			'unpaid_amount' => $sales_unpaid_amount,
			'cash_payments' => $cash_payments[0]->count_number,
			'cash_payments_amount' => $cash_payments_amount[0]->amount+$deposit_payments_amount[0]->amount,
			'check_payments' => $check_payments[0]->count_number,
			'check_payments_amount' => $check_payments_amount[0]->amount,
			'sales_overdue' => $sales_over_due_count,
			'sales_overdue_amount' => $sales_over_due_amount,
			'payments_pending' => $payments_pending,
			'payments_pending_amount_chk' => $payments_pending_amount_chk,
			'payments_pending_amount_cp' => $payments_pending_amount_cp,
			'payments_pending_amount_dp' => $payments_pending_amount_dp,

			]);
	}
	private function getMonthSales($month)
	{
		return DB::table('sales_invoice')
			->select(DB::raw('IFNULL(SUM(sales_invoice.amount_total-sales_invoice.discount),0) as amount'))
			->whereMonth('invoice_date', '=', $month)
			->whereYear('invoice_date', '=', date("Y"))
			->get();
	}
	private function getMonthProduct($month)
	{
		$str = DB::select("SELECT IFNULL(prod_name,'') as prod_name,count(qty) sold from transaction_items inner join products on products.id = transaction_items.product_id inner join sales_invoice on sales_invoice.id=transaction_items.sales_invoice_id WHERE sales_invoice_id IS NOT NULL AND MONTH(sales_invoice.invoice_date)=$month AND YEAR(sales_invoice.invoice_date)=".date("Y")." GROUP BY product_id ORDER BY sold DESC LIMIT 1");
		
			if(count($str)==0)
				$res = "";
			else
				$res =$str[0]->prod_name;
		return $res;
	}

	private function queryString($status,$type)
	{

		if($status=='paid' && $type=='amount')
			$str = "SUM(si.amount_total-si.discount
						) as amount";
		else if($status=='paid' && $type=='count')
			$str = "COUNT(DISTINCT(si.id)) as count";

		else if($status=='over-due' && $type=='amount')
			$str = "SUM(si.amount_total-si.discount
						) as amount";
		else if($status=='over-due' && $type=='count')
			$str = "COUNT(si.id) as count";

		else if ($status=='unpaid' && $type=='count')
			$str = "COUNT(si.id) as count";
		else if ($status=='unpaid' && $type=='amount')
			$str = "SUM(si.amount_total-si.discount) as amount";
		
		else if ($status=='partially-paid' && $type=='amount')
			$str = "SUM(si.amount_total-si.discount
						) as amount";
		else if ($status=='partially-paid' && $type=='count')
			$str = "COUNT(si.id) as count";

		else if ($status=='all' && $type=='count')
			$str = "COUNT(DISTINCT(si.id)) as count";
		else if ($status=='all' && $type=='amount')
			$str = "SUM(si.amount_total-si.discount
						) as amount";
		return "SELECT  ".$str."			

						FROM sales_invoice si
						INNER JOIN clients c on c.id = si.client_id
						INNER JOIN account_managers am on am.id=c.account_manager_id
						WHERE 
						si.archived=0 AND
						si.deleted_at IS NULL AND
						(
						(
						CASE 
						WHEN '".$status."'='paid' THEN 
						((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id and p.approved=1)-(si.amount_total-si.discount))=0

						WHEN '".$status."'='partially-paid' THEN 
						((si.amount_total) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id and p.approved=1)
						) AND 0!=(
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id and p.approved=1)
						))
						 AND (DATE_ADD(invoice_date, INTERVAL si.terms DAY)>NOW())
						 AND si.status != 'cancelled'


						WHEN '".$status."'='over-due' THEN 
						(si.amount_total-si.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id)
						)
						 AND (DATE_ADD(invoice_date, INTERVAL si.terms DAY)<NOW()) AND 
						 (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id)
						) = 0

						WHEN '".$status."'='unpaid' THEN
						((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id and p.approved=1))=0 AND (DATE_ADD(si.invoice_date, INTERVAL si.terms DAY)>NOW())
						
						AND !((si.amount_total-si.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id AND p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id AND p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id AND p.approved=1)
						)
						 AND
						 (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id )+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id)
						) > 0 )
						WHEN '".$status."'='all' THEN

						((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id and p.approved=1)-(si.amount_total-si.discount))=0

						OR 

						((si.amount_total-si.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id and p.approved=1)
						) AND 0!=(
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id and p.approved=1)
						))
						 AND (DATE_ADD(invoice_date, INTERVAL si.terms DAY)>NOW())

						 OR 

						(si.amount_total-si.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id)
						)
						 AND (DATE_ADD(invoice_date, INTERVAL si.terms DAY)<NOW()) AND 
						 (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id)
						) = 0

						OR 

						((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id and p.approved=1))=0 AND (DATE_ADD(si.invoice_date, INTERVAL si.terms DAY)>NOW())

						OR

						(si.amount_total-si.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id AND p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id AND p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id AND p.approved=1)
						)
						 AND
						 (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=si.id )+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=si.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=si.id)
						) > 0 

						END ))

						" ;
	}
}