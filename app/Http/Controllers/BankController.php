<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\Bank;
use Validator;
use Session;
use Redirect;
use Response;
use Log;
use View;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
class BankController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
		$banks = DB::table('banks')
			->whereNull('deleted_at')
			->paginate(10);
		return view('bank.list_banks',['banks' => $banks]);
	}

	public function bankSearch(Request $request)
	{

		$banks = DB::table('banks')
			->whereNull('deleted_at')
			->Where(function ($query) use ($request) {
				$query->where('bank_name','like', '%'.$request->search.'%');
			})							
			->paginate(10);
		return view('bank.list_banks',['banks' => $banks]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
		return view('bank.add_bank',[]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
		$validator = Validator::make($request->all(), [
			'bank_name' => 'required|max:50|min:4',
		]);

		if ($validator->fails()) {
			return redirect('/banks/create')
						->withErrors($validator)
						->withInput();
		}

		$client = Bank::create([
			'bank_name'  =>  $request->input('bank_name'),
		]);

		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'created bank :'.$request->bank_name,
			'comment'  =>  '',
			'family'  =>  'insert',
			'created_at' => \Carbon\Carbon::now()
			]);

		return redirect('/banks/create')->with('flash_message', 'Bank Added!!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
		$bank = DB::table('banks')
			->where('id','=',$id)
			->get();

		return View::make('bank.edit_bank')
			->with('bank', $bank);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
		$validator = Validator::make($request->all(), [
			'bank_name' => 'required|max:50|min:4',    
		]);

		if ($validator->fails()) {
			return redirect('banks/'.$id.'/edit')
						->withErrors($validator)
						->withInput();
		}

		$x = Bank::find($id);
		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'edited bank :'.$request->bank_name,
			'comment'  =>  '',
			'family'  =>  'update',
			'created_at' => \Carbon\Carbon::now()
			]);

		$x->bank_name = $request->input('bank_name');
		$x->save();

		

		return redirect('banks')->with('flash_message', 'Bank Updated!!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
		$x = Bank::find($id);

		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'deleted bank :'.$x->bank_name,
			'comment'  =>  '',
			'family'  =>  'delete',
			'created_at' => \Carbon\Carbon::now()
			]);


		if ($x) {
			$x->delete();
		}
		return redirect('banks')->with('flash_message', 'Bank Deleted!!');
	}
}
