<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\Payments;
use App\Bank;
use App\CashPayments;
use App\CheckPayments;
use App\DepositPayments;
use App\Invoice;
use Log;
use Illuminate\Support\Facades\Auth;
use Redirect;
class PaymentController extends Controller
{
	public function index()
	{
		$approved = (isset($_GET['approved'])) ? $_GET['approved'] : -1;
		$payments = DB::table('payments')
			->join('sales_invoice', 'sales_invoice.id', '=', 'payments.sales_invoice_id')
			->leftJoin('cash_payments', 'cash_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('check_payments', 'check_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('deposit_payments', 'deposit_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->orderBy('payments.created_at','desc')
			// ->select(DB::raw('payments.id as payment_id'),
			>select(DB::raw('payments.id as payment_id,payments.or_number as or_number,payments.less as less'),
				DB::raw('(SELECT client_name FROM clients WHERE id=sales_invoice.client_id) client_name'),
				DB::raw('(SELECT branch_name FROM client_branches WHERE id=sales_invoice.branch_id) branch_name'),
				DB::raw('payments.approved as approved'),
				DB::raw('payments.sales_invoice_id as sales_invoice_id'),
				DB::raw('payments.cash_payment_id as cash_payment_id'),
				DB::raw('payments.check_payment_id as check_payment_id'),
				DB::raw('payments.deposit_payment_id as deposit_payment_id'),
				'sales_invoice.*',
				DB::raw('(CASE WHEN payments.cash_payment_id IS NOT NULL THEN "cash" 
					WHEN payments.check_payment_id IS NOT NULL THEN "check"
					WHEN payments.deposit_payment_id IS NOT NULL THEN "deposit" END) AS paytype'),
				DB::raw('(CASE 
					WHEN payments.cash_payment_id IS NOT NULL THEN (SELECT amount from cash_payments where id=payments.cash_payment_id) 
					WHEN payments.check_payment_id IS NOT NULL THEN (SELECT amount from check_payments where id=payments.check_payment_id) 
					WHEN payments.deposit_payment_id IS NOT NULL THEN (SELECT amount from deposit_payments where id=payments.deposit_payment_id) END) AS amount')
			)
			->whereRaw("((".$approved."=-1) OR (payments.approved=".$approved."))")
			->groupBy('payments.id')
			->orderBy('payments.created_at','desc')
			->paginate(10);
		return view('payments.list_payments',['payments' => $payments]);
	}
	public function paymentSearch(Request $request)
	{
		$approved = (isset($_GET['approved'])) ? $_GET['approved'] : -1;

		$payments = DB::table('payments')
			->join('sales_invoice', 'sales_invoice.id', '=', 'payments.sales_invoice_id')
			->leftJoin('cash_payments', 'cash_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('check_payments', 'check_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('deposit_payments', 'deposit_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->orderBy('payments.created_at','desc')
			->select(DB::raw('payments.id as payment_id,payments.or_number as or_number,payments.less as less'),
				DB::raw('(SELECT client_name FROM clients WHERE id=sales_invoice.client_id) client_name'),
				DB::raw('(SELECT branch_name FROM client_branches WHERE id=sales_invoice.branch_id) branch_name'),
				DB::raw('payments.approved as approved'),
				DB::raw('payments.sales_invoice_id as sales_invoice_id'),
				DB::raw('payments.cash_payment_id as cash_payment_id'),
				DB::raw('payments.check_payment_id as check_payment_id'),
				DB::raw('payments.deposit_payment_id as deposit_payment_id'),
				'sales_invoice.*',
				DB::raw('(CASE WHEN payments.cash_payment_id IS NOT NULL THEN "cash" 
					WHEN payments.check_payment_id IS NOT NULL THEN "check"
					WHEN payments.deposit_payment_id IS NOT NULL THEN "deposit" END) AS paytype'),
				DB::raw('(CASE 
					WHEN payments.cash_payment_id IS NOT NULL THEN (SELECT amount from cash_payments where id=payments.cash_payment_id) 
					WHEN payments.check_payment_id IS NOT NULL THEN (SELECT amount from check_payments where id=payments.check_payment_id) 
					WHEN payments.deposit_payment_id IS NOT NULL THEN (SELECT amount from deposit_payments where id=payments.deposit_payment_id) END) AS amount')
				)
			->whereRaw("((".$approved."=-1) OR (payments.approved=".$approved."))")
			->Where(function ($query) use ($request) {
				$query->where('si','like', '%'.$request->search.'%')
				->orWhere('po','like', '%'.$request->search.'%');
			})
			->groupBy('payments.id')
			->orderBy('payments.created_at','desc')
			->paginate(10);
		
		return view('payments.list_payments',['payments' => $payments]);
	}

	public function AddPayment($id)
	{
		$get_date_invoice = DB::select("SELECT invoice_date from sales_invoice where id = $id");
		$account_managers = DB::table('account_managers')
			->whereNull('deleted_at')
			->get();
		$sales_invoice = DB::table('sales_invoice')
			->join('clients', 'clients.id', '=', 'sales_invoice.client_id')
			->leftjoin('account_managers', 'account_managers.id', '=', 'clients.account_manager_id')
			->leftJoin('cash_payments', 'cash_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('check_payments', 'check_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('deposit_payments', 'deposit_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id,id from cash_payments WHERE sales_invoice_id=".$id.") as cp"),function($join){
					$join->on("cp.sales_invoice_id","=","sales_invoice.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id,id from check_payments WHERE sales_invoice_id=".$id.") as chk"),function($join){
					$join->on("chk.sales_invoice_id","=","sales_invoice.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id,id from deposit_payments WHERE sales_invoice_id=".$id.") as dp"),function($join){
					$join->on("dp.sales_invoice_id","=","sales_invoice.id");
			})
			->select('sales_invoice.*',
				'sales_invoice.*',
				'clients.client_name',
				'clients.email',
				'clients.vatable',
				'clients.tin',
				'clients.credit_limit',
				DB::raw('(SELECT branch_name FROM client_branches WHERE id=sales_invoice.branch_id) branch_name'),
				'account_managers.first_name',
				'account_managers.last_name',
				DB::raw('cp.amount as cash_payment'),
				DB::raw('(SELECT SUM(less) less from payments where sales_invoice_id='.$id.') less'),
				DB::raw('chk.amount as check_payment'),
				DB::raw('dp.amount as deposit_payment'),
				DB::raw('DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) as due_date')		
				)
			->Where('sales_invoice.id','=',$id)
			->get();

		$payments = DB::select("SELECT pay_type,amount,created_at,trans_id,less,approved,payment_id,or_number FROM 
			(SELECT 'cash' AS pay_type,amount,created_at,id trans_id,
			(SELECT less from payments where cash_payment_id=trans_id) less,
			(SELECT or_number from payments where cash_payment_id=trans_id) or_number,
			(SELECT id payment_id from payments where cash_payment_id=trans_id) payment_id,
			(SELECT approved from payments where cash_payment_id=trans_id) approved FROM cash_payments WHERE cash_payments.sales_invoice_id=$id
			UNION ALL
			SELECT 'check' AS pay_type,amount,created_at,id trans_id,
			(SELECT less from payments where check_payment_id=trans_id) less,
			(SELECT or_number from payments where check_payment_id=trans_id) or_number,
			(SELECT id payment_id from payments where check_payment_id=trans_id) payment_id,
			(SELECT approved from payments where check_payment_id=trans_id) approved FROM check_payments WHERE check_payments.sales_invoice_id=$id
			UNION ALL
			SELECT 'deposit' AS pay_type,amount,created_at,id trans_id,
			(SELECT less from payments where deposit_payment_id=trans_id) less,
			(SELECT or_number from payments where deposit_payment_id=trans_id) or_number,
			(SELECT id payment_id from payments where deposit_payment_id=trans_id) payment_id,
			(SELECT approved from payments where deposit_payment_id=trans_id) approved FROM deposit_payments WHERE deposit_payments.sales_invoice_id=$id) as U");

		$balance = $sales_invoice[0]->amount_total-$sales_invoice[0]->discount-($sales_invoice[0]->cash_payment+$sales_invoice[0]->check_payment+$sales_invoice[0]->deposit_payment+$sales_invoice[0]->less);
		$get_date = DB::select("SELECT invoice_date from sales_invoice where id = $id");
		$od = DB::select("SELECT DATE_ADD(`invoice_date`, INTERVAL 30 DAY) as adddate FROM `sales_invoice` where id = $id;");
		$carbon_date = \Carbon\Carbon::today()->toDateString();
		if($balance > 0 && $carbon_date > $od[0]->adddate)
		{
			$invoice = Invoice::find($id);
			$invoice->status = 'over-due';
			$invoice->save();
		}
		elseif($balance==0)
		{
			$invoice = Invoice::find($id);
			$invoice->status = 'paid';
			$invoice->save();
		}

		return view('payments.payments',['sales_invoice' => $sales_invoice,'account_managers' => $account_managers,'payments'=>$payments]);
	}

	public function cashPayment(Request $request)
	{
		
		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'created cash payment for invoice id '.$request->input('sales_invoice_id').' amount:'.$request->input('cash_amount'),
			'comment'  =>  '',
			'family'  =>  'insert',
			'created_at' => \Carbon\Carbon::now()
			]);

		$cash_payment = CashPayments::create([
			'sales_invoice_id' => $request->input('sales_invoice_id'),
			'amount' => $request->input('cash_amount'),
			]);

		if($request->file('photo')==null || $request->file('photo')=='')
		{
			$photoFileName='';
		}
		else
		{
			$destinationPath = 'uploads';
			$photoExtension = $request->file('photo')->getClientOriginalExtension(); 
			$photoFileName = 'payment_img'.\Carbon\Carbon::now()->timestamp.'.'.$photoExtension;
			$request->file('photo')->move($destinationPath, $photoFileName);
		}
		
		$payment = Payments::create([
			'sales_invoice_id' => $request->input('sales_invoice_id'),
			'cash_payment_id' => $cash_payment->id,
			'or_number' => $request->input('or_number'),
			'less' => $request->input('less'),
			'photo' => $photoFileName,
			]);
		return redirect('/invoice/'.$request->input('sales_invoice_id').'/payments')->with('flash_message', 'Payment Added!!');
	}
	
	public function checkPayment(Request $request)
	{
	
		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'created check payment for invoice id '.$request->input('sales_invoice_id').' amount:'.$request->input('check_amount'),
			'comment'  =>  '',
			'family'  =>  'insert',
			'created_at' => \Carbon\Carbon::now()
			]);

		if($request->file('photo')==null || $request->file('photo')=='')
		{
			$photoFileName='';
		}
		else
		{
			$destinationPath = 'uploads';
			$photoExtension = $request->file('photo')->getClientOriginalExtension(); 
			$photoFileName = 'payment_img'.\Carbon\Carbon::now()->timestamp.'.'.$photoExtension;
			$request->file('photo')->move($destinationPath, $photoFileName);
		}


		$check_payment = CheckPayments::create([
			'sales_invoice_id' => $request->input('sales_invoice_id'),
			'amount' => $request->input('check_amount'),
			'bank_id' => $request->input('bank_id'),
			'account_number' => $request->input('account_number'),
			'check_number' => $request->input('check_number'),
			'check_date' => $request->input('check_date'),
			]);

		$payment = Payments::create([
			'sales_invoice_id' => $request->input('sales_invoice_id'),
			'check_payment_id' => $check_payment->id,
			'or_number' => $request->input('or_number'),
			'less' => $request->input('less'),
			'photo' => $photoFileName,
			]);
		return redirect('/invoice/'.$request->input('sales_invoice_id').'/payments')->with('flash_message', 'Payment Added!!');
	}

	public function depositPayment(Request $request)
	{
	
		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'created deposit payment for invoice id '.$request->input('sales_invoice_id').' amount:'.$request->input('deposit_amount'),
			'comment'  =>  '',
			'family'  =>  'insert',
			'created_at' => \Carbon\Carbon::now()
			]);

		if($request->file('photo')==null || $request->file('photo')=='')
		{
			$photoFileName='';
		}
		else
		{
			$destinationPath = 'uploads';
			$photoExtension = $request->file('photo')->getClientOriginalExtension(); 
			$photoFileName = 'payment_img'.\Carbon\Carbon::now()->timestamp.'.'.$photoExtension;
			$request->file('photo')->move($destinationPath, $photoFileName);
		}

		Log::info($request);
		$deposit_payment = DepositPayments::create([
			'sales_invoice_id' => $request->input('sales_invoice_id'),
			'amount' => $request->input('check_amount'),
			'bank_id' => $request->input('bank_id'),
			'deposit_date' => $request->input('check_date'),
			]);
		
		$payment = Payments::create([
			'sales_invoice_id' => $request->input('sales_invoice_id'),
			'deposit_payment_id' => $deposit_payment->id,
			'or_number' => $request->input('or_number'),
			'less' => $request->input('less'),
			'photo' => $photoFileName,
			]);
		return redirect('/invoice/'.$request->input('sales_invoice_id').'/payments')->with('flash_message', 'Payment Added!!');
	}

	public function checkDetails($id)
	{
		$check = CheckPayments::find($id);
		$payment = Payments::where('check_payment_id','=',$id)->first();

		$bank = Bank::find($check->bank_id);

		return view('payments.check_payment',['check' => $check,'bank'=>$bank,'payment'=>$payment]);
	}

	public function depositDetails($id)
	{
		$deposit = DepositPayments::find($id);
		$payment = Payments::where('deposit_payment_id','=',$id)->first();

		$bank = Bank::find($deposit->bank_id);

		return view('payments.deposit_payment',['deposit' => $deposit,'bank'=>$bank,'payment'=>$payment]);
	}

	public function cashDetails($id)
	{
		$cash = CashPayments::find($id);
		$payment = Payments::where('cash_payment_id','=',$id)->first();
		return view('payments.cash_payment',['cash' => $cash,'payment'=>$payment]);
	}
	public function destroy($id, $total, $invid)
	{
		$payment = Payments::find($id);
		$sales_invoice = DB::table('sales_invoice')
			->join('clients', 'clients.id', '=', 'sales_invoice.client_id')
			->leftjoin('account_managers', 'account_managers.id', '=', 'clients.account_manager_id')
			->leftJoin('cash_payments', 'cash_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('check_payments', 'check_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('deposit_payments', 'deposit_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id,id from cash_payments WHERE sales_invoice_id=".$payment->sales_invoice_id.") as cp"),function($join){
					$join->on("cp.sales_invoice_id","=","sales_invoice.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id,id from check_payments WHERE sales_invoice_id=".$payment->sales_invoice_id.") as chk"),function($join){
					$join->on("chk.sales_invoice_id","=","sales_invoice.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id,id from deposit_payments WHERE sales_invoice_id=".$payment->sales_invoice_id.") as dp"),function($join){
					$join->on("dp.sales_invoice_id","=","sales_invoice.id");
			})
			->select('sales_invoice.*',
				'sales_invoice.*',
				'clients.client_name',
				'clients.email',
				'clients.vatable',
				'clients.tin',
				'clients.credit_limit',
				DB::raw('(SELECT branch_name FROM client_branches WHERE id=sales_invoice.branch_id) branch_name'),
				'account_managers.first_name',
				'account_managers.last_name',
				DB::raw('cp.amount as cash_payment'),
				DB::raw('(SELECT SUM(less) less from payments where sales_invoice_id='.$payment->sales_invoice_id.') less'),
				DB::raw('chk.amount as check_payment'),
				DB::raw('dp.amount as deposit_payment'),
				DB::raw('DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) as due_date')		
				)
			->Where('sales_invoice.id','=',$payment->sales_invoice_id)
			->get();
			log::info($id);
			$od = DB::select("SELECT DATE_ADD(`invoice_date`, INTERVAL 30 DAY) as adddate FROM `sales_invoice` where id = $invid;");
		$carbon_date = \Carbon\Carbon::today()->toDateString();
			$balance3 = $sales_invoice[0]->amount_total/1;
			if($balance3 == $total && $carbon_date < $od[0]->adddate){
				$sales_invoice = Invoice::find($payment->sales_invoice_id);
				$sales_invoice->status = 'unpaid';
				$sales_invoice->save();
			}elseif($total > 0 && $carbon_date > $od[0]->adddate)
			{
				$sales_invoice = Invoice::find($payment->sales_invoice_id);
				$sales_invoice->status = 'over-due';
				$sales_invoice->save();
			}
			else{
				$sales_invoice = Invoice::find($payment->sales_invoice_id);
				$sales_invoice->status = 'partially-paid';
				$sales_invoice->save();
			}
		if($payment->cash_payment_id==null && $payment->deposit_payment_id==null)
		{
			$check_payment = CheckPayments::find($payment->check_payment_id);
			$check_payment->delete();
		}
		else if($payment->cash_payment_id==null && $payment->check_payment_id==null)
		{	
			$deposit_payment = DepositPayments::find($payment->deposit_payment_id);
			$deposit_payment->delete();
		}
		else
		{
			$cash_payemnt = CashPayments::find($payment->cash_payment_id);
			$cash_payemnt->delete();
		}
		return redirect('/invoice/'.$payment->sales_invoice_id.'/payments')->with('flash_message', 'Payment Deleted!!');

	}

	public function approvePayment($id,$type, $invoice_id)
	{
		if($type=='cash')
		{	
			$payment = Payments::where('cash_payment_id','=',$id)->first();
			$payment->approved = 1;
			$payment->save();
		}
		else if($type=='deposit')
		{	
			$payment = Payments::where('deposit_payment_id','=',$id)->first();
			$payment->approved = 1;
			$payment->save();
		}
		else
		{
			$payment = Payments::where('check_payment_id','=',$id)->first();
			$payment->approved = 1;
			$payment->save();
		}

	

		$sales_invoice = DB::table('sales_invoice')
			->join('clients', 'clients.id', '=', 'sales_invoice.client_id')
			->leftjoin('account_managers', 'account_managers.id', '=', 'clients.account_manager_id')
			->leftJoin('cash_payments', 'cash_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('check_payments', 'check_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('deposit_payments', 'deposit_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id,id from cash_payments WHERE sales_invoice_id=".$payment->sales_invoice_id.") as cp"),function($join){
					$join->on("cp.sales_invoice_id","=","sales_invoice.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id,id from check_payments WHERE sales_invoice_id=".$payment->sales_invoice_id.") as chk"),function($join){
					$join->on("chk.sales_invoice_id","=","sales_invoice.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id,id from deposit_payments WHERE sales_invoice_id=".$payment->sales_invoice_id.") as dp"),function($join){
					$join->on("dp.sales_invoice_id","=","sales_invoice.id");
			})
			->select('sales_invoice.*',
				'sales_invoice.*',
				'clients.client_name',
				'clients.email',
				'clients.vatable',
				'clients.tin',
				'clients.credit_limit',
				DB::raw('(SELECT branch_name FROM client_branches WHERE id=sales_invoice.branch_id) branch_name'),
				'account_managers.first_name',
				'account_managers.last_name',
				DB::raw('cp.amount as cash_payment'),
				DB::raw('(SELECT SUM(less) less from payments where sales_invoice_id='.$payment->sales_invoice_id.') less'),
				DB::raw('chk.amount as check_payment'),
				DB::raw('dp.amount as deposit_payment'),
				DB::raw('DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) as due_date')		
				)
			->Where('sales_invoice.id','=',$payment->sales_invoice_id)
			->get();

		$payments = DB::select("SELECT pay_type, amount,created_at,id,or_number,less,approved FROM (
			SELECT 'cash' AS pay_type, amount,cash_payments.created_at,cash_payments.id,(SELECT or_number from payments where cash_payment_id=cash_payments.id) or_number,(SELECT less from payments where cash_payment_id=cash_payments.id) less,(SELECT approved from payments where cash_payment_id=cash_payments.id) approved FROM cash_payments WHERE cash_payments.sales_invoice_id=$payment->sales_invoice_id
			UNION ALL
			SELECT 'deposit' AS pay_type, amount,deposit_payments.created_at,deposit_payments.id,(SELECT or_number from payments where deposit_payment_id=deposit_payments.id) or_number,(SELECT less from payments where deposit_payment_id=deposit_payments.id) less,(SELECT approved from payments where deposit_payment_id=deposit_payments.id) approved FROM deposit_payments WHERE deposit_payments.sales_invoice_id=$payment->sales_invoice_id
			UNION ALL
			SELECT 'check' AS pay_type,amount,check_payments.created_at,check_payments.id,(SELECT or_number from payments where check_payment_id=check_payments.id) or_number,(SELECT less from payments where check_payment_id=check_payments.id) less,(SELECT approved from payments where check_payment_id=check_payments.id) approved FROM check_payments WHERE check_payments.sales_invoice_id=$payment->sales_invoice_id) U
			ORDER BY created_at DESC");
		$current_date = DB::select("SELECT invoice_date from sales_invoice where id = $invoice_id");
		$od = DB::select("SELECT DATE_ADD(`invoice_date`, INTERVAL 30 DAY) as adddate FROM `sales_invoice` where id = $invoice_id;");

		$carbon_date = \Carbon\Carbon::today()->toDateString();
		
		$balance = $sales_invoice[0]->amount_total-($sales_invoice[0]->cash_payment+$sales_invoice[0]->deposit_payment+$sales_invoice[0]->check_payment+$sales_invoice[0]->less);
		if($balance==0)
		{
			$invoice = Invoice::find($payment->sales_invoice_id);
			$invoice->status = 'paid';
			$invoice->save();
		}
		if($balance > 0 && $carbon_date < $od[0]->adddate)
		{
			$invoice = Invoice::find($payment->sales_invoice_id);
			$invoice->status = 'partially-paid';
			$invoice->save();	
		}
		if($balance > 0 && $carbon_date > $od[0]->adddate)
		{
			$invoice = Invoice::find($payment->sales_invoice_id);
			$invoice->status = 'over-due';
			$invoice->save();
		}
		
		
		// if($balance > 0 && $)
		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'approved payment for payment id '.$id,
			'comment'  =>  '',
			'family'  =>  'insert',
			'created_at' => \Carbon\Carbon::now()
			]);
		return Redirect::back()->with('flash_message', 'Payment Approved!!');

	}

}
