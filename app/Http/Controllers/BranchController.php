<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Log;
use App\ClientBranch;
use App\Client;
use Validator;
use Session;
use Redirect;
use Response;
use View;
use DB;
use Illuminate\Support\Facades\Auth;
class BranchController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index($id)
	{
		$client = Client::where('id','=',$id)->first();
		$branches = DB::table('client_branches')
			->select(
				'client_branches.*',
				DB::raw('(SELECT IFNULL(SUM(amount_total),0) FROM sales_invoice WHERE branch_id=client_branches.id) amount_total'),
				DB::raw('(SELECT IFNULL(SUM(amount),0) cash_amount 
				FROM sales_invoice 
				LEFT JOIN cash_payments ON cash_payments.sales_invoice_id=sales_invoice.id  WHERE sales_invoice.branch_id=client_branches.id) cash_amount'),
				DB::raw('(SELECT IFNULL(SUM(amount),0) check_amount 
				FROM sales_invoice 
				LEFT JOIN check_payments on check_payments.sales_invoice_id=sales_invoice.id  where sales_invoice.branch_id=client_branches.id) check_amount')	
				)
			->where('client_branches.client_id','=',$id)
			->groupBy('client_branches.id')			
			->paginate(10);
		return view('branch.list_branch',['branches'=>$branches,'client'=>$client]);
	}
	public function branchSearch(Request $request,$id)
	{
		$client = Client::where('id','=',$id)->first();
		$branches = DB::table('client_branches')
			->select(
				'client_branches.*',
				DB::raw('(SELECT IFNULL(SUM(amount_total),0) FROM sales_invoice WHERE branch_id=client_branches.id) amount_total'),
				DB::raw('(SELECT IFNULL(SUM(amount),0) cash_amount 
				FROM sales_invoice 
				LEFT JOIN cash_payments ON cash_payments.sales_invoice_id=sales_invoice.id  WHERE sales_invoice.branch_id=client_branches.id) cash_amount'),
				DB::raw('(SELECT IFNULL(SUM(amount),0) check_amount 
				FROM sales_invoice 
				LEFT JOIN check_payments on check_payments.sales_invoice_id=sales_invoice.id  where sales_invoice.branch_id=client_branches.id) check_amount')	
				)
			->where('client_branches.client_id','=',$id)
			->Where(function ($query) use ($request) {
				$query->where('branch_name','like', '%'.$request->search.'%');
			})	
			->groupBy('client_branches.id')			
			->paginate(10);

		return view('branch.list_branch',['branches'=>$branches,'client'=>$client]);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create($id)
	{
		//
		$client = Client::find($id);
		return view('branch.add_branch',['client' => $client]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request,$id)
	{
		//
		$validator = Validator::make($request->all(), [
			'branch_name' => 'required|max:50|min:4',
			'contact_person' => 'required|max:30|min:4',
			'tel_no' => 'max:30|min:4',
			'mobile_no' => 'max:30|min:4',
			'fax_no' => 'max:30|min:4',
			'address' => 'max:255|min:4',
		]);

		if ($validator->fails()) {
			return redirect('/clients/'.$id.'/branch/create')
						->withErrors($validator)
						->withInput();
		}
		
		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'created branch :'.$request->branch_name,
			'comment'  =>  '',
			'family'  =>  'insert',
			'created_at' => \Carbon\Carbon::now()
			]);

		$personal = ($request->input('personal') == null ? 0 : $request->input('personal'));
		
		$client_branch = ClientBranch::create([			
			'client_id'  =>  $id,
			'branch_name'  =>  $request->input('branch_name'),
			'contact_person'  =>  $request->input('contact_person'),
			'tel_no'  =>  $request->input('tel_no'),
			'mobile_no'  =>  $request->input('mobile_no'),
			'fax_no'  =>  $request->input('fax_no'),		
			'address'  => $request->input('address'),
			'email'  => $request->input('email'),
			'personal'  => $personal,
		]);


		return redirect('/clients/'.$id.'/branch/create')->with('flash_message', 'Branch Added!!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id,$client_id)
	{
		//
		$client_branch = ClientBranch::find($client_id);
		$client = Client::find($id);
		return View::make('branch.edit_branch')
			->with('client_branch', $client_branch)
			->with('client', $client);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $client_id,$branch_id)
	{
		//

		$validator = Validator::make($request->all(), [
			'branch_name' => 'required|max:50|min:4',
			'contact_person' => 'required|max:30|min:4',
			'tel_no' => 'max:30|min:4',
			'mobile_no' => 'max:30|min:4',
			'fax_no' => 'max:30|min:4',
			'address' => 'max:255|min:4',
			'email' => 'email',
		]);

		if ($validator->fails()) {
			return redirect('/clients/'.$client_id.'/branch/'.$branch_id.'/edit')
						->withErrors($validator)
						->withInput();
		}
		
		$x = ClientBranch::find($branch_id);
		$personal = ($request->input('personal') == null ? 0 : $request->input('personal'));
		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'updated branch :'.$request->branch_name,
			'comment'  =>  '',
			'family'  =>  'update',
			'created_at' => \Carbon\Carbon::now()
			]);

		$x->branch_name = $request->input('branch_name');
		$x->contact_person = $request->input('contact_person');
		$x->tel_no = $request->input('tel_no');
		$x->mobile_no = $request->input('mobile_no');
		$x->fax_no = $request->input('fax_no');
		$x->address = $request->input('address');
		$x->email = $request->input('email');
		$x->personal = $personal;
		$x->save();     

		return redirect('/clients/'.$x->client_id.'/branch')->with('flash_message', 'Branch Updated!!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($client_id,$branch_id)
	{
		//
		$x = ClientBranch::find($branch_id);

		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'deleted branch :'.$x->branch_name,
			'comment'  =>  '',
			'family'  =>  'delete',
			'created_at' => \Carbon\Carbon::now()
			]);

		if ($x) {
			$x->delete();
		}
		return redirect('/clients/'.$client_id.'/branch')->with('flash_message', 'Branch Deleted!!');
	}
}
