<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\User;
use App\Roles;
use App\UserRoles;
use Validator;
use Session;
use Redirect;
use Response;
use Log;
use View;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Hash;
class UserController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
		$users = DB::table('users')
			->select('users.*','roles.role_name')
			->leftJoin('user_roles', 'user_roles.user_id', '=', 'users.id')
			->leftJoin('roles', 'roles.id', '=', 'user_roles.role_id')
			->whereNull('users.deleted_at')
			->paginate(10);
		return view('user.list_user',['users' => $users]);
	}

	public function userSearch(Request $request)
	{

		$users = DB::table('users')
			->select('users.*','roles.role_name')
			->leftJoin('user_roles', 'user_roles.user_id', '=', 'users.id')
			->leftJoin('roles', 'roles.id', '=', 'user_roles.role_id')
			->whereNull('users.deleted_at')
			->Where(function ($query) use ($request) {
				$query->where('username','like', '%'.$request->search.'%')
				->orWhere('first_name','like', '%'.$request->search.'%')
				->orWhere('last_name','like', '%'.$request->search.'%');
			})                          
			->paginate(10);
		return view('user.list_users',['users' => $users]);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
		return view('user.add_user');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
		$validator = Validator::make($request->all(), [
			'username' => 'required|unique:users|max:20|min:4',    
			'password' => 'required|max:20|min:4',    
			'first_name' => 'required|max:30|min:4',    
			'middle_name' => 'max:30|min:4',    
			'last_name' => 'max:30|min:4',    
			'contact' => 'max:255|min:4', 
			'photo' => 'mimes:jpeg,bmp,png'
		]);

		if ($validator->fails()) {
			return redirect('/users/create')
						->withErrors($validator)
						->withInput();
		}

		if($request->file('photo')==null || $request->file('photo')=='')
		{
			$photoFileName='anon.png';
		}
		else
		{
			$current_time = Carbon::now()->timestamp;
			$destinationPath = 'uploads';
			$photoExtension = $request->file('photo')->getClientOriginalExtension(); 
			$photoFileName = 'userphoto'.$current_time.'.'.$photoExtension;
			$request->file('photo')->move($destinationPath, $photoFileName);
		}


		$prod = User::create([
			'username'  =>  $request->input('username'),
			'password'  =>  Hash::make($request->input('password')),
			'first_name'  =>  $request->input('first_name'),
			'middle_name'  =>  $request->input('middle_name'),
			'last_name'  =>  $request->input('last_name'),
			'contact'  =>  $request->input('contact'),
			'photo'  =>  $photoFileName,
		]);


		return redirect('/users/create')->with('flash_message', 'User Added!!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$x = DB::table('users')
			->where('id','=',$id)
			->get();

		return View::make('user.edit_user')
			->with('user', $x);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
		$validator = Validator::make($request->all(), [

			'password' => 'max:20|min:4',    
			'first_name' => 'required|max:30|min:4',    
			'middle_name' => 'max:30|min:4',    
			'last_name' => 'max:30|min:4',    
			'contact' => 'max:255|min:4', 
			'photo' => 'mimes:jpeg,bmp,png'
		   
		]);

		if ($validator->fails()) {
			return redirect('users/'.$id.'/edit')
						->withErrors($validator)
						->withInput();
		}


		$x = User::find($id);
		$x->first_name = $request->input('first_name');
		$x->middle_name = $request->input('middle_name');
		$x->last_name = $request->input('last_name');
		$x->contact = $request->input('contact');

		if($request->file('photo')==null || $request->file('photo')=='')
		{

		}
		else
		{
			$current_time = Carbon::now()->timestamp;
			$destinationPath = 'uploads';
			$photoExtension = $request->file('photo')->getClientOriginalExtension(); 
			$photoFileName = 'userphoto'.$current_time.'.'.$photoExtension;
			$request->file('photo')->move($destinationPath, $photoFileName);
			$x->photo = $photoFileName;
		}

		$x->save();               

		return redirect('users')->with('flash_message', 'User Updated!!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
		$x = User::find($id);
		$username = Session('username');
		
		if ($x and $x->username <> $username) {
			$x->delete();
		}
		else
		{
			return redirect('users')->with('flash_error', 'User Cant be Deleted!!');
		}
		return redirect('users')->with('flash_message', 'User Deleted!!');
	}

	public function setRole($id)
	{
		//
		$user = User::find($id);
		$roles = Roles::all();
		$userRoles = UserRoles::where('user_id',$id)->first();
		$role_id = $userRoles== '' ? 0 : $userRoles->role_id;

		return View::make('user.set_role')
			->with('user', $user)
			->with('role_id', $role_id)
			->with('roles', $roles);
	}

	public function updateRole(Request $request,$id)
	{
		//
		$userRoles = UserRoles::where('user_id',$id)->first();
		$role_id = ($request->input('role_id') == null ? 0 : $request->input('role_id'));
		DB::table('user_roles')->where('user_id', '=', $id)->delete();
		if($role_id>0)
		{			
			$role = UserRoles::insert([
			'role_id' => $role_id,
			'user_id' => $id
			]);	
		}
		
		return redirect('users/'.$id.'/role')
			->with('flash_message', 'Role Updated!!');;
	}
}
