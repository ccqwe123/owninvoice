<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\Manager;
use App\Invoice;
use App\Client;
use App\ClientBranch;
use App\TransactionItems;
use Validator;
use Session;
use Redirect;
use Response;
use Log;
use View;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
class InvoiceController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{

		$sales_invoice = DB::table('sales_invoice')
			->paginate(10);
		return view('invoice.list_invoice',['sales_invoice' => $sales_invoice]);
	}

	public function invoiceSearch(Request $request)
	{
		DB::enableQueryLog();
		$sales_invoice_id = ($request->input('sales_invoice_id') == null ? 0 : $request->input('sales_invoice_id'));
		if(Input::get('sortby')!=null)
		{
			$val = explode(".",Input::get('sortby'));
			$sales_invoice = DB::table('sales_invoice')
				->join('clients', 'clients.id', '=', 'sales_invoice.client_id')
				->leftJoin(DB::raw("(SELECT id,first_name,last_name,middle_name FROM account_managers) AS account_managers"),function($join){
						$join->on("account_managers.id","=","sales_invoice.account_manager_id");
				})
				->leftJoin('payments', 'payments.sales_invoice_id', '=', 'sales_invoice.id')
				->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM cash_payments GROUP BY sales_invoice_id) AS cp"),function($join){
						$join->on("cp.sales_invoice_id","=","sales_invoice.id");
				})
				->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM deposit_payments GROUP BY sales_invoice_id) AS dp"),function($join){
						$join->on("dp.sales_invoice_id","=","sales_invoice.id");
				})
				->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM check_payments  GROUP BY sales_invoice_id) AS chk"),function($join){
						$join->on("chk.sales_invoice_id","=","sales_invoice.id");
				})
				->leftJoin(DB::raw("(SELECT SUM(less) less,sales_invoice_id FROM payments) AS less_payments"),function($join){
						$join->on("less_payments.sales_invoice_id","=","sales_invoice.id");
				})
				->leftJoin(DB::raw("(SELECT branch_name,id FROM client_branches) AS branch_name"),function($join){
						$join->on("branch_name.id","=","sales_invoice.branch_id");
				})
				->select(
					'sales_invoice.id',
					'sales_invoice.invoice_date',
					'sales_invoice.si',
					'sales_invoice.po',
					'sales_invoice.pay_type',
					'sales_invoice.client_id',
					'sales_invoice.branch_id',
					'sales_invoice.account_manager_id',
					'sales_invoice.tax_rate',
					'sales_invoice.terms',
					'sales_invoice.discount',
					DB::raw('CASE 

						WHEN sales_invoice.status="cancelled"
						then "cancelled"
			
						WHEN 
						(sales_invoice.amount_total-sales_invoice.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
						)
						 AND (DATE_ADD(invoice_date, INTERVAL sales_invoice.terms DAY)<NOW()) AND 
						 (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
						) = 0

						THEN "over-due"

						WHEN 
							(sales_invoice.amount_total-sales_invoice.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id AND p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id AND p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id AND p.approved=1)
						)
						 AND
						 (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id )+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
						) > 0 
						THEN "pending"

						WHEN 
						((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)-(sales_invoice.amount_total-sales_invoice.discount))=0

						THEN "paid"
		
						WHEN 
						((sales_invoice.amount_total-sales_invoice.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						) AND 0!=(
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						))
						 AND (DATE_ADD(invoice_date, INTERVAL sales_invoice.terms DAY)>NOW())

						THEN "partially-paid"
						WHEN 
						((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1))=0 AND (DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY)>NOW())

						 THEN "unpaid"
						
						ELSE sales_invoice.status END AS status'),
					'sales_invoice.amount_total',
					// 'sales_invoice.tax_amount',
					DB::raw('IF (clients.vatable>0,0,sales_invoice.tax_amount) tax_amount'),
					'clients.client_name',
					'clients.email',
					'clients.vatable',
					'clients.tin',
					'clients.credit_limit',
					'clients.personal',
					'branch_name.branch_name',
					'account_managers.first_name',
					'account_managers.last_name',
					DB::raw('cp.amount as cash_payment'),
					DB::raw('chk.amount as check_payment'),
					DB::raw('dp.amount as deposit_payment'),
					DB::raw('DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) as due_date'),
					DB::raw('(SELECT SUM(less) less FROM payments WHERE sales_invoice_id=sales_invoice.id) less'),				
					DB::raw('(sales_invoice.amount_total-(IFNULL(cp.amount,0)+IFNULL(dp.amount,0)+IFNULL(chk.amount,0)+IFNULL((SELECT SUM(less) less FROM payments WHERE sales_invoice_id=sales_invoice.id),0))) as balance'),
					DB::raw('CONCAT(account_managers.first_name," ",account_managers.last_name) as manager')
					)
					->whereRaw("((".$sales_invoice_id."=0) OR (sales_invoice.id=".$sales_invoice_id."))")
			->Where(function ($query) use ($request) {
					$query->where('si','like', '%'.$request->search.'%')
					->orWhere('client_name','like', '%'.$request->search.'%')
					->orWhere('branch_name.branch_name','like', '%'.$request->search.'%')
					->orWhere('po','like', '%'.$request->search.'%')
						;
				})
			->whereRaw('IF(sales_invoice.status="cancelled",archived=1,archived=0)')
			->groupBy('sales_invoice.id')
			->orderBy($val[0],$val[1])
			->paginate(10);
		}
		else
		{
		
			$sales_invoice = DB::table('sales_invoice')
				->join('clients', 'clients.id', '=', 'sales_invoice.client_id')
				->leftJoin(DB::raw("(SELECT id,first_name,last_name,middle_name FROM account_managers) AS account_managers"),function($join){
						$join->on("account_managers.id","=","sales_invoice.account_manager_id");
				})
	
				->leftJoin('payments', 'payments.sales_invoice_id', '=', 'sales_invoice.id')
				->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM cash_payments GROUP BY sales_invoice_id) AS cp"),function($join){
						$join->on("cp.sales_invoice_id","=","sales_invoice.id");
				})
				->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM deposit_payments GROUP BY sales_invoice_id) AS dp"),function($join){
						$join->on("dp.sales_invoice_id","=","sales_invoice.id");
				})
				->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM check_payments  GROUP BY sales_invoice_id) AS chk"),function($join){
						$join->on("chk.sales_invoice_id","=","sales_invoice.id");
				})
				->leftJoin(DB::raw("(SELECT SUM(less) less,sales_invoice_id FROM payments) AS less_payments"),function($join){
						$join->on("less_payments.sales_invoice_id","=","sales_invoice.id");
				})
				->leftJoin(DB::raw("(SELECT branch_name,id FROM client_branches) AS branch_name"),function($join){
						$join->on("branch_name.id","=","sales_invoice.branch_id");
				})
				->select(
					'sales_invoice.id',
					'sales_invoice.invoice_date',
					'sales_invoice.si',
					'sales_invoice.po',
					'sales_invoice.pay_type',
					'sales_invoice.client_id',
					'sales_invoice.branch_id',
					'sales_invoice.account_manager_id',
					'sales_invoice.tax_rate',
					'sales_invoice.terms',		
					'sales_invoice.discount',			
					DB::raw('CASE 
						WHEN sales_invoice.status="cancelled"
						then "cancelled"

						WHEN 
						(sales_invoice.amount_total-sales_invoice.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
						)
						 AND (DATE_ADD(invoice_date, INTERVAL sales_invoice.terms DAY)<NOW()) AND 
						 (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id )+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
						) = 0

						THEN "over-due"
						WHEN 
						((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)-(sales_invoice.amount_total))=0

						THEN "paid"
		
						WHEN 
						((sales_invoice.amount_total) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						) AND 0!=(
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						))
						 AND (DATE_ADD(invoice_date, INTERVAL sales_invoice.terms DAY)>NOW())

						THEN "partially-paid"
						WHEN 
							(sales_invoice.amount_total-sales_invoice.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id AND p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id AND p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id AND p.approved=1)
						)
						 AND
						 (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id )+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
						) > 0 
						THEN "pending"
						WHEN 
						((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1))=0 AND (DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY)>NOW())

						 THEN "unpaid"
						
						ELSE sales_invoice.status END AS status'),
					'sales_invoice.amount_total',
					DB::raw('IF (clients.vatable>0,0,sales_invoice.tax_amount) tax_amount'),
					'clients.client_name',
					'clients.email',
					'clients.vatable',
					'clients.tin',
					'clients.credit_limit',
					'clients.personal',
					'branch_name.branch_name',
					'account_managers.first_name',
					'account_managers.last_name',
					DB::raw('cp.amount as cash_payment'),
					DB::raw('chk.amount as check_payment'),
					DB::raw('dp.amount as deposit_payment'),
					DB::raw('DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) as due_date'),
					DB::raw('(SELECT SUM(less) less FROM payments WHERE sales_invoice_id=sales_invoice.id) less'),				
					DB::raw('(sales_invoice.amount_total-(IFNULL(cp.amount,0)+IFNULL(dp.amount,0)+IFNULL(chk.amount,0)+IFNULL((SELECT SUM(less) less FROM payments WHERE sales_invoice_id=sales_invoice.id),0))) as balance'),
					DB::raw('CONCAT(account_managers.first_name," ",account_managers.last_name) as manager')
					)
					
				->whereRaw("((".$sales_invoice_id."=0) OR (sales_invoice.id=".$sales_invoice_id."))")
			->Where(function ($query) use ($request) {
					$query->where('si','like', '%'.$request->search.'%')
					->orWhere('client_name','like', '%'.$request->search.'%')
					->orWhere('branch_name.branch_name','like', '%'.$request->search.'%')
					->orWhere('po','like', '%'.$request->search.'%')
						;
				})
			->whereRaw('IF(sales_invoice.status="cancelled",archived=1,archived=0)')
			->groupBy('sales_invoice.id')
			->orderBy('sales_invoice.created_at','desc')
			->paginate(10);	
		}	
		return view('invoice.list_invoice',['sales_invoice' => $sales_invoice]);
	}

	public function create()
	{
		//
		return view('invoice.add_invoice',[]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		// Log::info($request);
		$validator = Validator::make($request->all(), [
			'po' => 'required|unique:sales_invoice',    
			'si' => 'required|unique:sales_invoice|max:30|min:4', 
		]);

		if ($validator->fails()) {
			return back()
						->withErrors($validator)
						->withInput();
		}

		$account_manager_id = ($request->input('account_manager_id') == '' ? null : $request->input('account_manager_id'));
		$amount_total = preg_replace('/[,]/', '', $request->input('total_amount_due'));
		$vat_sales = preg_replace('/[,]/', '', $request->input('vat_sales'));

		$m = Invoice::create([
			'invoice_date'  =>  $request->input('invoice_date'),
			'si'  =>  $request->input('si'),
			'po'  =>  $request->input('po'),
			'pay_type'  =>  $request->input('pay_type'),
			'client_id'  =>  $request->input('client_id'),
			'branch_id'  =>  $request->input('branch_id'),
			'account_manager_id'  =>  $account_manager_id,
			'amount_total'  =>  $amount_total,
			'vat_sales'  =>  $vat_sales,
			'tax_amount'  =>  $amount_total - $vat_sales,
			'terms'  =>  $request->input('terms'),
			'discount'  =>  $request->input('discount'),
			'tax_rate'  => Session::get('tax_rate'),
		]);
		$si_id = $m->id;
		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'created sales_invoice :'.$request->input('si'),
			'comment'  =>  'Total amount:'.$request->input('total_amount_due'),
			'family'  =>  'insert',
			'created_at' => \Carbon\Carbon::now()
			]);
		for($x=0;$x<count($request->input('prod_id'));$x++)
		{
			TransactionItems::create([
				'sales_invoice_id'  =>  $si_id,
				'product_id'  =>  $request->input('prod_id')[$x],
				'qty'  =>  $request->input('prod_qty')[$x],
				'price'  =>  $request->input('prod_price')[$x],
				]);
		}
	
	  	Session::flash('print_now', $m->id);
		return redirect('invoice/create')->with('flash_message', 'Invoice Added!!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */

	public function archive($id)
	{
		$x = Invoice::where('si', '=', $id)->first();

		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'archived Invoice :'.$x->id,
			'comment'  =>  '',
			'family'  =>  'update',
			'created_at' => \Carbon\Carbon::now()
			]);

	
		$x->si = $x->si." - deleted";
		$x->po = $x->po." - deleted";
		$x->archived = 1;
		
		$x->save();               


		return redirect('invoice')->with('flash_message', 'Invoice Archved!!');
	}
	public function edit($id)
	{
		DB::enableQueryLog();
		$invoice = DB::table('sales_invoice')
				->join('clients', 'clients.id', '=', 'sales_invoice.client_id')
				->leftJoin(DB::raw("(SELECT id,first_name,last_name,middle_name FROM account_managers) AS account_managers"),function($join){
						$join->on("account_managers.id","=","sales_invoice.account_manager_id");
				})
				->leftJoin('cash_payments', 'cash_payments.sales_invoice_id', '=', 'sales_invoice.id')
				->leftJoin('check_payments', 'check_payments.sales_invoice_id', '=', 'sales_invoice.id')
				->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM cash_payments GROUP BY sales_invoice_id) AS cp"),function($join){
						$join->on("cp.sales_invoice_id","=","sales_invoice.id");
				})
				->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM check_payments  GROUP BY sales_invoice_id) AS chk"),function($join){
						$join->on("chk.sales_invoice_id","=","sales_invoice.id");
				})
				->select(
					'sales_invoice.id',
					'sales_invoice.invoice_date',
					'sales_invoice.si',
					'sales_invoice.po',
					'sales_invoice.pay_type',
					'sales_invoice.client_id',
					'sales_invoice.branch_id',
					'sales_invoice.account_manager_id',
					'sales_invoice.tax_rate',
					'sales_invoice.terms',
					DB::raw('CASE 
						WHEN ((DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) < NOW()) AND sales_invoice.status!="paid") THEN "over-due"
						WHEN sales_invoice.status="paid" THEN "paid"
						WHEN sales_invoice.status="cancelled" THEN "cancelled"
						WHEN ((IFNULL(cp.amount,0)+IFNULL(chk.amount,0))>0 AND sales_invoice.status="unpaid" AND (DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) > NOW()))  THEN "partially-paid"
						WHEN sales_invoice.status="unpaid" THEN "unpaid"
						
						ELSE sales_invoice.status END AS status'),
					'sales_invoice.amount_total',
					'sales_invoice.tax_amount',
					'clients.client_name',
					'clients.email',
					'clients.vatable',
					'clients.tin',
					'clients.credit_limit',
					'clients.personal',
					DB::raw('(SELECT branch_name FROM client_branches WHERE id=sales_invoice.branch_id) branch_name'),
					'account_managers.first_name',
					'account_managers.last_name',
					DB::raw('cp.amount as cash_payment'),
					DB::raw('chk.amount as check_payment'),
					DB::raw('DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) as due_date'),					
					DB::raw('(SELECT SUM(less) less FROM payments WHERE sales_invoice_id=sales_invoice.id) less'),				
					DB::raw('(sales_invoice.amount_total-(IFNULL(cp.amount,0)+IFNULL(chk.amount,0)+IFNULL((SELECT SUM(less) less FROM payments WHERE sales_invoice_id=sales_invoice.id),0))) as balance'),
					DB::raw('CONCAT(account_managers.first_name," ",account_managers.last_name) as manager')
					)
			
			->where('sales_invoice.id','=',$id)
			->get();	
		$client = Client::find($invoice[0]->client_id);
		$branch = ClientBranch::find($invoice[0]->branch_id);
		$manager = Manager::find($client->account_manager_id);
		$transactionitems = DB::table('transaction_items')
			->select(
				'products.id',
				'products.prod_code',
				'products.prod_name',
				DB::raw('(SELECT price FROM transaction_items WHERE sales_invoice_id='.$id.' AND transaction_items.product_id=products.id GROUP BY price) prod_price'),
				'transaction_items.qty',
				DB::raw('(price*transaction_items.qty) as total')
				)
			->join('products','products.id', '=', 'transaction_items.product_id')
			->where('sales_invoice_id','=',$id)
			->get();
		// Log::info($client);
		return view('invoice.edit_invoice',[
			'invoice'=>$invoice[0],
			'client'=>$client,
			'manager'=>$manager,
			'branch'=>$branch
			]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$x = Invoice::find($id);
		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'edited Invoice :'.$x->id,
			'comment'  =>  '',
			'family'  =>  'update',
			'created_at' => \Carbon\Carbon::now()
			]);
		$amount_total = preg_replace('/[,]/', '', $request->input('total_amount_due'));
		$vat_sales = preg_replace('/[,]/', '', $request->input('vat_sales'));

		$x->invoice_date = $request->input('invoice_date');

		$x->si = $request->input('si');
		$x->po = $request->input('po');
		$x->pay_type = $request->input('pay_type');
		$x->client_id = $request->input('client_id');
		$x->branch_id = $request->input('branch_id');
		$x->account_manager_id = $request->input('account_manager_id');
		$x->amount_total = $amount_total;
		$x->vat_sales = $vat_sales;
		$x->terms = $request->input('terms');
		$x->tax_amount = $amount_total - $vat_sales;
		$x->save();               

		DB::table('transaction_items')
			->where('sales_invoice_id', '=', $id)
			->delete();

		for($x=0;$x<count($request->input('prod_id'));$x++)
		{
			$t = TransactionItems::create([
				'sales_invoice_id'  =>  $id,
				'product_id'  =>  $request->input('prod_id')[$x],
				'qty'  =>  $request->input('prod_qty')[$x],
				'price'  =>  $request->input('prod_price')[$x],
				]);
		}

		return redirect('invoice')->with('flash_message', 'Invoice Updated!!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
		$x = Invoice::find($id);
		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'cancelled sales_invoice :'.$x->si,
			'comment'  =>  '',
			'family'  =>  'delete',
			'created_at' => \Carbon\Carbon::now()
			]);

		$x->status = 'cancelled';
		$x->save();
		if ($x) {
			$x->delete();
		}
		return redirect('invoice')->with('flash_message', 'Invoice Canceled!!');
	}

	public function transferInvoice(Request $request)
	{

		DB::table('transfer_invoice')->insert([
			'old_si'  => $request->input('old_sales_invoice'),
			'new_si'  => $request->input('sales_invoice'),
			'sales_invoice_id'  =>   $request->input('sales_invoice_id'),
			'created_at'  => \Carbon\Carbon::now(),
			]);
		$x = Invoice::find($request->input('sales_invoice_id'));
		$x->si = $request->input('sales_invoice');
		$x->save();
		return redirect('invoice')->with('flash_message', 'Invoice Transfered!!');
	}


}
