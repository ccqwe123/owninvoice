<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\Client;
use App\ClientBranch;
use Validator;
use Session;
use Redirect;
use Response;
use Log;
use View;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use PDF;
use Mail;
class ClientController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
		$clients = DB::table('clients')
			->select(
				'clients.*',
				DB::raw('(SELECT IFNULL(SUM(amount_total),0) FROM sales_invoice where sales_invoice.client_id=clients.id) as amount_total'),
				DB::raw('(SELECT IFNULL(SUM(amount),0) cash_amount 
				FROM sales_invoice 
				LEFT JOIN cash_payments on cash_payments.sales_invoice_id=sales_invoice.id  where sales_invoice.client_id=clients.id)'),
				DB::raw('(SELECT IFNULL(SUM(amount),0) check_amount 
				FROM sales_invoice 
				LEFT JOIN check_amount on check_amount.sales_invoice_id=sales_invoice.id  where sales_invoice.client_id=clients.id)')
				)
			->whereNull('deleted_at')
			->paginate(10);
		return view('client.list_client',['clients' => $clients]);
	}

	public function clientSearch(Request $request)
	{

		$clients = DB::table('clients')
			->select(
				'clients.*',
				DB::raw('(SELECT IFNULL(SUM(amount_total),0) FROM sales_invoice where sales_invoice.client_id=clients.id) as amount_total'),
				DB::raw('(SELECT IFNULL(SUM(amount),0) cash_amount 
				FROM sales_invoice 
				LEFT JOIN cash_payments ON cash_payments.sales_invoice_id=sales_invoice.id WHERE sales_invoice.client_id=clients.id) AS cash_amount'),
				DB::raw('(SELECT IFNULL(SUM(amount),0) check_amount 
				FROM sales_invoice 
				LEFT JOIN check_payments on check_payments.sales_invoice_id=sales_invoice.id  WHERE sales_invoice.client_id=clients.id) AS check_amount')
				)
			->whereNull('deleted_at')
			->Where(function ($query) use ($request) {
				$query->where('client_name','like', '%'.$request->search.'%');
			})							
			->paginate(10);
		return view('client.list_clients',['clients' => $clients]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$users = DB::table('account_managers')
			->whereNull('deleted_at')
			->get();

		return view('client.add_client',['users' => $users]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
		$validator = Validator::make($request->all(), [
			'client_name' => 'required|max:50|min:4',    
			'contact_person' => 'required|max:30|min:4',    
			'tel_no' => 'max:30|min:4',    
			'mobile_no' => 'max:30|min:4',    
			'fax_no' => 'max:30|min:4',    
			'email' => 'email|max:30|min:4',    
			'tin' => 'max:30|min:4',    
			'credit_limit' => 'required|numeric',    
			'terms' => 'integer',    
			'account_manager_id' => 'required',    
			'address' => 'max:255|min:4',
		]);

		if ($validator->fails()) {
			return redirect('/clients/create')
						->withErrors($validator)
						->withInput();
		}

		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'created client :'.$request->client_name,
			'comment'  =>  '',
			'family'  =>  'insert',
			'created_at' => \Carbon\Carbon::now()
			]);

		$account_manager_id = ($request->input('account_manager_id') == 0 ? null : $request->input('account_manager_id'));
		$vatable = ($request->input('vatable') == null ? 0 : $request->input('vatable'));
		$personal = ($request->input('personal') == null ? 0 : $request->input('personal'));
		$client = Client::create([
			'client_name'  =>  $request->input('client_name'),
			'email'  =>  $request->input('email'),
			'tin'  => $request->input('tin'),
			'credit_limit'  => $request->input('credit_limit'),
			'personal'  => $personal,
			'terms'  => $request->input('terms'),
			'vatable'  => $vatable,
			'account_manager_id'  => $account_manager_id,
		]);
		$branch = ($personal=='1' ? 'personal' : "main");
		$client_branch = ClientBranch::create([			
			'client_id'  =>  $client->id,
			'branch_name'  =>  $branch,
			'contact_person'  =>  $request->input('contact_person'),
			'tel_no'  =>  $request->input('tel_no'),
			'mobile_no'  =>  $request->input('mobile_no'),
			'fax_no'  =>  $request->input('fax_no'),		
			'address'  => $request->input('address'),
		]);


		return redirect('/clients/create')->with('flash_message', 'Client Added!!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
		$client = DB::table('clients')
			->where('id','=',$id)
			->get();

		$account_managers = DB::table('account_managers')
			->whereNull('deleted_at')
			->get();
		$branch = ClientBranch::where('client_id','=',$id)->first();

		return View::make('client.edit_client')
			->with('client', $client)
			->with('branch', $branch)
			->with('account_managers', $account_managers);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
		$validator = Validator::make($request->all(), [
			'client_name' => 'required|max:50|min:4',    
			'email' => 'email|max:30|min:4',    
			'tin' => 'max:30|min:4',    
			'credit_limit' => 'required|numeric',    
			'terms' => 'integer',    
		]);

		if ($validator->fails()) {
			return redirect('clients/'.$id.'/edit')
						->withErrors($validator)
						->withInput();
		}


		$account_manager_id = ($request->input('account_manager_id') == 0 ? null : $request->input('account_manager_id'));
		$personal = ($request->input('personal') == null ? 0 : $request->input('personal'));
		$vatable = ($request->input('vatable') == null ? 0 : $request->input('vatable'));
		$x = Client::find($id);

		if($x->personal==1)
		{
			// Log::info('personal');
			$branch = ClientBranch::where('client_id','=',$id)->first();
			$branch->address = $request->input('address');
			$branch->save();
		}

		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'edited client :'.$request->client_name,
			'comment'  =>  '',
			'family'  =>  'update',
			'created_at' => \Carbon\Carbon::now()
			]);

		$x->client_name = $request->input('client_name');
		$x->email = $request->input('email');
		$x->tin = $request->input('tin');
		$x->credit_limit = $request->input('credit_limit');
		$x->terms = $request->input('terms');
		$x->personal = $personal;
		$x->vatable = $vatable;
		$x->account_manager_id = $account_manager_id;
		$x->save();

		
		return redirect('clients')->with('flash_message', 'Client Updated!!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
		$x = Client::find($id);

		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'deleted client :'.$x->client_name,
			'comment'  =>  '',
			'family'  =>  'delete',
			'created_at' => \Carbon\Carbon::now()
			]);

		if ($x) {
			$x->delete();
		}
		return redirect('clients')->with('flash_message', 'Client Deleted!!');
	}

	public function billClient($id,$branch_id)
	{
		$client = Client::find($id);
		if($branch_id==0)
		{
			$g = Client::find($id)->branch;
			$client_branch = ClientBranch::find($g[0]->id);	
			
		}
		else
		{
			$client_branch = ClientBranch::find($branch_id);	
		}
		

		return view('client.bill_client',['client' => $client,'client_branch' => $client_branch,'id'=>$id]);
	}

	public function pdf($id,$branch_id)
	{
		$client = Client::find($id);
		$client_branch = ClientBranch::find($branch_id);
		$sales_invoice = DB::table('sales_invoice')
				->join('clients', 'clients.id', '=', 'sales_invoice.client_id')
				->leftJoin(DB::raw("(SELECT id,first_name,last_name,middle_name FROM account_managers) AS account_managers"),function($join){
						$join->on("account_managers.id","=","sales_invoice.account_manager_id");
				})
				->leftJoin('cash_payments', 'cash_payments.sales_invoice_id', '=', 'sales_invoice.id')
				->leftJoin('check_payments', 'check_payments.sales_invoice_id', '=', 'sales_invoice.id')
				->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM cash_payments GROUP BY sales_invoice_id) AS cp"),function($join){
						$join->on("cp.sales_invoice_id","=","sales_invoice.id");
				})
				->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM check_payments  GROUP BY sales_invoice_id) AS chk"),function($join){
						$join->on("chk.sales_invoice_id","=","sales_invoice.id");
				})
				->select(
					'sales_invoice.id',
					'sales_invoice.invoice_date',
					'sales_invoice.si',
					'sales_invoice.po',
					'sales_invoice.pay_type',
					'sales_invoice.client_id',
					'sales_invoice.branch_id',
					'sales_invoice.account_manager_id',
					'sales_invoice.tax_rate',
					'sales_invoice.terms',
					DB::raw('CASE 
						WHEN ((DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) < NOW()) AND sales_invoice.status!="paid") THEN "over-due"
						WHEN sales_invoice.status="paid" THEN "paid"
						WHEN sales_invoice.status="cancelled" THEN "cancelled"
						WHEN ((IFNULL(cp.amount,0)+IFNULL(chk.amount,0))>0 AND sales_invoice.status="unpaid" AND (DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) > NOW()))  THEN "partially-paid"
						WHEN sales_invoice.status="unpaid" THEN "unpaid"
						
						ELSE sales_invoice.status END AS status'),
					'sales_invoice.amount_total',
					'sales_invoice.tax_amount',
					'clients.id',
					'clients.client_name',
					'clients.email',
					'clients.vatable',
					'clients.tin',
					'clients.credit_limit',
					'clients.personal',
					DB::raw('(SELECT branch_name FROM client_branches WHERE id=sales_invoice.branch_id) branch_name'),
					DB::raw('(SELECT email FROM client_branches WHERE id=sales_invoice.branch_id) branch_email'),
					DB::raw('(SELECT address FROM client_branches WHERE id=sales_invoice.branch_id) address'),
					'account_managers.first_name',
					'account_managers.last_name',
					DB::raw('cp.amount as cash_payment'),
					DB::raw('chk.amount as check_payment'),
					DB::raw('DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) as due_date'),					
					DB::raw('(sales_invoice.amount_total-(IFNULL(cp.amount,0)+IFNULL(chk.amount,0))) as balance'),
					DB::raw('CONCAT(account_managers.first_name," ",account_managers.last_name) as manager')
					)

			->where('sales_invoice.branch_id','=',$branch_id)
			->where('clients.id','=',$id)
			->where('status','!=','paid')
			->where('archived','=',0)
			->groupBy('sales_invoice.id')
			->orderBy('sales_invoice.created_at','desc')
			->get();	

		$html = view('client.bill_preview',['client' => $client,'client_branch' => $client_branch,'sales_invoice' => $sales_invoice,'id'=>$id]);

		

		$pdf = PDF::loadHTML($html);
		$pdf->stream();
		$pdf_string =   $pdf->output();
		$file = 'uploads/'.$client->client_name.'-billing.pdf';
		file_put_contents($file, $pdf_string);


		$email = "";
		if($client_branch->email=='')		
			$email = $client->email;		
		else		
			$email = $client_branch->email;


		$data = array(
            'email'=>$email,
            'file'=>$file,
            'company_name'=>Session('company_name')
            ); 

	    Mail::send(['html'=>'email'], $data, function($message) use ($data) {
            $message->to($data['email'], $data['email'])->subject('Billing');
            $message->from('pogipol@gmail.com',$data['company_name']);
            $message->attach($data['file']);
        });

        return Response::json(['result'=>'okay'], 200, array(),JSON_PRETTY_PRINT);
	}
	public function billPreview($id,$branch_id)
	{
		$client = Client::find($id);
		$client_branch = ClientBranch::find($branch_id);
		$sales_invoice = DB::table('sales_invoice')
				->join('clients', 'clients.id', '=', 'sales_invoice.client_id')
				->leftJoin(DB::raw("(SELECT id,first_name,last_name,middle_name FROM account_managers) AS account_managers"),function($join){
						$join->on("account_managers.id","=","sales_invoice.account_manager_id");
				})
				->leftJoin('cash_payments', 'cash_payments.sales_invoice_id', '=', 'sales_invoice.id')
				->leftJoin('check_payments', 'check_payments.sales_invoice_id', '=', 'sales_invoice.id')
				->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM cash_payments GROUP BY sales_invoice_id) AS cp"),function($join){
						$join->on("cp.sales_invoice_id","=","sales_invoice.id");
				})
				->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM check_payments  GROUP BY sales_invoice_id) AS chk"),function($join){
						$join->on("chk.sales_invoice_id","=","sales_invoice.id");
				})
				->select(
					'sales_invoice.id',
					'sales_invoice.invoice_date',
					'sales_invoice.si',
					'sales_invoice.po',
					'sales_invoice.pay_type',
					'sales_invoice.client_id',
					'sales_invoice.branch_id',
					'sales_invoice.account_manager_id',
					'sales_invoice.tax_rate',
					'sales_invoice.terms',
					DB::raw('CASE 
						WHEN ((DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) < NOW()) AND sales_invoice.status!="paid") THEN "over-due"
						WHEN sales_invoice.status="paid" THEN "paid"
						WHEN sales_invoice.status="cancelled" THEN "cancelled"
						WHEN ((IFNULL(cp.amount,0)+IFNULL(chk.amount,0))>0 AND sales_invoice.status="unpaid" AND (DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) > NOW()))  THEN "partially-paid"
						WHEN sales_invoice.status="unpaid" THEN "unpaid"
						
						ELSE sales_invoice.status END AS status'),
					'sales_invoice.amount_total',
					'sales_invoice.tax_amount',
					'clients.id',
					'clients.client_name',
					'clients.email',
					'clients.vatable',
					'clients.tin',
					'clients.credit_limit',
					'clients.personal',
					DB::raw('(SELECT branch_name FROM client_branches WHERE id=sales_invoice.branch_id) branch_name'),
					DB::raw('(SELECT address FROM client_branches WHERE id=sales_invoice.branch_id) address'),
					'account_managers.first_name',
					'account_managers.last_name',
					DB::raw('cp.amount as cash_payment'),
					DB::raw('chk.amount as check_payment'),
					DB::raw('DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) as due_date'),					
					DB::raw('(sales_invoice.amount_total-(IFNULL(cp.amount,0)+IFNULL(chk.amount,0))) as balance'),
					DB::raw('CONCAT(account_managers.first_name," ",account_managers.last_name) as manager')
					)

			->where('sales_invoice.branch_id','=',$branch_id)
			->where('clients.id','=',$id)
			->where('status','!=','paid')
			->where('archived','=',0)
			->groupBy('sales_invoice.id')
			->orderBy('sales_invoice.created_at','desc')
			->get();	
		return view('client.bill_preview',['client' => $client,'client_branch' => $client_branch,'sales_invoice' => $sales_invoice,'id'=>$id]);
	}
}
