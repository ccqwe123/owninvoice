<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\ActivityLog;
use App\Roles;
use App\UserRoles;
use App\Privileges;
use App\RolePrivileges;
use App\Variable;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use DB;
use Log;
use Response;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
class AuthController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware($this->guestMiddleware(), ['except' => 'logout']);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make(Input::all(), [
			'username' => 'required|max:255',
			'password' => 'required|min:3|confirmed',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	protected function create(array $data)
	{
		return User::create([
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
		]);
	}
	public function login(){
		return view('index');
	}
	public function logout()
	{
		//Log::info('*******Logging IN *********'.Input::get('username'));
		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'logout',
			'comment'  =>  '',
			'family'  =>  'logout',
			'created_at' => \Carbon\Carbon::now()
			]);

		auth()->logout();
		return redirect('/');
	}

	public function authenticate(Request $request)
	{

		$credentials = [
			'username' => Input::get('username'),
			'password' => Input::get('password'),
		];



   
		if(!Auth::attempt($credentials))
		{
			Session::flash('flash_error','Wrong username/password!');

			DB::table('activity_log')->insert([
			'username'  =>  $request->input('username').'@'.\Request::ip(),
			'entry'  =>  'login-failure',
			'comment'  =>  '',
			'family'  =>  'login-failure',
			'created_at' => \Carbon\Carbon::now()
			]);

			//return Response::json(array('success' => false));
			return redirect()->back();
		}
		// $users = DB::table('users')
		// 	->leftJoin('persons','persons.id','=','users.person_id')
		// 	->where('users.username','=',Input::get('username'))
		// 	->take(1)
		// 	->get();


		// foreach($users as $y)
		// {
		// 	Log::info('*************************');
		// 	session(['person_id' =>  $y->id]); 
		// 	session(['admin' =>  $y->admin]); 
		// 	$admin = $y->admin;
		// 	session(['supervisor' =>  $y->supervisor]); 
		// 	$sup = $y->supervisor;
		// 	session(['operator' =>  $y->operator]); 
		// 	session(['accountant' =>  $y->accountant]); 
		// 	$act = $y->accountant;;
		// 	session(['fname' =>  $y->fname]); 
		// 	session(['lname' =>  $y->lname]); 
		// 	session(['mname' =>  $y->mname]); 
		// 	session(['photo' =>  $y->photo]); 
		// 	$x = DB::table('branches')
		// 		->where('person_id','=',$y->id)
		// 		->value('id');
		// 	session(['branch' => $x]);
		// 	$var=\App\ReportTrait::sample($y->id);
		// }
		
		
		// $priveleges = Privileges::where('name','is_allow_products')->get();

		// $roles = Roles::where('user_id',$id);
		$is_allow_products = User::find(Auth::user()->id)->checkPrivileges('is_allow_products');
		session(['is_allow_products' =>  $is_allow_products]); 

		$is_allow_clients = User::find(Auth::user()->id)->checkPrivileges('is_allow_clients');
		session(['is_allow_clients' =>  $is_allow_clients]); 

		$is_allow_invoice = User::find(Auth::user()->id)->checkPrivileges('is_allow_invoice');
		session(['is_allow_invoice' =>  $is_allow_invoice]); 

		$is_allow_payment = User::find(Auth::user()->id)->checkPrivileges('is_allow_payment');
		session(['is_allow_payment' =>  $is_allow_payment]); 
		
		$is_allow_user = User::find(Auth::user()->id)->checkPrivileges('is_allow_user');
		session(['is_allow_user' =>  $is_allow_user]); 
		
		$is_allow_managers = User::find(Auth::user()->id)->checkPrivileges('is_allow_managers');
		session(['is_allow_managers' =>  $is_allow_managers]); 
		
		$is_allow_banks = User::find(Auth::user()->id)->checkPrivileges('is_allow_banks');
		session(['is_allow_banks' =>  $is_allow_banks]); 
		
		$is_allow_settings = User::find(Auth::user()->id)->checkPrivileges('is_allow_settings');
		session(['is_allow_settings' =>  $is_allow_settings]); 
		
		$is_allow_reports = User::find(Auth::user()->id)->checkPrivileges('is_allow_reports');
		session(['is_allow_reports' =>  $is_allow_reports]); 
		
		$is_allow_roles = User::find(Auth::user()->id)->checkPrivileges('is_allow_roles');
		session(['is_allow_roles' =>  $is_allow_roles]); 
		
		$is_allow_logs = User::find(Auth::user()->id)->checkPrivileges('is_allow_logs');
		session(['is_allow_logs' =>  $is_allow_logs]); 
		
		$is_allow_logs = User::find(Auth::user()->id)->checkPrivileges('is_allow_payment_approval');
		session(['is_allow_payment_approval' =>  $is_allow_logs]); 
		
		$var = Variable::all();
		foreach ($var as $x) {
			session([$x->name=>$x->value]); 

		}
		
		Session::flash('flash_message','Logged in!');
		session(['username' =>  Input::get('username')]); 

		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  => 'login-success',
			'comment'  =>  '',
			'family'  =>  'login-success',
			'created_at' => \Carbon\Carbon::now()
			]);


		// Log::info('*******Logged IN *********'.Input::get('username'));
		return redirect('dashboard');
	
		
		//return Response::json(array('success' => true));      
	} 
}
