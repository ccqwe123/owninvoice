<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Log;
use App\ActivityLog;
use Validator;
use Session;
use Redirect;
use Response;
use View;
use DB;
use Illuminate\Support\Facades\Auth;

class ActivityLogController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$logs = ActivityLog::find($id);
		return view('log.list_log',['logs'=>$logs]);
	}

	public function logSearch(Request $request)
	{

		$from_date = ($request->input('from_date') == null ? 0 : $request->input('from_date'));
		$to_date = ($request->input('to_date') == null ? 0 : $request->input('to_date'));
		$log_type = ($request->input('log_type') == null ? 0 : $request->input('log_type'));

		$query = DB::table('activity_log')
				->whereRaw("(('".$from_date."'='0' AND '".$to_date."'='0') OR (activity_log.created_at BETWEEN '".$from_date."' AND '".$to_date."'))")
				->whereRaw('(("'.$log_type.'"="0") OR (
					CASE WHEN "'.$log_type.'"="void" THEN activity_log.family ="void"
					 WHEN "'.$log_type.'"="insert" THEN activity_log.family ="insert"
					 WHEN "'.$log_type.'"="update" THEN activity_log.family ="update"
					 WHEN "'.$log_type.'"="delete" THEN activity_log.family ="delete"
					 WHEN "'.$log_type.'"="login-success" THEN activity_log.family ="login-success"
					 WHEN "'.$log_type.'"="login-failure" THEN activity_log.family ="login-failure"
					 WHEN "'.$log_type.'"="logout" THEN activity_log.family ="logout"
					 WHEN "'.$log_type.'"="others" THEN activity_log.family="others" END
					)) order by id desc');

		$logs_all = $query->get();
		$logs = $query
			->paginate(10);

		$log_array=json_encode($query);
		// Log::info($payments_array);
		return view('logs.logs',[
				'logs'=>$logs,
				'log_array'=>$log_array,
				'logs_all'=>$logs_all
				]);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
