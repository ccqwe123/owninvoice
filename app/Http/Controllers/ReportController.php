<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Support\Jsonable;
use App\Http\Requests;
use Log;
use DB;
use View;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use App\ClientBranch;
use App\TransactionItems;
use App\ProductCategory;
use App\ProductType;
use PHPExcel_Worksheet_Drawing;

class ReportController extends Controller
{
	//
	public function reportSalesInvoice()
	{
		return view('reports.sales_invoice_new',[]);
	}

	public function reportSalesOrder()
	{
		
		return view('reports.sales_order_new',[]);
	}

	public function reportProducts()
	{
		$categories = ProductCategory::get();
		$types = ProductType::get();
		return view('reports.products_new',['categories'=>$categories,'types'=>$types]);
	}
	public function reportPurchase()
	{
		return view('reports.purchase_new',[]);
	}

	public function reportClients()
	{
		return view('reports.clients_new',[]);
	}

	public function reportPayments()
	{
		return view('reports.payments_new',[]);
	}
	public function reportTransaction()
	{
		return view('reports.transaction_new',[]);
	}

	public function reportAgent()
	{
		return view('reports.agents_new',[]);
	}
	public function reportPaymentsGet(Request $request)
	{
		DB::enableQueryLog();
		$from_date = ($request->input('from_date') == null ? 0 : $request->input('from_date'));
		$to_date = ($request->input('to_date') == null ? 0 : $request->input('to_date'));
		$pay_type = ($request->input('pay_type') == null ? 0 : $request->input('pay_type'));
		$approved = ($request->input('approved') == null ? -1 : $request->input('approved'));
		$query = DB::table('payments')
				->join('sales_invoice', 'sales_invoice.id', '=', 'payments.sales_invoice_id')
				->select(
					'payments.id as payments_id',
					DB::raw('(SELECT client_name FROM clients WHERE id=sales_invoice.client_id) client_name'),
					DB::raw('(SELECT branch_name FROM client_branches WHERE id=sales_invoice.branch_id) branch_name'),
					'payments.approved',
					'payments.less',
					DB::raw('payments.created_at as payment_date'),
					'sales_invoice.id',
					'sales_invoice.si',
					DB::raw('CASE 
				WHEN payments.cash_payment_id IS NOT NULL THEN "cash" 
				WHEN payments.check_payment_id IS NOT NULL THEN "check"
				WHEN payments.deposit_payment_id IS NOT NULL THEN "deposit" END pay_type'),
					DB::raw('COALESCE((SELECT amount FROM cash_payments WHERE id=payments.cash_payment_id),
				(SELECT amount FROM check_payments WHERE id=payments.check_payment_id),
				(SELECT amount FROM deposit_payments WHERE id=payments.deposit_payment_id),0) payment_amount'))
				->whereRaw('(cash_payment_id IS NOT NULL OR check_payment_id IS NOT NULL OR deposit_payment_id IS NOT NULL)')
				->whereRaw("((".$approved."=-1) OR (payments.approved=".$approved."))")
				->whereRaw("(('".$from_date."'='0' AND '".$to_date."'='0') OR (payments.created_at BETWEEN '".$from_date."' AND '".$to_date."'))")
				->whereRaw('(("'.$pay_type.'"="0") OR (
					CASE
					WHEN "'.$pay_type.'"="cashdeposit" THEN payments.cash_payment_id IS NOT NULL OR payments.deposit_payment_id IS NOT NULL
					WHEN "'.$pay_type.'"="cash" THEN payments.cash_payment_id IS NOT NULL
					WHEN "'.$pay_type.'"="deposit" THEN payments.deposit_payment_id IS NOT NULL
					 WHEN "'.$pay_type.'"="check" THEN payments.check_payment_id IS NOT NULL END
					))')
				->orderBy('payments_id','desc');

		$payments_all = $query->get();
		$payments = $query
			->paginate(10);
		$payments_array=json_encode($query);
		return view('reports.payments',[
				'payments'=>$payments,
				'payments_array'=>$payments_array,
				'payments_all'=>$payments_all
				]);
	}
	public function reportPaymentsExport(Request $request,$type)
	{
		$from_date = ($request->input('from_date') == null ? 0 : $request->input('from_date'));
		$to_date = ($request->input('to_date') == null ? 0 : $request->input('to_date'));
		$pay_type = ($request->input('pay_type') == null ? 0 : $request->input('pay_type'));
		$approved = ($request->input('approved') == null ? -1 : $request->input('approved'));
		$query = DB::table('payments')
				->join('sales_invoice', 'sales_invoice.id', '=', 'payments.sales_invoice_id')
				->select(
					'payments.id as payments_id',
					DB::raw('(SELECT client_name FROM clients WHERE id=sales_invoice.client_id) client_name'),
					DB::raw('(SELECT branch_name FROM client_branches WHERE id=sales_invoice.branch_id) branch_name'),
					'payments.approved',
					'payments.less',
					DB::raw('payments.created_at as payment_date'),
					'sales_invoice.id',
					'sales_invoice.si',
					DB::raw('CASE 
				WHEN payments.cash_payment_id IS NOT NULL THEN "cash" 
				WHEN payments.check_payment_id IS NOT NULL THEN "check"
				WHEN payments.deposit_payment_id IS NOT NULL THEN "deposit" END pay_type'),
					DB::raw('COALESCE((SELECT amount FROM cash_payments WHERE id=payments.cash_payment_id),
				(SELECT amount FROM check_payments WHERE id=payments.check_payment_id),
				(SELECT amount FROM deposit_payments WHERE id=payments.deposit_payment_id),0) payment_amount'))
				->whereRaw('(cash_payment_id IS NOT NULL OR check_payment_id IS NOT NULL OR deposit_payment_id IS NOT NULL)')
				->whereRaw("((".$approved."=-1) OR (payments.approved=".$approved."))")
				->whereRaw("(('".$from_date."'='0' AND '".$to_date."'='0') OR (payments.created_at BETWEEN '".$from_date."' AND '".$to_date."'))")
				->whereNull("sales_invoice.deleted_at")
				->where("sales_invoice.archived","=",0)
				->whereRaw('(("'.$pay_type.'"="0") OR (
					CASE 
					WHEN "'.$pay_type.'"="cashdeposit" THEN payments.cash_payment_id IS NOT NULL OR payments.deposit_payment_id IS NOT NULL
					WHEN "'.$pay_type.'"="cash" THEN payments.cash_payment_id IS NOT NULL
					WHEN "'.$pay_type.'"="deposit" THEN payments.deposit_payment_id IS NOT NULL
					 WHEN "'.$pay_type.'"="check" THEN payments.check_payment_id IS NOT NULL END
					))')
				->orderBy('payments_id','desc');

		$payments_array=json_encode($query->get());
		$data = json_decode($payments_array,true);
		return Excel::create('payments', function($excel) use ($data) {
			$excel->sheet('payments', function($sheet) use ($data)
			{
				foreach ($data as $key => $field)
				{
					$data[$key]['payment_amount'] = (float)$data[$key]['payment_amount'];
				}
				$sheet->fromArray($data, null, 'A7', false, true);	
				$sheet->setCellValue('J'.(count($data)+7+1),'=SUM(J8:J'.(count($data)+7).')');
				$sheet->setCellValue('A5',session('company_address'));
				$sheet->mergeCells('A5:E5');

			});
		})->download($type);
	}
	public function reportClientsGet(Request $request)
	{
		$client_id = ($request->input('client_id') == null ? 0 : $request->input('client_id'));
		$branch_id = ($request->input('branch_id') == null ? 0 : $request->input('branch_id'));
		$from_date = ($request->input('from_date') == null ? 0 : $request->input('from_date'));
		$to_date = ($request->input('to_date') == null ? 0 : $request->input('to_date'));
		$personal = ($request->input('personal') == null ? 0 : $request->input('personal'));
		$branch_personal = ($request->input('branch_personal') == null ? 0 : $request->input('branch_personal'));
		
		$query = DB::table('sales_invoice')
			->join('clients', 'clients.id', '=', 'sales_invoice.client_id')
			->leftJoin(DB::raw("(SELECT id,first_name,last_name,middle_name FROM account_managers) AS account_managers"),function($join){
					$join->on("account_managers.id","=","sales_invoice.account_manager_id");
			})
			->leftJoin('cash_payments', 'cash_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('check_payments', 'check_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM cash_payments GROUP BY sales_invoice_id) AS cp"),function($join){
					$join->on("cp.sales_invoice_id","=","sales_invoice.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM check_payments  GROUP BY sales_invoice_id) AS chk"),function($join){
					$join->on("chk.sales_invoice_id","=","sales_invoice.id");
			})
			->select(
				'sales_invoice.*',
				'clients.client_name',
				'clients.email',
				'clients.vatable',
				'clients.tin',
				'clients.credit_limit',
				'clients.personal',
				DB::raw('(SELECT branch_name FROM client_branches WHERE id=sales_invoice.branch_id) branch_name'),
				DB::raw('(SELECT personal FROM client_branches WHERE id=sales_invoice.branch_id) branch_personal'),
				DB::raw('CONCAT(account_managers.first_name," ",account_managers.last_name) account_manager'),
				DB::raw('cp.amount as cash_payment'),
				DB::raw('chk.amount as check_payment'),
				DB::raw('DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) as due_date')				
				)
			->whereRaw("((".$client_id."=0) OR (sales_invoice.client_id=".$client_id."))")
			->whereRaw("((".$personal."=-1) OR (clients.personal=".$personal."))")
			->whereRaw("((".$branch_id."=0) OR (sales_invoice.branch_id=".$branch_id."))")
			->whereRaw("((".$branch_personal."=0) OR ((SELECT personal FROM client_branches WHERE id=sales_invoice.branch_id) =".$branch_personal."))")
			->whereRaw("(('".$from_date."'='0' && '".$to_date."'='0') OR (sales_invoice.invoice_date BETWEEN '".$from_date."' AND '".$to_date."'))")
			->where('sales_invoice.archived','=',0)
			->groupBy('sales_invoice.id')
			->orderBy('account_manager','desc');
		$clients_all = $query->get();
		$clients = $query
			->paginate(10);
		$clients_array=json_encode($query->get());
		return view('reports.clients',[
				'clients'=>$clients,
				'clients_array'=>$clients_array,
				'clients_all'=>$clients_all
				]);
	}
	
	public function reportClientsExport(Request $request,$type)
	{
		$client_id = ($request->input('client_id') == null ? 0 : $request->input('client_id'));
		$branch_id = ($request->input('branch_id') == null ? 0 : $request->input('branch_id'));
		$from_date = ($request->input('from_date') == null ? 0 : $request->input('from_date'));
		$to_date = ($request->input('to_date') == null ? 0 : $request->input('to_date'));
		$personal = ($request->input('personal') == null ? 0 : $request->input('personal'));

		$query = DB::table('sales_invoice')
			->join('clients', 'clients.id', '=', 'sales_invoice.client_id')
			->leftJoin(DB::raw("(SELECT id,first_name,last_name,middle_name FROM account_managers) AS account_managers"),function($join){
					$join->on("account_managers.id","=","sales_invoice.account_manager_id");
			})
			->leftJoin('cash_payments', 'cash_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('check_payments', 'check_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM cash_payments GROUP BY sales_invoice_id) AS cp"),function($join){
					$join->on("cp.sales_invoice_id","=","sales_invoice.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM check_payments  GROUP BY sales_invoice_id) AS chk"),function($join){
					$join->on("chk.sales_invoice_id","=","sales_invoice.id");
			})
			->select(
				'sales_invoice.*',
				'clients.client_name',
				'clients.email',
				'clients.vatable',
				'clients.tin',
				'clients.credit_limit',
				'clients.personal',
				DB::raw('(SELECT branch_name FROM client_branches WHERE id=sales_invoice.branch_id) branch_name'),
				DB::raw('CONCAT(account_managers.first_name," ",account_managers.last_name) account_manager'),
				DB::raw('cp.amount as cash_payment'),
				DB::raw('chk.amount as check_payment'),
				DB::raw('DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) as due_date')				
				)
			->whereRaw("((".$client_id."=0) OR (sales_invoice.client_id=".$client_id."))")
			->whereRaw("((".$personal."=-1) OR (clients.personal=".$personal."))")
			->whereRaw("((".$branch_id."=0) OR (sales_invoice.branch_id=".$branch_id."))")
			->whereRaw("(('".$from_date."'='0' && '".$to_date."'='0') OR (sales_invoice.invoice_date BETWEEN '".$from_date."' AND '".$to_date."'))")
			->where('sales_invoice.archived','=',0)
			->groupBy('sales_invoice.id')
			->orderBy('account_manager','desc');

		$clients_all=json_encode($query->get());
		$data = json_decode($clients_all,true);
		return Excel::create('clients', function($excel) use ($data) {
			$excel->sheet('clients', function($sheet) use ($data)
			{
				
				foreach ($data as $key => $field)
				{
					$data[$key]['amount_total'] = (float)$data[$key]['amount_total'];
					$data[$key]['tax_amount'] = (float)$data[$key]['tax_amount'];
					$data[$key]['vat_sales'] = (float)$data[$key]['vat_sales'];
					$data[$key]['discount'] = (float)$data[$key]['discount'];
				}
				$sheet->fromArray($data, null, 'A7', false, true);	
				
				$sheet->setCellValue('L'.(count($data)+7+1),'=SUM(L8:L'.(count($data)+7).')');
				$sheet->setCellValue('M'.(count($data)+7+1),'=SUM(M8:M'.(count($data)+7).')');
				$sheet->setCellValue('N'.(count($data)+7+1),'=SUM(N8:N'.(count($data)+7).')');
				$sheet->setCellValue('O'.(count($data)+7+1),'=SUM(O8:O'.(count($data)+7).')');
				$sheet->setCellValue('A5',session('company_address'));
				$sheet->mergeCells('A5:E5');

			});
		})->download($type);
	}
	public function reportProductsGet(Request $request)
	{
		$product_id = ($request->input('product_id') == null ? 0 : $request->input('product_id'));
		$type_id = ($request->input('type_id') == null ? 0 : $request->type_id);
		$category_id = ($request->input('category_id') == null ? 0 : $request->input('category_id'));
		$query = DB::table('products')
			->select(
				'products.*',
				DB::raw('(SELECT IFNULL(SUM(qty),0) FROM transaction_items WHERE product_id=products.id) AS sold')
				)
			->whereRaw("((".$product_id."=0) OR (products.id=".$product_id."))")
			->whereRaw("((".$type_id."=0) OR (products.product_types_id=".$type_id."))")
			->whereRaw("((".$category_id."=0) OR (products.product_categories_id=".$category_id."))")

			->Where(function ($query) use ($request) {
				$query->where('products.prod_name','like', '%'.$request->filter.'%')
				->orWhere('products.prod_code','like', '%'.$request->filter.'%')
				->orWhere('products.prod_description','like', '%'.$request->filter.'%');
			})
			->groupBy('products.id')
			->orderBy('products.created_at','desc');
		$products_all = $query->get();
		$products = $query
			->paginate(10);
		$products_array=json_encode($query->get());

		$categories = ProductCategory::get();
		$types = ProductType::get();

		return view('reports.products',[
				'products'=>$products,
				'products_array'=>$products_array,
				'products_all'=>$products_all,
				'categories'=>$categories,
				'types'=>$types
				]);
	}

	public function reportPurchaseGet(Request $request)
	{
		$supplier_id = ($request->input('supplier_id') == null ? 0 : $request->input('supplier_id'));

		$query = DB::table('purchase_orders')
			->leftjoin('suppliers','suppliers.id','=','purchase_orders.suppliers_id')
			->select(
				'purchase_orders.*',
				'suppliers.supplier_name'
				)
			->whereRaw("((".$supplier_id."=0) OR (purchase_orders.suppliers_id=".$supplier_id."))")		
			->Where(function ($query) use ($request) {
				$query->where('suppliers.supplier_name','like', '%'.$request->filter.'%')
				->orWhere('purchase_orders.dr','like', '%'.$request->filter.'%')
				->orWhere('purchase_orders.pay_type','like', '%'.$request->filter.'%')
				->orWhere('purchase_orders.status','like', '%'.$request->filter.'%')
				->orWhere('purchase_orders.po','like', '%'.$request->filter.'%');
			})
			->orderBy('purchase_orders.created_at','desc');
		$products_all = $query->get();
		$products = $query
			->paginate(10);
		$products_array=json_encode($query->get());

		return view('reports.purchase',[
				'products'=>$products,
				'products_array'=>$products_array,
				'products_all'=>$products_all
				]);
	}

	public function reportSalesOrderGet(Request $request)
	{
		$sales_order_id = ($request->input('sales_orders_id') == null ? 0 : $request->input('sales_orders_id'));
		$client_id = ($request->input('client_id') == null ? 0 : $request->input('client_id'));
		$branch_id = ($request->input('branch_id') == null ? 0 : $request->input('branch_id'));
		$status = ($request->input('status') == null ? 0 : $request->input('status'));
		$pay_type = ($request->input('pay_type') == null ? 0 : $request->input('pay_type'));
		$from_date = ($request->input('from_date') == null ? 0 : $request->input('from_date'));
		$to_date = ($request->input('to_date') == null ? 0 : $request->input('to_date'));
		$client_type = ($request->input('client_type') == null ? -1 : $request->input('client_type'));
		$account_manager_id = ($request->input('account_manager_id') == null ? 0 : $request->input('account_manager_id'));

		$query = DB::table('sales_orders')
		->join('clients', 'clients.id', '=', 'sales_orders.client_id')
		->leftJoin(DB::raw("(SELECT id,first_name,last_name,middle_name FROM agents) AS agents"),function($join){
				$join->on("agents.id","=","sales_orders.agent_id");
		})
		->leftJoin(DB::raw("(SELECT id,first_name,last_name,middle_name FROM account_managers) AS account_managers"),function($join){
				$join->on("account_managers.id","=","sales_orders.account_manager_id");
		})
		->leftJoin(DB::raw("(SELECT branch_name,id FROM client_branches) AS branch_name"),function($join){
				$join->on("branch_name.id","=","sales_orders.branch_id");
		})
		->select(
			'sales_orders.id',
			'sales_orders.invoice_date',
			'sales_orders.so',
			'sales_orders.po',
			'sales_orders.client_id',
			'sales_orders.branch_id',
			'sales_orders.account_manager_id',
			'sales_orders.tax_rate',
			'sales_orders.agent_id',
			'sales_orders.amount_total',
			'sales_orders.tax_amount',
			'sales_orders.status',
			'sales_orders.archived',
			'sales_orders.terms',
			DB::raw('IF (sales_orders.approved="1","approved","not-approved") approved'),
			DB::raw('IF (clients.vatable>0,0,sales_orders.tax_amount) tax_amount'),
			'clients.client_name',
			'clients.vatable',
			'clients.personal',
			'branch_name.branch_name',
			'account_managers.first_name',
			'account_managers.last_name',
			'agents.first_name',
			'agents.last_name',
			DB::raw('DATE_ADD(sales_orders.invoice_date, INTERVAL sales_orders.terms DAY) as due_date'),
			DB::raw('CONCAT(account_managers.first_name," ",account_managers.last_name) as manager'),
			DB::raw('CONCAT(agents.first_name," ",agents.last_name) as agent')
			)
			->whereRaw("((".$sales_order_id."=0) OR (sales_orders.id=".$sales_order_id."))")
			->whereRaw("((".$client_id."=0) OR (sales_orders.client_id=".$client_id."))")
			->whereRaw("((".$account_manager_id."=0) OR (sales_orders.account_manager_id=".$account_manager_id."))")
			->whereRaw("((".$branch_id."=0) OR (sales_orders.branch_id=".$branch_id."))")
			->whereRaw("((	'".$client_type."'='-1') OR (clients.personal='".$client_type."'))")
			->whereRaw("(('".$from_date."'='0' && '".$to_date."'='0') OR (sales_orders.invoice_date BETWEEN '".$from_date."' AND '".$to_date."'))")
			->Where(function ($query) use ($request) {
				$query->where('clients.client_name','like', '%'.$request->filter.'%')
				->orWhere('sales_orders.so','like', '%'.$request->filter.'%')
				->orWhere('sales_orders.po','like', '%'.$request->filter.'%');
			})
			->groupBy('sales_orders.id')
			->orderBy('sales_orders.created_at','desc');

		$sales_order_all = $query->get();
		$sales_order = $query->paginate(10);
		$sales_orders_array=json_encode($query->get());

		return view('reports.sales_order',[
				'sales_order'=>$sales_order,
				'sales_order_array'=>$sales_orders_array,
				'sales_order_all'=>$sales_order_all
				]);
	}

	public function reportTransactionGet(Request $request)
	{
		$product_id = ($request->input('product_id') == null ? 0 : $request->input('product_id'));

		$query = DB::table('transaction_items')
			->leftjoin('products','products.id','=','transaction_items.product_id')
			->leftjoin('purchase_orders','purchase_orders.id','=','transaction_items.purchase_orders_id')
			->leftjoin('sales_invoice','sales_invoice.id','=','transaction_items.sales_invoice_id')
			->select(
				'transaction_items.id',
				'transaction_items.price',
				DB::raw('IF(transaction_items.purchase_orders_id is null,"Out","In") as stock'),
				'transaction_items.qty',
				DB::raw('transaction_items.created_at'),
				DB::raw('(transaction_items.qty * transaction_items.price) as amount'),
				'products.prod_name',
				DB::raw("IF(transaction_items.purchase_orders_id is null, sales_invoice.status,purchase_orders.status) status")
			)
			->whereRaw("((".$product_id."=0) OR (transaction_items.product_id=".$product_id."))")		
			->Where(function ($query) use ($request) {
				$query->where('products.prod_name','like', '%'.$request->filter.'%')
				->orWhere('transaction_items.created_at','like', '%'.$request->filter);
			})
			->orderBy('transaction_items.created_at','desc');
		$transaction_all = $query->get();
		$transaction = $query
			->paginate(10);
		$products_array=json_encode($query->get());

		return view('reports.transaction',[
				'transaction'=>$transaction,
				'products_array'=>$products_array,
				'transaction_all'=>$transaction_all
				]);
	}

	public function reportAgentGet(Request $request, $id)
	{
		if($id==1){
			$data1 = ($request->input('daily') == null ? 0 : $request->input('daily'));
			$query = DB::table('transaction_items')
				->leftjoin('sales_invoice','sales_invoice.id','=','transaction_items.sales_invoice_id')
				->leftjoin('clients','clients.id','=','sales_invoice.client_id')
				->select(
					'transaction_items.id',
					'transaction_items.price',
					'clients.client_name'
				)
				->whereRaw("((".$data1."=0) OR (DATE(transaction_items.created_at)='".$data1."'))")
				->whereNotNull('transaction_items.sales_invoice_id')
				->distinct()
				->orderBy('transaction_items.created_at','desc');
		}
		else if($id==2){
			$monthly = ($request->input('monthly') == null ? 0 : $request->input('monthly'));
			$year = ($request->input('year') == null ? 0 : $request->input('year'));
			$query = DB::table('transaction_items')
				->leftjoin('sales_invoice','sales_invoice.id','=','transaction_items.sales_invoice_id')
				->leftjoin('clients','clients.id','=','sales_invoice.client_id')
				->select(
					'transaction_items.id',
					'transaction_items.price',
					'clients.client_name'
				)
				->whereRaw("((".$monthly."=0) OR (MONTH(transaction_items.created_at)='".$monthly."'))")
				->whereRaw("((".$year."= 0) OR (Year(transaction_items.created_at)='".$year."'))")
				->whereNotNull('transaction_items.sales_invoice_id')
				->distinct()
				->orderBy('transaction_items.created_at','desc');
		}
		else if($id==3){
			$year = ($request->input('yearly') == null ? 0 : $request->input('yearly'));
			$query = DB::table('transaction_items')
				->select(
					DB::raw('monthname(created_at) as monthly'),
					DB::raw('year(created_at) as yearly'),
					DB::raw('(Select sum(price) from transaction_items where monthname(created_at) = monthly and year(created_at)=yearly) as sum')
					
				)
				->whereRaw("((".$year."= 0) OR (Year(transaction_items.created_at)='".$year."'))")
				->whereNotNull('sales_invoice_id')
				->distinct()
				->orderBy('created_at','desc');
				$agent_all = $query->get();
				$agent = $query
					->paginate(10);
				$agent_array=json_encode($query->get());
				return view('reports.agents',[
					'agent'=>$agent,
					'agent_array'=>$agent_array,
					'agent_all'=>$agent_all,
					'yearly'=>'trot',
					'id'=>$id
					]);
		}

		
		$agent_all = $query->get();
		$agent = $query
			->paginate(10);
		$agent_array=json_encode($query->get());
		return view('reports.agents',[
				'agent'=>$agent,
				'agent_array'=>$agent_array,
				'agent_all'=>$agent_all,
				'id'=>$id
				]);
	}
	

	public function reportProductsExport(Request $request,$type)
	{
		$product_id = ($request->input('product_id') == null ? 0 : $request->input('product_id'));
		$type_id = ($request->input('type_id') == null ? 0 : $request->input('type_id'));
		$category_id = ($request->input('category_id') == null ? 0 : $request->input('category_id'));

		$query = DB::table('products')
			->select(
				'products.*',
				DB::raw('(SELECT IFNULL(SUM(qty),0) FROM transaction_items WHERE product_id=products.id) AS sold')
				)
			->whereRaw("((".$product_id."=0) OR (products.id=".$product_id."))")
			->whereRaw("((".$type_id."=0) OR (products.product_types_id=".$type_id."))")
			->whereRaw("((".$category_id."=0) OR (products.product_categories_id=".$category_id."))")
			
			->Where(function ($query) use ($request) {
				$query->where('products.prod_name','like', '%'.$request->filter.'%')
				->orWhere('products.prod_code','like', '%'.$request->filter.'%')
				->orWhere('products.prod_description','like', '%'.$request->filter.'%');
			})
			->groupBy('products.id')
			->orderBy('products.created_at','desc');
		$products_array=json_encode($query->get());
		$data = json_decode($products_array,true);
		return Excel::create('products', function($excel) use ($data) {
			$excel->sheet('products', function($sheet) use ($data)
			{
				$sheet->fromArray($data, null, 'A7', false, true);	

				$sheet->setCellValue('A5',session('company_address'));
				$sheet->mergeCells('A5:E5');
			});
		})->download($type);
	}


	public function reportPurchaseExport(Request $request,$type)
	{
		$purchase_orders_id = ($request->input('purchase_orders_id') == null ? 0 : $request->input('purchase_orders_id'));

		$query = DB::table('purchase_orders')
			->leftjoin('suppliers','suppliers.id','=','purchase_orders.suppliers_id')
			->select(
				'purchase_orders.*',
				'suppliers.supplier_name'
				)
			->whereRaw("((".$purchase_orders_id."=0) OR (purchase_orders.id=".$purchase_orders_id."))")		
			->Where(function ($query) use ($request) {
				$query->where('suppliers.supplier_name','like', '%'.$request->filter.'%')
				->orWhere('purchase_orders.dr','like', '%'.$request->filter.'%')
				->orWhere('purchase_orders.po','like', '%'.$request->filter.'%');
			})
			->orderBy('purchase_orders.created_at','desc');
		$products_array=json_encode($query->get());
		$data = json_decode($products_array,true);
		return Excel::create('purchase', function($excel) use ($data) {
			$excel->sheet('purchase', function($sheet) use ($data)
			{
				$sheet->fromArray($data, null, 'A7', false, true);	

				$sheet->setCellValue('A5',session('company_address'));
				$sheet->mergeCells('A5:E5');
			});
		})->download($type);
	}


	public function reportTransactionExport(Request $request,$type)
	{
		$purchase_orders_id = ($request->input('purchase_orders_id') == null ? 0 : $request->input('purchase_orders_id'));

		$query = DB::table('transaction_items')
			->leftjoin('products','products.id','=','transaction_items.product_id')
			->select(
				'transaction_items.id',
				'transaction_items.price',
				DB::raw('IF(transaction_items.purchase_orders_id is null,"Out","In") as stock'),
				'transaction_items.qty',
				DB::raw('transaction_items.created_at'),
				DB::raw('(transaction_items.qty * transaction_items.price) as amount'),
				'products.prod_name'
			)
			->whereRaw("((".$purchase_orders_id."=0) OR (transaction_items.purchase_orders_id=".$purchase_orders_id."))")		
			->Where(function ($query) use ($request) {
				$query->where('products.prod_name','like', '%'.$request->filter.'%')
				->orWhere('transaction_items.created_at','like', '%'.$request->filter);
			})
			->orderBy('transaction_items.created_at','desc');
		$transaction_array=json_encode($query->get());
		$data = json_decode($transaction_array,true);
		return Excel::create('transaction', function($excel) use ($data) {
			$excel->sheet('transaction', function($sheet) use ($data)
			{
				$sheet->fromArray($data, null, 'A7', false, true);	

				$sheet->setCellValue('A5',session('company_address'));
				$sheet->mergeCells('A5:E5');
			});
		})->download($type);
	}

	public function reportAgentsExport(Request $request,$id,$type)
	{
		if($id==1){
			$data1 = ($request->input('daily') == null ? 0 : $request->input('daily'));
			$query = DB::table('transaction_items')
				->leftjoin('sales_invoice','sales_invoice.id','=','transaction_items.sales_invoice_id')
				->leftjoin('clients','clients.id','=','sales_invoice.client_id')
				->select(
					'transaction_items.id',
					'transaction_items.price',
					'clients.client_name'
				)
				->whereRaw("((".$data1."=0) OR (DATE(transaction_items.created_at)='".$data1."'))")
				->whereNotNull('transaction_items.sales_invoice_id')
				->distinct()
				->orderBy('transaction_items.created_at','desc');
		}
		else if($id==2){
			$monthly = ($request->input('monthly') == null ? 0 : $request->input('monthly'));
			$year = ($request->input('year') == null ? 0 : $request->input('year'));
			$query = DB::table('transaction_items')
				->leftjoin('sales_invoice','sales_invoice.id','=','transaction_items.sales_invoice_id')
				->leftjoin('clients','clients.id','=','sales_invoice.client_id')
				->select(
					'transaction_items.id',
					'transaction_items.price',
					'clients.client_name'
				)
				->whereRaw("((".$monthly."=0) OR (MONTH(transaction_items.created_at)='".$monthly."'))")
				->whereRaw("((".$year."= 0) OR (Year(transaction_items.created_at)='".$year."'))")
				->whereNotNull('transaction_items.sales_invoice_id')
				->distinct()
				->orderBy('transaction_items.created_at','desc');
		}
		else if($id==3){
			$year = ($request->input('yearly') == null ? 0 : $request->input('yearly'));
			$query = DB::table('transaction_items')
				->select(
					DB::raw('monthname(created_at) as monthly'),
					DB::raw('year(created_at) as yearly'),
					DB::raw('(Select sum(price) from transaction_items where monthname(created_at) = monthly and year(created_at)=yearly) as sum')
					
				)
				->whereRaw("((".$year."= 0) OR (Year(transaction_items.created_at)='".$year."'))")
				->whereNotNull('sales_invoice_id')
				->distinct()
				->orderBy('created_at','desc');
		$agent_array=json_encode($query->get());
		$data = json_decode($agent_array,true);
		return Excel::create('agents', function($excel) use ($data) {
			$excel->sheet('agents', function($sheet) use ($data)
			{
				$sheet->fromArray($data, null, 'A7', false, true);	

				$sheet->setCellValue('A5',session('company_address'));
				$sheet->mergeCells('A5:E5');
			});
		})->download($type);
	}
}

	public function reportSalesOrderExport(Request $request,$type)
	{
		$sales_order_id = ($request->input('sales_orders_id') == null ? 0 : $request->input('sales_orders_id'));
		$client_id = ($request->input('client_id') == null ? 0 : $request->input('client_id'));
		$branch_id = ($request->input('branch_id') == null ? 0 : $request->input('branch_id'));
		$status = ($request->input('status') == null ? 0 : $request->input('status'));
		$pay_type = ($request->input('pay_type') == null ? 0 : $request->input('pay_type'));
		$from_date = ($request->input('from_date') == null ? 0 : $request->input('from_date'));
		$to_date = ($request->input('to_date') == null ? 0 : $request->input('to_date'));
		$client_type = ($request->input('client_type') == null ? -1 : $request->input('client_type'));
		$account_manager_id = ($request->input('account_manager_id') == null ? 0 : $request->input('account_manager_id'));

		$query = DB::table('sales_orders')
		->join('clients', 'clients.id', '=', 'sales_orders.client_id')
		->leftJoin(DB::raw("(SELECT id,first_name,last_name,middle_name FROM agents) AS agents"),function($join){
				$join->on("agents.id","=","sales_orders.agent_id");
		})
		->leftJoin(DB::raw("(SELECT id,first_name,last_name,middle_name FROM account_managers) AS account_managers"),function($join){
				$join->on("account_managers.id","=","sales_orders.account_manager_id");
		})
		->leftJoin(DB::raw("(SELECT branch_name,id FROM client_branches) AS branch_name"),function($join){
				$join->on("branch_name.id","=","sales_orders.branch_id");
		})
		->select(
			'sales_orders.id',
			'sales_orders.invoice_date',
			'sales_orders.so',
			'sales_orders.po',
			'sales_orders.client_id',
			'sales_orders.branch_id',
			'sales_orders.account_manager_id',
			'sales_orders.tax_rate',
			'sales_orders.agent_id',
			'sales_orders.amount_total',
			'sales_orders.tax_amount',
			'sales_orders.status',
			'sales_orders.archived',
			'sales_orders.terms',
			DB::raw('IF (sales_orders.approved="1","approved","not-approved") approved'),
			DB::raw('IF (clients.vatable>0,0,sales_orders.tax_amount) tax_amount'),
			'clients.client_name',
			'clients.vatable',
			'clients.personal',
			'branch_name.branch_name',
			'account_managers.first_name',
			'account_managers.last_name',
			'agents.first_name',
			'agents.last_name',
			DB::raw('DATE_ADD(sales_orders.invoice_date, INTERVAL sales_orders.terms DAY) as due_date'),
			DB::raw('CONCAT(account_managers.first_name," ",account_managers.last_name) as manager'),
			DB::raw('CONCAT(agents.first_name," ",agents.last_name) as agent')
			)
			->whereRaw("((".$sales_order_id."=0) OR (sales_orders.id=".$sales_order_id."))")
			->whereRaw("((".$client_id."=0) OR (sales_orders.client_id=".$client_id."))")
			->whereRaw("((".$account_manager_id."=0) OR (sales_orders.account_manager_id=".$account_manager_id."))")
			->whereRaw("((".$branch_id."=0) OR (sales_orders.branch_id=".$branch_id."))")
			->whereRaw("((	'".$client_type."'='-1') OR (clients.personal='".$client_type."'))")
			->whereRaw("(('".$from_date."'='0' && '".$to_date."'='0') OR (sales_orders.invoice_date BETWEEN '".$from_date."' AND '".$to_date."'))")
			->Where(function ($query) use ($request) {
				$query->where('clients.client_name','like', '%'.$request->filter.'%')
				->orWhere('sales_orders.so','like', '%'.$request->filter.'%')
				->orWhere('sales_orders.po','like', '%'.$request->filter.'%');
			})
			->groupBy('sales_orders.id')
			->orderBy('sales_orders.created_at','desc');

		$sales_order_all = $query->get();
		$sales_order_array=json_encode($query->get());
		$data = json_decode($sales_order_array,true);
		return Excel::create('salesorder', function($excel) use ($data) {
			$excel->sheet('salesorder', function($sheet) use ($data)
			{
				$sheet->fromArray($data, null, 'A7', false, true);	

				$sheet->setCellValue('A5',session('company_address'));
				$sheet->mergeCells('A5:E5');
			});
		})->download($type);
	}


	public function reportSalesInvoiceGet(Request $request)
	{
		Log::info('asd	');
		Log::info($request);
		DB::enableQueryLog();
		$client_id = ($request->input('client_id') == null ? 0 : $request->input('client_id'));
		$branch_id = ($request->input('branch_id') == null ? 0 : $request->input('branch_id'));
		$status = ($request->input('status') == null ? 0 : $request->input('status'));
		$pay_type = ($request->input('pay_type') == null ? 0 : $request->input('pay_type'));
		$from_date = ($request->input('from_date') == null ? 0 : $request->input('from_date'));
		$to_date = ($request->input('to_date') == null ? 0 : $request->input('to_date'));
		$client_type = ($request->input('client_type') == null ? -1 : $request->input('client_type'));
		$account_manager_id = ($request->input('account_manager_id') == null ? 0 : $request->input('account_manager_id'));
		$query = DB::table('sales_invoice')
			->join('clients', 'clients.id', '=', 'sales_invoice.client_id')
			->leftjoin('account_managers', 'account_managers.id', '=', 'sales_invoice.account_manager_id')
			->leftJoin('cash_payments', 'cash_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('check_payments', 'check_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('payments', 'payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM cash_payments GROUP BY sales_invoice_id) AS cp"),function($join){
					$join->on("cp.sales_invoice_id","=","sales_invoice.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM check_payments  GROUP BY sales_invoice_id) AS chk"),function($join){
					$join->on("chk.sales_invoice_id","=","sales_invoice.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM deposit_payments  GROUP BY sales_invoice_id) AS dp"),function($join){
					$join->on("dp.sales_invoice_id","=","sales_invoice.id");
			})
			->select(
				'sales_invoice.id',
					'sales_invoice.invoice_date',
					'sales_invoice.si',
					'sales_invoice.po',
					'sales_invoice.pay_type',
					'sales_invoice.client_id',
					'sales_invoice.branch_id',
					'sales_invoice.account_manager_id',
					'sales_invoice.tax_rate',
					'sales_invoice.terms',
					'sales_invoice.discount',
					DB::raw('CASE 
						WHEN sales_invoice.status="cancelled"
						then "cancelled"

						WHEN 
							(sales_invoice.amount_total-sales_invoice.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id AND p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id AND p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id AND p.approved=1)
						)
						 AND
						 (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id AND p.approved=0)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id AND p.approved=0)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id AND p.approved=0)
						) > 0 
						AND sales_invoice.status != "cancelled"
						THEN "pending"

						WHEN 
						(sales_invoice.amount_total-sales_invoice.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
						)
						 AND (DATE_ADD(invoice_date, INTERVAL sales_invoice.terms DAY)<NOW()) AND 
						 (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id )+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
						) = 0
						AND sales_invoice.status != "cancelled"
						THEN "over-due"
						WHEN 
						((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)-(sales_invoice.amount_total))=0
						AND sales_invoice.status != "cancelled"

						THEN "paid"
		
						WHEN 
						((sales_invoice.amount_total) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						) AND 0!=(
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						))
						 AND (DATE_ADD(invoice_date, INTERVAL sales_invoice.terms DAY)>NOW())
						 AND sales_invoice.status != "cancelled"

						THEN "partially-paid"
						WHEN 
						((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1))=0 AND (DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY)>NOW())
						AND sales_invoice.status != "cancelled"
						 THEN "unpaid"
						
						ELSE sales_invoice.status END AS status'),
					'sales_invoice.amount_total',
					// 'sales_invoice.tax_amount',
					DB::raw('IF (clients.vatable=0,0,sales_invoice.tax_amount) tax_amount'),
					'clients.client_name',
					'clients.email',
					'clients.vatable',
					'clients.tin',
					'clients.credit_limit',
					'clients.personal',
					DB::raw('(SELECT branch_name FROM client_branches WHERE id=sales_invoice.branch_id) branch_name'),
					'account_managers.first_name',
					'account_managers.last_name',
					DB::raw('cp.amount as cash_payment'),
					DB::raw('chk.amount as check_payment'),
					DB::raw('DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) as due_date'),
					DB::raw('(SELECT SUM(less) less FROM payments WHERE sales_invoice_id=sales_invoice.id) less'),				
					DB::raw('(sales_invoice.amount_total-(IFNULL(cp.amount,0)+IFNULL(dp.amount,0)+IFNULL(chk.amount,0)+IFNULL((SELECT SUM(less) less FROM payments WHERE sales_invoice_id=sales_invoice.id),0))) as balance'),
					DB::raw('CONCAT(account_managers.first_name," ",account_managers.last_name) as manager')		
				)
			// ->where('sales_invoice.client_id','=',$request->input('client_id'))
			->where('archived','=',0)
			->whereRaw("((".$client_id."=0) OR (sales_invoice.client_id=".$client_id."))")
			->whereRaw("((".$account_manager_id."=0) OR (sales_invoice.account_manager_id=".$account_manager_id."))")
			->whereRaw("((".$branch_id."=0) OR (sales_invoice.branch_id=".$branch_id."))")
			->whereRaw("(
					CASE 
						WHEN '".$status."'='cancelled' THEN
							sales_invoice.status='cancelled'

					
						WHEN '".$status."'='over-due' THEN 
							(sales_invoice.amount_total-sales_invoice.discount) != (
							(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id)+
							(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
							(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
							)
							 AND (DATE_ADD(invoice_date, INTERVAL sales_invoice.terms DAY)<NOW()) AND 
							 (
							(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id )+
							(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
							(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
							) = 0
						WHEN '".$status."'='paid' THEN 
							((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
							(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
							(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)-(sales_invoice.amount_total))=0
						WHEN '".$status."'='partially-paid' THEN 
								
							((sales_invoice.amount_total) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						) AND 0!=(
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						))
						 AND (DATE_ADD(invoice_date, INTERVAL sales_invoice.terms DAY)>NOW())
						 AND sales_invoice.status != 'cancelled'

						WHEN '".$status."'='unpaid' THEN 
							((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
							(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
							(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1))=0 AND (DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY)>NOW())
						WHEN '".$status."'='pending' THEN 
							(sales_invoice.amount_total-sales_invoice.discount) != (
							(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id AND p.approved=1)+
							(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id AND p.approved=1)+
							(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id AND p.approved=1)
							)
							 AND
							 (
							(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id AND p.approved=0)+
							(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id AND p.approved=0)+
							(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id AND p.approved=0)
							) > 0 
						WHEN '".$status."'='0' THEN

						'0'='0'
						END )")
			->whereRaw("((	'".$pay_type."'='0') OR (sales_invoice.pay_type='".$pay_type."'))")
			->whereRaw("((	'".$client_type."'='-1') OR (clients.personal='".$client_type."'))")
			->whereRaw("(('".$from_date."'='0' && '".$to_date."'='0') OR (sales_invoice.invoice_date BETWEEN '".$from_date."' AND '".$to_date."'))")
			->Where(function ($query) use ($request) {
				$query->where('clients.client_name','like', '%'.$request->filter.'%')
				->orWhere('sales_invoice.si','like', '%'.$request->filter.'%')
				->orWhere('sales_invoice.po','like', '%'.$request->filter.'%');
			})
			->orderBy('sales_invoice.created_at','desc');

		$sales_invoice_all = $query->get();

		$sales_invoice = $query
			->paginate(10);
		Log::info(DB::getQueryLog());
		$sales_invoice_array=json_encode($query->get());
		return view('reports.sales_invoice',[
				'sales_invoice'=>$sales_invoice,
				'sales_invoice_array'=>$sales_invoice_array,
				'sales_invoice_all'=>$sales_invoice_all
				]);
	}
	public function reportSalesInvoiceExport(Request $request,$type)
	{
		$client_id = ($request->input('client_id') == null ? 0 : $request->input('client_id'));
		$branch_id = ($request->input('branch_id') == null ? 0 : $request->input('branch_id'));
		$status = ($request->input('status') == null ? 0 : $request->input('status'));
		$pay_type = ($request->input('pay_type') == null ? 0 : $request->input('pay_type'));
		$from_date = ($request->input('from_date') == null ? 0 : $request->input('from_date'));
		$to_date = ($request->input('to_date') == null ? 0 : $request->input('to_date'));
		$client_type = ($request->input('client_type') == null ? -1 : $request->input('client_type'));
		$account_manager_id = ($request->input('account_manager_id') == null ? 0 : $request->input('account_manager_id'));

		$query = DB::table('sales_invoice')
			->join('clients', 'clients.id', '=', 'sales_invoice.client_id')
			->leftjoin('account_managers', 'account_managers.id', '=', 'clients.account_manager_id')
			->leftJoin('cash_payments', 'cash_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('check_payments', 'check_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('payments', 'payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM cash_payments GROUP BY sales_invoice_id) AS cp"),function($join){
					$join->on("cp.sales_invoice_id","=","sales_invoice.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM check_payments  GROUP BY sales_invoice_id) AS chk"),function($join){
					$join->on("chk.sales_invoice_id","=","sales_invoice.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM deposit_payments  GROUP BY sales_invoice_id) AS dp"),function($join){
					$join->on("dp.sales_invoice_id","=","sales_invoice.id");
			})
			->select(
				'sales_invoice.id',
					'sales_invoice.invoice_date',
					'sales_invoice.si',
					'sales_invoice.po',
					'sales_invoice.pay_type',
					'sales_invoice.client_id',
					'sales_invoice.branch_id',
					'sales_invoice.account_manager_id',
					'sales_invoice.tax_rate',
					'sales_invoice.terms',
					DB::raw('sales_invoice.discount as "Discount"'),
					DB::raw('CASE 
						WHEN sales_invoice.status="cancelled"
						then "cancelled"

						WHEN 
							(sales_invoice.amount_total-sales_invoice.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id AND p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id AND p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id AND p.approved=1)
						)
						 AND
						 (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id AND p.approved=0)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id AND p.approved=0)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id AND p.approved=0)
						) > 0 
						AND sales_invoice.status != "cancelled"
						THEN "pending"

						WHEN 
						(sales_invoice.amount_total-sales_invoice.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
						)
						 AND (DATE_ADD(invoice_date, INTERVAL sales_invoice.terms DAY)<NOW()) AND 
						 (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id )+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
						) = 0
						AND sales_invoice.status != "cancelled"
						THEN "over-due"
						WHEN 
						((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)-(sales_invoice.amount_total))=0
						AND sales_invoice.status != "cancelled"

						THEN "paid"
		
						WHEN 
						((sales_invoice.amount_total) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						) AND 0!=(
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						))
						 AND (DATE_ADD(invoice_date, INTERVAL sales_invoice.terms DAY)>NOW())
						 AND sales_invoice.status != "cancelled"

						THEN "partially-paid"
						WHEN 
						((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1))=0 AND (DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY)>NOW())
						AND sales_invoice.status != "cancelled"
						 THEN "unpaid"
						
						ELSE sales_invoice.status END AS status'),
					DB::raw('sales_invoice.amount_total as "Sales Amount"'),
					DB::raw("IF (clients.vatable=0,0,sales_invoice.tax_amount) AS 'Tax AMT'"),
					'clients.client_name',
					'clients.email',
					'clients.vatable',
					'clients.tin',
					'clients.credit_limit',
					'clients.personal',
					DB::raw('(SELECT branch_name FROM client_branches WHERE id=sales_invoice.branch_id) branch_name'),
					'account_managers.first_name',
					'account_managers.last_name',
					DB::raw('cp.amount as cash_payment'),
					DB::raw('chk.amount as check_payment'),
					DB::raw('DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) as due_date'),
					DB::raw('(SELECT SUM(less) less FROM payments WHERE sales_invoice_id=sales_invoice.id) less'),				
					DB::raw('sales_invoice.amount_total-
						(
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						) as "Balance"'),
					DB::raw('(
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						) as "Amount Paid"'),
					DB::raw('CONCAT(account_managers.first_name," ",account_managers.last_name) as manager')		
				)
			// ->where('sales_invoice.client_id','=',$request->input('client_id'))
			->where('archived','=',0)
			->where('sales_invoice.status','!=','cancelled')
			->whereRaw("((".$client_id."=0) OR (sales_invoice.client_id=".$client_id."))")
			->whereRaw("((".$account_manager_id."=0) OR (sales_invoice.account_manager_id=".$account_manager_id."))")
			->whereRaw("((".$branch_id."=0) OR (sales_invoice.branch_id=".$branch_id."))")
			->whereRaw("(
					CASE 
						WHEN '".$status."'='cancelled' THEN
							sales_invoice.status='cancelled'

						WHEN '".$status."'='pending' THEN 
							(sales_invoice.amount_total-sales_invoice.discount) != (
							(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id AND p.approved=1)+
							(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id AND p.approved=1)+
							(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id AND p.approved=1)
							)
							 AND
							 (
							(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id AND p.approved=0)+
							(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id AND p.approved=0)+
							(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id AND p.approved=0)
							) > 0 
						WHEN '".$status."'='over-due' THEN 
							(sales_invoice.amount_total-sales_invoice.discount) != (
							(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id)+
							(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
							(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
							)
							 AND (DATE_ADD(invoice_date, INTERVAL sales_invoice.terms DAY)<NOW()) AND 
							 (
							(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id )+
							(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
							(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
							) = 0
						WHEN '".$status."'='paid' THEN 
							((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
							(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
							(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)-(sales_invoice.amount_total))=0
						WHEN '".$status."'='partially-paid' THEN 
								
							((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
							(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
							(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
							) > 0 AND ((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
							(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
							(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)-(sales_invoice.amount_total))!=0
						WHEN '".$status."'='unpaid' THEN 
							((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
							(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
							(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1))=0 AND (DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY)>NOW())
						WHEN '".$status."'='0' THEN

						'0'='0'
						END )")
			->whereRaw("((	'".$pay_type."'='0') OR (sales_invoice.pay_type='".$pay_type."'))")
			->whereRaw("((	'".$client_type."'='-1') OR (clients.personal='".$client_type."'))")
			->whereRaw("(('".$from_date."'='0' && '".$to_date."'='0') OR (sales_invoice.invoice_date BETWEEN '".$from_date."' AND '".$to_date."'))")
			->Where(function ($query) use ($request) {
				$query->where('clients.client_name','like', '%'.$request->filter.'%')
				->orWhere('sales_invoice.si','like', '%'.$request->filter.'%')
				->orWhere('sales_invoice.po','like', '%'.$request->filter.'%');
			})
			->orderBy('sales_invoice.created_at','desc');

		$sales_invoice_all = $query->get();	
		$sales_invoice_array=json_encode($sales_invoice_all);
	

		$data = json_decode($sales_invoice_array,true);	
		$filename=$status;
		if($status=='0')
			$filename='invoice';

		return Excel::create($filename, function($excel) use ($data) {
			$excel->sheet('invoice', function($sheet) use ($data)
			{
				
				foreach ($data as $key => $field)
				{
					$data[$key]['Sales Amount'] = (float)$data[$key]['Sales Amount'];					
					$data[$key]['Tax AMT'] = (float)$data[$key]['Tax AMT'];
					$data[$key]['Balance'] = (float)$data[$key]['Balance'];
					$data[$key]['Discount'] = (float)$data[$key]['Discount'];
					$data[$key]['Amount Paid'] = (float)$data[$key]['Amount Paid'];
				}

				$sheet->cells('A7:O7', function($cells) {
				   $cells->setFontWeight('bold');
				});
				$arrayHeight=[];
				for($k = 0;$k<count($data)+7+1;$k++)
				{
					$arrayHeight[$k+1] = 20;
				}
				$sheet->setHeight($arrayHeight);
				$sheet->fromArray($data, null, 'A7', true, true);	
				$sheet->setBorder('A7:O'.(count($data)+7+1), 'thin');			
				$sheet->setCellValue('F'.(count($data)+7+1),'=SUM(F8:F'.(count($data)+7).')');
				$sheet->setCellValue('G'.(count($data)+7+1),'=SUM(G8:G'.(count($data)+7).')');
				$sheet->setCellValue('H'.(count($data)+7+1),'=SUM(H8:H'.(count($data)+7).')');
				$sheet->setCellValue('I'.(count($data)+7+1),'=SUM(I8:I'.(count($data)+7).')');
				$sheet->setCellValue('J'.(count($data)+7+1),'=SUM(J8:J'.(count($data)+7).')');

				$sheet->setColumnFormat(array(
					    'A7:O'.(count($data)+7+1) => \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
					));

				$sheet->setCellValue('A5',session('company_address'));
				$sheet->mergeCells('A5:E5');
			});
		})->download($type);
	}


	public function printSalesInvoice(Request $request)
	{
		$client_id = ($request->input('client_id') == null ? 0 : $request->input('client_id'));
		$branch_id = ($request->input('branch_id') == null ? 0 : $request->input('branch_id'));
		$status = ($request->input('status') == null ? 0 : $request->input('status'));
		$pay_type = ($request->input('pay_type') == null ? 0 : $request->input('pay_type'));
		$from_date = ($request->input('from_date') == null ? 0 : $request->input('from_date'));
		$to_date = ($request->input('to_date') == null ? 0 : $request->input('to_date'));
		$client_type = ($request->input('client_type') == null ? -1 : $request->input('client_type'));
		$account_manager_id = ($request->input('account_manager_id') == null ? 0 : $request->input('account_manager_id'));


		$query = DB::table('sales_invoice')
			->join('clients', 'clients.id', '=', 'sales_invoice.client_id')
			->leftjoin('account_managers', 'account_managers.id', '=', 'clients.account_manager_id')
			->leftJoin('cash_payments', 'cash_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('check_payments', 'check_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('payments', 'payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM cash_payments GROUP BY sales_invoice_id) AS cp"),function($join){
					$join->on("cp.sales_invoice_id","=","sales_invoice.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM check_payments  GROUP BY sales_invoice_id) AS chk"),function($join){
					$join->on("chk.sales_invoice_id","=","sales_invoice.id");
			})
			->select(
				'sales_invoice.id',
				'sales_invoice.invoice_date',
				'sales_invoice.si',
				'sales_invoice.po',
				'sales_invoice.pay_type',
				DB::raw("IF (clients.vatable=0,0,sales_invoice.tax_amount) AS 'tax_amount'"),
				
				DB::raw('sales_invoice.discount as "discount"'),
				
				DB::raw('sales_invoice.amount_total as "amount_total"'),
				DB::raw('sales_invoice.amount_total-
						(
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						) as "balance"'),
				DB::raw('(
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						) as "Amount Paid"'),
				'sales_invoice.terms',
				DB::raw('CASE 
			
						WHEN 
						(sales_invoice.amount_total-sales_invoice.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
						)
						 AND (DATE_ADD(invoice_date, INTERVAL sales_invoice.terms DAY)<NOW()) AND 
						 (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
						) = 0
						THEN "over-due"
						WHEN 
						(sales_invoice.amount_total-sales_invoice.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id AND p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id AND p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id AND p.approved=1)
						)
						 AND
						 (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id )+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
						) > 0 
						THEN "pending"
						WHEN 
						((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)-(sales_invoice.amount_total-sales_invoice.discount))=0

						THEN "paid"
		
						WHEN 
						((sales_invoice.amount_total) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						) AND 0!=(
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						))
						 AND (DATE_ADD(invoice_date, INTERVAL sales_invoice.terms DAY)>NOW())

						THEN "partially-paid"
						WHEN 
						((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1))=0 AND (DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY)>NOW())

						 THEN "unpaid"
						
						ELSE sales_invoice.status END AS status'),
				'clients.client_name',
				DB::raw('(SELECT branch_name FROM client_branches where id=sales_invoice.branch_id) as branch_name'),
				'clients.email',
				'clients.tin',
				'clients.personal',
				'account_managers.first_name',
				'account_managers.last_name',
				DB::raw('DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) as due_date')
				)
			->whereRaw("((".$client_id."=0) OR (sales_invoice.client_id=".$client_id."))")
			->whereRaw("((".$account_manager_id."=0) OR (sales_invoice.account_manager_id=".$account_manager_id."))")
			->whereRaw("((".$branch_id."=0) OR (sales_invoice.branch_id=".$branch_id."))")
			->whereRaw("(
					CASE 
					WHEN '".$status."'='paid' THEN ((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)-(sales_invoice.amount_total-sales_invoice.discount))=0
					WHEN '".$status."'='partially-paid' THEN ((sales_invoice.amount_total-sales_invoice.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						) AND 0!=(
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1)
						))
						 AND (DATE_ADD(invoice_date, INTERVAL sales_invoice.terms DAY)>NOW())
					WHEN '".$status."'='cancelled' THEN sales_invoice.status='cancelled'
					WHEN '".$status."'='unpaid' THEN ((SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id and p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id and p.approved=1))=0 AND (DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY)>NOW())
						WHEN '".$status."'='all' THEN

						'0'='0'
					WHEN '".$status."'='over-due' THEN 
						(sales_invoice.amount_total-sales_invoice.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
						)
						 AND (DATE_ADD(invoice_date, INTERVAL sales_invoice.terms DAY)<NOW()) AND 
						 (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
						) = 0
					
					WHEN '".$status."'='pending' THEN 
						(sales_invoice.amount_total-sales_invoice.discount) != (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id AND p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id AND p.approved=1)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id AND p.approved=1)
						)
						 AND
						 (
						(SELECT IFNULL(SUM(amount),0) FROM cash_payments cp LEFT JOIN payments p ON p.cash_payment_id=cp.id WHERE cp.sales_invoice_id=sales_invoice.id )+
						(SELECT IFNULL(SUM(amount),0) FROM check_payments chk LEFT JOIN payments p ON p.check_payment_id=chk.id WHERE chk.sales_invoice_id=sales_invoice.id)+
						(SELECT IFNULL(SUM(amount),0) FROM deposit_payments dp LEFT JOIN payments p ON p.deposit_payment_id=dp.id WHERE dp.sales_invoice_id=sales_invoice.id)
						) > 0 
					
					WHEN '".$status."'='partially-paid' THEN ((IFNULL(cp.amount,0)+IFNULL(chk.amount,0))>0 && sales_invoice.status='unpaid' && (DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) > NOW())) 
					ELSE
						'".$status."'='0'
					END )")
			->whereRaw("((	'".$pay_type."'='0') OR (sales_invoice.pay_type='".$pay_type."'))")
			->whereRaw("((	'".$client_type."'='-1') OR (clients.personal='".$client_type."'))")
			->whereRaw("(('".$from_date."'='0' && '".$to_date."'='0') OR (sales_invoice.invoice_date BETWEEN '".$from_date."' AND '".$to_date."'))")
			->Where(function ($query) use ($request) {
				$query->where('clients.client_name','like', '%'.$request->filter.'%')
				->orWhere('sales_invoice.si','like', '%'.$request->filter.'%')
				->orWhere('sales_invoice.po','like', '%'.$request->filter.'%');
			})
			->where('archived','=',0)
			->groupBy('sales_invoice.id')
			->orderBy('sales_invoice.created_at','desc');

		$sales_invoice_all = $query->get();
	
		$sales_invoice_array=json_encode($query->get());

		return view('reports.sales_invoice_print',[
				'sales_invoice_all'=>$sales_invoice_all,
				]);
	}

	
	public function printProducts(Request $request)
	{
		$product_id = ($request->input('product_id') == null ? 0 : $request->input('product_id'));
		$type_id = ($request->input('type_id') == null ? 0 : $request->input('type_id'));
		$category_id = ($request->input('category_id') == null ? 0 : $request->input('category_id'));

		$query = DB::table('products')
			->select(
				'products.*',
				DB::raw('(SELECT IFNULL(SUM(qty),0) FROM transaction_items WHERE product_id=products.id) AS sold')
				)
			->whereRaw("((".$product_id."=0) OR (products.id=".$product_id."))")
			->whereRaw("((".$type_id."=0) OR (products.product_types_id=".$type_id."))")
			->whereRaw("((".$category_id."=0) OR (products.product_categories_id=".$category_id."))")
			
			->Where(function ($query) use ($request) {
				$query->where('products.prod_name','like', '%'.$request->filter.'%')
				->orWhere('products.prod_code','like', '%'.$request->filter.'%')
				->orWhere('products.prod_description','like', '%'.$request->filter.'%');
			})
			->groupBy('products.id')
			->orderBy('products.created_at','desc');
		$products_all = $query->get();

		return view('reports.products_print',['products_all'=>$products_all]);
	}


	public function printPurchase(Request $request)
	{
		$purchase_orders_id = ($request->input('purchase_orders_id') == null ? 0 : $request->input('purchase_orders_id'));

		$query = DB::table('purchase_orders')
			->leftjoin('suppliers','suppliers.id','=','purchase_orders.suppliers_id')
			->select(
				'purchase_orders.*',
				'suppliers.supplier_name'
				)
			->whereRaw("((".$purchase_orders_id."=0) OR (purchase_orders.id=".$purchase_orders_id."))")		
			->Where(function ($query) use ($request) {
				$query->where('suppliers.supplier_name','like', '%'.$request->filter.'%')
				->orWhere('purchase_orders.dr','like', '%'.$request->filter.'%')
				->orWhere('purchase_orders.po','like', '%'.$request->filter.'%');
			})
			->orderBy('purchase_orders.created_at','desc');
		$products_all = $query->get();
		return view('reports.purchase_print',['products_all'=>$products_all]);
	}

	public function printOrders(Request $request)
	{
		$sales_order_id = ($request->input('sales_orders_id') == null ? 0 : $request->input('sales_orders_id'));
		$client_id = ($request->input('client_id') == null ? 0 : $request->input('client_id'));
		$branch_id = ($request->input('branch_id') == null ? 0 : $request->input('branch_id'));
		$status = ($request->input('status') == null ? 0 : $request->input('status'));
		$pay_type = ($request->input('pay_type') == null ? 0 : $request->input('pay_type'));
		$from_date = ($request->input('from_date') == null ? 0 : $request->input('from_date'));
		$to_date = ($request->input('to_date') == null ? 0 : $request->input('to_date'));
		$client_type = ($request->input('client_type') == null ? -1 : $request->input('client_type'));
		$account_manager_id = ($request->input('account_manager_id') == null ? 0 : $request->input('account_manager_id'));

		$query = DB::table('sales_orders')
		->join('clients', 'clients.id', '=', 'sales_orders.client_id')
		->leftJoin(DB::raw("(SELECT id,first_name,last_name,middle_name FROM agents) AS agents"),function($join){
				$join->on("agents.id","=","sales_orders.agent_id");
		})
		->leftJoin(DB::raw("(SELECT id,first_name,last_name,middle_name FROM account_managers) AS account_managers"),function($join){
				$join->on("account_managers.id","=","sales_orders.account_manager_id");
		})
		->leftJoin(DB::raw("(SELECT branch_name,id FROM client_branches) AS branch_name"),function($join){
				$join->on("branch_name.id","=","sales_orders.branch_id");
		})
		->select(
			'sales_orders.id',
			'sales_orders.invoice_date',
			'sales_orders.so',
			'sales_orders.po',
			'sales_orders.client_id',
			'sales_orders.branch_id',
			'sales_orders.account_manager_id',
			'sales_orders.tax_rate',
			'sales_orders.agent_id',
			'sales_orders.amount_total',
			'sales_orders.tax_amount',
			'sales_orders.status',
			'sales_orders.archived',
			'sales_orders.terms',
			DB::raw('IF (sales_orders.approved="1","approved","not-approved") approved'),
			DB::raw('IF (clients.vatable>0,0,sales_orders.tax_amount) tax_amount'),
			'clients.client_name',
			'clients.vatable',
			'clients.personal',
			'branch_name.branch_name',
			'account_managers.first_name',
			'account_managers.last_name',
			'agents.first_name',
			'agents.last_name',
			DB::raw('DATE_ADD(sales_orders.invoice_date, INTERVAL sales_orders.terms DAY) as due_date'),
			DB::raw('CONCAT(account_managers.first_name," ",account_managers.last_name) as manager'),
			DB::raw('CONCAT(agents.first_name," ",agents.last_name) as agent')
			)
			->whereRaw("((".$sales_order_id."=0) OR (sales_orders.id=".$sales_order_id."))")
			->whereRaw("((".$client_id."=0) OR (sales_orders.client_id=".$client_id."))")
			->whereRaw("((".$account_manager_id."=0) OR (sales_orders.account_manager_id=".$account_manager_id."))")
			->whereRaw("((".$branch_id."=0) OR (sales_orders.branch_id=".$branch_id."))")
			->whereRaw("((	'".$client_type."'='-1') OR (clients.personal='".$client_type."'))")
			->whereRaw("(('".$from_date."'='0' && '".$to_date."'='0') OR (sales_orders.invoice_date BETWEEN '".$from_date."' AND '".$to_date."'))")
			->Where(function ($query) use ($request) {
				$query->where('clients.client_name','like', '%'.$request->filter.'%')
				->orWhere('sales_orders.so','like', '%'.$request->filter.'%')
				->orWhere('sales_orders.po','like', '%'.$request->filter.'%');
			})
			->groupBy('sales_orders.id')
			->orderBy('sales_orders.created_at','desc');
		$sales_order_all = $query->get();
		$sales_orders_array=json_encode($query->get());

		return view('reports.sales_order_print',['sales_order_all'=>$sales_order_all]);
	}
	

	public function printTransaction(Request $request)
	{
		$purchase_orders_id = ($request->input('purchase_orders_id') == null ? 0 : $request->input('purchase_orders_id'));

		$query = DB::table('transaction_items')
			->leftjoin('products','products.id','=','transaction_items.product_id')
			->select(
				'transaction_items.id',
				'transaction_items.price',
				DB::raw('IF(transaction_items.purchase_orders_id is null,"Out","In") as stock'),
				'transaction_items.qty',
				DB::raw('transaction_items.created_at'),
				DB::raw('(transaction_items.qty * transaction_items.price) as amount'),
				'products.prod_name'
			)
			->whereRaw("((".$purchase_orders_id."=0) OR (transaction_items.purchase_orders_id=".$purchase_orders_id."))")		
			->Where(function ($query) use ($request) {
				$query->where('products.prod_name','like', '%'.$request->filter.'%')
				->orWhere('transaction_items.created_at','like', '%'.$request->filter);
			})
			->orderBy('transaction_items.created_at','desc');
		$transaction_all = $query->get();
		$transaction = $query;

		return view('reports.transaction_print',['transaction_all'=>$transaction_all]);
	}

	public function printAgent(Request $request,$id)
	{
		if($id==1){
			$data1 = ($request->input('daily') == null ? 0 : $request->input('daily'));
			$query = DB::table('transaction_items')
				->leftjoin('sales_invoice','sales_invoice.id','=','transaction_items.sales_invoice_id')
				->leftjoin('clients','clients.id','=','sales_invoice.client_id')
				->select(
					'transaction_items.id',
					'transaction_items.price',
					'clients.client_name'
				)
				->whereRaw("((".$data1."=0) OR (DATE(transaction_items.created_at)='".$data1."'))")
				->whereNotNull('transaction_items.sales_invoice_id')
				->distinct()
				->orderBy('transaction_items.created_at','desc');
		}
		else if($id==2){
			$monthly = ($request->input('monthly') == null ? 0 : $request->input('monthly'));
			$year = ($request->input('year') == null ? 0 : $request->input('year'));
			$query = DB::table('transaction_items')
				->leftjoin('sales_invoice','sales_invoice.id','=','transaction_items.sales_invoice_id')
				->leftjoin('clients','clients.id','=','sales_invoice.client_id')
				->select(
					'transaction_items.id',
					'transaction_items.price',
					'clients.client_name'
				)
				->whereRaw("((".$monthly."=0) OR (MONTH(transaction_items.created_at)='".$monthly."'))")
				->whereRaw("((".$year."= 0) OR (Year(transaction_items.created_at)='".$year."'))")
				->whereNotNull('transaction_items.sales_invoice_id')
				->distinct()
				->orderBy('transaction_items.created_at','desc');
		}
		else if($id==3){
			$year = ($request->input('yearly') == null ? 0 : $request->input('yearly'));
			$query = DB::table('transaction_items')
				->select(
					DB::raw('monthname(created_at) as monthly'),
					DB::raw('year(created_at) as yearly'),
					DB::raw('(Select sum(price) from transaction_items where monthname(created_at) = monthly and year(created_at)=yearly) as sum')
					
				)
				->whereRaw("((".$year."= 0) OR (Year(transaction_items.created_at)='".$year."'))")
				->whereNotNull('sales_invoice_id')
				->distinct()
				->orderBy('created_at','desc');
				$agent_all = $query->get();
				$agent = $query
					->paginate(10);
					// dd($agent);

		
				$agent_array=json_encode($query->get());
				return view('reports.agents_print',[
					'agent'=>$agent,
					'agent_array'=>$agent_array,
					'agent_all'=>$agent_all,
					'yearly'=>'trot'
					]);
		}

		
		$agent_all = $query->get();
		$agent = $query
			->paginate(10);
		
		$agent_array=json_encode($query->get());

		return view('reports.agents_print',[
				'agent'=>$agent,
				'agent_array'=>$agent_array,
				'agent_all'=>$agent_all
				]);
	}

	public function printClients(Request $request)
	{
		$client_id = ($request->input('client_id') == null ? 0 : $request->input('client_id'));
		$branch_id = ($request->input('branch_id') == null ? 0 : $request->input('branch_id'));
		$from_date = ($request->input('from_date') == null ? 0 : $request->input('from_date'));
		$to_date = ($request->input('to_date') == null ? 0 : $request->input('to_date'));
		$personal = ($request->input('personal') == null ? 0 : $request->input('personal'));


		$query = DB::table('sales_invoice')
			->join('clients', 'clients.id', '=', 'sales_invoice.client_id')
			->leftJoin(DB::raw("(SELECT id,first_name,last_name,middle_name FROM account_managers) AS account_managers"),function($join){
					$join->on("account_managers.id","=","sales_invoice.account_manager_id");
			})
			->leftJoin('cash_payments', 'cash_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('check_payments', 'check_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM cash_payments GROUP BY sales_invoice_id) AS cp"),function($join){
					$join->on("cp.sales_invoice_id","=","sales_invoice.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM check_payments  GROUP BY sales_invoice_id) AS chk"),function($join){
					$join->on("chk.sales_invoice_id","=","sales_invoice.id");
			})
			->select(
				'sales_invoice.id',
				'sales_invoice.invoice_date',
				'sales_invoice.si',
				'sales_invoice.po',
				'sales_invoice.pay_type',
				'sales_invoice.client_id',
				'sales_invoice.branch_id',
				'sales_invoice.account_manager_id',
				'sales_invoice.tax_rate',
				'sales_invoice.terms',
				'sales_invoice.amount_total',
				'sales_invoice.status',
				DB::raw('IF (clients.vatable=0,0,sales_invoice.tax_amount) tax_amount'),
				'clients.client_name',
				'clients.email',
				'clients.vatable',
				'clients.tin',
				'clients.credit_limit',
				'clients.personal',
				DB::raw('(SELECT branch_name FROM client_branches WHERE id=sales_invoice.branch_id) branch_name'),
				DB::raw('CONCAT(account_managers.first_name," ",account_managers.last_name) account_manager'),
				DB::raw('cp.amount as cash_payment'),
				DB::raw('chk.amount as check_payment'),
				DB::raw('DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) as due_date')				
				)
			->whereRaw("((".$personal."=-1) OR (clients.personal=".$personal."))")
			->whereRaw("((".$client_id."=0) OR (sales_invoice.client_id=".$client_id."))")
			->whereRaw("((".$branch_id."=0) OR (sales_invoice.branch_id=".$branch_id."))")
			->whereRaw("(('".$from_date."'='0' && '".$to_date."'='0') OR (sales_invoice.invoice_date BETWEEN '".$from_date."' AND '".$to_date."'))")
			->where('sales_invoice.archived','=',0)
			->groupBy('sales_invoice.id')
			->orderBy('account_manager','desc');
		$clients_all = $query->get();
	
		return view('reports.clients_print',['clients_all'=>$clients_all]);
	}

	public function printPayments(Request $request)
	{

		$from_date = ($request->input('from_date') == null ? 0 : $request->input('from_date'));
		$to_date = ($request->input('to_date') == null ? 0 : $request->input('to_date'));
		$pay_type = ($request->input('pay_type') == null ? 0 : $request->input('pay_type'));
		$query = DB::table('payments')
				->join('sales_invoice', 'sales_invoice.id', '=', 'payments.sales_invoice_id')
				->select(
					'payments.id as payments_id',
					DB::raw('(SELECT client_name FROM clients WHERE id=sales_invoice.client_id) client_name'),
					DB::raw('(SELECT branch_name FROM client_branches WHERE id=sales_invoice.branch_id) branch_name'),
					'payments.approved',
					'payments.less',
					DB::raw('payments.created_at as payment_date'),
					'sales_invoice.id',
					'sales_invoice.si',
					DB::raw('CASE 
				WHEN payments.cash_payment_id IS NOT NULL THEN "cash" 
				WHEN payments.check_payment_id IS NOT NULL THEN "check" END pay_type'),
					DB::raw('COALESCE((SELECT amount FROM cash_payments WHERE id=payments.cash_payment_id),
				(SELECT amount FROM check_payments WHERE id=payments.check_payment_id),0) payment_amount'))
				->whereRaw('(cash_payment_id IS NOT NULL OR check_payment_id IS NOT NULL)')
				->whereRaw("(('".$from_date."'='0' AND '".$to_date."'='0') OR (payments.created_at BETWEEN '".$from_date."' AND '".$to_date."'))")
				->whereRaw('(("'.$pay_type.'"="0") OR (
					CASE WHEN "'.$pay_type.'"="cash" THEN payments.cash_payment_id IS NOT NULL
					 WHEN "'.$pay_type.'"="check" THEN payments.check_payment_id IS NOT NULL END
					))');

		$payments_all = $query->get();
	
		return view('reports.payments_print',['payments_all'=>$payments_all]);
	}

	public function printSalesInvoiceAdd($id)
	{


		$query = DB::table('sales_invoice')
			->join('clients', 'clients.id', '=', 'sales_invoice.client_id')
			->leftjoin('account_managers', 'account_managers.id', '=', 'clients.account_manager_id')
			->leftJoin('cash_payments', 'cash_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin('check_payments', 'check_payments.sales_invoice_id', '=', 'sales_invoice.id')
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM cash_payments GROUP BY sales_invoice_id) AS cp"),function($join){
					$join->on("cp.sales_invoice_id","=","sales_invoice.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,sales_invoice_id FROM check_payments  GROUP BY sales_invoice_id) AS chk"),function($join){
					$join->on("chk.sales_invoice_id","=","sales_invoice.id");
			})
			->select(
				'sales_invoice.id',
				'sales_invoice.invoice_date',
				'sales_invoice.si',
				'sales_invoice.po',
				'sales_invoice.pay_type',
				'sales_invoice.client_id',
				'sales_invoice.branch_id',
				'sales_invoice.account_manager_id',
				'sales_invoice.tax_rate',
				'sales_invoice.terms',
				'sales_invoice.discount',
				DB::raw('CASE 
					WHEN ((DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) < NOW()) && sales_invoice.status!="paid") THEN "over-due"
					WHEN sales_invoice.status="paid" THEN "paid"
					WHEN sales_invoice.status="cancelled" THEN "cancelled"
					WHEN ((IFNULL(cp.amount,0)+IFNULL(chk.amount,0))>0 && sales_invoice.status="unpaid" && (DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) > NOW()))  THEN "partially-paid"
					WHEN sales_invoice.status="unpaid" THEN "unpaid"
					
					ELSE sales_invoice.status END AS status'),
				'sales_invoice.amount_total',
				'sales_invoice.tax_amount',
				'clients.client_name',
				'clients.email',
				'clients.vatable',
				'clients.tin',
				'clients.credit_limit',
				'clients.personal',
				DB::raw('(SELECT branch_name FROM client_branches where id=sales_invoice.branch_id) as branch_name'),
				DB::raw('(SELECT address FROM client_branches where id=sales_invoice.branch_id) as address'),
				'account_managers.first_name',
				'account_managers.last_name',
				DB::raw('cp.amount as cash_payment'),
				DB::raw('chk.amount as check_payment'),
				DB::raw('DATE_ADD(sales_invoice.invoice_date, INTERVAL sales_invoice.terms DAY) as due_date'),
				DB::raw('(SELECT SUM(less) less FROM payments WHERE sales_invoice_id=sales_invoice.id) less'),				
				DB::raw('(sales_invoice.amount_total-(IFNULL(cp.amount,0)+IFNULL(chk.amount,0)+IFNULL((SELECT SUM(less) less FROM payments WHERE sales_invoice_id=sales_invoice.id),0))) as balance')
				)
			->where('sales_invoice.id',"=",$id);

		$sales_invoice = $query->get();

		$branch = ClientBranch::find($sales_invoice[0]->id);
		//Log::info($branch);
		$items = DB::table('transaction_items')
			->join('products', 'products.id', '=', 'transaction_items.product_id')
			->select(
					'transaction_items.qty as qty',
					'products.prod_name as prod_name',
					DB::raw('(SELECT price FROM transaction_items WHERE sales_invoice_id='.$id.' AND transaction_items.product_id=products.id GROUP BY price) prod_price')
					)
			->where('transaction_items.sales_invoice_id','=',$id)
			->get();

		return view('reports.sales_invoice_print_add',[
				'sales_invoice'=>$sales_invoice,'items'=>$items
				]);
	}

	public function printPurchaseOrderAdd($id)
	{


		$query = DB::table('purchase_orders')
			->join('suppliers', 'suppliers.id', '=', 'purchase_orders.suppliers_id')
			->leftJoin('cash_payments', 'cash_payments.purchase_orders_id', '=', 'purchase_orders.id')
			->leftJoin('check_payments', 'check_payments.purchase_orders_id', '=', 'purchase_orders.id')
			->leftJoin('deposit_payments', 'deposit_payments.purchase_orders_id', '=', 'purchase_orders.id')
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,purchase_orders_id FROM cash_payments GROUP BY purchase_orders_id) AS cp"),function($join){
					$join->on("cp.purchase_orders_id","=","purchase_orders.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,purchase_orders_id FROM check_payments  GROUP BY purchase_orders_id) AS chk"),function($join){
					$join->on("chk.purchase_orders_id","=","purchase_orders.id");
			})
			->leftJoin(DB::raw("(SELECT SUM(amount) amount,purchase_orders_id FROM deposit_payments  GROUP BY purchase_orders_id) AS dp"),function($join){
					$join->on("dp.purchase_orders_id","=","purchase_orders.id");
			})
			->select(
				'purchase_orders.id',
				'purchase_orders.order_date',
				'purchase_orders.dr',
				'purchase_orders.po',
				'purchase_orders.pay_type',
				'purchase_orders.suppliers_id',
				'purchase_orders.status',
				'purchase_orders.amount_total',
				'suppliers.supplier_name',
				'suppliers.suppliers_code',
				'suppliers.contact_person',
				'suppliers.address',
				DB::raw('cp.amount as cash_payment'),
				DB::raw('chk.amount as check_payment'),
				DB::raw('dp.amount as deposit_payment'),			
				DB::raw('(purchase_orders.amount_total-(IFNULL(cp.amount,0)+IFNULL(chk.amount,0)+IFNULL(dp.amount,0))) as balance')
				)
			->where('purchase_orders.id',"=",$id);

		$purchase_order = $query->get();

		$items = DB::table('transaction_items')
			->join('products', 'products.id', '=', 'transaction_items.product_id')
			->select(
					'transaction_items.qty as qty',
					'products.prod_name as prod_name',
					DB::raw('(SELECT price FROM transaction_items WHERE sales_invoice_id='.$id.' AND transaction_items.product_id=products.id GROUP BY price) prod_price')
					)
			->where('transaction_items.purchase_orders_id','=',$id)
			->get();

		return view('reports.purchase_order_print_add',[
				'purchase_order'=>$purchase_order,'items'=>$items
				]);
	}


//reports hris
	public function attendanceReport(Request $request){
		return view('reports.attendance');
	}

	public function exportExcelAttendance(){
        return redirect()->back()->with('flash_message','Exported in Excel!!');
    }

    public function exportCSVAttendance(){
        return redirect()->back()->with('flash_message','Exported in CSV!!');
    }

    public function exportPDFAttendance(){
        return redirect()->back()->with('flash_message','Exported in PDF!!');
    }

    public function employeeHeadcountReport(Request $request){
		return view('reports.employee_headcount');
	}

	public function exportExcelEmployeeHeadcount(){
        return redirect()->back()->with('flash_message','Exported in Excel!!');
    }

    public function exportCSVEmployeeHeadcount(){
        return redirect()->back()->with('flash_message','Exported in CSV!!');
    }

    public function exportPDFEmployeeHeadcount(){
        return redirect()->back()->with('flash_message','Exported in PDF!!');
    }

    public function payrollSummaryReport(Request $request){
		return view('reports.payroll_summary');
	}

	public function exportExcelPayrollSummary(){
        return redirect()->back()->with('flash_message','Exported in Excel!!');
    }

    public function exportCSVPayrollSummary(){
        return redirect()->back()->with('flash_message','Exported in CSV!!');
    }

    public function exportPDFPayrollSummary(){
        return redirect()->back()->with('flash_message','Exported in PDF!!');
    }
    public function employeeBenefitReport(Request $request){
		return view('reports.employee_benefit');
	}

	public function exportExcelEmployeeBenefit(){
        return redirect()->back()->with('flash_message','Exported in Excel!!');
    }

    public function exportCSVEmployeeBenefit(){
        return redirect()->back()->with('flash_message','Exported in CSV!!');
    }

    public function exportPDFEmployeeBenefit(){
        return redirect()->back()->with('flash_message','Exported in PDF!!');
    }
}
