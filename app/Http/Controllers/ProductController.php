<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\Product;
use Validator;
use Session;
use Redirect;
use Response;
use Log;
use View;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
class ProductController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{

		$products = DB::table('products')
			->whereNull('deleted_at')
			->paginate(10);
		return view('product.list_products',['products' => $products]);
	}

	public function productSearch(Request $request)
	{
		if(Input::get('sortby')!=null)
		{
			$val = explode(".",Input::get('sortby'));
			$products = DB::table('products')
			->whereNull('deleted_at')
			->Where(function ($query) use ($request) {
				$query->where('prod_name','like', '%'.$request->search.'%')
				->orWhere('prod_code','like', '%'.$request->search.'%');
			})
			->orderBy($val[0],$val[1])
			->paginate(10);
		}
		else
		{
			$products = DB::table('products')
			->whereNull('deleted_at')
			->Where(function ($query) use ($request) {
				$query->where('prod_name','like', '%'.$request->search.'%')
				->orWhere('prod_code','like', '%'.$request->search.'%');
			})
			->paginate(10);
		}
		return view('product.list_products',['products' => $products]);
	}
	public function productSetActive($id)
	{

		$p = Product::find($id);
		$p->status = ($p->status == 0) ? 1 : 0;
		$p->save();
		return back();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
		return view('product.add_product');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'prod_code' => 'required|unique:products|max:20|min:3',    
			'prod_name' => 'required|max:30|min:4',    
			'prod_description' => 'required|max:255|min:4', 
			'prod_price' => 'required', 
		]);

		if ($validator->fails()) {
			return redirect('/products/create')
						->withErrors($validator)
						->withInput();
		}
		$prod = Product::create([
			'prod_code'  =>  $request->input('prod_code'),
			'prod_name'  =>  $request->input('prod_name'),
			'prod_description'  =>  $request->input('prod_description'),
			'prod_price'  =>  $request->input('prod_price'),
			'prod_bar_code'  =>  $request->input('prod_bar_code'),
		]);

		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'created product :'.$request->prod_name,
			'comment'  =>  '',
			'family'  =>  'insert',
			'created_at' => \Carbon\Carbon::now()
			]);
		return redirect('/products/create')->with('flash_message', 'Product Added!!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
		$x = DB::table('products')
			->where('id','=',$id)
			->get();

		return View::make('product.edit_product')
			->with('product', $x);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
		$validator = Validator::make($request->all(), [
			'prod_code' => 'required|max:20|min:3',    
			'prod_name' => 'required|max:30|min:4',    
			'prod_description' => 'required|max:255|min:4',    
			'prod_price' => 'required',    
		   
		]);

		if ($validator->fails()) {
			return redirect('products/'.$id.'/edit')
						->withErrors($validator)
						->withInput();
		}

		$x = Product::find($id);
		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'edited product :'.$request->prod_name,
			'comment'  =>  '',
			'family'  =>  'update',
			'created_at' => \Carbon\Carbon::now()
			]);

		$x->prod_code = $request->input('prod_code');
		$x->prod_name = $request->input('prod_name');
		$x->prod_description = $request->input('prod_description');
		$x->prod_price = $request->input('prod_price');
		$x->prod_bar_code = $request->input('prod_bar_code');
		$x->save();               

		return redirect('products')->with('flash_message', 'Product Updated!!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//

		$x = Product::find($id);

		DB::table('activity_log')->insert([
			'username'  =>  Auth::user()->username.'@'.\Request::ip(),
			'entry'  =>  'deleted product :'.$x->prod_name,
			'comment'  =>  '',
			'family'  =>  'delete',
			'created_at' => \Carbon\Carbon::now()
			]);

		if ($x) {
			$x->status = 0;
			$x->save();
		}
		return redirect('products')->with('flash_message', 'Product Deleted!!');
	}


	public function productImport()
	{
		return view('product.import_product');
	}
	public function productImportCsv(Request $request)
	{
		$validator = Validator::make(Input::all(), [
			'import_file' => '|mimes:csv,txt',			
		]);
		if ($validator->fails()) {
			return redirect('/product/import')->with('flash_message', 'Invalid File Type');
		}

		try{
			if(Input::hasFile('import_file')){
				$path = Input::file('import_file')->getRealPath();
				$data = Excel::load($path, function($reader) {
				})->get();
				if(!empty($data) && $data->count()){
					foreach ($data as $key => $value) {
						$insert[] = [
							'prod_code' => $value->prod_code,
							'prod_name' => $value->prod_name,
							'prod_description' => $value->prod_description,						
							'prod_price' => $value->prod_price,
							'prod_bar_code' => $value->prod_bar_code,];
					}
					if(!empty($insert)){
						DB::table('products')->insert($insert);
					}
				}
			}
			else
			{
				return redirect('/product/import')->with('flash_error', 'No csv files attached');
			}
		}
		catch(\Exception $e){
			return redirect('/product/import')->with('flash_error', 'Error in uploading, Please check CSV');
		}

		return redirect('/product/import')->with('flash_message', 'Uploaded '.$data->count().' products');
	}
	public function productExport($type)
	{
		$data = Product::select('prod_code', 'prod_name','prod_description','prod_price','prod_bar_code')->get()->toArray();	
		return Excel::create('prod_code', function($excel) use ($data) {
			$excel->sheet('prod_code', function($sheet) use ($data)
			{
				$sheet->fromArray($data);
			});
		})->download($type);
	}
}
