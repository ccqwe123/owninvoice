<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Log;
use App\Variable;
use App\User;
use Session;
use Validator;
use Hash;
class SettingsController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
		$software_name = DB::table('var')
			->where('name','=','software_name')
			->first();
		$company_name = DB::table('var')
			->where('name','=','company_name')
			->first();
		$company_address = DB::table('var')
			->where('name','=','company_address')
			->first();
		$company_phone = DB::table('var')
			->where('name','=','company_phone')
			->first();
		$tax_rate = DB::table('var')
			->where('name','=','tax_rate')
			->first();
		$decimal = DB::table('var')
			->where('name','=','decimal')
			->first();
		$font_size = DB::table('var')
			->where('name','=','font_size')
			->first();
		$letter_head = DB::table('var')
			->where('name','=','letter_head')
			->first();
		$soa_footer = DB::table('var')
			->where('name','=','soa_footer')
			->first();
		$company_logo = DB::table('var')
			->where('name','=','company_logo')
			->first();
		$report_footer = DB::table('var')
			->where('name','=','report_footer')
			->first();
		
		return view('settings.settings',[
			'software_name'=>$software_name,
			'company_name'=>$company_name,
			'company_address'=>$company_address,
			'company_phone'=>$company_phone,
			'tax_rate'=>$tax_rate,
			'decimal'=>$decimal,
			'font_size'=>$font_size,
			'letter_head'=>$letter_head,
			'soa_footer'=>$soa_footer,
			'company_logo'=>$company_logo,
			'report_footer'=>$report_footer,
			]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit()
	{
		//
		$software_name = DB::table('var')
			->where('name','=','software_name')
			->first();
		$company_name = DB::table('var')
			->where('name','=','company_name')
			->first();
		$company_address = DB::table('var')
			->where('name','=','company_address')
			->first();
		$company_phone = DB::table('var')
			->where('name','=','company_phone')
			->first();
		$tax_rate = DB::table('var')
			->where('name','=','tax_rate')
			->first();
		$decimal = DB::table('var')
			->where('name','=','decimal')
			->first();
		$font_size = DB::table('var')
			->where('name','=','font_size')
			->first();
		$letter_head = DB::table('var')
			->where('name','=','letter_head')
			->first();
		$soa_footer = DB::table('var')
			->where('name','=','soa_footer')
			->first();
		$billing_auto_email = DB::table('var')
			->where('name','=','billing_auto_email')
			->first();
		$report_footer = DB::table('var')
			->where('name','=','report_footer')
			->first();

		$company_logo = DB::table('var')
			->where('name','=','company_logo')
			->first();
		return view('settings.settings',[
			'software_name'=>$software_name,
			'company_name'=>$company_name,
			'company_address'=>$company_address,
			'company_phone'=>$company_phone,
			'tax_rate'=>$tax_rate,
			'decimal'=>$decimal,
			'font_size'=>$font_size,
			'letter_head'=>$letter_head,
			'soa_footer'=>$soa_footer,
			'billing_auto_email'=>$billing_auto_email,
			'company_logo'=>$company_logo,
			'report_footer'=>$report_footer,
			]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request,$id)
	{
	
		$i=0;
		for($i=0;$i<count($request->input('settings_name'));$i++)
		{
			$settings_name = $request->input('settings_name')[$i];
			$settings_id = $request->input('settings_id')[$i];
			if($settings_name=='company_logo')
			{

				if($request->file('photo')!='')
				{
					$destinationPath = 'uploads'; // upload path
					$photoExtension = $request->file('photo')->getClientOriginalExtension(); 

					$photoFileName = $request->input('settings_name')[$i].".".$photoExtension;
					$request->file('photo')->move($destinationPath, $photoFileName);
					$x = Variable::find($settings_id);
					$x->value = $photoFileName;
					$x->save();	
				}
					
			}
			else
			{
				$x = Variable::find($settings_id);
				$x->value = $request->input($settings_name);
				$x->save();		
			}
			
		}

		$var = Variable::all();
		foreach ($var as $x) {
			session([$x->name => $x->value]); 
		}
	   
		return redirect('settings')->with('flash_message', 'Settings Updated!!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

	public function changePassword()
	{
		return View('settings.change_password',[]);
	}

	public function updatePassword(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'old_password' => 'required|max:20|min:8',    
			'new_password' => 'required|max:20|min:8',    
			'repeat_password' => 'required|max:30|min:8|same:new_password',    
		   
		]);

		if ($validator->fails()) {
			return redirect('settings/password/change')
						->withErrors($validator)
						->withInput();
		}
		$x = User::where('username','=',session('username'))->get();

		if (Hash::check($request->input('old_password'),$x[0]->password)) {
			$x[0]->password = hash::make($request->input('new_password'));
			$x[0]->save();
			return redirect('settings/password/change')->with('flash_message', 'Password Updated!!');
		}
		else
		{
			return redirect('settings/password/change')->with('flash_error', 'Old Password Incorrect!!');
		}
	}
}
