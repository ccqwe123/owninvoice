<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Client extends Model
{
	//
	use SoftDeletes;
	protected $fillable = [
		'client_name', 'email', 'vatable', 'credit_limit','terms','account_manager_id','personal','tin'
	];


	protected $table = 'clients';

	public function branch()
	{
		return $this->hasMany('App\ClientBranch');
	}

}
