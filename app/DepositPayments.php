<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepositPayments extends Model
{
    //
    protected $fillable = [
		'sales_invoice_id', 'amount','bank_id','deposit_date'
	];


	protected $table = 'deposit_payments';
	
	public function payment_info()
	{
		return $this->belongsTo('App\Payments', 'sales_invoice_id');
	}
}
