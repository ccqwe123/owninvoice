<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	use SoftDeletes;
	//
	 protected $fillable = [
		'prod_code', 'prod_name', 'prod_description', 'prod_price','prod_bar_code'
	];
	
	protected $dates = ['deleted_at'];
}
