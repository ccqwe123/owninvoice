<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manager extends Model
{
    //
     protected $fillable = [
		'emp_id', 'first_name', 'middle_name', 'last_name', 'user_id', 
	];


	protected $table = 'account_managers';
}
