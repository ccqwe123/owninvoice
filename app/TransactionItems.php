<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionItems extends Model
{
    //
    use SoftDeletes;
	//
	 protected $fillable = [
		'sales_invoice_id', 'product_id', 'qty', 'price'
	];
	
	protected $table = 'transaction_items';
}
