<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
	//
	protected $fillable = [
		'sales_invoice_id', 'cash_payment_id','deposit_payment_id', 'check_payment_id','or_number','photo','less','approved'
	];


	protected $table = 'payments';
}
