<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Invoice extends Model
{
	//
	use SoftDeletes;
	protected $fillable = [
		'invoice_date',
		'si', 
		'po', 
		'pay_type', 
		'client_id', 
		'branch_id', 
		'account_manager_id',
		'tax_rate',
		'terms',
		'status',
		'amount_total',
		'vat_sales',
		'total',
		'tax_amount',
		'discount',
	];


	protected $table = 'sales_invoice';

	public function client()
	{
		return $this->belongsTo('App\Client', 'client_id');
	}

	public function client_branch()
	{
		return $this->belongsTo('App\ClientBranch', 'branch_id');
	}

	public function transaction_items()
	{
		return $this->hasMany('App\TransactionItems','sales_invoice_id');
	}


}
