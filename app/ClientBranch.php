<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientBranch extends Model
{
	//
	protected $fillable = [
		'client_id','branch_name', 'contact_person', 'tel_no', 'mobile_no', 'fax_no', 'address','personal','email'
	];
	protected $table = 'client_branches';

	public function client()
	{
		return $this->belongsTo('App\Client', 'id');
	}

	public function invoice()
	{
		return $this->hasMany('App\Invoice','branch_id');
	}

}
