<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckPayments extends Model
{
	//
	protected $fillable = [
		'sales_invoice_id', 'amount','bank_id','account_number','check_number','check_date'
	];


	protected $table = 'check_payments';
	
	public function payment_info()
	{
		return $this->belongsTo('App\Payments', 'sales_invoice_id');
	}
}
