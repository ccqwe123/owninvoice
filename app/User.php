<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Log;
class User extends Authenticatable
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'username', 'password', 'first_name', 'middle_name', 'last_name', 'contact', 'photo',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	protected $table = 'users';

	public function roles()
	{
		return $this->hasMany('App\UserRoles');
	}

	public function checkPrivileges($privileges)
	{
		$id = $this->id;
		$userRoles = UserRoles::where('user_id',$id)->first();
		$role_id = $userRoles== '' ? 0 : $userRoles->role_id;
		$Privileges = Privileges::where('name',$privileges)->first();
		$RolePrivileges = RolePrivileges::where('role_id',$role_id)
			->where('privilege_id',$Privileges->id)
			->get();
		return count($RolePrivileges)>0 ? 1 : 0;
		
	}


}
