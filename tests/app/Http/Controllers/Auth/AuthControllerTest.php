<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthControllerTest extends TestCase
{
	use DatabaseMigrations;
	use DatabaseTransactions;
	// use WithoutMiddleware;
	protected $user;
	protected $password = 'admin';

	public function get_user()
	{

		if ($this->user) return;

		$this->user = factory(App\User::class)->create([
			'username' => 'admintest',
			'password' => Hash::make('password'),
			'first_name' => 'fname',
			'middle_name' => 'mname',
			'last_name' => 'lname',
			'contact' => '7777',
			'photo' => 'anon.png',

		]);

		$this->seeInDatabase('users', ['username' => 'admintest']);
	}

	public function test_show_login_page()
	{
		$this->visit('/')
			->see('Login')
			->seeStatusCode(200);
	}
	public function test_redirect_to_login_if_not_authenticated()
	{
		$this->get_user();
        $this->visit(route('dashboard'));
        $this->seePageIs('/');
	}

	public function test_dashboard_correct_login()
	{

		$this->get_user();
		$this->visit('/')
			->see('Login')
			->type($this->user->username, 'username')
			->type($this->password, 'password')
			->press('Login');

		$this->actingAs($this->user)
			->visit('/dashboard')
			->see('Dashboard'); 
        
	}
	public function test_dashboard_incorrect_login()
	{

			$this->visit('/')
			->see('Login')
			->type('gasd', 'username')
			->type('asd', 'password')
			->press('Login')
			->seePageIs('/');
        
	}



}
