app.factory('LoginProvider',function($http, CONSTANT){
	var factory = {};
	factory.doLogin = function(params){
		return factory.doPostHttpRequest(params);
	};

	factory.doPostHttpRequest = function(params){
		return $http({
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			url: CONSTANT.AUTH_URL,
			data : params
		});
	};
	return factory;
});

app.factory('FetchClientsProvider',function($http, CONSTANT){
	var factory = {};
	factory.doFetch = function(){
		return factory.doPostHttpRequest();
	};

	factory.doPostHttpRequest = function(){
		return $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: CONSTANT.FETCH_CLIENTS
		});
	};
	return factory;
});
app.factory('FetchClientBranchesProvider',function($http, CONSTANT){
	var factory = {};
	var id;
	factory.doFetch = function(ev){
		id = ev;
		return factory.doPostHttpRequest();
	};

	factory.doPostHttpRequest = function(){
	
		return $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: CONSTANT.FETCH_CLIENT_BRANCH + id
		});
	};
	return factory;
});
app.factory('FetchProductsProvider',function($http, CONSTANT){
	var factory = {};

	factory.doFetch = function(){
		return factory.doPostHttpRequest();
	};

	factory.doPostHttpRequest = function(){
	
		return $http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: CONSTANT.FETCH_PRODUCTS
		});
	};
	return factory;
});
