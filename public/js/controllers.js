app.config(function($mdDateLocaleProvider) {
	$mdDateLocaleProvider.formatDate = function(date) {
	   return moment(date).format('YYYY-MM-DD');
	};
});
app.directive('mdDatepicker', function () {
  function link(scope, element, attrs, ngModel) {
	var parser = function (val) {
	  val = moment(val).format('YYYY-MM-DD');
	  return val;
	};
	var formatter = function (val) {
	  if (!val) return val;
	  val = moment(val).toDate();
	  return val;
	};
	ngModel.$parsers.push(parser);
	ngModel.$formatters.push(formatter);
  }
  return {
	require: 'ngModel',
	link: link,
	restrict: 'EA',
	priority: 1
  }
});
app.controller('iFrameController',function($scope,$http,$window,$location,LoginProvider,CONSTANT,$mdDialog)
{
	$scope.value = "";

    window.updatedata = function(data) {
      $scope.$apply(function(){
        $scope.value = data;
      });
    };
});
app.controller('PrintController',function($scope,$http,$window,$location,LoginProvider,CONSTANT,$mdDialog)
{
	$scope.sending = 0;
	$scope.sendemail = function(id,branch_id)
	{
		$scope.sending = 1;
		$http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: '/clients/'+id+'/branch/'+branch_id+'/pdf',
		})
		.success(function (data) {
			$scope.sending = 0;
			console.log(data);
			document.getElementById('sendemailbtn').removeAttribute("disabled");
			alert('email sent!!');
		})
		.error(function(data){
			console.log('false ');
			$scope.sending = 0;
			document.getElementById('sendemailbtn').removeAttribute("disabled");
			alert('Error... Email sending failed');
		});
	}

	$scope.updateIframe = function() {
		document.getElementById('print-iframe').contentWindow.updatedata($scope.data);
	}
});
app.controller('ListInvoiceController',function($scope,$http,$window,$location,LoginProvider,CONSTANT,$mdDialog)
{
	console.log('ListInvoiceController');
	$scope.sales_invoice_id = 0;
	$scope.si = "";
	$scope.showTransferModal = function(id,si)
	{		
		$scope.sales_invoice_id = id;
		$scope.si = si;
		$mdDialog.show({
			clickOutsideToClose: true,
			scope: $scope,
			preserveScope: true,
			flex: 80,
			templateUrl: '/modal/transfer_modal',
			controller: function DialogController($scope, $mdDialog) {		

				$scope.closeDialog = function() {
					$mdDialog.hide();
				}
				$scope.printDiv = function(divName) {
					$mdDialog.hide();
				}
			}
		});
	}
});
app.controller('SampleController',function($scope,$http,$window,$location,LoginProvider,CONSTANT)
{
	$scope.invalid = false;
	$scope.emailFormat = /(?!.*?--)(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)/;
	$scope.token = {};
	$scope.validID = false;
	$scope.Login = function()
	{

		var params = {
					'email' : $scope.email,
					'password' : $scope.password
				};


		LoginProvider.doLogin(params)
			.success(function(data) {
				localStorage.setItem("token", data.token);
				$scope.invalid = false;
				$scope.token = data.token;
				if(data.admin==1)
				{
					//$window.location.href = CONSTANT.ADMIN_HOME;
				}
				else
				{
					//$window.location.href = CONSTANT.HOME_URL;
				}
				
			})
			.error(function(error) {
				$scope.invalid = true;
				console.log(error);
			});
	};

});

app.controller('PaymentReportController',function($scope,$http,$window,$location,LoginProvider,CONSTANT,$mdDialog)
{
	$scope.printDiv = function(divName) {
		$mdDialog.hide();
		var printContents = document.getElementById(divName).innerHTML;
		var popupWin = window.open('', '_blank', 'width=1024,height=800');
		popupWin.document.open();
		popupWin.document.write('<html><head></head><body style="margin: 0 auto;	text-align: center !important; 	background-color: #fff;	clear: both;	display: block;	text-align: center;	width: 100%;" onload="window.print()">' + printContents + '</body></html>');
		popupWin.document.close();
	}

	$scope.showReportForm = function(){
		$scope.show_form = ($scope.show_form == true ? false : true );  
		$scope.btn_caption = ($scope.btn_caption == 'Hide Form' ? 'Show Form' : 'Hide Form' );  
	};
});
app.controller('ActivityLogController',function($scope,$http,$window,$location,LoginProvider,CONSTANT,$mdDialog)
{
	$scope.printDiv = function(divName) {
		$mdDialog.hide();
		var printContents = document.getElementById(divName).innerHTML;
		var popupWin = window.open('', '_blank', 'width=1024,height=800');
		popupWin.document.open();
		popupWin.document.write('<html><head></head><body style="margin: 0 auto;	text-align: center !important; 	background-color: #fff;	clear: both;	display: block;	text-align: center;	width: 100%;" onload="window.print()">' + printContents + '</body></html>');
		popupWin.document.close();
	}

	$scope.showReportForm = function(){
		$scope.show_form = ($scope.show_form == true ? false : true );  
		$scope.btn_caption = ($scope.btn_caption == 'Hide Form' ? 'Show Form' : 'Hide Form' );  
	};
});
app.controller('SalesReportController',function($scope,$http,$window,$location,LoginProvider,CONSTANT,$mdDialog)
{
	$scope.selected_client = undefined;
	$scope.selected_account_manager = undefined;
	$scope.client_branches ={};
	var def_client = {
		'account_manager_id' : 0,
		'address' : null,
		'branch_id' : 0,
		'branch_name' : null,
		'client_name' : 'ALL',
		'contact_person' : null,
		'created_at' : null,
		'credit_limit' : null,
		'deleted_at' : null,
		'email' : null,
		'emp_id' : null,
		'first_name' :null,
		'id' : 0,
		'last_name' : null,
		'middle_name' : null,
		'terms' : null,
		'tin' : null,
		'updated_at' : null,
		'vatable' : null
			};
	var def_branch = {
		'address' : 0,
		'branch_name' : 'ALL',
		'client_id' : 0,
		'contact_person' : null,
		'created_at' : 'ALL',
		'deleted_at' : null,
		'fax_no' : null,
		'id' : null,
		'mobile_no' : null,
		'tel_no' : null
			};
	var def_manager = {
		'deleted_at' : 0,
		'emp_id' : 0,
		'first_name' : 'ALL',
		'id' : null,
		'last_name' : '',
		'middle_name' : null,
		'updated_at' : null,
		'user_id' : null
			};
		
	$http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: '/api/fetch-clients',
		})
		.success(function (data) {
			$scope.clients = data.clients;
			$scope.clients.unshift(def_client);
		})
		.error(function(data){
			console.log('false ');
		});

	$http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: '/api/fetch-account-managers',
		})
		.success(function (data) {
			$scope.account_managers = data.account_managers;
			$scope.account_managers.unshift(def_manager);
		})
		.error(function(data){
			console.log('false ');
		});

	$scope.printDiv = function(divName) {
		$mdDialog.hide();
		var printContents = document.getElementById(divName).innerHTML;
		var popupWin = window.open('', '_blank', 'width=1024,height=800');
		popupWin.document.open();
		popupWin.document.write('<html><head></head><body style="margin: 0 auto;	text-align: center !important; 	background-color: #fff;	clear: both;	display: block;	text-align: center;	width: 100%;" onload="window.print()">' + printContents + '</body></html>');
		popupWin.document.close();
	}

	$scope.showReportForm = function(){
		$scope.show_form = ($scope.show_form == true ? false : true );  
		$scope.btn_caption = ($scope.btn_caption == 'Hide Form' ? 'Show Form' : 'Hide Form' );  
	};

	$scope.$watch('selected_client', function() {
		$http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: '/api/fetch-clients/'+$scope.selected_client,
		})
		.success(function (data) {
			$scope.clients = data.clients;
			$scope.clients.unshift(def_client);
		})
		.error(function(data){
			console.log('false ');
		});
		if($scope.selected_client)
		{
			if($scope.selected_client.id!=undefined)
			{
				$http({
					method: 'GET',
					headers: { 'Content-Type': 'application/json' },
					url: '/api/fetch-clients/branch/'+$scope.selected_client.id,
				})
				.success(function (data) {
					$scope.client_branches = data.client_branches;
					$scope.client_branches.unshift(def_branch);
					//console.log($scope.client_branches);
				})
				.error(function(data){
					console.log('false ');
				});
			}
		}
		
	});

});
app.controller('ProductReportController',function($scope,$http,$window,$location,LoginProvider,CONSTANT,$mdDialog)
{
	$scope.show_form  = false;
	var def_prod = {
		'created_at' : 0,
		'deleted_at' : 0,
		'id' : 0,
		'prod_code' : null,
		'prod_description' : '',
		'prod_name' : 'ALL',
		'prod_price' : null,
		'updated_at' : null
			};
	
	$scope.selected_product = undefined;
	$http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: '/api/fetch-products',
		})
		.success(function (data) {
			$scope.products = data.products;
			$scope.products.unshift(def_prod);
		})
		.error(function(data){
			console.log('false ');
		});


	$scope.printDiv = function(divName) {
		$mdDialog.hide();
		var printContents = document.getElementById(divName).innerHTML;
		var popupWin = window.open('', '_blank', 'width=1024,height=800');
		popupWin.document.open();
		popupWin.document.write('<html><head></head><body style="margin: 0 auto;	text-align: center !important; 	background-color: #fff;	clear: both;	display: block;	text-align: center;	width: 100%;" onload="window.print()">' + printContents + '</body></html>');
		popupWin.document.close();
	}

	$scope.showReportForm = function(){
		$scope.show_form = ($scope.show_form == true ? false : true );  
		$scope.btn_caption = ($scope.btn_caption == 'Hide Form' ? 'Show Form' : 'Hide Form' );  
	};
});
app.controller('ClientReportController',function($scope,$http,$window,$location,LoginProvider,CONSTANT,$mdDialog)
{
	var def_client = {
		'account_manager_id' : 0,
		'address' : null,
		'branch_id' : 0,
		'branch_name' : null,
		'client_name' : 'ALL',
		'contact_person' : null,
		'created_at' : null,
		'credit_limit' : null,
		'deleted_at' : null,
		'email' : null,
		'emp_id' : null,
		'first_name' :null,
		'id' : 0,
		'last_name' : null,
		'middle_name' : null,
		'terms' : null,
		'tin' : null,
		'updated_at' : null,
		'vatable' : null
			};
	var def_branch = {
		'address' : 0,
		'branch_name' : 'ALL',
		'client_id' : 0,
		'contact_person' : null,
		'created_at' : 'ALL',
		'deleted_at' : null,
		'fax_no' : null,
		'id' : null,
		'mobile_no' : null,
		'tel_no' : null
			};
	var def_manager = {
		'deleted_at' : 0,
		'emp_id' : 0,
		'first_name' : 'ALL',
		'id' : null,
		'last_name' : '',
		'middle_name' : null,
		'updated_at' : null,
		'user_id' : null
			};
		
	$http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: '/api/fetch-clients',
		})
		.success(function (data) {
			$scope.clients = data.clients;
			$scope.clients.unshift(def_client);
		})
		.error(function(data){
			console.log('false ');
		});


	$scope.printDiv = function(divName) {
		$mdDialog.hide();
		var printContents = document.getElementById(divName).innerHTML;
		var popupWin = window.open('', '_blank', 'width=1024,height=800');
		popupWin.document.open();
		popupWin.document.write('<html><head></head><body style="margin: 0 auto;	text-align: center !important; 	background-color: #fff;	clear: both;	display: block;	text-align: center;	width: 100%;" onload="window.print()">' + printContents + '</body></html>');
		popupWin.document.close();
	}

	$scope.showReportForm = function(){
		$scope.show_form = ($scope.show_form == true ? false : true );  
		$scope.btn_caption = ($scope.btn_caption == 'Hide Form' ? 'Show Form' : 'Hide Form' );  
	};

	$scope.$watch('selected_client', function() {
		$http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: '/api/fetch-clients/'+$scope.selected_client,
		})
		.success(function (data) {
			$scope.clients = data.clients;
			$scope.clients.unshift(def_client);
		})
		.error(function(data){
			console.log('false ');
		});
		if($scope.selected_client)
		{
			if($scope.selected_client.id!=undefined)
			{
				$http({
					method: 'GET',
					headers: { 'Content-Type': 'application/json' },
					url: '/api/fetch-clients/branch/'+$scope.selected_client.id,
				})
				.success(function (data) {
					$scope.client_branches = data.client_branches;
					$scope.client_branches.unshift(def_branch);
					//console.log($scope.client_branches);
				})
				.error(function(data){
					console.log('false ');
				});
			}
		}
		
	});
});

app.controller('PaymentsController',function($scope,$http,$window,$location,CONSTANT,$mdDialog,$filter)
{
	console.log('PaymentsController');
	$scope.sales_invoice_id=0;	
	$scope.isValid = function (blurMode) 
	{  
		if( ($scope.level.num > $scope.maxNum) || ($scope.level.nums < $scope.minNum && blurMode)) {
			$scope.level.num = $scope.minNum;
		}
	};
	$scope.close = function()
	{
		$mdDialog.hide();
	}
	$scope.addPayment = function(sales_invoice_id)
	{		
		$scope.sales_invoice_id = sales_invoice_id;
		$mdDialog.show({
			clickOutsideToClose: true,
			scope: $scope,
			preserveScope: true,
			flex: 80,
			templateUrl: '/modal/payments_modal',
			controller: function DialogController($scope, $mdDialog) {		
				$scope.check_date = $filter('date')(new Date(), 'yyyy-MM-dd')

				$scope.closeDialog = function() {
					$mdDialog.hide();
				}
				$scope.printDiv = function(divName) {
					$mdDialog.hide();
				}
			}
		});
	}
	$scope.addPaymentCash = function(sales_invoice_id)
	{		
		$scope.sales_invoice_id = sales_invoice_id;
		$mdDialog.show({
			clickOutsideToClose: true,
			scope: $scope,
			preserveScope: true,
			flex: 80,
			templateUrl: '/modal/payments_modal_cash',
			controller: function DialogController($scope, $mdDialog) {		
				$scope.check_date = $filter('date')(new Date(), 'yyyy-MM-dd')

				$scope.closeDialog = function() {
					$mdDialog.hide();
				}
				$scope.printDiv = function(divName) {
					$mdDialog.hide();
				}
			}
		});
	}
	$scope.addPaymentCheck = function(sales_invoice_id)
	{		
		$scope.sales_invoice_id = sales_invoice_id;
		$mdDialog.show({
			clickOutsideToClose: true,
			scope: $scope,
			preserveScope: true,
			flex: 80,
			templateUrl: '/modal/payments_modal_check',
			controller: function DialogController($scope, $mdDialog) {		
				$scope.check_date = $filter('date')(new Date(), 'yyyy-MM-dd')

				$scope.closeDialog = function() {
					$mdDialog.hide();
				}
				$scope.printDiv = function(divName) {
					$mdDialog.hide();
				}
			}
		});
	}
	$scope.addPaymentBankDeposit = function(sales_invoice_id)
	{		
		$scope.sales_invoice_id = sales_invoice_id;
		$mdDialog.show({
			clickOutsideToClose: true,
			scope: $scope,
			preserveScope: true,
			flex: 80,
			templateUrl: '/modal/payments_modal_bankdeposit',
			controller: function DialogController($scope, $mdDialog) {		
				$scope.check_date = $filter('date')(new Date(), 'yyyy-MM-dd')

				$scope.closeDialog = function() {
					$mdDialog.hide();
				}
				$scope.printDiv = function(divName) {
					$mdDialog.hide();
				}
			}
		});
	}
});
app.controller('InvoiceController',function($filter,$scope,$http,$window,$location,FetchClientsProvider,FetchClientBranchesProvider,FetchProductsProvider,CONSTANT,$mdDialog)
{
	console.log('InvoiceController');
	$scope.clients ={};
	$scope.products ={};
	$scope.client_branches ={};
	$scope.payment = 0;
	$scope.balance = 0;
	$scope.discount = 0;
	$scope.total_amount_due = 0;
	$scope.check_date = $filter('date')(new Date(), 'yyyy-MM-dd');
	$scope.invoice_date = $filter('date')(new Date(), 'yyyy-MM-dd');
	$scope.product_search = "";
	$scope.changeindex = 0;
	$scope.changeqty = 0;
	$scope.valid_si = false;
	$scope.valid_po = false;
	$scope.vatable = 0;
	$http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: '/api/fetch-clients',
		})
		.success(function (data) {			
			$scope.clients = data.clients;

		})
		.error(function(data){
			console.log('false ');
		})

	

	$http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: '/api/fetch-products-name/mouse',
		})
		.success(function (data) {
			$scope.products = data.products;
		})
		.error(function(data){
			console.log('false ');
		})

	$scope.$watch('discount', function() {
		$scope.total_amount_due = $scope.total_amount_due - $scope.discount;
		$scope.recalculate();
	});

	$scope.$watch('total_amount_due', function() {
		$scope.balance = $filter('number')($scope.total_amount_due - $scope.payment, $scope.decimal_value);	
	});

	$scope.$watch('payment', function() {
		$scope.balance = $filter('number')($scope.total_amount_due - $scope.payment, $scope.decimal_value);	
	});

	$scope.$watch('si', function() {

		if($scope.si)
		{
			$scope.checkIfTaken('si',$scope.si);
		}
		
	});
	$scope.$watch('po', function() {

		if($scope.po)
		{
			$scope.checkIfTaken('po',$scope.po);
		}
		
	});

	$scope.$watch('tax_rate', function() {

		$scope.recalculate();
		
	});
	
	$scope.checkIfTaken = function(type,num)
	{	
		$http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: '/api/check/'+type+'/number/'+num,
		})
		.success(function (data) {			
			if(type=='si')
			{
				$scope.valid_si = data.result;
			}
			else
			{
				$scope.valid_po = data.result;
			}
			
			
		})
		.error(function(data){
			console.log('false ');
		})
	}



	$scope.$watch('selected_client', function() {
		$http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: '/api/fetch-clients/'+$scope.selected_client,
		})
		.success(function (data) {
			$scope.clients = data.clients;
		})
		.error(function(data){
			console.log('false ');
		});
		
		if($scope.selected_client)
		{

			$scope.account_manager="";
			$scope.selected_branch ="";
			if($scope.selected_client.id!=undefined)
			{
		
				// 
				$http({
					method: 'GET',
					headers: { 'Content-Type': 'application/json' },
					url: '/api/fetch-clients/account_manager/'+$scope.selected_client.account_manager_id,
				})
				.success(function (data) {
					$scope.account_manager = data.account_managers[0].first_name + " " + data.account_managers[0].last_name;					
				})
				.error(function(data){
					console.log('false ');
				});

				$http({
					method: 'GET',
					headers: { 'Content-Type': 'application/json' },
					url: '/api/fetch-clients/balance/'+$scope.selected_client.id,
				})
				.success(function (data) {
					$scope.selected_client.balance = data.balance[0].balance;					
				})
				.error(function(data){
					console.log('false ');
				});

				$http({
					method: 'GET',
					headers: { 'Content-Type': 'application/json' },
					url: '/api/fetch-clients/branch/'+$scope.selected_client.id,
				})
				.success(function (data) {
					$scope.client_branches = data.client_branches;
					//console.log($scope.client_branches);
				})
				.error(function(data){
					console.log('false ');
				});
			}

		}
		if($scope.selected_client.vatable==0)
			{
				$scope.tax_rate = 1;
			}
			$scope.recalculate();

		
	});
	$scope.vatable = function(vat)
	{
		$scope.vatable = vat;
	}

	$scope.init = function (id) {
		
		console.log('init');
		$http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: '/api/fetch-transaction-items/'+id,
		})
		.success(function (data) {
			$scope.selected_product = data.selected_product;
			$scope.recalculate();

		})
		.error(function(data){
			console.log('false ');
		})
	};
	$scope.initadd = function () {
		console.log('initadd');
		$scope.selected_product = [];

	};
	$scope.initbalance = function (id) {
		console.log('initbalance');
		$http({
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
			url: '/api/fetch-client/'+id,
		})
		.success(function (data) {
			
			$scope.selected_client = data.clients[0];
			$scope.balance = $filter('number')($scope.selected_client.balance, $scope.decimal_value);	
			$scope.recalculate();
		})
		.error(function(data){
			console.log('false ');
		})


	};
	$scope.Save = function ()
	{
		console.log('save');
	}
	$scope.recalculate = function()
	{

		if($scope.selected_product.length!=0)
		{	
			console.log('recalculate');
			var total = 0;

			for(var i = 0; i < $scope.selected_product.length; i++){
				total += parseFloat($scope.selected_product[i].total);
			}		
			$scope.total_amount_due = parseFloat(total).toFixed(2)-$scope.discount;	

			if($scope.vatable==0)
			{

			}
			else
			{
				// console.log('dito gigi pogi');
				// $scope.vat_sales = parseFloat(total - ($scope.total_amount_due/($scope.tax_rate+1))).toFixed(2);
				// $scope.tax_amount = parseFloat(total - $scope.vat_sales).toFixed(2);
				$scope.tax_amount =parseFloat(total - ($scope.total_amount_due/($scope.tax_rate+1))).toFixed(2);
				$scope.vat_sales = parseFloat(total - $scope.tax_amount).toFixed(2);
			}
	
		}
		else
		{
			$scope.total_amount_due =0;
			$scope.vat_sales =0;
			$scope.tax_amount = 0;
		}



		
	}
	$scope.removeItem = function(id)
	{
		$scope.selected_product.splice(id, 1);
		$scope.recalculate();
	}
	$scope.changePrice = function(id)
	{
		console.log('changePrice ' + id);
		$scope.changeindex =id;
		$scope.changeprice = $filter('number')(parseFloat($scope.selected_product[id].prod_price), $scope.decimal_value);

		$mdDialog.show({
					clickOutsideToClose: true,
					scope: $scope,
					preserveScope: true,
					templateUrl: '/modal/change_price_modal',
					controller: function DialogController($scope, $mdDialog) {
						
						$scope.changePriceValue = function()
						{
							$scope.selected_product[id].prod_price = $scope.price_value;
							$scope.selected_product[id].total = $scope.selected_product[id].qty*$scope.selected_product[id].prod_price;
							$scope.recalculate();
							$mdDialog.hide();
						}

						$scope.closeDialog = function() {
							$mdDialog.hide();
						}
					}
			   });
	}
	$scope.changeQTY = function(e)
	{
		console.log('changeQty ' + e);
		$scope.changeindex =e;
		$scope.changeqty = $scope.selected_product[e].qty;

		$mdDialog.show({
					clickOutsideToClose: true,
					scope: $scope,
					preserveScope: true,
					templateUrl: '/modal/change_qty_modal',
					controller: function DialogController($scope, $mdDialog) {
						
						$scope.changeQTYvalue = function()
						{
							$scope.selected_product[e].qty = parseInt($scope.qty_value);

							$scope.selected_product[e].total = parseFloat($scope.selected_product[e].qty*$scope.selected_product[e].prod_price);
							$scope.recalculate();
							$mdDialog.hide();
						}

						$scope.closeDialog = function() {
							$mdDialog.hide();
						}
					}
			   });
	}
	$scope.AddItem = function()
	{
		FetchProductsProvider.doFetch()
			.success(function(data) {
				$mdDialog.show({
					clickOutsideToClose: true,
					scope: $scope,
					preserveScope: true,
					templateUrl: '/modal/products_modal',
					controller: function DialogController($scope, $mdDialog) {
					$scope.qty = [];
					$scope.search_product = $scope.product_search;
						$scope.$watch('search_product', function() {
						console.log('DialogController');
							if($scope.search_product)
							{
								if($scope.search_product.length>1)
								{
									$http({
										method: 'GET',
										headers: { 'Content-Type': 'application/json' },
										url: '/api/fetch-products-name/'+$scope.search_product,
									})
									.success(function (data) {
										$scope.products = data.products;
									})
									.error(function(data){
										console.log('false ');
									});
								}
							}	
						});
						$scope.addToCartAll = function()
						{
							var total_product_amount=0;
							for(var z =0;z < $scope.products.length;z++)
							{
								if($scope.products[z].qty>0)
								{
									total_product_amount = total_product_amount + $scope.products[z].qty * $scope.products[z].prod_price;
								}
							}

							if($scope.is_edit==1)
							{
								var total = parseFloat($scope.total_amount_due) + parseFloat(total_product_amount);
							}
							else
							{

								var total = parseFloat($scope.total_amount_due) + parseFloat($scope.selected_client.balance) + parseFloat(total_product_amount);
							}

							if(parseFloat(total)>parseFloat($scope.selected_client.credit_limit))
							{
								window.alert("Cannot add Product!! Credit Limit Reached!!");
							}
							else
							{
								for(var z =0;z < $scope.products.length;z++)
								{
									if($scope.products[z].qty>0)
									{
										var params = {
											'id' : $scope.products[z].id,
											'prod_code' : $scope.products[z].prod_code,
											'prod_name' : $scope.products[z].prod_name,
											'prod_price' : $scope.products[z].prod_price,
											'qty' : $scope.products[z].qty,
											'total' : $scope.products[z].qty*$scope.products[z].prod_price ,
											};

										console.log($scope.products[z].id + $scope.products[z].prod_name);
										if($scope.products[z].qty>0)
											$scope.selected_product.push(params);

										$http({
											method: 'GET',
											headers: { 'Content-Type': 'application/json' },
											url: '/api/fetch-products-name/'+$scope.search_product,
										})
										.success(function (data) {
											$scope.products = data.products;
										})
										.error(function(data){
											console.log('false ');
										})
											
									}
									// console.log($scope.qty[z] + " index " + $scope.products[z].prod_name);
								}

								
							}
							$scope.recalculate();
							$scope.product_search = "";
							$mdDialog.hide();
						}
						$scope.closeDialog = function() {
							$mdDialog.hide();
						}
						$scope.printDiv = function(divName) {
							$mdDialog.hide();
						}
					}
			   });
			})
			.error(function(error) {
				console.log(error);
		});
	}
	
	$scope.close = function()
	{
		$mdDialog.hide();
	}

	$scope.FetchClientBranches = function()
	{

		FetchClientBranchesProvider.doFetch($scope.selected_client.id)
			.success(function(data) {
				$mdDialog.show({
					clickOutsideToClose: true,
					scope: $scope,
					preserveScope: true,
					templateUrl: '/modal/client_branches_modal',
					controller: function DialogController($scope, $mdDialog) {
					$scope.setClientBranch = function(ev)
					{
						$scope.selected_branch = ev;
						$mdDialog.hide();
					}

					$scope.closeDialog = function() {
						$mdDialog.hide();
					}
					$scope.printDiv = function(divName) {
						$mdDialog.hide();
					}
				  }
			   });
			})
			.error(function(error) {
			console.log(error);
		});
	}

	$scope.newClient = function()
	{
		console.log('FetchClients');
		$scope.form_account_manager_id = null;
		$mdDialog.show({
			clickOutsideToClose: true,
			scope: $scope,
			preserveScope: true,
			templateUrl: '/modal/new_client_modal',
			controller: function DialogController($scope, $mdDialog) {
				$scope.form_terms = 30;
				$scope.form_credit_limit = 30000;

				$scope.submitClient = function(ev)
				{
					console.log('submitClient');
					var params = {
						'client_name': $scope.form_client_name,
						'mobile_no': $scope.form_mobile_no,
						'credit_limit': $scope.form_credit_limit,
						'contact_person': $scope.form_contact_person,
						'fax_no': $scope.form_fax_no,
						'terms': $scope.form_terms,
						'address': $scope.form_address,
						'email': $scope.form_email,
						'vatable': $scope.form_vatable,
						'personal': $scope.form_personal,
						'tel_no': $scope.form_tel_no,
						'tin': $scope.form_tin,
						'account_manager_id': $scope.form_account_manager_id
					}
					console.log(params);
					$http({
						method: 'POST',
						headers: { 'Content-Type': 'application/json' },
						url: '/api/new-client/create',
						data: params 
					})
					.success(function (data) {
						console.log(data);
						$scope.clients.push(data.client);
						$scope.selected_client = data.client;
						$scope.selected_client.account_manager_id = $scope.form_account_manager_id;
						$scope.account_manager = data.client.first_name + " " + data.client.last_name;	
						$mdDialog.hide();
					})
					.error(function(data){
						console.log('false ');
					})

				}

				$scope.closeDialog = function() {
					$mdDialog.hide();
				}
				$scope.printDiv = function(divName) {
					$mdDialog.hide();
				}
			  }
		   });
	}

	$scope.newItem = function()
	{
		console.log('New Item');
		
		$mdDialog.show({
			clickOutsideToClose: true,
			scope: $scope,
			preserveScope: true,
			templateUrl: '/modal/new_item_modal',
			controller: function DialogController($scope, $mdDialog) {
	
				$scope.submitProduct = function(ev)
				{
					console.log('submitItem');
					var params = {
						'prod_code': $scope.form_prod_code,
						'prod_name': $scope.form_prod_name,
						'prod_description': $scope.form_prod_description,
						'prod_price': $scope.form_prod_price
					}
					console.log(params);
					$http({
						method: 'POST',
						headers: { 'Content-Type': 'application/json' },
						url: '/api/new-item/create',
						data: params 
					})
					.success(function (data) {
						console.log(data);
						$http({
							method: 'GET',
							headers: { 'Content-Type': 'application/json' },
							url: '/api/fetch-products',
						})
						.success(function (data) {
							$scope.products = data.products;
							$scope.form_prod_code = "";
							$scope.form_prod_name = "";
							$scope.form_prod_description = "";
							$scope.form_prod_price = ""

						})
						.error(function(data){
							console.log('false ');
						})
						$mdDialog.hide();
						$scope.product_search = params.prod_name;
						console.log($scope.product_search);
						$scope.AddItem();
					})
					.error(function(data){
						console.log('false ');
					})

				}

				$scope.closeDialog = function() {
					$mdDialog.hide();
				}
				$scope.printDiv = function(divName) {
					$mdDialog.hide();
				}
			  }
		   });
	}

	$scope.newBranch = function()
	{
		console.log('newBranch');

		$mdDialog.show({
			clickOutsideToClose: true,
			scope: $scope,
			preserveScope: true,
			templateUrl: '/modal/new_branch_modal',
			controller: function DialogController($scope, $mdDialog) {
				$scope.submitBranch = function(ev)
				{
					var params = {
						'branch_name': $scope.form_branch_name,
						'mobile_no': $scope.form_mobile_no,
						'contact_person': $scope.form_contact_person,
						'fax_no': $scope.form_fax_no,
						'address': $scope.form_address,
						'tel_no': $scope.form_tel_no,
						'personal': $scope.form_personal,
					}
					console.log(params);
					$http({
						method: 'POST',
						headers: { 'Content-Type': 'application/json' },
						url: '/api/new-branch/'+$scope.selected_client.id+'/create',
						data: params 
					})
					.success(function (data) {
						console.log(data);
						$scope.selected_branch = data.branch;
						$scope.client_branches.push(data.branch);
						$mdDialog.hide();
					})
					.error(function(data){
						console.log('false ');
					})

				}

				$scope.closeDialog = function() {
					$mdDialog.hide();
				}
				$scope.printDiv = function(divName) {
					$mdDialog.hide();
				}
			  }
		   });
	}




});
