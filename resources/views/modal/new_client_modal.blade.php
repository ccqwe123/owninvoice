<md-dialog aria-label="Privacy Policy" >
<div class="modalreport" id="modalreport" style="width: 800px;height: 400px; overflow:scroll">

	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" ng-click="close();">&times;</button>
	     <h3>New Client</h3>
	</div>
	<div class="modal-body">		
		<form name="saveClient" action="#" ng-submit="submitClient()">
			<div class="row">
				<div class="col-lg-4">
					{{ Form::label('client_name', 'Client Name') }}
					<input type="text" name="form_client_name" class="form-control span6" placeholder="Client Name" ng-model="form_client_name" ng-class="{ 'submitted' : (saveClient.form_client_name.$invalid && saveClient.form_client_name.$dirty) }" required tabindex="1">
					<span ng-show="saveClient.form_client_name.$invalid && saveClient.form_client_name.$dirty" style="color:#FF0000">Client name required!</span>
				</div>
				<div class="col-lg-4">
					{{ Form::label('mobile_no', 'Mobile') }}
					<input type="text" name="form_mobile_no" class="form-control span6" placeholder="Mobile" ng-model="form_mobile_no" ng-class="{ 'submitted' : (saveClient.form_mobile_no.$invalid && saveClient.form_mobile_no.$dirty) }"  tabindex="5">
					<span ng-show="saveClient.form_mobile_no.$invalid && saveClient.form_mobile_no.$dirty" style="color:#FF0000">Mobile number Required!</span>
				</div>
				<div class="col-lg-4">
					{{ Form::label('credit_limit', 'Credit Limit') }}
					<input type="number" name="form_credit_limit" value="30000" class="form-control span6" placeholder="Credit Limit" ng-model="form_credit_limit" ng-class="{ 'submitted' : (saveClient.form_credit_limit.$invalid && saveClient.form_credit_limit.$dirty) }" required tabindex="9">
					<span ng-show="saveClient.form_credit_limit.$invalid && saveClient.form_credit_limit.$dirty" style="color:#FF0000">Credit Limit Required!</span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					{{ Form::label('contact_person', 'Contact Person') }}
					<input type="text" name="form_contact_person"  class="form-control span6" placeholder="Contact Person" ng-model="form_contact_person" ng-class="{ 'submitted' : (saveClient.form_contact_person.$invalid && saveClient.form_contact_person.$dirty) }" required tabindex="2">
					<span ng-show="saveClient.form_contact_person.$invalid && saveClient.form_contact_person.$dirty" style="color:#FF0000">Contact Person Required!</span>
				</div>
				<div class="col-lg-4">
					{{ Form::label('fax_no', 'Fax') }}
					<input type="text" name="form_fax_no"  class="form-control span6" placeholder="Fax" ng-model="form_fax_no" ng-class="{ 'submitted' : (saveClient.form_fax_no.$invalid && saveClient.form_fax_no.$dirty) }" tabindex="6">
				</div>
				<div class="col-lg-4">
					{{ Form::label('terms', 'Terms(days)') }}
					<input type="number" name="form_terms" value="30" class="form-control span6" placeholder="Terms(days)" ng-model="form_terms" ng-class="{ 'submitted' : (saveClient.form_terms.$invalid && saveClient.form_terms.$dirty) }" required tabindex="10">
					<span ng-show="saveClient.form_terms.$invalid && saveClient.form_terms.$dirty" style="color:#FF0000">Terms Required!</span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					{{ Form::label('address', 'Address') }}
					<input type="text" name="form_address" class="form-control span6" placeholder="Address" ng-model="form_address" ng-class="{ 'submitted' : (saveClient.form_address.$invalid && saveClient.form_address.$dirty) }"  tabindex="3">
					<span ng-show="saveClient.form_address.$invalid && saveClient.form_address.$dirty" style="color:#FF0000">Address Required!</span>
				</div>
				<div class="col-lg-4">
					{{ Form::label('email', 'Email') }}
					<input type="email" name="form_email" class="form-control span6" placeholder="Email" ng-model="form_email" ng-class="{ 'submitted' : (saveClient.form_email.$invalid && saveClient.form_email.$dirty) }" tabindex="7">
					<span ng-show="saveClient.form_email.$invalid && saveClient.form_email.$dirty" style="color:#FF0000">Invalid Email</span>
				</div>
				<div class="col-lg-2">
					<br/>
					{{ Form::label('vatable', 'Vatable') }}
					<input type="checkbox" name="vatable" ng-model="form_vatable" ng-true-value="1" ng-false-value="0" tabindex="11">
					<span class="errors" style="color:#FF0000">{{$errors->first('vatable')}}</span>
				</div>
				<div class="col-lg-2">
					<br/>
					{{ Form::label('personal', 'Personal') }}
					<input type="checkbox" name="personal" ng-model="form_personal" ng-true-value="1" ng-false-value="0" tabindex="12">
					<span class="errors" style="color:#FF0000">{{$errors->first('personal')}}</span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					{{ Form::label('tel_no', 'Telephone') }}
					<input type="text" name="form_tel_no" class="form-control span6" placeholder="Telephone" ng-model="form_tel_no" ng-class="{ 'submitted' : (saveClient.form_tel_no.$invalid && saveClient.form_tel_no.$dirty) }" tabindex="4">
				</div>
				<div class="col-lg-4">
					{{ Form::label('tin', 'TIN') }}
					<input type="text" name="form_tin" class="form-control span6" placeholder="TIN" ng-model="form_tin" ng-class="{ 'submitted' : (saveClient.form_tin.$invalid && saveClient.form_tin.$dirty) }" tabindex="8">
				</div>
				<div class="col-lg-4">
					{{ Form::label('account_manager_id', 'Account Manager') }}
					<select class="form-control span6" id="account_manager_id" ng-model="form_account_manager_id" name="account_manager_id" ng-class="{ 'submitted' : (saveClient.account_manager_id.$invalid && saveClient.account_manager_id.$dirty) }" required tabindex="13">
						@foreach ($users as $x)
							<option value="{!! $x->id !!}">{!! $x->first_name !!} {!! $x->last_name !!}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
				</div>
				<div class="col-lg-4">
				</div>
				<div class="col-lg-4" style="text-align: right;">
					<input type="button" ng-disabled="saveClient.$invalid" ng-click="submitClient()" value="Submit" class="btn btn-primary" tabindex="14"/>
				</div>
			</div>
		</form>
	</div>
</div>
</md-dialog>