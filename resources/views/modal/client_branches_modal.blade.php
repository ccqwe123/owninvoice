<md-dialog aria-label="Privacy Policy" >
<div class="modalreport" id="modalreport" style="width: 600px;height: 400px; overflow:scroll">
	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" ng-click="close();">&times;</button>
	    <h3>Branch Name</h3>
	</div>
	<div class="modal-body">
		Search<input type="text" ng-model="search_branch">
		<table class="table table-responsive">			
			<tr ng-repeat="client_branch in client_branches | filter:search_branch">
				<td><a href="#" ng-click="setClientBranch(client_branch)"><% client_branch.branch_name %></a></td> 
			</tr>
		</table>
	</div>
</div>
</md-dialog>