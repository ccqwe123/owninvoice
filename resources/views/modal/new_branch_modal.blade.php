<md-dialog aria-label="Privacy Policy" >
<div class="modalreport" id="modalreport" style="width: 800px;height: 400px; overflow:scroll">

	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" ng-click="close();">&times;</button>
	     <h3>New Branch</h3>
	</div>
	<div class="modal-body">		
		<form name="saveBranch" action="#" ng-submit="submitBranch()">
			<div class="row">
				<div class="col-lg-4">
					{{ Form::label('client_name', 'Branch Name') }}
					<input type="text" name="form_branch_name" class="form-control span6" placeholder="Client Name" ng-model="form_branch_name" ng-class="{ 'submitted' : (saveBranch.form_branch_name.$invalid && saveBranch.form_branch_name.$dirty) }" required tabindex="1">
					<span ng-show="saveBranch.form_branch_name.$invalid && saveBranch.form_branch_name.$dirty" style="color:#FF0000">Client name required!</span>
				</div>
				<div class="col-lg-4">
					{{ Form::label('mobile_no', 'Mobile') }}
					<input type="text" name="form_mobile_no" class="form-control span6" placeholder="Mobile" ng-model="form_mobile_no" ng-class="{ 'submitted' : (saveBranch.form_mobile_no.$invalid && saveBranch.form_mobile_no.$dirty) }" required tabindex="5">
					<span ng-show="saveBranch.form_mobile_no.$invalid && saveBranch.form_mobile_no.$dirty" style="color:#FF0000">Mobile number Required!</span>
				</div>
				<div class="col-lg-4">
				</div>
			</div>

			<div class="row">
				<div class="col-lg-4">
					{{ Form::label('contact_person', 'Contact Person') }}
					<input type="text" name="form_contact_person"  class="form-control span6" placeholder="Contact Person" ng-model="form_contact_person" ng-class="{ 'submitted' : (saveBranch.form_contact_person.$invalid && saveBranch.form_contact_person.$dirty) }" required tabindex="2">
					<span ng-show="saveBranch.form_contact_person.$invalid && saveBranch.form_contact_person.$dirty" style="color:#FF0000">Contact Person Required!</span>
				</div>
				<div class="col-lg-4">
					{{ Form::label('fax_no', 'Fax') }}
					<input type="text" name="form_fax_no"  class="form-control span6" placeholder="Fax" ng-model="form_fax_no" ng-class="{ 'submitted' : (saveBranch.form_fax_no.$invalid && saveBranch.form_fax_no.$dirty) }" tabindex="6">
				</div>
				<div class="col-lg-4">
				</div>
			</div>

			<div class="row">
				<div class="col-lg-4">
					{{ Form::label('address', 'Address') }}
					<input type="text" name="form_address" class="form-control span6" placeholder="Address" ng-model="form_address" ng-class="{ 'submitted' : (saveBranch.form_address.$invalid && saveBranch.form_address.$dirty) }" required tabindex="3">
					<span ng-show="saveBranch.form_address.$invalid && saveBranch.form_address.$dirty" style="color:#FF0000">Address Required!</span>
				</div>
				<div class="col-lg-4">
						<div class="col-lg-2">
						{{ Form::label('personal', 'Personal') }}
						<input name="personal" type="checkbox" value="1" id="personal" ng-model="form_personal">
						<span class="errors" style="color:#FF0000">{{$errors->first('personal')}}</span>
					</div>
				</div>
				<div class="col-lg-2">
				</div>
				<div class="col-lg-2">
			
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					{{ Form::label('tel_no', 'Telephone') }}
					<input type="text" name="form_tel_no" class="form-control span6" placeholder="Telephone" ng-model="form_tel_no" ng-class="{ 'submitted' : (saveBranch.form_tel_no.$invalid && saveBranch.form_tel_no.$dirty) }" tabindex="4">
				</div>
				<div class="col-lg-4">
				
				</div>
				<div class="col-lg-4">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
				</div>
				<div class="col-lg-4">
				</div>
				<div class="col-lg-4" style="text-align: right;">
					<input type="button" ng-disabled="saveBranch.$invalid" ng-click="submitBranch()" value="Submit" class="btn btn-primary" tabindex="14"/>
				</div>
			</div>
		</form>
	</div>
</div>
</md-dialog>