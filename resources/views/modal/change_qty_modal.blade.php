<md-dialog aria-label="Privacy Policy" >
<div class="modalreport" id="modalreport" style="width: 600px;height: 250px; overflow:scroll">
	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" ng-click="close();">&times;</button>
	     <h3>Change Quantity</h3>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-lg-6">
				Current<input type="text" class="form-control" ng-model="changeqty" readonly> 
			</div>
			<div class="col-lg-6">
				To <input ng-model="qty_value" type="text" value="<% changeqty %>" class="form-control" >
			</div>
		</div>
		<input type="button" ng-click="changeQTYvalue()" value="Change QTY" class="btn btn-primary pull-right" tabindex="14"/>	
	</div>
</div>
</md-dialog>
