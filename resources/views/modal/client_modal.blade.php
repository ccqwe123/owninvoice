<md-dialog aria-label="Privacy Policy" >
<div class="modalreport" id="modalreport" style="width: 600px;height: 400px; overflow:scroll">
	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" ng-click="close();">&times;</button>
	     <h3>Client Name</h3>
	</div>
	<div class="modal-body">
		Search<input type="text" ng-model="search_client">
		<table class="table table-responsive">
			<tr ng-repeat="client in clients | filter:search_client">
				<td><a href="#" ng-click="setClient(client)"><% client.client_name %></a></td> 
			</tr>
		</table>
	</div>
</div>
</md-dialog>