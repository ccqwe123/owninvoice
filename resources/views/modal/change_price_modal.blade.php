<md-dialog aria-label="Privacy Policy" >
<div class="modalreport" id="modalreport" style="width: 600px;height: 250px; overflow:scroll">
	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" ng-click="close();">&times;</button>
	     <h3>Change Price</h3>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-lg-6">
				Current<input type="text" class="form-control" ng-model="changeprice" readonly> 
			</div>
			<div class="col-lg-6">
				To <input ng-model="price_value" type="text" value="<% changeprice %>" class="form-control" >
			</div>
		</div>
		<input type="button" ng-click="changePriceValue()" value="Change Price" class="btn btn-primary pull-right" tabindex="14"/>
	</div>
</div>
</md-dialog>
