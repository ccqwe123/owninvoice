<md-dialog aria-label="Privacy Policy" class="modalg active">
<md-dialog-content>
<div id="modalreport">
<style type="text/css">
	
.md-datepicker-button {
  display: none;
}
.md-datepicker-input-container {
	width: 100%;
	margin-left:0;
}

</style>
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" ng-click="close();">&times;</button>
		<h2>Balance <% balance | number:decimal_value %></h2> 
	</div>
	<div class="modal-body" style="overflow:hidden;">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">

					<div class="panel-heading">
						<h3 class="panel-title">Bank Deposit</h3>
					</div>
					<div class="panel-body">
						{{ Form::open(array('url' => '/payment/deposit', 'name' => 'depositPaymentForm',  'method' => 'store','files'=>true, 'role'=>'form')) }}
						<div class="col-lg-12">
							<input type="hidden" name="sales_invoice_id" ng-model="sales_invoice_id" ng-value="sales_invoice_id"/>
							<label for="cash_amount" class="col-lg-4 input-sm">Amount:</label>
							<div class="col-lg-8">								
								<input type="number" name="check_amount" ng-model="check_amount" class="form-control span6 input-sm" ng-class="{ 'submitted' : depositPaymentForm.check_amount.$invalid && depositPaymentForm.check_amount.$dirty}" max="<% balance-less %>" min="1" step=".01" required>
							</div>				
							<span class="errors" style="color:#FF0000">{{$errors->first('check_amount')}}</span>
						</div>
						<div class="col-lg-12" data-tap-disabled="true">
							<label for="bank_id" class="col-lg-4 input-sm">Bank</label>
							<div class="col-lg-8">
								<select class="form-control span6" id="bank_id" name="bank_id" style="margin:1px">
										@foreach ($banks as $x)
										<option value="{!! $x->id !!}">{!! $x->bank_name !!}</option>
									@endforeach
								</select>
							</div>
							<span class="errors" style="color:#FF0000">{{$errors->first('bank_id')}}</span>
						</div>	
						<div class="col-lg-12">
							<label for="check_date" class="col-lg-4 input-sm">Deposit Date:</label>
							<div class="col-lg-8">

								<input type="hidden" ng-model="check_date" name="check_date" value="<% check_date %>"/>
								<md-datepicker style="width: 100%;padding-left:0;padding-right:0;text-align:right" class="form-control input-sm" ng-model="check_date" name="check_date_dtp" id="check_date" md-placeholder="Enter date"/>
								 </md-datepicker>
							</div>
						</div>		
						<div class="col-lg-12">
							<label for="less" class="col-lg-4 input-sm">Less</label>
							<div class="col-lg-8">
								<input type="number" class="form-control input-sm" name="less" id="less" ng-model="less" ng-value="less">
							</div>
						</div>	
						<div class="col-lg-12">
							<label for="less" class="col-lg-4 input-sm">OR Number</label>
							<div class="col-lg-8">
								<input type="text" class="form-control input-sm" name="or_number" id="or_number" ng-model="or_number" ng-value="or_number" required>
							</div>
						</div>	
						<div class="col-lg-12">
							<label for="check_date" class="col-lg-4 input-sm">Deposit Slip</label>
							<div class="col-lg-8">
								{!! Form::file('photo') !!}
								<p class="errors">{{$errors->first('photo')}}</p>

							</div>
						</div>		
						<div class="col-lg-12">			
							<label class="col-lg-4 input-sm"></label>				
							<div class="col-lg-8">
								<input type="submit" value="Submit" class="btn btn-default" ng-disabled="depositPaymentForm.$invalid" />
								<input type="hidden" class="form-control input-sm" name="or_number" value="<% or_number %>" ng-value="or_number">
								<input type="hidden" class="form-control input-sm" name="less" value="<% less %>" ng-value="less">
							</div>
							<span class="errors" style="color:#FF0000">{{$errors->first('check_number')}}</span>
						</div>	
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</md-dialog-content>
</md-dialog>