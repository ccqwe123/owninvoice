<md-dialog aria-label="Privacy Policy" >
<style type="text/css">
table {
        table-layout: fixed;
        word-wrap: break-word;
    }
        table th, table td {
            overflow: hidden;
        }
</style>
<div class="modalreport" id="modalreport" style="width: 800px;height: 710px;">
	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" ng-click="close();">&times;</button>
	     <h3>Products Name</h3>
	</div>
	<div class="modal-body">
		<strong>Search</strong> &nbsp; <input type="text" ng-model="search_product">
		<table class="table table-striped">
			<thead>
				<tr>
					<th class="col-xs-2">Product Code</th>
					<th class="col-xs-6">Product Name</th>		
					<th class="col-xs-2">Product Price</th>	
					<th class="col-xs-2">QTY</th>	
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="product in products">
					<td class="col-xs-2 text-nowrap"><% product.prod_code %></td>
					<td class="col-xs-6 text-nowrap"><% product.prod_name %></td>						
					<td class="col-xs-2 text-nowrap"><% product.prod_price| number : decimal_value %></td>									
					<td class="col-xs-2 text-nowrap"><input type="number" class="form-control" value="0" ng-model="product.qty" min="0" /></td>	
				</tr>		
			</tbody>				
		</table>
		<button class="btn btn-primary pull-right" ng-click="addToCartAll()">Add</button>
	</div>
</div>
</md-dialog>