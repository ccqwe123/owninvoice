<md-dialog aria-label="Privacy Policy" >
<div class="modalreport" id="modalreport" style="width: 800px;height: 400px; overflow:scroll">

	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" ng-click="close();">&times;</button>
	     <h3>New Product</h3>
	</div>
	<div class="modal-body">		
		<form name="saveProduct" action="#" ng-submit="submitProduct()">
			<div class="row">
				<div class="col-lg-4">
					{{ Form::label('prod_code', 'Product Code') }}
					<input type="text" name="form_prod_code" class="form-control span6" placeholder="Product Code" ng-model="form_prod_code" ng-class="{ 'submitted' : (saveProduct.form_prod_code.$invalid && saveProduct.form_prod_code.$dirty) }" required tabindex="1">
					<span ng-show="saveProduct.form_prod_code.$invalid && saveProduct.form_prod_code.$dirty" style="color:#FF0000">Product Code required!</span>
				</div>
				<div class="col-lg-4">
					{{ Form::label('prod_name', 'Product Name') }}
					<input type="text" name="form_prod_name" class="form-control span6" placeholder="Product Name" ng-model="form_prod_name" ng-class="{ 'submitted' : (saveProduct.form_prod_name.$invalid && saveProduct.form_prod_name.$dirty) }" required tabindex="1">
					<span ng-show="saveProduct.form_prod_name.$invalid && saveProduct.form_prod_name.$dirty" style="color:#FF0000">Product name required!</span>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					{{ Form::label('prod_description', 'Description') }}
					<input type="text" name="form_prod_description" class="form-control span6" placeholder="Description" ng-model="form_prod_description" ng-class="{ 'submitted' : (saveProduct.form_prod_description.$invalid && saveProduct.form_prod_description.$dirty) }"  tabindex="3">
					<span ng-show="saveProduct.form_prod_description$invalid && saveProduct.form_prod_description.$dirty" style="color:#FF0000">Address Required!</span>
				</div>
				<div class="col-lg-4">
					{{ Form::label('prod_price', 'Price') }}
					<input type="number" name="form_prod_price"  class="form-control span6" placeholder="0.00" ng-model="form_prod_price" ng-class="{ 'submitted' : (saveProduct.form_prod_price.$invalid && saveProduct.form_prod_price.$dirty) }" tabindex="6" required>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
				</div>
				<div class="col-lg-4">
				</div>
				<div class="col-lg-4" style="text-align: right;">
					<input type="button" ng-disabled="saveProduct.$invalid" ng-click="submitProduct()" value="Submit" class="btn btn-primary" tabindex="14"/>
				</div>
			</div>
		</form>
	</div>
</div>
</md-dialog>