<md-dialog aria-label="Privacy Policy" class="active">
<md-dialog-content>
<div id="modalreport">
<style type="text/css">
	
.md-datepicker-button {
  display: none;
}
.md-datepicker-input-container {
	width: 100%;
	margin-left:0;
}
</style>
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" ng-click="close();">&times;</button>
		<h2>Balance <% balance | number:decimal_value %></h2> 
	</div>
	<div class="modal-body" style="overflow:hidden;">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">

					<div class="panel-heading">
						<h3 class="panel-title">Cash Payment</h3>
					</div>
					<div class="panel-body">
						{{ Form::open(array('url' => '/payment/cash', 'name' => 'cashPaymentForm', 'method' => 'store','files'=>true,'ng-class'=>'{error: cashPaymentForm.name.$invalid}')) }}
						<div class="row">
							<div class="col-lg-6">
								<div class="row">
									<div class="col-lg-12">
										{{ Form::label('cash_amount', 'Amount') }}						
										<input type="number" class="form-control" id="cash_amount" name="cash_amount" max="<% balance-less %>" min="1" required ng-class="{ 'submitted' : cashPaymentForm.cash_amount.$invalid && cashPaymentForm.cash_amount.$dirty}" ng-model="cash_amount" step=".01"/>								
										<span ng-show="cashPaymentForm.cash_amount.$invalid && cashPaymentForm.cash_amount.$dirty" style="color:#FF0000">Invalid Input</span>
										<input type="hidden" name="sales_invoice_id" ng-model="sales_invoice_id" ng-value="sales_invoice_id"/>
										<input type="hidden" class="form-control input-sm" name="or_number"  value="<% or_number %>" ng-value="or_number" required>
										<input type="hidden" class="form-control input-sm" name="less"  value="<% less %>" ng-value="less">
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										{{ Form::label('photo', 'Photo') }}
										{!! Form::file('photo') !!}
										<p class="errors">{{$errors->first('photo')}}</p>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<input type="submit" value="Submit" class="btn btn-default" ng-disabled="cashPaymentForm.$invalid" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="row">
									<div class="col-lg-12">
										{{ Form::label('less', 'Less') }}						
										<input type="number" class="form-control input-sm" name="less" id="less" ng-model="less" ng-value="less">										
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										{{ Form::label('or_number', 'OR Number') }}						
										<input type="text" class="form-control input-sm" name="or_number" id="or_number" ng-model="or_number" ng-value="or_number" required>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										
									</div>
								</div>
							</div>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</md-dialog-content>
</md-dialog>