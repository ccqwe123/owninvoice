<md-dialog aria-label="Privacy Policy" >
<div class="modalreport" id="modalreport" style="width: 500px;height: 250px; overflow:scroll">
	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" ng-click="close();">&times;</button>
	     <h3>Transfer Invoice</h3>	     
	</div>
	<div class="modal-body">
		{{ Form::open(array('url' => '/invoice/transfer/', 'name' => 'transferForm', 'method' => 'post')) }}
			<div class="row">
				<div class="col-lg-6">					
					{{ Form::label('sales_invoice', 'Old SI No') }}
					<div class="input-group input-group-unstyled">
						<input type="text" class="form-control" id="old_sales_invoice" name="old_sales_invoice" value="<% si %>" readonly/>
					</div>				
				</div>
				<div class="col-lg-6">					
					{{ Form::label('sales_invoice', 'New SI No') }}
					<div class="input-group input-group-unstyled">
						<input type="text" class="form-control" id="sales_invoice" name="sales_invoice" ng-model="sales_invoice" required/>					
					</div>
						<input type="hidden" name="sales_invoice_id" ng-model="sales_invoice_id" ng-value="sales_invoice_id"/>	
						<input type="hidden" name="si" ng-model="si" ng-value="si"/>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
				</div>
				<div class="col-lg-6">
					<input type="submit" value="Submit" class="btn btn-primary" ng-disabled="transferForm.$invalid" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
				</div>
			</div>
		{!! Form::close() !!}
	</div>
</div>
</md-dialog>