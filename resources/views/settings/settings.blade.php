@extends('layout.app')
@section('title', 'Edit Settings')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px;" class="ng-cloak">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Edit Settings <small></small>
			</h1>
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-pencil" aria-hidden="true"></i> Edit Settings
				</li>
			</ol>
		</div>
	</div>
	<div class="row">
		@if(Session::has('flash_message'))
			<div class="alert alert-success">{{Session::get('flash_message')}}</div>
		@endif

		@if(Session::has('flash_error'))
			<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
		@endif
	</div>

	{{ Form::open(array('url' => '/settings/1', 'method' => 'PUT','files'=>true)) }}
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label($company_logo->name, $company_logo->description) }}
			<image src="/uploads/{{$company_logo->value}}" style="height:100px;width: 300px"/>
			{!! Form::file('photo') !!}
			<input type="hidden" name="settings_id[]" value="{{ $company_logo->id }}" />
			<input type="hidden" name="settings_name[]" value="{{ $company_logo->name }}" />
			<span class="errors" style="color:#FF0000">{{$errors->first($company_logo->name)}}</span>
		</div>
	</div>
	<div class="row">	
		<div class="col-lg-4">
			{{ Form::label($software_name->name, $software_name->description) }}
			{{ Form::text($software_name->name,$software_name->value,array('class'=>'form-control span6','placeholder' => $software_name->description)) }}
			<input type="hidden" name="settings_id[]" value="{{ $software_name->id }}" />
			<input type="hidden" name="settings_name[]" value="{{ $software_name->name }}" />
			<span class="errors" style="color:#FF0000">{{$errors->first($software_name->name)}}</span>
		</div>
		<div class="col-lg-4">
			{{ Form::label($company_name->name, $company_name->description) }}
			{{ Form::text($company_name->name,$company_name->value,array('class'=>'form-control span6','placeholder' => $company_name->description)) }}
			<input type="hidden" name="settings_id[]" value="{{ $company_name->id }}" />
			<input type="hidden" name="settings_name[]" value="{{ $company_name->name }}" />
			<span class="errors" style="color:#FF0000">{{$errors->first($company_name->name)}}</span>
		</div>
		<div class="col-lg-4">
			{{ Form::label($company_address->name, $company_address->description) }}
			{{ Form::text($company_address->name,$company_address->value,array('class'=>'form-control span6','placeholder' => $company_address->description)) }}
			<input type="hidden" name="settings_id[]" value="{{ $company_address->id }}" />
			<input type="hidden" name="settings_name[]" value="{{ $company_address->name }}" />
			<span class="errors" style="color:#FF0000">{{$errors->first($company_address->name)}}</span>
		</div>
		<div class="col-lg-4">
			{{ Form::label($company_phone->name, $company_phone->description) }}
			{{ Form::text($company_phone->name,$company_phone->value,array('class'=>'form-control span6','placeholder' => $company_phone->description)) }}
			<input type="hidden" name="settings_id[]" value="{{ $company_phone->id }}" />
			<input type="hidden" name="settings_name[]" value="{{ $company_phone->name }}" />
			<span class="errors" style="color:#FF0000">{{$errors->first($company_phone->name)}}</span>
		</div>
		<div class="col-lg-4">
			{{ Form::label($tax_rate->name, $tax_rate->description) }}
			{{ Form::text($tax_rate->name,$tax_rate->value,array('class'=>'form-control span6','placeholder' => $tax_rate->description)) }}
			<input type="hidden" name="settings_id[]" value="{{ $tax_rate->id }}" />
			<input type="hidden" name="settings_name[]" value="{{ $tax_rate->name }}" />
			<span class="errors" style="color:#FF0000">{{$errors->first($tax_rate->name)}}</span>
		</div>
		<div class="col-lg-4">
			{{ Form::label($decimal->name, $decimal->description) }}
			{{ Form::text($decimal->name,$decimal->value,array('class'=>'form-control span6','placeholder' => $decimal->description)) }}
			<input type="hidden" name="settings_id[]" value="{{ $decimal->id }}" />
			<input type="hidden" name="settings_name[]" value="{{ $decimal->name }}" />
			<span class="errors" style="color:#FF0000">{{$errors->first($decimal->name)}}</span>
		</div>
		<div class="col-lg-4">
			{{ Form::label($font_size->name, $font_size->description) }}
			{{ Form::text($font_size->name,$font_size->value,array('class'=>'form-control span6','placeholder' => $font_size->description)) }}
			<input type="hidden" name="settings_id[]" value="{{ $font_size->id }}" />
			<input type="hidden" name="settings_name[]" value="{{ $font_size->name }}" />
			<span class="errors" style="color:#FF0000">{{$errors->first($font_size->name)}}</span>
		</div>	
	</div>
	<div class="row">
		<div class="col-lg-12" >
			{{ Form::label($letter_head->name, $letter_head->description) }}
			<textarea class="input-block-level" id="summernote" name="{{$letter_head->name}}">
			{{ $letter_head->value }}
			</textarea>
			<input type="hidden" name="settings_id[]" value="{{ $letter_head->id }}" />
			<input type="hidden" name="settings_name[]" value="{{ $letter_head->name }}" />
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12" >
			{{ Form::label($soa_footer->name, $soa_footer->description) }}
			<textarea class="input-block-level" id="summernote2" name="{{$soa_footer->name}}">
			{{ $soa_footer->value }}
			</textarea>
			<input type="hidden" name="settings_id[]" value="{{ $soa_footer->id }}" />
			<input type="hidden" name="settings_name[]" value="{{ $soa_footer->name }}" />
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12" >
			{{ Form::label($report_footer->name, $report_footer->description) }}
			<textarea class="input-block-level" id="summernote3" name="{{$report_footer->name}}">
			{{ $report_footer->value }}
			</textarea>
			<input type="hidden" name="settings_id[]" value="{{ $report_footer->id }}" />
			<input type="hidden" name="settings_name[]" value="{{ $report_footer->name }}" />
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			<input type="submit" class="btn btn-primary" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
		</div>
	</div>
	{!! Form::close() !!}
</div>
@stop