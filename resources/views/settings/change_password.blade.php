@extends('layout.app')
@section('title', 'Change Password')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Change Password <small></small>
			</h1>
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-pencil" aria-hidden="true"></i> Change Password
				</li>
			</ol>
		</div>
	</div>
	<div class="row">
		@if(Session::has('flash_message'))
			<div class="alert alert-success">{{Session::get('flash_message')}}</div>
		@endif

		@if(Session::has('flash_error'))
			<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
		@endif
	</div>
	{{ Form::open(array('url' => '/settings/password/update', 'method' => 'POST')) }}
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('old_password', 'Old Password') }}
			{{ Form::password('old_password',array('class'=>'form-control span6','placeholder' => 'Old Password')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('old_password')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('new_password', 'New Password') }}
			{{ Form::password('new_password',array('class'=>'form-control span6','placeholder' => 'New Password')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('new_password')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('repeat_password', 'Repeat Password') }}
			{{ Form::password('repeat_password',array('class'=>'form-control span6','placeholder' => 'Repeat Password')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('repeat_password')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			<input type="submit" value="Submit" class="btn btn-default" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
		</div>
	</div>
	{!! Form::close() !!}
</div>
@stop