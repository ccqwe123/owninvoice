@extends('layout.app')
@section('title', 'Logs')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px;" ng-controller="ActivityLogController" @if(!isset($logs)) ng-init="show_form=true;btn_caption='Hide Form'" @else ng-init="btn_caption='Show Form'" @endif >
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Logs<small></small>
			</h1>
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-pencil" aria-hidden="true"></i> Logs
				</li>
				<button id="mybutton" ng-click="showReportForm()"><% btn_caption %></button>
			</ol>
		</div>
	</div>
	<div ng-show="show_form">
		{{ Form::open(array('url' => '/logs', 'method' => 'GET')) }}
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-4">	
						{{ Form::label('log_type', 'Log Type') }}
						<select name="log_type" class="form-control">
							<option value="0">ALL</option>
							<option value="void">void</option>
							<option value="insert">insert</option>
							<option value="update">update</option>
							<option value="delete">delete</option>
							<option value="delete">delete</option>
							<option value="login-success">login-success</option>
							<option value="login-failure">login-failure</option>
							<option value="logout">logout</option>
							<option value="others">others</option>
						</select>
					</div>
					<div class="col-lg-4">
						{{ Form::label('from_date', 'From :') }}
						<div class="controls input-group date" id='dtp1'>
							<input type='text' placeholder="From :" name="from_date" class="form-control span6"  name="from_date" type="from_date" id="from_date" required/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>

						{{ Form::label('to_date', 'To :') }}
						<div class="controls input-group date" id='dtp2'>
							<input type='text' placeholder="To :" name="to_date" class="form-control span6"  name="to_date" type="to_date"  id="to_date" required />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row top10">
			<div class="col-lg-4">
				<input type="submit" class="btn btn-primary" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
			</div>
		</div>
		{!! Form::close() !!}
	</div>
	@if(isset($logs) && count($logs)>0)
	<div class="row">
			<div class="col-lg-12">
				<div class="row">
					@if(isset($logs))					
					<div class="col-lg-1 col-sm-3">
						<button type="button" class="btn btn-default" ng-click="printDiv('printable')">Print</button>
					</div>
					@endif
				</div>
			</div>
		</div>
		<div id="printable" style="display:none">
			<h2 class="text-center" style="line-height: 0.6em">{{ Session::get('company_name') }}</h2>
			<p class="text-center" style="line-height: 0.6em">{{ Session::get('company_address') }}</p>
			<p class="text-center" style="line-height: 0.6em">{{ Session::get('company_phone') }}</p>
			<h3 class="text-center">Client Reports</h3>
			<div class="row">
				<div class="col-lg-12">
				</div>
			</div>
			<table class="table" style="border: 1px solid black; text-align: center;width:100%">
				<tr style="border: 1px solid black;">
					<th style="font-size:0.500em">username</th>
					<th style="font-size:0.500em">entry</th>
					<th style="font-size:0.500em">comment</th>
					<th style="font-size:0.500em">family</th>
					<th style="font-size:0.500em">Date</th>
				</tr>
				  	@foreach ($logs_all as $x)
					<tr style="border: 1px solid black;">
						<td style="font-size:0.500em">{{ $x->username }}</td>
						<td style="font-size:0.500em">{{ $x->entry }}</td>
						<td style="font-size:0.500em">{{ $x->comment }}</td>
						<td style="font-size:0.500em">{{ $x->family }}</td>
						<td style="font-size:0.500em">{{ $x->created_at }}</td>
					</tr>
					@endforeach 
			</table>
		</div>
		<table class="table" style="border: 1px solid black;overflow: scroll;">
			<tr style="border: 1px solid black;">
				<th>username</th>
				<th>entry</th>
				<th>comment</th>
				<th>family</th>
				<th>Date</th>
			</tr>
			  	@foreach ($logs as $x)
				<tr style="border: 1px solid black;">
					<td><h6>{{ $x->username }}</h6></td>
					<td><h6>{{ $x->entry }}</h6></td>
					<td><h6>{{ $x->comment }}</h6></td>
					<td><h6>{{ $x->family }}</h6></td>
					<td><h6>{{ $x->created_at }}</h6></td>
				</tr>
				@endforeach 
		</table>

		<div style="text-align: center">
			{{ $logs->appends(
				[
				'log_type' =>  (isset($_GET["log_type"]) ? $_GET["log_type"]:"" ),
				'from_date' =>  (isset($_GET["from_date"]) ? $_GET["from_date"]:"" ),
				'to_date' =>  (isset($_GET["to_date"]) ? $_GET["to_date"]:"" )
				]
				)->links() }}
		</div>
	@endif
</div>
@stop