@extends('layout.app')
@section('title', 'Edit Client')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Client <small></small>
			</h1>
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-pencil" aria-hidden="true"></i> Edit Client
				</li>
			</ol>
		</div>
	</div>
	<div class="row">
		@if(Session::has('flash_message'))
			<div class="alert alert-success">{{Session::get('flash_message')}}</div>
		@endif
		@if(Session::has('flash_error'))
			<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
		@endif
	</div>
	{{ Form::open(array('url' => '/clients/'.$client[0]->id, 'method' => 'PUT')) }}

	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('client_name', 'Client Name') }}
			{{ Form::text('client_name',$client[0]->client_name,array('class'=>'form-control span6','placeholder' => 'Client Name')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('client_name')}}</span>
		</div>
	</div>
	@if($client[0]->personal==1)
		<div class="row">
			<div class="col-lg-4">
				{{ Form::label('address', 'Address') }}
				{{ Form::text('address',$branch->address,array('class'=>'form-control span6','placeholder' => 'Address')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('address')}}</span>
			</div>
		</div>
	@endif
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('email', 'Email') }}
			{{ Form::text('email',$client[0]->email,array('class'=>'form-control span6','placeholder' => 'Email')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('email')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('tin', 'TIN') }}
			{{ Form::text('tin',$client[0]->tin,array('class'=>'form-control span6','placeholder' => 'TIN')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('tin')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('credit_limit', 'Credit Limit') }}
			{{ Form::text('credit_limit',$client[0]->credit_limit,array('class'=>'form-control span6','placeholder' => 'Credit Limit')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('credit_limit')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('terms', 'Terms(days)') }}
			{{ Form::text('terms',$client[0]->terms,array('class'=>'form-control span6','placeholder' => 'Terms (days)')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('terms')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-2">
			{{ Form::label('vatable', 'Vatable') }}
			{{ Form::checkbox('vatable','1',$client[0]->vatable) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('vatable')}}</span>
		</div>
		<div class="col-lg-2">
			{{ Form::label('personal', 'Personal') }}
			{{ Form::checkbox('personal','1',$client[0]->personal) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('personal')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			{{ Form::label('account_manager', 'Account Manager') }}
			<select class="form-control span6" name="account_manager_id">
				<option value="0"></option>
				@foreach ($account_managers as $x)
					<option value="{!! $x->id !!}"<?php if ($client[0]->account_manager_id==$x->id)	echo ' selected';?>>{!! $x->first_name !!} {!! $x->last_name !!}</option>
				@endforeach
			</select>
			<span class="errors" style="color:#FF0000">{{$errors->first('account_manager')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			<input type="submit" class="btn btn-default" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
		</div>
	</div>
	{!! Form::close() !!}
</div>
@stop