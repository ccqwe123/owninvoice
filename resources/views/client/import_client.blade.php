@extends('layout.app')
@section('title', 'Import Product')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Products <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif
			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-shopping-basket" aria-hidden="true"></i> Import Product by csv
				</li>
			</ol>
		</div>
	</div>
	<form style="" action="{{ URL::to('product/import/csv') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
		<input type="file" name="import_file" />
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<button class="btn btn-primary" onclick="this.disabled=true;this.value='Uploading, please wait...';this.form.submit();">Import File</button>
	</form>
</div>
@stop