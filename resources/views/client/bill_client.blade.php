 @extends('layout.app')
@section('title', 'Client List')
@section('app_name', Session::get('software_name'))
@section('content')

<style type="text/css">
	
	#wrap { width: 600px; height: 800px; padding: 0; overflow: hidden; }
#print-iframe { width: 800px; height: 1200px; border: 1px solid black; }
#print-iframe { zoom: 0.75; -moz-transform: scale(0.75); -moz-transform-origin: 0 0; }
</style>
<script>
function printBill()
{
	document.getElementById("print-iframe").contentWindow.print();
}
</script>

<div style=" height: 800px; overflow: scroll;" ng-controller="PrintController">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Bill Client <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('comment', 'Note:') }}
			<textarea class="form-control" rows="5" id="comment" ng:change="updateIframe()" ng:model="data"></textarea>
			<button class="btn btn-primary" onclick="printBill()"> Print</button>
			<button id="sendemailbtn" onclick="this.disabled=true;this.value='Submitted, please wait...';" class="btn btn-primary" ng-click="sendemail({{$id}},{{$client_branch->id}})"> Send Email <i ng-show="sending" class="fa fa-refresh fa-spin"></i></button>
		</div>
		<div id="wrap" class="embed-responsive embed-responsive-16by9">
		  <iframe class="embed-responsive-item" id="print-iframe" src="{{ url('/clients/'.$id.'/branch/'.$client_branch->id.'/bill_preview') }}" width="100%"></iframe>
		</div>
	</div>
</div>
@stop