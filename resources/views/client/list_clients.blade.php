@extends('layout.app')
@section('title', 'Client List')
@section('app_name', Session::get('software_name'))
@section('content')


<div style=" height: 800px; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Clients <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-search"></i> Client Search
				</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::open(array('url' => '/clients', 'method' => 'get')) }} 
			<div class="input-group margin-bottom-sm">
			    <span class="input-group-addon"><button class="fa fa-search" style="border:none; background-color:transparent;"></button></span>
			    {{ Form::text('search','',array('class'=>'form-control span6','placeholder' => 'Search')) }}
			    <span class="errors" style="color:#FF0000">{{$errors->first('search')}}</span>
			</div>
			{{ Form::close() }}
		
		</div>
		<div class="col-lg-4">
			<a href="/clients/create"><button class="btn btn-primary">Add Client</button></a>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-shopping-basket"></i> Client List
				</li>
			</ol>
			
		</div>
	</div>
	<table class="table" style="border: 1px solid black;white-space: nowrap;font-size: {{ Session('font_size') }}px;">
		<tr style="border: 1px solid black;">
			<th>Client Name</th>
			<th>Email</th>
			<th>Credit Limit</th>
			<th>Terms(days)</th>
			<th>Total Invoice Amount</th>
			<th>Total Balance Due</th>
			<th></th>
		</tr>
		  	@foreach ($clients as $x)
			<tr style="border: 1px solid black;">
				<td>{{ $x->client_name }}</td>
				<td>{{ $x->email }}</td>
				<td>{{ number_format($x->credit_limit,Session('decimal')) }}</td>
				<td>{{ $x->terms }}</td>
				<td>{{ number_format($x->amount_total,Session('decimal')) }}</td>
				<td>{{ number_format($x->amount_total-($x->cash_amount+$x->check_amount),Session('decimal')) }}</td>				
				<td>
					<div class="dropdown">
						<button class="btn btn-primary dropdown-toggle btn-sm" type="button" data-toggle="dropdown">Actions
						<span class="caret"></span></button>
						<ul class="dropdown-menu dropdown-menu-right">
							@if($x->personal<>1)
								<li><a href="/clients/{{$x->id}}/branch" title="Branch"><i class="fa fa-university" aria-hidden="true"></i>Branch</a></li>
							@else
								<li><a href="/clients/{{$x->id}}/branch/0/bill" title="Branch"><i class="fa fa-money" aria-hidden="true"></i>SOA</a></li>
							@endif
							<li><a href="/clients/{{$x->id}}/edit"><i class="fa fa-pencil margin-right"></i>Edit</a></li>
							<li><a href="/client-delete/{{$x->id}}" class="confirmation"><i class="fa fa-trash margin-right"></i>Delete</a></li>
						</ul>
					</div>
				</td>
			</tr>
			@endforeach 
	</table>
	<div style="text-align: center">
		{{ $clients->links() }}
	</div>
</div>
@stop