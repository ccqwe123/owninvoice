@extends('layout.app')
@section('title', 'Add Client')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 900px;width: 100%; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Clients <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-shopping-basket" aria-hidden="true"></i> Add Client
				</li>
			</ol>
		</div>
	</div>
	{{ Form::open(array('url' => '/clients', 'method' => 'store')) }}
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('client_name', 'Client Name') }}
			{{ Form::text('client_name','',array('class'=>'form-control span6','placeholder' => 'Client Name')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('client_name')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('contact_person', 'Contact Person') }}
			{{ Form::text('contact_person','',array('class'=>'form-control span6','placeholder' => 'Contact Person')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('contact_person')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('address', 'Address') }}
			{{ Form::text('address','',array('class'=>'form-control span6','placeholder' => 'Address')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('address')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('tel_no', 'Telephone') }}
			{{ Form::text('tel_no','',array('class'=>'form-control span6','placeholder' => 'Telephone')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('tel_no')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('mobile_no', 'Mobile') }}
			{{ Form::text('mobile_no','',array('class'=>'form-control span6','placeholder' => 'Mobile')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('mobile_no')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('fax_no', 'Fax') }}
			{{ Form::text('fax_no','',array('class'=>'form-control span6','placeholder' => 'Fax')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('fax_no')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('email', 'Email') }}
			{{ Form::text('email','',array('class'=>'form-control span6','placeholder' => 'Email')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('email')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('tin', 'TIN') }}
			{{ Form::text('tin','',array('class'=>'form-control span6','placeholder' => 'TIN')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('tin')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('credit_limit', 'Credit Limit') }}
			{{ Form::text('credit_limit','30000',array('class'=>'form-control span6','placeholder' => 'Credit Limit')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('credit_limit')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('terms', 'Terms(days)') }}
			{{ Form::text('terms','30',array('class'=>'form-control span6','placeholder' => 'Terms (days)')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('terms')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-2">
			{{ Form::label('vatable', 'Vatable') }}
			{{ Form::checkbox('vatable','1') }}
			<span class="errors" style="color:#FF0000">{{$errors->first('vatable')}}</span>
		</div>
		<div class="col-lg-2">
			{{ Form::label('personal', 'Personal') }}
			{{ Form::checkbox('personal','1') }}
			<span class="errors" style="color:#FF0000">{{$errors->first('personal')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			{{ Form::label('account_manager_id', 'Account Manager') }}
			<select class="form-control span6" id="account_manager_id" name="account_manager_id" required>
				@foreach ($users as $x)
					<option value="{!! $x->id !!}">{!! $x->first_name !!} {!! $x->last_name !!}</option>
				@endforeach
			</select>
			<span class="errors" style="color:#FF0000">{{$errors->first('account_manager_id')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			<input type="submit" value="Submit" class="btn btn-primary" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
		</div>
	</div>
	{!! Form::close() !!}
</div>
@stop