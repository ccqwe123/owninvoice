<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<head>
<style type="text/css">
@page {
        margin: 0px 50px 0px 50px;
    }
body {
    height: 100%;
     margin: 0px 50px 0px 50px;
    background-size: 100% 100%;
    background-repeat: no-repeat;
}
header{ height:50px; background:lightcyan; }
body {
  position: relative;
}

body::after {
  content: '';
  display: block;
  height: 300px; 
}

footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 300px;
  padding-left: 50px;
  padding-right: 50px;
}
 @media print{
        .table>tbody>tr.danger>td{
            background-color: #f2dede !important;
            -webkit-print-color-adjust:exact ;
        }
    }
 
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.5/angular.js"></script>
<link href="{{ URL::asset('css/angular-material.min.css') }}" rel="stylesheet">
<script src="{{ URL::asset('js/angular-animate.min.js') }}"></script>
<script src="{{ URL::asset('js/angular-aria.min.js') }}"></script>
<script src="{{ URL::asset('js/angular-messages.min.js') }}"></script>
<script src="{{ URL::asset('js/angular-material.min.js') }}"></script>
<script src="{{ URL::asset('js/ui-bootstrap.js') }}"></script>
<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>

<body ng-app="app" ng-cloak ng-controller="iFrameController">

  <article>
  	<div id="printable">
	<div class="row">
		<div class="col-lg-12">
		{!! Session('letter_head') !!}
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-lg-12">
			<table>
				<tr>
					<td>Date:</td>
					<td>{{ date("Y-m-d") }}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>To:</td>
					<td>Accounting Deparment</td>
				</tr>
				<tr>
					<td></td>
					<td>{{ $client->client_name }}</td>
				</tr>
				<tr>
					<td></td>
					<td><?php if(count($sales_invoice)>0 && isset($sales_invoice)) { echo $sales_invoice[0]->address; } ?></td>		
				</tr>
			</table>
			<br/><br/>
			Sir/Madam:<br/>
			This is to update you on your account with us<br/>
		</div>
	</div>
	<br/>
	<br/>
	
	<table class="table" style="border: 1px solid black;font-size: {{Session('font_size')}}px;">
		<tr style="border: 1px solid black;font-size: {{Session('font_size')}}px;">
			<th>LN</th>
			<th>SI No.</th>
			<th>Invoice Date</th>
			<th>PO No.</th>
			<th style="text-align: right;width: 100px">Amount Due</th>
			<th style="text-align: right;width: 100px">Balance</th>
			<th>Terms</th>
			<th>Due Date</th>
		</tr>
			<?php 
			$i=1;
			$total =0;
			$balance =0;
			 ?>
			@foreach ($sales_invoice as $x)
			<tr style="border: 1px solid black;" @if ($x->status=='cancelled') class="warning" @elseif ($x->status=='paid') class="success" @elseif ($x->status=='over-due') class="danger" @endif >
				<td>{{ $i }}</td>
				<td>{{ $x->si }}</td>
				<td>{{ $x->invoice_date }}</td>
				<td>{{ $x->po }}</td>
				<td style="text-align: right;width: 100px">{{ number_format($x->amount_total,Session('decimal')) }}</td>
				<td style="text-align: right;width: 100px">{{ number_format($x->balance,Session('decimal')) }}</td>
				<td>{{ $x->terms }}</td>
				<td>{{ $x->due_date }}</td>
				<?php 
					$total = $total + $x->amount_total;
					$balance = $balance + $x->balance;
				 ?>
			</tr>
			<?php $i++ ?>
			@endforeach 
			<tr style="border: 1px solid black;">
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td style="text-align: right;width: 100px ">{{ number_format($total,Session('decimal')) }}</td>
				<td style="text-align: right;width: 100px ">{{ number_format($balance,Session('decimal')) }}</td>
				<td></td>
				<td></td>
			</tr>
	</table>
</div>
<div class="row">
		<div class="col-lg-12">
			<pre style="border: 0; background-color: transparent;font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;"><% value %></pre>
		</div>
	</div>
  </article>
  <footer>{!! Session('soa_footer') !!}</footer>
  	<script src="{{ URL::asset('js/app.js') }}"></script>
	<script src="{{ URL::asset('js/controllers.js') }}"></script>
	<script src="{{ URL::asset('js/providers.js') }}"></script>
	<script src="{{ URL::asset('js/constants.js') }}"></script>
</body>

