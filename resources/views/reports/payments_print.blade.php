<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page { margin: 10; }
td {
	font-size: {{ Session('font_size') }}px;
}
th {
	font-size: {{ Session('font_size') }}px;
}
</style>
<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
<div id="printable">
	<div class="row">
		<div class="col-lg-12">
		{!! Session('letter_head') !!}
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
		</div>
	</div>
	<table class="table .table-bordered" style="overflow: scroll;white-space: nowrap;">
		<tr>
			<th>Payment ID</th>
			<th>Client</th>
			<th>SI</th>
			<th>Pay Type</th>
			<th>Payment Amount</th>
			<th>Less</th>
			<th>Payment Date</th>
		</tr>
		<?php 
		$amt = 0;
		$tax = 0;
		$bal = 0;
		?>
	  	@foreach ($payments_all as $x)
		<tr>
			<td>{{ $x->id }}</td>
			<td>{{ $x->client_name }} - {{$x->branch_name}}</td>
			<td>{{ $x->si }}</td>
			<td>{{ $x->pay_type }}</td>
			<td style="text-align: right">{{ number_format($x->payment_amount,Session('decimal')) }}</td>
			<td style="text-align: right">{{ number_format($x->less,Session('decimal')) }}</td>
			<td>{{ $x->payment_date }}</td></td>
			<?php
			$amt = $amt + $x->payment_amount;
			$tax = $tax + $x->less;
			?>
		</tr>
		@endforeach 
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td style="text-align: right">{{ number_format($amt,Session('decimal')) }}</td>
			<td style="text-align: right">{{ number_format($tax,Session('decimal')) }}</td>
			<td></td></td>
		</tr>
	</table>
	<div class="row">
		<div class="col-lg-12">
		{!! Session('report_footer') !!}
		</div>
	</div>
</div>