<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page { margin: 10; }
td {
	font-size: {{ Session('font_size') }}px;
}
th {
	font-size: {{ Session('font_size') }}px;
}
</style>
<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
<div id="printable">
	<div class="row">
		<div class="col-lg-12">
		{!! Session('letter_head') !!}
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
		</div>
	</div>
	<table class="table .table-bordered" style="overflow: scroll;white-space: nowrap;">
		<tr>
			<th>Product Code</th>
			<th>Product Name</th>
			<th>Description</th>
			<th>Price</th>
			<th>Total Sold</th>
		</tr>
	  	@foreach ($products_all as $x)
		<tr>
			<td>{{ $x->prod_code }}</td>
			<td>{{ $x->prod_name }}</td>
			<td>{{ $x->prod_description }}</td>
			<td style="text-align: right">{{ number_format($x->prod_price,Session('decimal')) }}</td>
			<td>{{ $x->sold }}</td>
		</tr>
		@endforeach 
	</table>
	<div class="row">
		<div class="col-lg-12">
		{!! Session('report_footer') !!}
		</div>
	</div>
</div>