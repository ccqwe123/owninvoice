@extends('layout.app')
@section('title', 'Invoice Reports')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px;" ng-controller="SalesReportController" @if(!isset($sales_invoice)) ng-init="show_form=true;btn_caption='Hide Form'" @else ng-init="btn_caption='Show Form'" @endif >
	<div class="row"><br>
		<div class="col-lg-12">
			<h1 class="page-header">
				Sales Invoice Report<small></small>
			</h1>
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-pencil" aria-hidden="true"></i> Sales Invoice Report
				</li>
				<button id="mybutton" ng-click="showReportForm()"><% btn_caption %></button>
			</ol>
		</div>
	</div>
	<div ng-show="show_form">
		{{ Form::open(array('url' => '/reports/sales/', 'method' => 'GET')) }}
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-4">
						{{ Form::label('selected_client', 'Client') }}
						<input type="text" id="selected_client" ng-model="selected_client" uib-typeahead="client as client.client_name for client in clients | filter:{client_name:$viewValue}" class="form-control" typeahead-show-hint="true" typeahead-min-length="0" autocomplete="off">
						<input type="hidden" class="form-control span6 input-sm"  name="client_id" ng-value="selected_client.id" ng-model="selected_client.id" id="client_id">

						{{ Form::label('selected_branch', 'Client Branch') }}
						<input type="text" ng-model="selected_branch" uib-typeahead="branch as branch.branch_name for branch in client_branches | filter:{branch_name:$viewValue}" class="form-control" typeahead-show-hint="true" typeahead-min-length="0" autocomplete="off">
						<input type="hidden" class="form-control span6 input-sm"  name="branch_id" ng-value="selected_branch.id" ng-model="selected_branch.id" id="branch_id">

						{{ Form::label('selected_client', 'Account Manager') }}
						<input type="text" id="selected_account_manager" ng-model="selected_account_manager" uib-typeahead="account_manager as account_manager.first_name+' '+account_manager.last_name for account_manager in account_managers | filter:{first_name:$viewValue}" class="form-control" typeahead-show-hint="true" typeahead-min-length="0">
						<input type="hidden" class="form-control span6 input-sm"  name="account_manager_id" ng-value="selected_account_manager.id" ng-model="selected_account_manager.id" id="account_manager_id">
					</div>
					<div class="col-lg-4">	
						{{ Form::label('status', 'Status') }}
						<select name="status" class="form-control">
							<option value="0">ALL</option>
							<option value="unpaid">unpaid</option>
							<option value="paid">paid</option>
							<option value="partially-paid">partially-paid</option>
							<option value="over-due">over-due</option>
						</select>

						{{ Form::label('pay_type', 'Pay Type') }}
						<select name="pay_type" class="form-control">
							<option value="0">ALL</option>
							<option value="cash">Cash</option>
							<option value="check">Check</option>
						</select>
						{{ Form::label('client_type', 'Client Type') }}
						<select name="client_type" class="form-control">
							<option value="-1">ALL</option>
							<option value="1">Personal</option>
							<option value="0">Company</option>
						</select>
					</div>
					<div class="col-lg-4">
						{{ Form::label('from_date', 'From :') }}
						<div class="controls input-group date" id='dtp1'>
							<input type='text' placeholder="From :" name="from_date" class="form-control span6"  name="from_date" type="from_date" id="from_date"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
						{{ Form::label('to_date', 'To :') }}
						<div class="controls input-group date" id='dtp2'>
							<input type='text' placeholder="To :" name="to_date" class="form-control span6"  name="to_date" type="to_date"  id="to_date"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
						{{ Form::label('filter', 'Filter') }}
						{{ Form::text('filter','',array('class'=>'form-control span6','placeholder' => 'Filter')) }}
					</div>
				</div>
			</div>
		</div>
		<div class="row top10">
			<div class="col-lg-4">
				<input type="submit" value="Submit" class="btn btn-success" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
			</div>
		</div>
		{!! Form::close() !!}
	</div>
	@if(isset($sales_invoice) && count($sales_invoice)>0)
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-1 col-sm-3">
						@if(isset($sales_invoice))
							{{ Form::open(array('url' => '/reports/sales/export/csv', 'method' => 'POST')) }}
								<input type="hidden" name="json" value="{{ $sales_invoice_array }}"/>
								<input type="submit" value="Export to csv" class="btn btn-default" />
							{!! Form::close() !!}							
						@endif
					</div>
					<div class="col-lg-1 col-sm-3">
						@if(isset($sales_invoice))
							{{ Form::open(array('url' => '/reports/sales/export/xls', 'method' => 'POST')) }}
								<input type="hidden" name="json" value="{{ $sales_invoice_array }}"/>
								<input type="submit" value="Export to xls" class="btn btn-default" />
							{!! Form::close() !!}							
						@endif
					</div>
					<div class="col-lg-1 col-sm-3">
						@if(isset($sales_invoice))
							{{ Form::open(array('url' => '/reports/sales/export/xlsx', 'method' => 'POST')) }}
								<input type="hidden" name="json" value="{{ $sales_invoice_array }}"/>
								<input type="submit" value="Export to xlsx" class="btn btn-default" />
							{!! Form::close() !!}							
						@endif
					</div>
					<div class="col-lg-1 col-sm-3">
						@if(isset($sales_invoice))
							{{ Form::open(array('url' => '/report/sales_invoice/print', 'method' => 'POST')) }}
								<input type="hidden" name="json" value="{{ $sales_invoice_array }}"/>{{ app('request') }}
								<input type="submit" value="Export to xlsx" class="btn btn-default" />
							{!! Form::close() !!}							
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="row  text-center">
			<div class="col-lg-3">					
			</div>
			<div class="col-lg-2">
				<a href="#" class="btn btn-sq-xs btn-warning">
				 &nbsp;&nbsp;
				</a>Cancelled			
			</div>
			<div class="col-lg-2">
				<a href="#" class="btn btn-sq-xs btn-success">
				 &nbsp;&nbsp;
				</a>Paid						
			</div>
			<div class="col-lg-2">
				<a href="#" class="btn btn-sq-xs btn-danger">
				 &nbsp;&nbsp;
				</a>Overdue					
			</div>
			<div class="col-lg-3">					
			</div>
		</div>
	
		<table class="table" style="border: 1px solid black;overflow: scroll;">
			<tr style="border: 1px solid black;">
				<th>Client</th>
				<th>Branch</th>
				<th>S.I. No</th>
				<th>PO No.</th>
				<th>S.I. Date</th>
				<th>Sales amount</th>
				<th>Tax AMT</th>
				<th>Balance</th>
				<th>Discount</th>
				<th>Pay Type</th>
				<th>Terms</th>
				<th>Due Date</th>
				<th>Status</th>
				<th>Account Manager</th>
			</tr>
			  	@foreach ($sales_invoice as $x)
				<tr style="border: 1px solid black;" @if ($x->status=='cancelled') class="warning" @elseif ($x->status=='paid') class="success" @elseif ($x->status=='over-due') class="danger" @endif>
					<td>{{ $x->client_name }}</td>
					<td>@if($x->personal==1) personal @else {{$x->branch_name}} @endif </td>
					<td>{{ $x->si }}</td>
					<td>{{ $x->po }}</td>
					<td>{{ $x->invoice_date }}</td>
					<td>{{ number_format($x->amount_total-$x->discount,Session('decimal')) }}</td>
					<td>{{ $x->tax_amount }}</td>
					<td>{{ number_format($x->balance,Session('decimal')) }}</td>
					<td>{{ $x->discount }}</td>
					<td>{{ $x->pay_type }}</td>
					<td>{{ $x->terms }}</td>
					<td>{{ $x->due_date }}</td>
					<td>{{ $x->status }}</td>
					<td>{{ $x->first_name }} {{ $x->last_name}}</td>
				</tr>
				@endforeach 
		</table>
		<div style="text-align: center;">
			{{ $sales_invoice->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links() }}
		</div>
	@endif
</div>	
@stop