@extends('layout.app')
@section('title', 'Client Reports')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px;" ng-controller="ClientReportController" @if(!isset($clients)) ng-init="show_form=true;btn_caption='Hide Form'" @else ng-init="btn_caption='Show Form'" @endif >
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Client Report<small></small>
			</h1>
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-pencil" aria-hidden="true"></i> Client Report
				</li>
				<button id="mybutton" ng-click="showReportForm()"><% btn_caption %></button>
			</ol>
		</div>
	</div>
	<div ng-show="show_form">
		{{ Form::open(array('url' => '/reports/clients/', 'method' => 'GET')) }}
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-4">
						{{ Form::label('selected_client', 'Client') }}
						<input type="text" id="selected_client" ng-model="selected_client" uib-typeahead="client as client.client_name for client in clients | filter:{client_name:$viewValue}" class="form-control" typeahead-show-hint="true" typeahead-min-length="0" autocomplete="off">
						<input type="hidden" class="form-control span6 input-sm"  name="client_id" ng-value="selected_client.id" ng-model="selected_client.id" id="client_id">

						{{ Form::label('selected_branch', 'Client Branch') }}
						<input type="text" ng-model="selected_branch" uib-typeahead="branch as branch.branch_name for branch in client_branches | filter:{branch_name:$viewValue}" class="form-control" typeahead-show-hint="true" typeahead-min-length="0" autocomplete="off">
						<input type="hidden" class="form-control span6 input-sm"  name="branch_id" ng-value="selected_branch.id" ng-model="selected_branch.id" id="branch_id">
						{{ Form::label('personal', 'Type') }}
						<select name="personal" class="form-control">
							<option value="-1">ALL</option>
							<option value="0">Company</option>
							<option value="1">Personal</option>
						</select>
					</div>
					<div class="col-lg-4">
						{{ Form::label('from_date', 'From :') }}
						<div class="controls input-group date" id='dtp1'>
							<input type='text' placeholder="From :" name="from_date" class="form-control span6"  name="from_date" type="from_date" id="from_date" required/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
						{{ Form::label('to_date', 'To :') }}
						<div class="controls input-group date" id='dtp2'>
							<input type='text' placeholder="To :" name="to_date" class="form-control span6"  name="to_date" type="to_date"  id="to_date" required />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
						{{ Form::label('filter', 'Filter') }}
						{{ Form::text('filter','',array('class'=>'form-control span6','placeholder' => 'Filter')) }}
					</div>
					<div class="col-lg-4">								
						{{ Form::label('personal', 'Branch personal') }} <br/>
						<input name="branch_personal" type="checkbox" value="1" id="branch_personal">
						<span class="errors" style="color:#FF0000">{{$errors->first('branch_personal')}}</span>
					</div>
				</div>
			</div>
		</div>
		<div class="row top10">
			<div class="col-lg-4">
				<input type="submit" class="btn btn-primary" value="Submit"/>
			</div>
		</div>
		
		{!! Form::close() !!}
	</div>
	@if(isset($clients) && count($clients)>0)

		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					@if(isset($clients))
					<div class="col-lg-2 col-sm-3">						
						{{ Form::open(array('url' => '/reports/clients/export/csv', 'method' => 'POST')) }}
							<input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
							<input type="hidden" name="branch_id" value="{{ app('request')->input('branch_id') }}" />
							<input type="hidden" name="from_date" value="{{ app('request')->input('from_date') }}" />
							<input type="hidden" name="to_date" value="{{ app('request')->input('to_date') }}" />
							<input type="hidden" name="filter" value="{{ app('request')->input('filter') }}" />
							<input type="hidden" name="personal" value="{{ app('request')->input('personal') }}" />
							<input type="hidden" name="branch_personal" value="{{ app('request')->input('branch_personal') }}" />
							<input type="submit" value="Export to csv" class="btn btn-default" />
						{!! Form::close() !!}
					</div>
					<div class="col-lg-2 col-sm-3">
						{{ Form::open(array('url' => '/reports/clients/export/xls', 'method' => 'POST')) }}
							<input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
							<input type="hidden" name="branch_id" value="{{ app('request')->input('branch_id') }}" />
							<input type="hidden" name="from_date" value="{{ app('request')->input('from_date') }}" />
							<input type="hidden" name="to_date" value="{{ app('request')->input('to_date') }}" />
							<input type="hidden" name="filter" value="{{ app('request')->input('filter') }}" />
							<input type="hidden" name="personal" value="{{ app('request')->input('personal') }}" />
							<input type="hidden" name="branch_personal" value="{{ app('request')->input('branch_personal') }}" />
							<input type="submit" value="Export to xls" class="btn btn-default" />
						{!! Form::close() !!}							
					</div>
					<div class="col-lg-2 col-sm-3">
						{{ Form::open(array('url' => '/reports/clients/export/xlsx', 'method' => 'POST')) }}
							<input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
							<input type="hidden" name="branch_id" value="{{ app('request')->input('branch_id') }}" />
							<input type="hidden" name="from_date" value="{{ app('request')->input('from_date') }}" />
							<input type="hidden" name="to_date" value="{{ app('request')->input('to_date') }}" />
							<input type="hidden" name="filter" value="{{ app('request')->input('filter') }}" />
							<input type="hidden" name="personal" value="{{ app('request')->input('personal') }}" />
							<input type="hidden" name="branch_personal" value="{{ app('request')->input('branch_personal') }}" />
							<input type="submit" value="Export to xlsx" class="btn btn-default" />
						{!! Form::close() !!}							
					</div>
					<div class="col-lg-2 col-sm-3">
						{{ Form::open(array('url' => '/report/clients/print/', 'method' => 'GET','target' => '_blank')) }}
							<input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
							<input type="hidden" name="branch_id" value="{{ app('request')->input('branch_id') }}" />
							<input type="hidden" name="from_date" value="{{ app('request')->input('from_date') }}" />
							<input type="hidden" name="to_date" value="{{ app('request')->input('to_date') }}" />
							<input type="hidden" name="filter" value="{{ app('request')->input('filter') }}" />
							<input type="hidden" name="personal" value="{{ app('request')->input('personal') }}" />
							<input type="hidden" name="branch_personal" value="{{ app('request')->input('branch_personal') }}" />
							<input type="submit" value="print" class="btn btn-default" />
						{!! Form::close() !!}	
					</div>
					@endif
				</div>
			</div>
		</div>
		<table class="table  table-bordered" style="border: 1px solid black;overflow: scroll;white-space: nowrap;font-size: {{ Session('font_size') }}px;">
			<tr style="border: 1px solid black;">
				<th class="text-center col-md-1">Account Manager</th>
				<th class="text-center col-md-3">Client</th>
				<th class="text-center col-md-1">Branch</th>
				<th class="text-center col-md-1">SI</th>
				<th class="text-center col-md-1">SI Date</th>
				<th class="text-center col-md-1">Amount due</th>
				<th class="text-center col-md-1">Terms</th>
				<th class="text-center col-md-1">Due Date</th>
				<th class="text-center col-md-1">Status</th>
				<th class="text-center col-md-1">Balance</th>
			</tr>
			  	@foreach ($clients as $x)
				<tr style="border: 1px solid black;white-space: normal;">
					<td>{{ $x->account_manager }}</td>
					<td>{{ $x->client_name }}</td>
					<td>@if($x->personal==1)Personal @else {{ $x->branch_name }} @endif</td>
					<td style="text-align: right">{{ $x->si }}</td>
					<td>{{ $x->invoice_date }}</td>
					<td style="text-align: right">{{ number_format($x->amount_total,Session('decimal'))  }}</td>
					<td>{{ $x->terms }}</td>
					<td>{{ $x->due_date }}</td>
					<td>{{ $x->status }}</td>
					<td style="text-align: right">{{ number_format($x->amount_total-($x->cash_payment+$x->check_payment),Session('decimal'))  }}</td>
				</tr>
				@endforeach 
		</table>

		<div style="text-align: center">
			{{ $clients->appends(
				[
				'client_id' =>  (isset($_GET["client_id"]) ? $_GET["client_id"]:"" ),
				'branch_id' =>  (isset($_GET["branch_id"]) ? $_GET["branch_id"]:"" ),
				'account_manager_id' =>  (isset($_GET["account_manager_id"]) ? $_GET["account_manager_id"]:"" ),
				'status' =>  (isset($_GET["status"]) ? $_GET["status"]:"" ),
				'pay_type' =>  (isset($_GET["pay_type"]) ? $_GET["pay_type"]:"" ),
				'client_type' =>  (isset($_GET["client_type"]) ? $_GET["client_type"]:"" ),
				'from_date' =>  (isset($_GET["from_date"]) ? $_GET["from_date"]:"" ),
				'to_date' =>  (isset($_GET["to_date"]) ? $_GET["to_date"]:"" )
				]
				)->links() }}
		</div>
	@else
		<div class="col-lg-12">			
			<div class="alert alert-danger">No results</div>		
		</div>
	@endif
</div>
@stop