@extends('layout.app')
@section('title', 'Invoice Reports')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px;" ng-controller="SalesReportController" @if(!isset($sales_invoice)) ng-init="show_form=true;btn_caption='Hide Form'" @else ng-init="btn_caption='Show Form'" @endif >
	<div class="row"><br>
		<div class="col-lg-12">
			<h1 class="page-header">
				Sales Invoice Reports<small></small>
			</h1>
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-pencil" aria-hidden="true"></i> Sales Invoice Report
				</li>
				<button id="mybutton" ng-click="showReportForm()"><% btn_caption %></button>
			</ol>
		</div>
	</div>
	<div ng-show="show_form">
		{{ Form::open(array('url' => '/reports/sales/', 'method' => 'GET')) }}
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-4">
						{{ Form::label('selected_client', 'Client') }}
						<input type="text" id="selected_client" ng-model="selected_client" uib-typeahead="client as client.client_name for client in clients | filter:{client_name:$viewValue}" class="form-control" typeahead-show-hint="true" typeahead-min-length="0" autocomplete="off">
						<input type="hidden" class="form-control span6 input-sm"  name="client_id" ng-value="selected_client.id" ng-model="selected_client.id" id="client_id">

						{{ Form::label('selected_branch', 'Client Branch') }}
						<input type="text" ng-model="selected_branch" uib-typeahead="branch as branch.branch_name for branch in client_branches | filter:{branch_name:$viewValue}" class="form-control" typeahead-show-hint="true" typeahead-min-length="0" autocomplete="off">
						<input type="hidden" class="form-control span6 input-sm"  name="branch_id" ng-value="selected_branch.id" ng-model="selected_branch.id" id="branch_id">

						{{ Form::label('selected_client', 'Account Manager') }}
						<input type="text" id="selected_account_manager" ng-model="selected_account_manager" uib-typeahead="account_manager as account_manager.first_name+' '+account_manager.last_name for account_manager in account_managers | filter:{first_name:$viewValue}" class="form-control" typeahead-show-hint="true" typeahead-min-length="0">
						<input type="hidden" class="form-control span6 input-sm"  name="account_manager_id" ng-value="selected_account_manager.id" ng-model="selected_account_manager.id" id="account_manager_id">
					</div>
					<div class="col-lg-4">	
						{{ Form::label('status', 'Status') }}
						<select name="status" class="form-control">
							<option value="0">ALL</option>
							<option value="unpaid">unpaid</option>
							<option value="paid">paid</option>
							<option value="partially-paid">partially-paid</option>
							<option value="over-due">over-due</option>
						</select>

						{{ Form::label('pay_type', 'Pay Type') }}
						<select name="pay_type" class="form-control">
							<option value="0">ALL</option>
							<option value="cash">Cash</option>
							<option value="check">Check</option>
						</select>

						{{ Form::label('client_type', 'Type') }}
						<select name="client_type" class="form-control">
							<option value="-1">ALL</option>
							<option value="1">Personal</option>
							<option value="0">Company</option>
						</select>
					</div>
					<div class="col-lg-4">
						{{ Form::label('from_date', 'From :') }}
						<div class="controls input-group date" id='dtp1'>
							<input type='text' placeholder="From :" name="from_date" class="form-control span6"  name="from_date" type="from_date" id="from_date"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>

						{{ Form::label('to_date', 'To :') }}
						<div class="controls input-group date" id='dtp2'>
							<input type='text' placeholder="To :" name="to_date" class="form-control span6"  name="to_date" type="to_date"  id="to_date"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
						{{ Form::label('filter', 'Filter') }}
						{{ Form::text('filter','',array('class'=>'form-control span6','placeholder' => 'Filter')) }}
					</div>
				</div>
			</div>
		</div>

		<div class="row top10">
			<div class="col-lg-4">
				<input type="submit" value="Submit" class="btn btn-success" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
			</div>
		</div>
		
		{!! Form::close() !!}
	</div>
	<div class="row">
		
		<div class="col-lg-12 pull-right">
				@if(isset($sales_invoice))
							{{ Form::open(array('url' => '/reports/sales/export/csv', 'method' => 'POST')) }}
								<input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
								<input type="hidden" name="branch_id" value="{{ app('request')->input('branch_id') }}" />
								<input type="hidden" name="account_manager_id" value="{{ app('request')->input('account_manager_id') }}" />
								<input type="hidden" name="status" value="{{ app('request')->input('status') }}" />
								<input type="hidden" name="pay_type" value="{{ app('request')->input('pay_type') }}" />
								<input type="hidden" name="client_type" value="{{ app('request')->input('client_type') }}" />
								<input type="hidden" name="from_date" value="{{ app('request')->input('from_date') }}" />
								<input type="hidden" name="to_date" value="{{ app('request')->input('to_date') }}" />
								<input type="hidden" name="filter" value="{{ app('request')->input('filter') }}" />
								<button type="submit" class="btn btn-default pull-right"  style="margin-left:5px;" >
								    <i class="fa fa-table margin-right"></i> Export to csv
								</button>
							{!! Form::close() !!}							
						@endif

						@if(isset($sales_invoice))
							{{ Form::open(array('url' => '/reports/sales/export/xlsx', 'method' => 'POST')) }}
								<input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
								<input type="hidden" name="branch_id" value="{{ app('request')->input('branch_id') }}" />
								<input type="hidden" name="account_manager_id" value="{{ app('request')->input('account_manager_id') }}" />
								<input type="hidden" name="status" value="{{ app('request')->input('status') }}" />
								<input type="hidden" name="pay_type" value="{{ app('request')->input('pay_type') }}" />
								<input type="hidden" name="client_type" value="{{ app('request')->input('client_type') }}" />
								<input type="hidden" name="from_date" value="{{ app('request')->input('from_date') }}" />
								<input type="hidden" name="to_date" value="{{ app('request')->input('to_date') }}" />
								<input type="hidden" name="filter" value="{{ app('request')->input('filter') }}" />
								<button type="submit" class="btn btn-default pull-right"  style="margin-left:5px;" >
								    <i class="fa fa-file-excel-o margin-right"></i> Export to xlsx
								</button>
							{!! Form::close() !!}							
						@endif

						@if(isset($sales_invoice))
							{{ Form::open(array('url' => '/reports/sales/export/xls', 'method' => 'POST')) }}
								<input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
								<input type="hidden" name="branch_id" value="{{ app('request')->input('branch_id') }}" />
								<input type="hidden" name="account_manager_id" value="{{ app('request')->input('account_manager_id') }}" />
								<input type="hidden" name="status" value="{{ app('request')->input('status') }}" />
								<input type="hidden" name="pay_type" value="{{ app('request')->input('pay_type') }}" />
								<input type="hidden" name="client_type" value="{{ app('request')->input('client_type') }}" />
								<input type="hidden" name="from_date" value="{{ app('request')->input('from_date') }}" />
								<input type="hidden" name="to_date" value="{{ app('request')->input('to_date') }}" />
								<input type="hidden" name="filter" value="{{ app('request')->input('filter') }}" />
								<button type="submit" class="btn btn-default pull-right"  style="margin-left:5px;" >
								    <i class="fa fa-file-excel-o margin-right"></i> Export to xls
								</button>
							{!! Form::close() !!}							
						@endif

						@if(isset($sales_invoice))
							{{ Form::open(array('url' => '/report/sales_invoice/print/', 'method' => 'GET','target' => '_blank')) }}
								<input type="hidden" name="client_id" value="{{ app('request')->input('client_id') }}" />
								<input type="hidden" name="branch_id" value="{{ app('request')->input('branch_id') }}" />
								<input type="hidden" name="account_manager_id" value="{{ app('request')->input('account_manager_id') }}" />
								<input type="hidden" name="status" value="{{ app('request')->input('status') }}" />
								<input type="hidden" name="pay_type" value="{{ app('request')->input('pay_type') }}" />
								<input type="hidden" name="client_type" value="{{ app('request')->input('client_type') }}" />
								<input type="hidden" name="from_date" value="{{ app('request')->input('from_date') }}" />
								<input type="hidden" name="to_date" value="{{ app('request')->input('to_date') }}" />
								<input type="hidden" name="filter" value="{{ app('request')->input('filter') }}" />
								<button type="submit" class="btn btn-default pull-right"  style="margin-left:5px;" >
								    <i class="fa fa-file-o margin-right"></i> Print
								</button>
							{!! Form::close() !!}							
						@endif
				
		</div>
	</div>
	@if(isset($sales_invoice) && count($sales_invoice)>0)

	<div class="panel panel-default top20" >
		<div class="panel-heading" style="background-color: #09548e; color: white;  ">
			<h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i>Sales Invoice Report List</h3>		
		</div>
		<div class="panel-body" style="height: 65vh;">
				<table class="table table-bordered" style=" white-space: nowrap;font-size: {{ Session('font_size') }}px;">
					<tr>
						<th>Client</th>
						<th>Branch</th>
						<th>S.I. No</th>
						<th>PO No.</th>
						<th>S.I. Date</th>
						<th>Sales amount</th>
						<th>Tax AMT</th>
						<th>Balance</th>
						<th>Discount</th>
						<th>Pay Type</th>
						<th>Terms</th>
						<th>Due Date</th>
						<th>Status</th>
						<th>Account Manager</th>
					</tr>
					  	@foreach ($sales_invoice as $x)
						<tr @if ($x->status=='cancelled') class="warning" @elseif ($x->status=='paid') class="success" @elseif ($x->status=='over-due') class="danger" @endif>
							<td>{{ $x->client_name }}</td>
							<td>@if($x->personal==1) personal @else {{$x->branch_name}} @endif </td>
							<td>{{ $x->si }}</td>
							<td>{{ $x->po }}</td>
							<td>{{ $x->invoice_date }}</td>
							<td>{{ number_format($x->amount_total-$x->discount,Session('decimal')) }}</td>
							<td>{{ number_format($x->tax_amount,Session('decimal')) }}</td>
							<td>{{ number_format($x->balance,Session('decimal')) }}</td>
							<td>{{ $x->discount }}</td>
							<td>{{ $x->pay_type }}</td>
							<td>{{ $x->terms }}</td>
							<td>{{ $x->due_date }}</td>
							<td>{{ $x->status }}</td>
							<td>{{ $x->first_name }} {{ $x->last_name}}</td>
						</tr>
						@endforeach 
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">				
					<div class="col-lg-2 col-sm-3">	
					</div>
					<div class="col-lg-2 col-sm-3">
					</div>
					<div class="col-lg-2 col-sm-3">
					</div>
					<div class="col-lg-2 col-sm-3">
					</div>
			</div>
		</div>
		<div style="text-align: center;">
			{{ $sales_invoice->appends(
				['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" ),
				'client_id' =>  (isset($_GET["client_id"]) ? $_GET["client_id"]:"" ),
				'branch_id' =>  (isset($_GET["branch_id"]) ? $_GET["branch_id"]:"" ),
				'account_manager_id' =>  (isset($_GET["account_manager_id"]) ? $_GET["account_manager_id"]:"" ),
				'status' =>  (isset($_GET["status"]) ? $_GET["status"]:"" ),
				'pay_type' =>  (isset($_GET["pay_type"]) ? $_GET["pay_type"]:"" ),
				'client_type' =>  (isset($_GET["client_type"]) ? $_GET["client_type"]:"" ),
				'from_date' =>  (isset($_GET["from_date"]) ? $_GET["from_date"]:"" ),
				'to_date' =>  (isset($_GET["to_date"]) ? $_GET["to_date"]:"" )
				]
				)->links() }}
		</div>
	@else
		<div class="col-lg-12">			
			<div class="alert alert-danger">No results</div>		
		</div>
	@endif
</div>
@stop