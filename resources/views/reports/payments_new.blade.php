@extends('layout.app')
@section('title', 'Payment Reportsssss')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px;" ng-controller="PaymentReportController" @if(!isset($payments)) ng-init="show_form=true;btn_caption='Hide Form'" @else ng-init="btn_caption='Show Form'" @endif >
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Payments Report<small></small>
			</h1>
			
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-pencil" aria-hidden="true"></i> Payments Report
				</li>
				<button id="mybutton" ng-click="showReportForm()"><% btn_caption %></button>
			</ol>
		</div>
	</div>
	<div ng-show="show_form">
		{{ Form::open(array('url' => '/reports/payments/', 'method' => 'GET')) }}
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-4">
						{{ Form::label('pay_type', 'Pay Type') }}
						<select name="pay_type" class="form-control span6">
							<option value="0">All</option>
							<option value="cash">Cash</option>
							<option value="check">Check</option>
							<option value="deposit">Deposit</option>
						</select>
						{{ Form::label('approved', 'Status') }}
						<select name="approved" class="form-control span6">
							<option value="-1">All</option>
							<option value="1">Approved</option>
							<option value="0">Pending</option>
						</select>
					</div>
					
					<div class="col-lg-4">
						{{ Form::label('from_date', 'From :') }}
						<div class="controls input-group date" id='dtp1'>
							<input type='text' placeholder="From :" name="from_date" class="form-control span6"  name="from_date" type="from_date" id="from_date"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>

						{{ Form::label('to_date', 'To :') }}
						<div class="controls input-group date" id='dtp2'>
							<input type='text' placeholder="To :" name="to_date" class="form-control span6"  name="to_date" type="to_date"  id="to_date"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row top10">
			<div class="col-lg-4">
				<input type="submit" value="Submit" class="btn btn-primary" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
			</div>
		</div>
		{!! Form::close() !!}
	</div>
	@if(isset($payments) && count($payments)>0)
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					@if(isset($payments))
					<div class="col-lg-1 col-sm-3">						
						{{ Form::open(array('url' => '/reports/payments/export/csv', 'method' => 'POST')) }}
							<input type="hidden" name="json" value="{{ $payments_array }}"/>
							<input type="submit" value="Export to csv" class="btn btn-default" />
						{!! Form::close() !!}
					</div>
					<div class="col-lg-1 col-sm-3">
						{{ Form::open(array('url' => '/reports/payments/export/xls', 'method' => 'POST')) }}
							<input type="hidden" name="json" value="{{ $payments_array }}"/>
							<input type="submit" value="Export to xls" class="btn btn-default" />
						{!! Form::close() !!}							
					</div>
					<div class="col-lg-1 col-sm-3">
						{{ Form::open(array('url' => '/reports/payments/export/xlsx', 'method' => 'POST')) }}
							<input type="hidden" name="json" value="{{ $payments_array }}"/>
							<input type="submit" value="Export to xlsx" class="btn btn-default" />
						{!! Form::close() !!}							
					</div>
					<div class="col-lg-1 col-sm-3">
						<button type="button" class="btn btn-default" ng-click="printDiv('printable')">Print</button>
					</div>
					@endif
				</div>
			</div>
		</div>
		<table class="table" style="border: 1px solid black;overflow: scroll;">
			<tr style="border: 1px solid black;">
				<th>Payment ID</th>
				<th>Client</th>
				<th>SI</th>
				<th>Pay Type</th>
				<th>Payment Amount</th>
				<th>Less</th>
				<th>Payment Date</th>
				<th>Status</th>
			</tr>
			  	@foreach ($payments as $x)
				<tr style="border: 1px solid black;">
					<td>{{ $x->payments_id }}</td>
					<td>{{ $x->client_name }} - {{$x->branch_name}}</td>
					<td>{{ $x->si }}</td>
					<td>{{ $x->pay_type }}</td>
					<td>{{ $x->payment_amount }}</td>
					<td style="text-align: right">{{ number_format($x->less,Session('decimal')) }}</td>
					<td>{{ $x->payment_date }}</td></td>
					<td>@if($x->approved==1) approved @else pending @endif</td>
				</tr>
				@endforeach 
		</table>
		<div style="text-align: center">
			{{ $payments->links() }}
		</div>
	@endif
</div>
@stop