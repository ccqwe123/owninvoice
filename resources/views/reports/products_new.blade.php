@extends('layout.app')
@section('title', 'Product Reports')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px;" ng-controller="ProductReportController" @if(!isset($products)) ng-init="show_form=true;btn_caption='Hide Form'" @else ng-init="btn_caption='Show Form'" @endif >
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Product Report<small></small>
			</h1>
			
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-pencil" aria-hidden="true"></i> Product Report
				</li>
				<button id="mybutton" ng-click="showReportForm()"><% btn_caption %></button>
			</ol>
		</div>
	</div>
	<div ng-show="show_form">
		{{ Form::open(array('url' => '/reports/products/', 'method' => 'GET')) }}
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-4">
						{{ Form::label('selected_Product', 'Product') }}
						<input type="text" id="selected_Product" ng-model="selected_Product" uib-typeahead="product as product.prod_name for product in products | filter:{prod_name:$viewValue}" class="form-control" typeahead-show-hint="true" typeahead-min-length="0">
						<input type="hidden" class="form-control span6 input-sm"  name="product_id" ng-value="selected_Product.id" ng-model="selected_Product.id" id="product_id">
					</div>
					<div class="col-lg-4">
						{{ Form::label('filter', 'Filter') }}
						{{ Form::text('filter','',array('class'=>'form-control span6','placeholder' => 'Filter')) }}
					</div>
				</div>
			</div>
		</div>
		<div class="row top10">
			<div class="col-lg-4">
				<input type="submit" value="Submit" class="btn btn-primary" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
			</div>
		</div>
		{!! Form::close() !!}
	</div>
	@if(isset($products) && count($products)>0)
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					@if(isset($products))
					<div class="col-lg-1 col-sm-3">						
						{{ Form::open(array('url' => '/reports/products/export/csv', 'method' => 'POST')) }}
							<input type="hidden" name="json" value="{{ $products_array }}"/>
							<input type="submit" value="Export to csv" class="btn btn-default" />
						{!! Form::close() !!}
					</div>
					<div class="col-lg-1 col-sm-3">
						{{ Form::open(array('url' => '/reports/products/export/xls', 'method' => 'POST')) }}
							<input type="hidden" name="json" value="{{ $products_array }}"/>
							<input type="submit" value="Export to xls" class="btn btn-default" />
						{!! Form::close() !!}							
					</div>
					<div class="col-lg-1 col-sm-3">
						{{ Form::open(array('url' => '/reports/products/export/xlsx', 'method' => 'POST')) }}
							<input type="hidden" name="json" value="{{ $products_array }}"/>
							<input type="submit" value="Export to xlsx" class="btn btn-default" />
						{!! Form::close() !!}							
					</div>
					<div class="col-lg-1 col-sm-3">
						<button type="button" class="btn btn-default" ng-click="printDiv('printable')">Print</button>
					</div>
					@endif
				</div>
			</div>
		</div>
		<table class="table" style="border: 1px solid black;overflow: scroll;">
			<tr style="border: 1px solid black;">
				<th>Product Code</th>
				<th>Product Name</th>
				<th>Description</th>
				<th>Price</th>
				<th>Total Sold</th>
			</tr>
			  	@foreach ($products as $x)
				<tr style="border: 1px solid black;">
					<td>{{ $x->prod_code }}</td>
					<td>{{ $x->prod_name }}</td>
					<td>{{ $x->prod_description }}</td>
					<td>{{ $x->prod_price }}</td>
					<td>{{ $x->sold }}</td>
				</tr>
				@endforeach 
		</table>
		<div style="text-align: center">
			{{ $products->links() }}
		</div>
	@endif
</div>
@stop