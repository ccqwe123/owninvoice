<script>window.onload = function() { window.print(); }</script>
<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" media="all" >
<style type="text/css">
@media print {
  [class*="col-sm-"] {
    float: left;
  }
  .rightalign
  {
  	text-align:right;display: block;
  }
}
@page { margin: 20px 20px 10px 20px; }
td {
	font-size: {{ Session('font_size') }}px;
}

th {
	font-size: {{ Session('font_size') }}px;
}
 .col-sm-12 {
    width: 100%;
  }
  .col-sm-11 {
    width: 91.66666667%;
  }
  .col-sm-10 {
    width: 83.33333333%;
  }
  .col-sm-9 {
    width: 75%;
  }
  .col-sm-8 {
    width: 66.66666667%;
  }
  .col-sm-7 {
    width: 58.33333333%;
  }
  .col-sm-6 {
    width: 50%;
  }
  .col-sm-5 {
    width: 41.66666667%;
  }
  .col-sm-4 {
    width: 33.33333333%;
  }
  .col-sm-3 {
    width: 25%;
  }
  .col-sm-2 {
    width: 16.66666667%;
  }
  .col-sm-1 {
    width: 8.33333333%;
  }
</style>
<div id="printable">
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-7">{{ $sales_invoice[0]->client_name}} - {{ $sales_invoice[0]->branch_name}}</div>
		<div class="col-sm-2">{{ $sales_invoice[0]->invoice_date}}</div>
	</div>
	<div class="row">
		<div class="col-sm-2">&nbsp;</div>
		<div class="col-sm-7">&nbsp;</div>
		<div class="col-sm-1">{{ $sales_invoice[0]->po}}</div>
	</div>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-7">{{ $sales_invoice[0]->address}}</div>
		<div class="col-sm-2">{{ $sales_invoice[0]->terms}}</div>
	</div>
	<br/>
	<br/>
	<br/>
	<table style="overflow: scroll;white-space: nowrap;">
		<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
		<?php $total_unit_price =0; ?>
		<?php $total =0; ?>
	  	@foreach ($items as $x)
	  	<div class="row">
	  		<div class="col-sm-1">&nbsp;</div>
			<div class="col-sm-1">{{ $x->qty }}</div>
			<div class="col-sm-1">pc</div>
			<div class="col-sm-5">{{ $x->prod_name }}</div>
			<div class="col-sm-1"><span class="pull-right">{{ number_format($x->prod_price,Session('decimal')) }}</span></div>
			<div class="col-sm-1">&nbsp;</div>
			<div class="col-sm-1"><span class="pull-right">{{ number_format($x->prod_price*$x->qty,Session('decimal')) }}</span></div>
			<div class="col-sm-1">&nbsp;</div>
		</div>		
		<?php $total_unit_price = $total_unit_price+$x->prod_price; ?>
		<?php $total = $total+($x->prod_price*$x->qty); ?>
		@endforeach 
	</table>
	<div style="position: fixed; bottom: 210px; width: 100%;">
	
		<div class="row">
	  		<div class="col-sm-1">&nbsp;</div>
			<div class="col-sm-1">&nbsp;</div>
			<div class="col-sm-1">&nbsp;</div>
			<div class="col-sm-5">&nbsp;</div>
			<div class="col-sm-3"></div>
			<div class="col-sm-3" style="text-align:right;">@if($sales_invoice[0]->vatable==0) {{ number_format($total,Session('decimal')) }} @else {{ number_format($total-$sales_invoice[0]->tax_amount,Session('decimal')) }} @endif</div>
			<div class="col-sm-1"></div>
		</div>	
		<div class="row">
	  		<div class="col-sm-1">&nbsp;</div>
			<div class="col-sm-1">&nbsp;</div>
			<div class="col-sm-1">&nbsp;</div>
			<div class="col-sm-5">&nbsp;</div>
			<div class="col-sm-3"></div>
			<div class="col-sm-3" style="text-align:right;"> {{ number_format($sales_invoice[0]->discount,Session('decimal')) }} </div>
			<div class="col-sm-1"></div>
		</div>
		<br/>
		<div class="row">
	  		<div class="col-sm-1">&nbsp;</div>
			<div class="col-sm-1">&nbsp;</div>
			<div class="col-sm-1">&nbsp;</div>
			<div class="col-sm-5">&nbsp;</div>
			<div class="col-sm-3" style="text-align:right;">@if($sales_invoice[0]->vatable==0) 0 @else {{ number_format($sales_invoice[0]->
			tax_amount,Session('decimal')) }} @endif</div>
			<div class="col-sm-1"></div>
		</div>
		<div class="row">
	  		<div class="col-sm-1">&nbsp;</div>
			<div class="col-sm-1">&nbsp;</div>
			<div class="col-sm-1">&nbsp;</div>
			<div class="col-sm-5">&nbsp;</div>		
			<div class="col-sm-3" style="text-align:right;	">@if($sales_invoice[0]->vatable==0) {{ number_format($total-$sales_invoice[0]->discount,Session('decimal')) }} @else {{ number_format($total-$sales_invoice[0]->discount,Session('decimal')) }} @endif</div>
			<div class="col-sm-1"></div>
		</div>
	</div>
</div>