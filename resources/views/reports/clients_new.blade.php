@extends('layout.app')
@section('title', 'Client Reports')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px;" ng-controller="ClientReportController" @if(!isset($clients)) ng-init="show_form=true;btn_caption='Hide Form'" @else ng-init="btn_caption='Show Form'" @endif >
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Client Report<small></small>
			</h1>
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-pencil" aria-hidden="true"></i> Client Report
				</li>
				<button id="mybutton" ng-click="showReportForm()"><% btn_caption %></button>
			</ol>
		</div>
	</div>
	<div>
		{{ Form::open(array('url' => '/reports/clients/', 'method' => 'GET')) }}
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-4">
						{{ Form::label('selected_client', 'Client') }}
						<input type="text" id="selected_client" ng-model="selected_client" uib-typeahead="client as client.client_name for client in clients | filter:{client_name:$viewValue}" class="form-control" typeahead-show-hint="true" typeahead-min-length="0" autocomplete="off">
						<input type="hidden" class="form-control span6 input-sm"  name="client_id" ng-value="selected_client.id" ng-model="selected_client.id" id="client_id">
						{{ Form::label('selected_branch', 'Client Branch') }}
						<input type="text" ng-model="selected_branch" uib-typeahead="branch as branch.branch_name for branch in client_branches | filter:{branch_name:$viewValue}" class="form-control" typeahead-show-hint="true" typeahead-min-length="0" autocomplete="off">
						<input type="hidden" class="form-control span6 input-sm"  name="branch_id" ng-value="selected_branch.id" ng-model="selected_branch.id" id="branch_id">
						{{ Form::label('personal', 'Type') }}
						<select name="personal" class="form-control">
							<option value="-1">ALL</option>
							<option value="0">Company</option>
							<option value="1">Personal</option>
						</select>
					</div>
					<div class="col-lg-4">
						{{ Form::label('from_date', 'From :') }}
						<div class="controls input-group date" id='dtp1'>
							<input type='text' placeholder="From :" name="from_date" class="form-control span6"  name="from_date" type="from_date" id="from_date" required/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
						{{ Form::label('to_date', 'To :') }}
						<div class="controls input-group date" id='dtp2'>
							<input type='text' placeholder="To :" name="to_date" class="form-control span6"  name="to_date" type="to_date"  id="to_date" required />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
						{{ Form::label('filter', 'Filter') }}
						{{ Form::text('filter','',array('class'=>'form-control span6','placeholder' => 'Filter')) }}
					</div>
					<div class="col-lg-4">								
						{{ Form::label('branch_personal', 'Branch personal') }} <br/>
						<input name="branch_personal" type="checkbox" value="1" id="personal">
						<span class="errors" style="color:#FF0000">{{$errors->first('branch_personal')}}</span>
					</div>
				</div>
			</div>
		</div>
		<div class="row top10">
			<div class="col-lg-4">
				<input type="submit" value="Submit" class="btn btn-primary"/>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@stop