<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page { margin: 10; }
td {
	font-size: {{ Session('font_size') }}px;
}
th {
	font-size: {{ Session('font_size') }}px;
}
</style>
<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">

<div id="printable">
	<div class="row">
		<div class="col-lg-12">
		{!! Session('letter_head') !!}
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
		</div>
	</div>
	<br/>
	<br/>
	<br/>
	<table class="table .table-bordered" style="overflow: scroll;white-space: nowrap;">
		<tr>
			<th>Account Manager</th>
			<th>Client</th>
			<th>Branch</th>
			<th>SI</th>
			<th>SI Date</th>
			<th>Terms</th>
			<th>Amount due</th>
			<th>Balance</th>
			<th>Due Date</th>
			<th>Status</th>
		</tr>
		<?php 
		$amt = 0;
		$tax = 0;
		$bal = 0;
		?>
	  	@foreach ($clients_all as $x)
		<tr>
			<td>{{ $x->account_manager }}</td>
			<td>{{ $x->client_name }}</td>
			<td>@if($x->personal==1)Personal @else {{ $x->branch_name }} @endif</td>
			<td>{{ $x->si }}</td>
			<td>{{ $x->invoice_date }}</td>
			<td>{{ $x->terms }}</td>
			<td style="text-align: right">{{ number_format($x->amount_total,Session('decimal')) }}</td>
			<td style="text-align: right">{{ number_format($x->amount_total-($x->cash_payment+$x->check_payment),Session('decimal'))  }}</td>
			<td>{{ $x->due_date }}</td>
			<td>{{ $x->status }}</td>
			<?php
			$amt = $amt + $x->amount_total;
			$tax = $tax + $x->amount_total-($x->cash_payment+$x->check_payment);
			?>
		</tr>
		@endforeach 
		<tr>
			<td>TOTAL AMOUNT:</td>
			<td></td>
			<td></td>
			<td></td>			
			<td></td>
			<td></td>
			<td style="text-align: right">{{ number_format($amt,Session('decimal')) }}</td>
			<td style="text-align: right">{{ number_format($tax,Session('decimal'))  }}</td>			
			<td></td>
			<td></td>
		</tr>
	</table>
	<div class="row">
		<div class="col-lg-12">
		{!! Session('report_footer') !!}
		</div>
	</div>
</div>