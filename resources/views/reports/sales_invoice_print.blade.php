<script>

window.onload = function() {
var css = '@page { size: landscape; }',
    head = document.head || document.getElementsByTagName('head')[0],
    style = document.createElement('style');
style.type = 'text/css';
style.media = 'print';
if (style.styleSheet){
  style.styleSheet.cssText = css;
} else {
  style.appendChild(document.createTextNode(css));
}
head.appendChild(style);
 window.print();
  }
</script>
<style type="text/css">
@page { margin: 10; }
td {
	font-size: {{ Session('font_size') }}px;
}
th {
	font-size: {{ Session('font_size') }}px;
}
</style>
<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
<div id="printable">
	<div class="row">
		<div class="col-lg-12">
		{!! Session('letter_head') !!}
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
		</div>
	</div>
	<br/>
	<br/>
	<br/>
	<table class="table table-bordered" style="overflow: scroll;white-space: nowrap;">
		<tr>
			<th>Client</th>
			<th>Branch</th>
			<th>S.I. No</th>
			<th>PO No.</th>
			<th>S.I. Date</th>
			<th>Sales amount</th>
			<th>Tax AMT</th>
			<th>Balance</th>
			<th>Discount</th>
			<th>Pay Type</th>
			<th>Terms</th>
			<th>Due Date</th>
			<th>Status</th>
			<th>Account Manager</th>
		</tr>
		<?php 
		$amt = 0;
		$tax = 0;
		$bal = 0;
		$discount = 0;
		?>
	  	@foreach ($sales_invoice_all as $x)
		<tr>
			<td>{{ $x->client_name }}</td>
			<td>@if($x->personal==1) personal @else {{$x->branch_name}} @endif </td>
			<td>{{ $x->si }}</td>
			<td>{{ $x->po }}</td>
			<td>{{ $x->invoice_date }}</td>
			<td style="text-align: right">{{ number_format($x->amount_total,Session('decimal')) }}</td>
			<td style="text-align: right">{{ number_format($x->tax_amount,Session('decimal')) }}</td>
			<td style="text-align: right">{{ number_format($x->balance,Session('decimal')) }}</td>
			<td>{{ $x->discount }}</td>
			<td>{{ $x->pay_type }}</td>
			<td>{{ $x->terms }}</td>
			<td>{{ $x->due_date }}</td>
			<td>{{ $x->status }}</td>
			<td>{{ $x->first_name }} {{ $x->last_name}}</td>
			<?php
			$amt = $amt + $x->amount_total;
			$tax = $tax + $x->tax_amount;
			$bal = $bal + $x->balance;
			$discount = $discount + $x->discount;
			?>
		</tr>
		@endforeach 
		<tr>
			<td>Total Amount:</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td style="text-align: right"><strong>{{ number_format($amt,Session('decimal')) }}</strong></td>
			<td style="text-align: right"><strong>{{ number_format($tax,Session('decimal')) }}</strong></td>
			<td style="text-align: right"><strong>{{ number_format($bal,Session('decimal')) }}</strong></td>
			<td style="text-align: right"><strong>{{ number_format($discount,Session('decimal')) }}</strong></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	</table>
	<div class="row">
		<div class="col-lg-12">
		{!! Session('report_footer') !!}
		</div>
	</div>
</div>