@extends('layout.app')
@section('title', 'Product Reports')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px;" ng-controller="ProductReportController" @if(!isset($products)) ng-init="show_form=true;btn_caption='Hide Form'" @else ng-init="btn_caption='Show Form'" @endif >
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Product Report<small></small>
			</h1>
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-pencil" aria-hidden="true"></i> Product Report
				</li>
				<button id="mybutton" ng-click="showReportForm()"><% btn_caption %></button>
			</ol>
		</div>
	</div>
	<div>
		{{ Form::open(array('url' => '/reports/products/', 'method' => 'GET')) }}
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-4">
						{{ Form::label('selected_Product', 'Product') }}
						<input type="text" id="selected_Product" ng-model="selected_Product" uib-typeahead="product as product.prod_name for product in products | filter:{prod_name:$viewValue}" class="form-control" typeahead-show-hint="true" typeahead-min-length="0">
						<input type="hidden" class="form-control span6 input-sm"  name="product_id" ng-value="selected_Product.id" ng-model="selected_Product.id" id="product_id">
					</div>
					<div class="col-lg-4">
						{{ Form::label('filter', 'Filter') }}
						{{ Form::text('filter','',array('class'=>'form-control span6','placeholder' => 'Filter')) }}
					</div>
				</div>
			</div>
		</div>
		<div class="row top10">
			<div class="col-lg-4">
				<input type="submit" value="Submit" class="btn btn-primary" />
			</div>
		</div>
		{!! Form::close() !!}
	</div>
	@if(isset($products) && count($products)>0)
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					@if(isset($products))
					<div class="col-lg-2 col-sm-3">						
						{{ Form::open(array('url' => '/reports/products/export/csv', 'method' => 'POST')) }}
							<input type="hidden" name="product_id" value="{{ app('request')->input('product_id') }}" />
							<input type="hidden" name="filter" value="{{ app('request')->input('filter') }}" />
							<input type="submit" value="Export to csv" class="btn btn-default" />
						{!! Form::close() !!}
					</div>
					<div class="col-lg-2 col-sm-3">
						{{ Form::open(array('url' => '/reports/products/export/xls', 'method' => 'POST')) }}
							<input type="hidden" name="product_id" value="{{ app('request')->input('product_id') }}" />
							<input type="hidden" name="filter" value="{{ app('request')->input('filter') }}" />
							<input type="submit" value="Export to xls" class="btn btn-default" />
						{!! Form::close() !!}							
					</div>
					<div class="col-lg-2 col-sm-3">
						{{ Form::open(array('url' => '/reports/products/export/xlsx', 'method' => 'POST')) }}
							<input type="hidden" name="product_id" value="{{ app('request')->input('product_id') }}" />
							<input type="hidden" name="filter" value="{{ app('request')->input('filter') }}" />
							<input type="submit" value="Export to xlsx" class="btn btn-default" />
						{!! Form::close() !!}							
					</div>
					<div class="col-lg-2 col-sm-3">						
						{{ Form::open(array('url' => '/report/products/print/', 'method' => 'GET','target' => '_blank')) }}
							<input type="hidden" name="product_id" value="{{ app('request')->input('product_id') }}" />
							<input type="hidden" name="filter" value="{{ app('request')->input('filter') }}" />
							<input type="submit" value="print" class="btn btn-default" />
						{!! Form::close() !!}						
					</div>
					@endif
				</div>
			</div>
		</div>

		<table class="table table-bordered" style="border: 1px solid black;overflow: scroll;white-space: nowrap;font-size: {{ Session('font_size') }}px;">
			<tr style="border: 1px solid black;">
				<th class="text-center col-md-2">Product Code</th>
				<th class="text-center col-md-4">Product Name</th>
				<th class="text-center col-md-4">Description</th>
				<th class="text-center col-md-1">Price</th>
				<th class="text-center col-md-1">Total Sold</th>
			</tr>
			  	@foreach ($products as $x)
				<tr style="border: 1px solid black;white-space: normal;">
					<td>{{ $x->prod_code }}</td>
					<td>{{ $x->prod_name }}</td>
					<td>{{ $x->prod_description }}</td>
					<td style="text-align: right">{{ number_format($x->prod_price,Session('decimal')) }}</td>
					<td style="text-align: right">{{ $x->sold }}</td>
				</tr>
				@endforeach 
		</table>
		<div style="text-align: center">
			{{ $products->appends(
				[
				'product_id' =>  (isset($_GET["product_id"]) ? $_GET["product_id"]:"" ),
				'filter' =>  (isset($_GET["filter"]) ? $_GET["filter"]:"" )
				]
				)->links() }}
		</div>
	@else
		<div class="col-lg-12">			
			<div class="alert alert-danger">No results</div>		
		</div>
	@endif
</div>
@stop