@extends('layout.app')
@section('title', 'Payments')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Check Payments<small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-search"></i> Check Info
				</li>
			</ol>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('or_number', 'OR Number') }}
				{{ Form::text('or_number',$payment->or_number,array('class'=>'form-control span6','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('or_number')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('amount', 'Amount') }}
				{{ Form::text('amount',number_format($check->amount,Session("decimal")),array('class'=>'form-control span6','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('amount')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('less', 'Less') }}
				{{ Form::text('less',number_format($payment->less,Session("decimal")),array('class'=>'form-control span6','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('less')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('total', 'Total') }}
				{{ Form::text('total',number_format($payment->less+$check->amount,Session("decimal")),array('class'=>'form-control span6','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('total')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('account_number', 'Bank') }}
				{{ Form::text('account_number',$bank->bank_name,array('class'=>'form-control span6','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('account_number')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('account_number', 'Account Number') }}
				{{ Form::text('account_number',$check->account_number,array('class'=>'form-control span6','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('account_number')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('check_number', 'Check Number') }}
				{{ Form::text('check_number',$check->check_number,array('class'=>'form-control span6','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('check_number')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('check_date', 'Check Date') }}
				{{ Form::text('check_date',$check->check_date,array('class'=>'form-control span6','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('check_date')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<br/>
				@if($payment->photo!='')
					<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">View Attachment</button>
				@endif 
			</div>
		</div>
	</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog" >
	<div class="modal-content" style=" height: 100%;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Attachment</h4>
		</div>
		<div class="modal-body">
			<a href="/uploads/{{$payment->photo}}"> <img src="/uploads/{{$payment->photo}}" style="width: 100%;height: 100%;object-fit: contain"/></a>
		</div>
	</div>
	</div>
</div>
@stop