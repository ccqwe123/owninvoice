@extends('layout.app')
@section('title', 'Payments')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px; overflow: scroll;" ng-controller="PaymentsController" ng-init="balance={{ $sales_invoice[0]->amount_total-$sales_invoice[0]->discount-($sales_invoice[0]->cash_payment+$sales_invoice[0]->check_payment+$sales_invoice[0]->less) }}">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Invoice Payments<small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-search"></i> Invoice Info
				</li>
			</ol>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('invoice_date', 'Invoice Date') }}
				{{ Form::text('invoice_date',$sales_invoice[0]->invoice_date,array('class'=>'form-control span6','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('invoice_date')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('si', 'S.i No.') }}
				{{ Form::text('si',$sales_invoice[0]->si,array('class'=>'form-control span6','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('si')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('po', 'PO No.') }}
				{{ Form::text('po',$sales_invoice[0]->po,array('class'=>'form-control span6','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('po')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('client_name', 'Client Name') }}
				{{ Form::text('client_name',$sales_invoice[0]->client_name,array('class'=>'form-control span6','readonly=true','placeholder' => 'Client Name')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('client_name')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('branch_name', 'Branch Name') }}
				{{ Form::text('branch_name',$sales_invoice[0]->branch_name,array('class'=>'form-control span6','placeholder' => 'Branch Name','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('branch_name')}}</span>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('account_manager_id', 'Account Manager') }}
		
				{{ Form::text('account_manager',$sales_invoice[0]->first_name." ".$sales_invoice[0]->last_name,array('class'=>'form-control span6','placeholder' => 'Branch Name','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('invoice_date')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('tax_rate', 'Tax Rate') }}
				{{ Form::text('tax_rate',number_format($sales_invoice[0]->tax_rate,session('decimal')),array('class'=>'form-control span6','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('tax_rate')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('terms', 'Terms') }}
				{{ Form::text('terms',$sales_invoice[0]->terms,array('class'=>'form-control span6','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('terms')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('status', 'Status') }}
				{{ Form::text('status',$sales_invoice[0]->status,array('class'=>'form-control span6','placeholder' => 'Status','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('status')}}</span>
			</div>
		</div>
	</div>
	<div class="col-lg-4">		
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('amount_total', 'Amount Total') }}
				{{ Form::text('amount_total',number_format($sales_invoice[0]->amount_total-$sales_invoice[0]->discount,Session('decimal')),array('class'=>'form-control span6','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('amount_total')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('vat_sales', 'Vat Sales') }}
				{{ Form::text('vat_sales',number_format($sales_invoice[0]->vat_sales,Session('decimal')),array('class'=>'form-control span6','readonly=true')) }}
				<span class="errors" style="color:#FF0000">{{$errors->first('vat_sales')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('tax_amount', 'Tax Amount') }}
				@if($sales_invoice[0]->vatable==1)
				{{ Form::text('tax_amount',number_format(($sales_invoice[0]->amount_total-$sales_invoice[0]->discount)*Session('tax_rate'),Session('decimal')),array('class'=>'form-control span6','readonly=true')) }}
				@else
				{{ Form::text('tax_amount',number_format(0,Session('decimal')),array('class'=>'form-control span6','readonly=true')) }}
				@endif
				<span class="errors" style="color:#FF0000">{{$errors->first('tax_amount')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				{{ Form::label('balance', 'Balance') }}
				@if($sales_invoice[0]->vatable==1)
					{{ Form::text('balance',number_format($sales_invoice[0]->amount_total-$sales_invoice[0]->discount-($sales_invoice[0]->cash_payment+$sales_invoice[0]->check_payment+$sales_invoice[0]->deposit_payment+$sales_invoice[0]->less),Session('decimal')),array('class'=>'form-control span6','placeholder' => '0','readonly=true')) }}
				@else
					{{ Form::text('balance',number_format($sales_invoice[0]->amount_total-$sales_invoice[0]->discount-($sales_invoice[0]->cash_payment+$sales_invoice[0]->check_payment+$sales_invoice[0]->deposit_payment+$sales_invoice[0]->less),Session('decimal')),array('class'=>'form-control span6','placeholder' => '0','readonly=true')) }}
				@endif
				<span class="errors" style="color:#FF0000">{{$errors->first('balance')}}</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12" style="margin-top:15px;">
				<div class="dropdown">
					<button class="btn btn-default dropdown-toggle" ng-disabled="balance==0" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Add Payment<span class="caret"></span>
					</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<li><a href="#" ng-click="addPaymentCash({{ $sales_invoice[0]->id }})" >Cash</a></li>
					<li><a href="#" ng-click="addPaymentCheck({{ $sales_invoice[0]->id }})">Check</a></li>
					<li><a href="#" ng-click="addPaymentBankDeposit({{ $sales_invoice[0]->id }})">Bank Deposit</a></li>
				</ul>
				</div>
				<span class="errors" style="color:#FF0000">{{$errors->first('balance')}}</span>
			</div>

		</div>	
	</div>
	<div class="row">
		<div class="col-lg-12">
			
			<ol class="breadcrumb" style="margin-top:15px;">
				<li class="active">
					<i class="fa fa-shopping-basket"></i> Payments
				</li>
			</ol>
			
		</div>
	</div>
	<table class="table" style="border: 1px solid black;">
		<tr style="border: 1px solid black;">
			<th>Pay Type</th>
			<th>OR Number</th>
			<th>Amount</th>			
			<th>Less</th>
			<th>Total</th>
			<th>Date</th>
			<th></th>
			<th></th>
		</tr>
		  	@foreach ($payments as $x)
			<tr style="border: 1px solid black;">
				<td>{{ $x->pay_type }}</td>
				<td>{{ $x->or_number }}</td>
				<td>{{ number_format($x->amount,Session('decimal')) }}</td>				
				<td>{{ number_format($x->less,Session('decimal')) }}</td>
				<td>{{ number_format($x->amount+$x->less,Session('decimal')) }}</td>
				<td>{{ $x->created_at }}</td>
				<td>@if($x->approved==1) approved @else pending @endif</td>
				<td>
					<div class="dropdown">
						<button class="btn btn-primary dropdown-toggle btn-sm" type="button" data-toggle="dropdown">Actions
						<span class="caret"></span></button>
						<ul class="dropdown-menu dropdown-menu-right">
							@if (Session::get('is_allow_payment_approval') > 0)
								@if($x->approved==0)
									@if($x->pay_type=='cash')
										<li><a href="/payment/{{$x->trans_id}}/approve/cash/invoice/{{ $sales_invoice[0]->id }}" class="confirmation"><i class="fa fa-thumbs-up" aria-hidden="true" ng-click=""></i>Approve</a></li>
										<li><a href="/payments/{{$x->trans_id}}/cash-details" class="Details"><i class="fa fa-info-circle" aria-hidden="true"></i>Details</a></li>
									@elseif($x->pay_type=='deposit')
										<li><a href="/payment/{{$x->trans_id}}/approve/deposit/invoice/{{ $sales_invoice[0]->id }}" class="confirmation"><i class="fa fa-thumbs-up" aria-hidden="true"></i>Approve</a></li>
										<li><a href="/payments/{{$x->trans_id}}/deposit-details" class="Details"><i class="fa fa-info-circle" aria-hidden="true"></i>Details</a></li>
									@else
										<li><a href="/payment/{{$x->trans_id}}/approve/check/invoice/{{ $sales_invoice[0]->id }}" class="confirmation"><i class="fa fa-thumbs-up" aria-hidden="true"></i>Approve</a></li>
										<li><a href="/payments/{{$x->trans_id}}/check-details" class="Details"><i class="fa fa-info-circle" aria-hidden="true"></i>Details</a></li>
									@endif

								@endif
							@endif
							<li><a href="/payments/{{$x->payment_id}}/delete/total/{{ $sales_invoice[0]->amount_total-$sales_invoice[0]->discount-($sales_invoice[0]->cash_payment+$sales_invoice[0]->check_payment+$sales_invoice[0]->deposit_payment+$sales_invoice[0]->less)+$x->amount }}/invoiceid/{{ $sales_invoice[0]->id }}" class="confirmation"><i class="fa fa-ban" aria-hidden="true"></i>Delete</a></li>
						</ul>
					</div>
				</td>
			</tr>
			@endforeach 
	</table>
</div>
@stop