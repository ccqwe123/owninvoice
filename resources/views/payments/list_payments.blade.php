@extends('layout.app')
@section('title', 'Payments List')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Payments <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-search"></i> Payments Search
				</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::open(array('url' => '/payments', 'method' => 'get')) }}  
			@if(null !==app('request')->input('approved'))
    			<input type="hidden" name="approved" value="{{ app('request')->input('approved') }}" />
			@endif
			
			<div class="input-group margin-bottom-sm">
			    <span class="input-group-addon"><button class="fa fa-search" style="border:none; background-color:transparent;"></button></span>
			    {{ Form::text('search','',array('class'=>'form-control span6','placeholder' => 'Search')) }}
			    <span class="errors" style="color:#FF0000">{{$errors->first('search')}}</span>
			</div>
			{{ Form::close() }}
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-shopping-basket"></i> Payments
				</li>
			</ol>
			
		</div>
	</div>
	<table class="table" style="border: 1px solid black; >
		<tr style="border: 1px solid black;">
			<th>Payment ID</th>
			<th>Client</th>
			<th>PayType</th>
			<th>S.I. No</th>
			<th>OR No</th>
			<th>Less</th>
			<th>Amount</th>
			<th>Total</th>
			<th>Status</th>
			<th></th>
		</tr>
		  	@foreach ($payments as $x)
			<tr style="border: 1px solid black;">
				<td>{{ $x->payment_id }}</td>
				<td>{{ $x->client_name }} - {{$x->branch_name}}</td>
				<td>{{ $x->paytype }}</td>
				<td>{{ $x->si }}</td>
				<td>{{ $x->or_number }}</td>
				<td>{{ number_format($x->less,Session('decimal')) }}</td>
				<td>{{ number_format($x->amount,Session('decimal')) }}</td>
				<td>{{ number_format($x->amount+$x->less,Session('decimal')) }}</td>
				<td>@if($x->approved==1) approved @else pending @endif</td>
				<td>
					<div class="dropdown">
						<button class="btn btn-primary dropdown-toggle btn-sm" type="button" data-toggle="dropdown">Actions 
						<span class="caret"></span></button>						
						<ul class="dropdown-menu dropdown-menu-right">
							@if (Session::get('is_allow_payment_approval') > 0)
								@if($x->approved==0)
									@if($x->paytype=='cash')
										<li><a href="/payment/{{$x->cash_payment_id}}/approve/cash" class="confirmation"><i class="fa fa-thumbs-up" aria-hidden="true"></i>Approve</a></li>
									@elseif($x->paytype=='deposit')
										<li><a href="/payment/{{$x->deposit_payment_id}}/approve/deposit" class="confirmation"><i class="fa fa-thumbs-up" aria-hidden="true"></i>Approve</a></li>
									@else
										<li><a href="/payment/{{$x->check_payment_id}}/approve/check" class="confirmation"><i class="fa fa-thumbs-up" aria-hidden="true"></i>Approve</a></li>
									@endif
								@endif
							@endif
							

							@if($x->paytype=='cash')
								<li><a href="/payments/{{$x->cash_payment_id}}/cash-details"><i class="fa fa-info-circle" aria-hidden="true"></i>Details</a></li>
							@elseif($x->paytype=='deposit')
								<li><a href="/payments/{{$x->deposit_payment_id}}/deposit-details"><i class="fa fa-info-circle" aria-hidden="true"></i>Details</a></li>
							@else
								<li><a href="/payments/{{$x->check_payment_id}}/check-details"><i class="fa fa-info-circle" aria-hidden="true"></i>Details</a></li>
							@endif
						</ul>					
					</div>
				</td>
			</tr>
			@endforeach 
	</table>
	<div style="text-align: center">

		@if(Request::get('approved')!=null)
			{{ $payments->appends(['approved' => Request::get('approved')])->links() }}
		@else
			{{ $payments->links() }}
		@endif
	</div>
</div>
@stop