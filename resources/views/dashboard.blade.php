@extends('layout.app')
@section('title', 'Welcome to '.Session::get('software_name'))
@section('app_name', Session::get('software_name'))
@section('content')
<div class="container-fluid">

				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">
							Dashboard <small>Overview</small>
						</h1>
						<ol class="breadcrumb">
							<li class="active">
								<i class="fa fa-dashboard"></i> Invoice Overview 
							</li>
						</ol>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-calculator fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge1">{{ number_format($sales_invoice_amount,0) }}</div>
										<div>Invoice total: {{ $sales_invoice_count }}</div>
									</div>
								</div>
							</div>
							{{ Form::open(array('url' => '/reports/sales/', 'method' => 'GET','name'=>'invoice','style'=>'margin: 0','class'=>'panel-primary')) }}
							<a href="#" onclick="document.invoice.submit()">
								<div class="panel-footer">
									<span class="pull-left">View Details</span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
							{!! Form::close() !!}
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-green">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-check fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge1">{{ number_format($sales_invoice_paid_amount,0) }}</div>
										<div>Paid total: {{ $sales_invoice_paid }} </div>
									</div>
								</div>
							</div>
							{{ Form::open(array('url' => '/reports/sales/', 'method' => 'GET','name'=>'paid','style'=>'margin: 0','class'=>'panel-green')) }}
							<a href="#" onclick="document.paid.submit()">
							<input type="hidden" name="status" value="paid"/>
								<div class="panel-footer">
									<span class="pull-left">View Details</span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
							{!! Form::close() !!}
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-yellow">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-check-square fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge1">{{ number_format($sales_partially_paid_amount,0) }}</div>
										<div>Partially Paid :{{ $sales_partially_paid }}</div>
									</div>
								</div>
							</div>
							{{ Form::open(array('url' => '/reports/sales/', 'method' => 'GET','name'=>'partially_paid','style'=>'margin: 0','class'=>'panel-yellow')) }}
							<input type="hidden" name="status" value="partially-paid"/>
							<a href="#" onclick="document.partially_paid.submit()">
								<div class="panel-footer">
									<span class="pull-left">View Details</span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
							{!! Form::close() !!}
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-purple">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-ellipsis-h fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge1">{{ number_format($unpaid_amount,0) }} </div>
										<div>Unpaid total:{{ $sales_unpaid }}</div>
									</div>
								</div>
							</div>
							{{ Form::open(array('url' => '/reports/sales/', 'method' => 'GET','name'=>'unpaid','style'=>'margin: 0','class'=>'panel-purple')) }}
							<a href="#" onclick="document.unpaid.submit()">
							<input type="hidden" name="status" value="unpaid"/>
								<div class="panel-footer">
									<span class="pull-left">View Details </span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-pink">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-money fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge1">{{ number_format($cash_payments_amount,0) }}</div>
										<div>Cash/Deposit :{{ $cash_payments }}</div>
									</div>
								</div>
							</div>
							{{ Form::open(array('url' => '/reports/payments/', 'method' => 'GET','name'=>'cash','style'=>'margin: 0','class'=>'panel-pink')) }}
							<a href="#" onclick="document.cash.submit()">
							<input type="hidden" name="pay_type" value="cashdeposit"/>
							<input type="hidden" name="approved" value="1"/>
								<div class="panel-footer">
									<span class="pull-left">View Details</span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
							{!! Form::close() !!}
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-grey">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-pencil-square-o fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge1">{{ number_format($check_payments_amount,0) }}</div>
										<div>Check Payments:{{ $check_payments }}</div>
									</div>
								</div>
							</div>
							{{ Form::open(array('url' => '/reports/payments/', 'method' => 'GET','name'=>'check','style'=>'margin: 0','class'=>'panel-grey')) }}
							<a href="#" onclick="document.check.submit()">
							<input type="hidden" name="pay_type" value="check"/>
							<input type="hidden" name="approved" value="1"/>
								<div class="panel-footer">
									<span class="pull-left">View Details</span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
							{!! Form::close() !!}
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-red">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-exclamation-circle fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge1">{{ number_format($sales_overdue_amount,0) }}</div>
										<div>Overdue: {{ $sales_overdue }}</div>
									</div>
								</div>
							</div>
							{{ Form::open(array('url' => '/reports/sales/', 'method' => 'GET','name'=>'overdue','style'=>'margin: 0','class'=>'panel-red')) }}
							<a href="#" onclick="document.overdue.submit()">
							<input type="hidden" name="status" value="over-due"/>
								<div class="panel-footer">
									<span class="pull-left">View Details</span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
							{!! Form::close() !!}
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-black">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-ban fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge1">{{ number_format($payments_pending_amount_cp+$payments_pending_amount_chk+$payments_pending_amount_dp,0) }} </div>
										<div>Pending total: {{ $payments_pending }}</div>
									</div>
								</div>
							</div>
							
							<a href="/payments?approved=0" onclick="document.pending.submit()">
								<div class="panel-footer">
									<span class="pull-left">View Details</span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
							
						</div>
					</div>					
				</div>
				<div class="row">
					<div class="col-lg-4">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i>Overview</h3>
							</div>
							<div class="panel-body">
								<div id="morris-donut-chart"></div>
							</div>
						</div>
					</div>
					<div class="col-lg-8">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i>Quick Links</h3>
							</div>
							<div class="panel-body">
								<div class="list-group">
									<div class="row">
										<div class="col-sm-4">
											<div class="panel panel-red">
												<div class="panel-heading">
													<div class="text-center">
														<a href="/invoice/create" style="color:#fff">Add Invoice</a>
													</div>													
												</div>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="panel panel-yellow">
												<div class="panel-heading">
													<div class="text-center">
														<a href="/invoice" style="color:#fff">List Invoice</a>
													</div>													
												</div>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="panel panel-purple">
												<div class="panel-heading">
													<div class="text-center">
														<a href="/clients/create" style="color:#fff">Add Client</a>
													</div>													
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="panel panel-green">
												<div class="panel-heading">
													<div class="text-center">
														<a href="/payments" style="color:#fff">Payments</a>
													</div>													
												</div>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="panel panel-grey">
												<div class="panel-heading">
													<div class="text-center">
														<a href="/managers/create" style="color:#fff">Add Manager</a>
													</div>													
												</div>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="panel panel-primary">
												<div class="panel-heading">
													<div class="text-center">
														<a href="/products/create" style="color:#fff">Add Product</a>
													</div>													
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="panel panel-teal">
												<div class="panel-heading">
													<div class="text-center">
														<a href="/users/create" style="color:#fff">Add User</a>
													</div>
													
												</div>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="panel panel-dark-green">
												<div class="panel-heading">
													<div class="text-center">
														<a href="/product/import" style="color:#fff">Import Products</a>
													</div>
													
												</div>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="panel panel-black">
												<div class="panel-heading">
													<div class="text-center">
														<a href="/auth/logout" style="color:#fff">Logout</a>
													</div>
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>					
				</div>
			</div>

<script src="{{ URL::asset('js/jquery.js') }}"></script>
<script>
$( document ).ready(function() {

	$(function() {
	Morris.Donut({
		element: 'morris-donut-chart',
		data: [{
			label: "Paid:{!! number_format($sales_invoice_paid_amount,Session('decimal')) !!}",
			value: {!! $sales_invoice_paid !!}
		}, {
			label: "Partially Paid:{!! number_format($sales_invoice_amount-$unpaid_amount-$sales_invoice_paid_amount,0) !!}",
			value: {!! $sales_partially_paid !!}
		}, {
			label: "Unpaid Total:{!! number_format($unpaid_amount,Session('decimal')) !!}",
			value: {!! $sales_unpaid !!} 
		}, {
			label: "Overdue: {!! number_format($sales_overdue_amount,Session('decimal')) !!}",
			value: {!! $sales_overdue !!}
		}, {
			label: "Pending : {!! number_format($payments_pending_amount_chk+$payments_pending_amount_cp,Session('decimal')) !!}",
			value: {!! $sales_invoice_paid !!}
		}, {
			label: "Total Invoice : {!! number_format($sales_invoice_amount,Session('decimal')) !!}",
			value: {!! $sales_invoice_count !!}
		}],
		resize: true
	});
});
});
</script>
@endsection