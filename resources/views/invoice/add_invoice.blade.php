@extends('layout.app')
@section('title', 'Add Invoice')
@section('app_name', Session::get('software_name'))
@section('content')
<style type="text/css">
	
.md-datepicker-button {
  display: none;
}
.md-datepicker-input-container {
	width: 100%;
	margin-left:0;
	padding-bottom: 0px; 
    border-bottom-width: 0px; 
    border-bottom-style: solid; 
}
.checkmark {
  width: 200px;
  margin: 0 auto;
  padding-top: 40px;
}
.path {
  stroke-dasharray: 1000;
  stroke-dashoffset: 0;
  animation: dash 2s ease-in-out;
  -webkit-animation: dash 2s ease-in-out;
}

.spin {
  animation: spin 2s;
  -webkit-animation: spin 2s;
  transform-origin: 50% 50%;
  -webkit-transform-origin: 50% 50%;
}
p {
  font-family: sans-serif;
  color: pink;
  font-size: 30px;
  font-weight: bold;
  margin: 20px auto;
  text-align: center;
  animation: text .5s linear .4s;
  -webkit-animation: text .4s linear .3s;
}
@-webkit-keyframes dash {
 0% {
   stroke-dashoffset: 1000;
 }
 100% {
   stroke-dashoffset: 0;
 }
}
@keyframes dash {
 0% {
   stroke-dashoffset: 1000;
 }
 100% {
   stroke-dashoffset: 0;
 }
}
@-webkit-keyframes spin {
  0% {
    -webkit-transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
  }
}
@keyframes spin {
  0% {
    -webkit-transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
  }
}
@-webkit-keyframes text {
  0% {
    opacity: 0; }
  100% {
    opacity: 1;
  }
  @keyframes text {
  0% {
    opacity: 0; }
  100% {
    opacity: 1;
  }
}
.ellipsis {
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
}
.row {
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display:         flex;
  flex-wrap: wrap;
}
.row > [class*='col-'] {
  display: flex;
  flex-direction: column;
}

.clearfixfix {
  content: "";
  clear: both;
  display: block;
}
</style>
<div style="height: 100%;width: 100%;margin-top: 50px;" class="ng-cloak" ng-controller="InvoiceController" ng-init="tax_rate={{ Session::get('tax_rate') }};initadd();decimal_value={{ Session::get('decimal') }};is_edit=0">
	<div class="row">
		<div class="col-lg-12">
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-shopping-basket" aria-hidden="true"></i> Add Invoice
				</li>
			</ol>
			@if(Session::has('print_now'))
			@endif
			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
		</div>
	</div>
	{{ Form::open(array('url' => '/invoice', 'method' => 'store','name' => 'invoiceForm','class' => 'form-register')) }}
	<div class="row">
		<div class="col-lg-3">		
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('invoice_date', 'Invoice Date') }}
				
					<input type="hidden" ng-model="invoice_date" value="<% invoice_date %>" name="invoice_date">
					<md-datepicker style="width: 100%;padding-left:10;padding-right:0;" class="form-control input-sm" ng-model="invoice_date" name="invoice_date" id="invoice_date" md-placeholder="Enter date">
					 </md-datepicker>
				<span class="errors" style="color:#FF0000">{{$errors->first('invoice_date')}}</span>
				<div class="col-sm-8" ng-show="invoiceForm.invoice_date.$dirty && invoiceForm.invoice_date.$invalid">
					<span class="errors" style="color:#FF0000" ng-show="invoiceForm.invoice_date.$error.required">Invoice date is required</span>
				</div>								
			</div>
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('si', 'S.I. No') }}
				<input class="form-control span6 input-sm" ng-class="{ 'submitted' : invoiceForm.si.$invalid && invoiceForm.si.$dirty}" placeholder="S.I. No" name="si" type="si" ng-model="si" id="si" ng-minlength="4" required>
				<span class="errors" style="color:#FF0000">{{$errors->first('si')}}</span>
				<div class="col-sm-12" ng-show="valid_si==1">
					<span class="errors" style="color:#FF0000" >SI Already taken</span>					
				</div>
				<div class="col-sm-12" ng-show="invoiceForm.si.$dirty && invoiceForm.si.$invalid">
					<span class="errors" style="color:#FF0000" ng-show="invoiceForm.si.$error.required">S.I. No. is required</span>
					<span class="errors" style="color:#FF0000" ng-show="invoiceForm.si.$error.minlength">S.I. min of 4 characters</span>
				</div>
			</div>
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('po', 'P.O. No') }}
				<input class="form-control span6 input-sm" ng-class="{ 'submitted' : invoiceForm.po.$invalid && invoiceForm.po.$dirty}" placeholder="P.O. No" name="po" type="po" ng-model="po" id="po" ng-minlength="4" ng-disabled="(valid_si==1) || !invoiceForm.si.$dirty || invoiceForm.si.$invalid" required>
				<span class="errors" style="color:#FF0000">{{$errors->first('po')}}</span>
				<div class="col-sm-12" ng-show="valid_po==1">
					<span class="errors" style="color:#FF0000" >PO Already taken</span>					
				</div>
				<div class="col-sm-8" ng-show="invoiceForm.po.$dirty && invoiceForm.po.$invalid">
					<span class="errors" style="color:#FF0000" ng-show="invoiceForm.po.$error.required">P.O. No. is required</span>
				</div>
			</div>
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('pay_type', 'Pay Type') }}
				<select name="pay_type" class="form-control span6 input-sm" ng-disabled="(valid_si==1) || !invoiceForm.si.$dirty || invoiceForm.si.$invalid">
					<option value="cash">Cash</option>
					<option value="check">Check</option>
				</select>
				<span class="errors" style="color:#FF0000">{{$errors->first('pay_type')}}</span>
			</div>

			<div class="row" style="margin-left: 10px">
				{{ Form::label('client_name', 'Client Name') }}
				<div class="input-group">
					<input class="form-control span6 input-sm" placeholder="Client Name" type="text" id="selected_client" ng-model="selected_client" uib-typeahead="client as client.client_name for client in clients | filter:{client_name:$viewValue}" class="form-control" typeahead-show-hint="true" typeahead-min-length="0" ng-disabled="(valid_si==1) || !invoiceForm.si.$dirty || invoiceForm.si.$invalid" autocomplete="off">
					<input type="hidden" class="form-control span6 input-sm"  name="client_id" ng-value="selected_client.id" ng-model="selected_client.id" id="client_id">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button" ng-click="newClient()"><i class="fa fa-plus" aria-hidden="true"></i></button>
					</span>
				</div>
				<span class="errors" style="color:#FF0000">{{$errors->first('client_name')}}</span>
				<div class="col-sm-8" ng-show="invoiceForm.client_name.$dirty && invoiceForm.client_name.$invalid">
					<span class="errors" style="color:#FF0000" ng-show="invoiceForm.client_name.$error.required">Client is required</span>
				</div>
			</div>
			<div class="row" style="margin-left: 10px">
				{{ Form::label('branch_id', 'Branch') }}
				<div class="input-group">
					<input class="form-control span6 input-sm" placeholder="Client Branch" type="text" ng-model="selected_branch" uib-typeahead="branch as branch.branch_name for branch in client_branches | filter:{branch_name:$viewValue}" class="form-control" typeahead-show-hint="true" typeahead-min-length="0" ng-disabled="(valid_si==1) || !invoiceForm.si.$dirty || invoiceForm.si.$invalid" autocomplete="off">
					<input type="hidden" class="form-control span6 input-sm"  name="branch_id" ng-value="selected_branch.id" ng-model="selected_branch.id" id="branch_id">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button" ng-disabled="selected_client.id==undefined" ng-click="newBranch()"><i class="fa fa-plus" aria-hidden="true"></i></button>
					</span>
				</div>
				<span class="errors" style="color:#FF0000">{{$errors->first('branch_name')}}</span>
				<div class="col-sm-8" ng-show="invoiceForm.branch_name.$dirty && invoiceForm.branch_name.$invalid">
					<span class="errors" style="color:#FF0000" ng-show="invoiceForm.branch_name.$error.required">Branch is required</span>
				</div>
			</div>
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('address', 'Address') }}
				<input class="form-control span6 input-sm" placeholder="Address" name="address" type="text" ng-model="selected_branch.address" id="address" readonly="true">
				<span class="errors" style="color:#FF0000">{{$errors->first('address')}}</span>
			</div>

			<div class="row" style="margin-left: 10px">				
				{{ Form::label('account_manager_id', 'Account Manager') }}
				<input class="form-control span6 input-sm" placeholder="Account Manager" name="account_manager" type="account_manager" ng-model="account_manager" id="account_manager" readonly="true">
				<input type="hidden" class="form-control span6 input-sm" placeholder="Account Manager" name="account_manager_id" ng-model="selected_client.account_manager_id" ng-value="selected_client.account_manager_id" id="account_manager_id">
				<span class="errors" style="color:#FF0000">{{$errors->first('account_manager_id')}}</span>
			</div>
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('credit_limit', 'Credit Limit') }}
				<input class="form-control span6 input-sm" placeholder="Credit Limit" name="credit_limit" type="text" ng-model="selected_client.credit_limit | number:2" id="credit_limit" readonly="true" >
				<span class="errors" style="color:#FF0000">{{$errors->first('credit_limit')}}</span>
			</div>
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('balance', 'Balance') }}
				<input class="form-control span6 input-sm" placeholder="Balance" name="balance" type="text" ng-model="selected_client.balance  | number:2" id="balance" readonly="true" >
				<span class="errors" style="color:#FF0000">{{$errors->first('balance')}}</span>
			</div>

			<div class="row" style="margin-left: 10px">				
				{{ Form::label('terms', 'Terms(days)') }}
				<input class="form-control span6 input-sm" ng-class="{ 'submitted' : invoiceForm.terms.$invalid && invoiceForm.terms.$dirty}" placeholder="Terms(days)" name="terms" type="number" ng-model="selected_client.terms" id="terms">
				<span class="errors" style="color:#FF0000">{{$errors->first('terms')}}</span>
			</div>
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('status', 'Status') }}
				<select name="status" class="form-control span6 input-sm">
					<option value="unpaid">Un-paid</option>
					<option value="paid">Paid</option>					
				</select>
				<span class="errors" style="color:#FF0000">{{$errors->first('status')}}</span>
			</div>
		</div>
		<div class="col-lg-9" style="overflow:auto;" >		
			{{ Form::label('items', 'Particulars') }}
			<div class="well" style="height: 300px;max-height: 300px; overflow-y:scroll; ">
				<div class="row">
					<div class="col-xs-2 col-lg-2"><strong>Product Code</strong></div>
					<div class="col-xs-3 col-lg-3"><strong>Product</strong></div>						
					<div class="col-xs-2 col-lg-2"><strong>Unit Cost</strong></div>
					<div class="col-xs-2 col-lg-2"><strong>QTY</strong></div>
					<div class="col-xs-2 col-lg-2"><strong>Total Amount</strong></div>
					<div class="col-xs-1 col-lg-1"></div>
				</div>
				<div class="row" ng-repeat="x in selected_product">
					<input type="hidden" name="prod_id[]" value="<% x.id %>">
					<input type="hidden" name="prod_qty[]"  value="<% x.qty %>">
					<input type="hidden" name="prod_price[]" value="<% x.prod_price %>">
					<div class="col-xs-2 col-lg-2"><% x.prod_code %></div>
					<div class="col-xs-3 col-lg-3"><% x.prod_name %></div>						
					<div class="col-xs-2 col-lg-2"><% x.prod_price | number:decimal_value %></div>
					<div class="col-xs-2 col-lg-2"><% x.qty %></div>
					<div class="col-xs-2 col-lg-2"><% x.total | number:decimal_value %></div>
					<div class="col-xs-1 col-lg-1">
						<div class="dropdown">
							<button class="btn btn-primary dropdown-toggle btn-sm" type="button" data-toggle="dropdown">
							<span class="fa fa-caret-square-o-down"></span></button>
							<ul class="dropdown-menu dropdown-menu-right">
								<li><a href="" ng-click="changePrice($index)"><i class="fa fa-usd margin-right"></i>Change Price</a></li>
								<li><a href="" ng-click="changeQTY($index)"><i class="fa fa-exchange margin-right"></i>Change QTY</a></li>
								<li><a href="" class="confirmation" ng-click="removeItem($index)"><i class="fa fa-trash margin-right"></i>Delete</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<input type="button" class="btn btn-primary" ng-click="AddItem()" value="Choose Product" ng-disabled="selected_item==[] ||invoiceForm.si.$invalid || invoiceForm.po.$invalid || invoiceForm.client_name.$invalid || invoiceForm.branch_name.$invalid || selected_client.id==undefined || selected_branch.id==undefined"/>	

			<input type="button" class="btn btn-primary" ng-click="newItem()" value="Add New Product" ng-disabled="selected_item==[] ||invoiceForm.si.$invalid || invoiceForm.po.$invalid || invoiceForm.client_name.$invalid || invoiceForm.branch_name.$invalid || selected_client.id==undefined || selected_branch.id==undefined"/>	
		</div>
		<div class="col-lg-4" style="margin-top: 20px;">		
			<div class="col-lg-8">	
				<div class="row">				
					{{ Form::label('discount', 'Discount',['class' => 'form-inline']) }}
					<input class="form-control span6 input-sm" placeholder="0" name="discount" type="number" ng-model="discount" id="discount">
					<span class="errors" style="color:#FF0000">{{$errors->first('discount')}}</span>
				</div>

				<div class="row">				
					{{ Form::label('vat_sales', 'Vat Sales',['class' => 'form-inline']) }}
					<input class="form-control span6 input-sm" placeholder="0" name="vat_sales" type="vat_sales" ng-model="vat_sales | number:2" id="vat_sales" readonly="true">
					<span class="errors" style="color:#FF0000">{{$errors->first('vat_sales')}}</span>
				</div>

				<div class="row">				
					{{ Form::label('tax_amount', 'Tax Amount',['class' => 'form-inline']) }}
					<input class="form-control span6 input-sm" placeholder="0" name="tax_amount" type="tax_amount" ng-model="tax_amount | number:2" id="tax_amount" readonly="true">
					<span class="errors" style="color:#FF0000">{{$errors->first('tax_amount')}}</span>
				</div>
				<div class="row">				
					{{ Form::label('total_amount_due', 'Total Amount Due',['class' => 'form-inline']) }}
					<input class="form-control span6 input-sm" placeholder="0" name="total_amount_due" type="total_amount_due" ng-model="total_amount_due | number:2" id="total_amount_due" readonly="true">
					<span class="errors" style="color:#FF0000">{{$errors->first('total_amount_due')}}</span>
				</div>
			</div>	
		</div>
		
		<div class="col-lg-4" style="margin-top: 20px;">		
			<div class="col-lg-8">	
				<div class="row">			
					<input type="submit" id="save_invoice_btn" class="btn btn-primary" value="Save Invoice" ng-disabled="selected_item==[] ||invoiceForm.si.$invalid || invoiceForm.po.$invalid || invoiceForm.client_name.$invalid || invoiceForm.branch_name.$invalid || selected_client.id==undefined || selected_branch.id==undefined || valid_po==1 || valid_si==1 || total_amount_due>selected_client.credit_limit"/>	
				</div>				
			</div>	
		</div>	
	</div>
	{!! Form::close() !!}
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" style="text-align:center">
      	<div class="checkmark">
			  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="0 0 161.2 161.2" enable-background="new 0 0 161.2 161.2" xml:space="preserve">
			<path class="path" fill="none" stroke="#5cb85c" stroke-miterlimit="10" d="M425.9,52.1L425.9,52.1c-2.2-2.6-6-2.6-8.3-0.1l-42.7,46.2l-14.3-16.4
				c-2.3-2.7-6.2-2.7-8.6-0.1c-1.9,2.1-2,5.6-0.1,7.7l17.6,20.3c0.2,0.3,0.4,0.6,0.6,0.9c1.8,2,4.4,2.5,6.6,1.4c0.7-0.3,1.4-0.8,2-1.5
				c0.3-0.3,0.5-0.6,0.7-0.9l46.3-50.1C427.7,57.5,427.7,54.2,425.9,52.1z"/>
			<circle class="path" fill="none" stroke="#5cb85c" stroke-width="4" stroke-miterlimit="10" cx="80.6" cy="80.6" r="62.1"/>
			<polyline class="path" fill="none" stroke="#5cb85c" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="113,52.8 
				74.1,108.4 48.2,86.4 "/>

			<circle class="spin" fill="none" stroke="#5cb85c" stroke-width="4" stroke-miterlimit="10" stroke-dasharray="12.2175,12.2175" cx="80.6" cy="80.6" r="73.9"/>
			</svg>
			</div>
			<p style="color:#5cb85c">Invoice Added!!</p>
       <a href="/report/invoice/{{ Session::get('print_now') }}/print/" target="_blank"><button class="btn btn-primary">PRINT INVOICE</button></a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@stop

