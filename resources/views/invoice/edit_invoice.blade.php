@extends('layout.app')
@section('title', 'Add Invoice')
@section('app_name', Session::get('software_name'))
@section('content')
<style type="text/css">
.table {
    table-layout:fixed;
}
.table td {
}
</style>
<div style="height: 100%;width: 100%;margin-top: 50px;" class="ng-cloak" ng-controller="InvoiceController" ng-init="tax_rate={{ Session::get('tax_rate') }};init({{$invoice->id}});decimal_value={{ Session::get('decimal') }};initbalance({{$client->id}});vatable({{$client->vatable}});is_edit=1">
	<div class="row">
		<div class="col-lg-12">
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-shopping-basket" aria-hidden="true"></i> Add Invoice
				</li>
			</ol>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
		</div>
	</div>
	{{ Form::open(array('url' => '/invoice/'.$invoice->id, 'method' => 'PUT','name' => 'invoiceForm','class' => 'form-register')) }}
	<div class="row">
		<div class="col-lg-3">		
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('invoice_date', 'Invoice Date',['class' => 'form-inline']) }}
				<div class="controls input-group date" id='dtp1' >
					<input type='text' placeholder="Invoice Date" name="invoice_date" class="form-control span6"  name="invoice_date" type="invoice_date"  value="{{$invoice->invoice_date}}" id="invoice_date" required="true" on-click="testClick();"/>
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
				<span class="errors" style="color:#FF0000">{{$errors->first('invoice_date')}}</span>
				<div class="col-sm-8" ng-show="invoiceForm.invoice_date.$dirty && invoiceForm.invoice_date.$invalid">
					<span class="errors" style="color:#FF0000" ng-show="invoiceForm.invoice_date.$error.required">Invoice date is required</span>
				</div>								
			</div>
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('si', 'S.I. No') }}
				<input class="form-control span6 input-sm" ng-class="{ 'submitted' : invoiceForm.si.$invalid && invoiceForm.si.$dirty}" placeholder="S.I. No" value="{{$invoice->si}}" name="si" type="si" id="si" ng-minlength="5" readonly="true">
				<span class="errors" style="color:#FF0000">{{$errors->first('si')}}</span>
				<div class="col-sm-8" ng-show="invoiceForm.si.$dirty && invoiceForm.si.$invalid">
					<span class="errors" style="color:#FF0000" ng-show="invoiceForm.si.$error.required">S.I. No. is required</span>
				</div>
			</div>
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('po', 'P.O. No') }}
				<input class="form-control span6 input-sm" ng-class="{ 'submitted' : invoiceForm.po.$invalid && invoiceForm.po.$dirty}" placeholder="P.O. No" name="po" value="{{$invoice->po}}" type="po" id="po" ng-minlength="5" readonly="true">
				<span class="errors" style="color:#FF0000">{{$errors->first('po')}}</span>
				<div class="col-sm-8" ng-show="invoiceForm.po.$dirty && invoiceForm.po.$invalid">
					<span class="errors" style="color:#FF0000" ng-show="invoiceForm.po.$error.required">P.O. No. is required</span>
				</div>
			</div>
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('pay_type', 'Pay Type') }}
				<select name="pay_type" class="form-control span6 input-sm">
					<option value="cash">Cash</option>
					<option value="check">Check</option>
				</select>
				<span class="errors" style="color:#FF0000">{{$errors->first('pay_type')}}</span>
			</div>
			<div class="row" style="margin-left: 10px">
				{{ Form::label('client_name', 'Client Name') }}
				<div class="input-group">
					<input class="form-control span6 input-sm" placeholder="Client Name" name="client_name" type="text" value="{{$client->client_name}}" id="client_name" required="true" readonly="true" >
					<input type="hidden" class="form-control span6 input-sm"  name="client_id" value="{{$client->id}}" id="client_id">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button" disabled="true"><i class="fa fa-plus" aria-hidden="true"></i></button>
					</span>
				</div>
				
			</div>

			<div class="row" style="margin-left: 10px">
				{{ Form::label('branch_id', 'Branch') }}
				<div class="input-group">
					<input class="form-control span6 input-sm" placeholder="Branch" name="branch_name" type="text" value="{{$branch->branch_name}}" id="branch_name" required="true" readonly="true">
					<input type="hidden" class="form-control span6 input-sm"  name="branch_id" value="{{$branch->id}}" id="branch_id">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button" disabled="true" ng-click="FetchClientBranches()"><i class="fa fa-plus" aria-hidden="true"></i></button>
					</span>
				</div>
				<span class="errors" style="color:#FF0000">{{$errors->first('branch_name')}}</span>
				<div class="col-sm-8" ng-show="invoiceForm.branch_name.$dirty && invoiceForm.branch_name.$invalid">
					<span class="errors" style="color:#FF0000" ng-show="invoiceForm.branch_name.$error.required">Branch is required</span>
				</div>
			</div>
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('address', 'Address') }}
				<input class="form-control span6 input-sm" placeholder="Address" name="address" type="text" value="{{$branch->address}}" id="address" readonly="true">
				<span class="errors" style="color:#FF0000">{{$errors->first('address')}}</span>
			</div>
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('account_manager_id', 'Account Manager') }}
				<input class="form-control span6 input-sm" placeholder="Account Manager" name="account_manager" type="account_manager" value="{{ $manager->first_name }} {{$manager->last_name}}" id="account_manager" readonly="true">
				<input type="hidden" class="form-control span6 input-sm" placeholder="Account Manager" name="account_manager_id" value="{{$manager->id}}" id="account_manager_id">
				<span class="errors" style="color:#FF0000">{{$errors->first('account_manager_id')}}</span>
			</div>
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('credit_limit', 'Credit Limit') }}
				<input class="form-control span6 input-sm" placeholder="Credit Limit" name="credit_limit" type="text" value="{{ number_format($client->credit_limit,Session('decimal')) }} " id="credit_limit" readonly="true">
				<span class="errors" style="color:#FF0000">{{$errors->first('credit_limit')}}</span>
			</div>
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('balance', 'Balance') }}
				<input class="form-control span6 input-sm" placeholder="Balance" name="balance" type="text" value="{{ number_format($invoice->balance,Session('decimal')) }}" id="balance" readonly="true">
				<span class="errors" style="color:#FF0000">{{$errors->first('balance')}}</span>
			</div>

			<div class="row" style="margin-left: 10px">				
				{{ Form::label('terms', 'Terms(days)') }}
				<input class="form-control span6 input-sm" ng-class="{ 'submitted' : invoiceForm.terms.$invalid && invoiceForm.terms.$dirty}" placeholder="Terms(days)" name="terms" type="number" value="{{$client->terms}}" id="terms">
				<span class="errors" style="color:#FF0000">{{$errors->first('terms')}}</span>
			</div>
			<div class="row" style="margin-left: 10px">				
				{{ Form::label('status', 'Status') }}
				<select name="status" class="form-control span6 input-sm">
					<option value="unpaid">Un-paid</option>
					<option value="paid">Paid</option>					
				</select>
				<span class="errors" style="color:#FF0000">{{$errors->first('status')}}</span>
			</div>
		</div>
		<div class="col-lg-9" style="overflow:auto;" id="particulars" >		
			{{ Form::label('items', 'Particulars') }}
			{{ Form::label('items', 'Particulars') }}
			<div class="well" style="height: 300px;max-height: 300px; overflow-y:scroll; ">
				<div class="row">
					<div class="col-xs-2 col-lg-2"><strong>Product Code</strong></div>
					<div class="col-xs-3 col-lg-3"><strong>Product</strong></div>						
					<div class="col-xs-2 col-lg-2"><strong>Unit Cost</strong></div>
					<div class="col-xs-2 col-lg-2"><strong>QTY</strong></div>
					<div class="col-xs-2 col-lg-2"><strong>Total Amount</strong></div>
					<div class="col-xs-1 col-lg-1"></div>
				</div>
				<div class="row" ng-repeat="x in selected_product">
					<input type="hidden" name="prod_id[]" value="<% x.id %>">
					<input type="hidden" name="prod_qty[]" value="<% x.qty %>">
					<input type="hidden" name="prod_price[]" value="<% x.prod_price %>">
					<div class="col-xs-2 col-lg-2"><% x.prod_code %></div>
					<div class="col-xs-3 col-lg-3"><% x.prod_name %></div>						
					<div class="col-xs-2 col-lg-2"><% x.prod_price | number:decimal_value %></div>
					<div class="col-xs-2 col-lg-2"><% x.qty %></div>
					<div class="col-xs-2 col-lg-2"><% x.total | number:decimal_value %></div>
					<div class="col-xs-1 col-lg-1">
						<div class="dropdown">
							<button class="btn btn-primary dropdown-toggle btn-sm" type="button" data-toggle="dropdown">
							<span class="fa fa-caret-square-o-down"></span></button>
							<ul class="dropdown-menu dropdown-menu-right">
								<li><a href="" ng-click="changePrice($index)"><i class="fa fa-usd margin-right"></i>Change Price</a></li>
								<li><a href="" ng-click="changeQTY($index)"><i class="fa fa-exchange margin-right"></i>Change QTY</a></li>
								<li><a href="" class="confirmation" ng-click="removeItem($index)"><i class="fa fa-trash margin-right"></i>Delete</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<input type="button" class="btn btn-primary" ng-click="AddItem()" value="Choose Product" />	

			<input type="button" class="btn btn-primary" ng-click="newItem()" value="Add New Product" />	

			
		</div>
		
		
		<div class="col-lg-4" style="margin-top: 20px;">		
			<div class="col-lg-8">	
				<div class="row">				
					{{ Form::label('discount', 'Discount',['class' => 'form-inline']) }}
					<input class="form-control span6 input-sm" placeholder="0" name="discount" type="number" ng-model="discount" id="discount">
					<span class="errors" style="color:#FF0000">{{$errors->first('discount')}}</span>
				</div>
				<div class="row">				
					{{ Form::label('vat_sales', 'Vat Sales',['class' => 'form-inline']) }}
					<input class="form-control span6 input-sm" placeholder="0" name="vat_sales" type="vat_sales" ng-model="vat_sales" id="vat_sales" readonly="true">
					<span class="errors" style="color:#FF0000">{{$errors->first('vat_sales')}}</span>
				</div>
				<div class="row">				
					{{ Form::label('tax_amount', 'Tax Amount',['class' => 'form-inline']) }}
					<input class="form-control span6 input-sm" placeholder="0" name="tax_amount" type="tax_amount" ng-model="tax_amount" id="tax_amount" readonly="true">
					<span class="errors" style="color:#FF0000">{{$errors->first('tax_amount')}}</span>
				</div>
				<div class="row">				
					{{ Form::label('total_amount_due', 'Total Amount Due',['class' => 'form-inline']) }}
					<input class="form-control span6 input-sm" placeholder="0" name="total_amount_due" type="total_amount_due" ng-model="total_amount_due" id="total_amount_due" readonly="true">
					<span class="errors" style="color:#FF0000">{{$errors->first('total_amount_due')}}</span>
				</div>
			</div>	
		</div>

		<div class="col-lg-4" style="margin-top: 20px;">		
			<div class="col-lg-8">	
				<div class="row">				
					<input type="submit" id="update_invoice_btn" class="btn btn-default" ng-click="Save()" value="Update Invoice" ng-disabled="invoiceForm.si.$invalid || invoiceForm.po.$invalid || invoiceForm.client_name.$invalid || invoiceForm.branch_name.$invalid || total_amount_due>selected_client.credit_limit"/>	
				</div>				
			</div>	
		</div>	
	</div>
	{!! Form::close() !!}
</div>
@stop