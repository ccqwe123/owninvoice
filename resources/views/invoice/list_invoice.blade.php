@extends('layout.app')
@section('title', 'Sales Invoice List')
@section('app_name', Session::get('software_name'))
@section('content')


<div style=" height: 800px; overflow: scroll;" ng-controller="ListInvoiceController">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Sales Invoice <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-search"></i> Sales Invoice Search
				</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::open(array('url' => '/invoice', 'method' => 'get')) }}  
			<div class="input-group margin-bottom-sm">
			    <span class="input-group-addon"><button class="fa fa-search" style="border:none; background-color:transparent;"></button></span>
			    {{ Form::text('search','',array('class'=>'form-control span6','placeholder' => 'Search')) }}
			    <span class="errors" style="color:#FF0000">{{$errors->first('search')}}</span>
			</div>
			{{ Form::close() }}
		</div>
		<div class="col-lg-4">
			<a href="/invoice/create"><button class="btn btn-primary">Add Invoice</button></a>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-shopping-basket"></i> Sales Invoice
				</li>
			</ol>
		</div>
	</div>
	<div class="row text-center">
		<div class="col-lg-3">					
		</div>
		<div class="col-lg-2">
			<a href="#" class="btn btn-sq-xs btn-warning">
			 &nbsp;&nbsp;
			</a>Cancelled			
		</div>
		<div class="col-lg-2">
			<a href="#" class="btn btn-sq-xs btn-success">
			 &nbsp;&nbsp;
			</a>Paid						
		</div>
		<div class="col-lg-2">
			<a href="#" class="btn btn-sq-xs btn-danger">
			 &nbsp;&nbsp;
			</a>Overdue					
		</div>
		<div class="col-lg-3">					
		</div>
	</div>
	<br/>
	<table class="table" style="border: 1px solid black;font-size: {{ Session('font_size') }}px;">
		<tr style="border: 1px solid black;font-size: {{ Session('font_size') }}px;">
			@if(Request::get('sortby')!='client_name.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'client_name.asc']) }}">Client<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='client_name.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'client_name.desc']) }}">Client<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif
			
			@if(Request::get('sortby')!='branch_name.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'branch_name.asc']) }}">Branch<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='branch_name.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'branch_name.desc']) }}">Branch<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif

			@if(Request::get('sortby')!='si.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'si.asc']) }}">SI No.<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='si.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'si.desc']) }}">SI No.<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif

			@if(Request::get('sortby')!='po.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'po.asc']) }}">PO No.<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='po.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'po.desc']) }}">PO No.<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif

			@if(Request::get('sortby')!='invoice_date.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'invoice_date.asc']) }}">Invoice Date<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='invoice_date.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'invoice_date.desc']) }}">Invoice Date<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif

			@if(Request::get('sortby')!='amount_total.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'amount_total.asc']) }}">Sales amount<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='amount_total.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'amount_total.desc']) }}">Sales amount<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif

			@if(Request::get('sortby')!='tax_amount.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'tax_amount.asc']) }}">Tax AMT<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='tax_amount.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'tax_amount.desc']) }}">Tax AMT<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif

			@if(Request::get('sortby')!='balance.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'balance.asc']) }}">Balance<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='balance.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'balance.desc']) }}">Balance<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif

			@if(Request::get('sortby')!='pay_type.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'pay_type.asc']) }}">Pay Type<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='pay_type.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'pay_type.desc']) }}">Pay Type<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif

			@if(Request::get('sortby')!='terms.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'terms.asc']) }}">Terms<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='terms.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'terms.desc']) }}">Terms<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif

			@if(Request::get('sortby')!='due_date.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'due_date.asc']) }}">Due Date<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='due_date.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'due_date.desc']) }}">Due Date<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif

			@if(Request::get('sortby')!='status.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'status.asc']) }}">Status<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='status.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'status.desc']) }}">Status<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif

			@if(Request::get('sortby')!='manager.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'manager.asc']) }}">Manager<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='manager.asc')
				<th><a href="{{ URL::route('invoice-search',['sortby'=>'manager.desc']) }}">Manager<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif
			<th></th>
		</tr>
			@foreach ($sales_invoice as $x)
			<tr style="border: 1px solid black;" @if ($x->status=='cancelled') class="warning" @elseif ($x->status=='paid') class="success" @elseif ($x->status=='over-due') class="danger" @endif>
				<td>{{ $x->client_name }}</td>
				<td> @if($x->personal==1) personal @else {{$x->branch_name}} @endif </td>
				<td>{{ $x->si }}</td>
				<td>{{ $x->po }}</td>
				<td>{{ $x->invoice_date }}</td>
				<td>{{ number_format($x->amount_total,Session('decimal')) }}</td>
				<td> @if($x->vatable==1) {{ number_format(($x->amount_total-$x->discount)*Session('tax_rate'),Session('decimal')) }} @else {{number_format(0,Session('decimal')) }}@endif  </td>
				<td>{{ number_format($x->balance,Session('decimal')) }}</td>
				<td>{{ $x->pay_type }}</td>
				<td>{{ $x->terms }}</td>
				<td>{{ $x->due_date }}</td>
				<td>{{ $x->status }}</td>
				<td>{{ $x->manager }}</td>
				<td>
					<div class="dropdown">
						<button class="btn btn-primary dropdown-toggle btn-sm" type="button" data-toggle="dropdown">Actions
						<span class="caret"></span></button>
						<ul class="dropdown-menu dropdown-menu-right">
							<li>@if ($x->status!='cancelled')<a href="/invoice/{{$x->id}}/payments" title="Payments"><i class="fa fa-money" aria-hidden="true"></i>Payments</a>@endif</li>
							<li>@if ($x->status!='cancelled')<a href="#" title="Transfer" ng-click="showTransferModal({{$x->id}},{{$x->si}})"><i class="fa fa-arrows-h margin-right"></i>Transfer</a>@endif</li>
							<li><a href="/report/invoice/{{$x->id}}/print/" target="_blank" title="Print"><i class="fa fa-print margin-right"></i>Print</a></li>
							<li>@if ($x->status!='cancelled')<a href="/invoice/{{$x->id}}/edit" title="Edit"><i class="fa fa-pencil margin-right"></i>Edit</a>@endif</li>
							<li>@if ($x->status!='cancelled')<a href="/invoice-delete/{{$x->id}}" title="Delete" class="confirmation"><i class="fa fa-ban margin-right"></i>Cancel</a>@endif</li>

						</ul>
					</div>
				</td>
			</tr>
			@endforeach 
	</table>
	<div style="text-align: center">
		{{ $sales_invoice->appends(
				['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" ),
				'client_id' =>  (isset($_GET["client_id"]) ? $_GET["client_id"]:"" ),
				'branch_id' =>  (isset($_GET["branch_id"]) ? $_GET["branch_id"]:"" ),
				'account_manager_id' =>  (isset($_GET["account_manager_id"]) ? $_GET["account_manager_id"]:"" ),
				'status' =>  (isset($_GET["status"]) ? $_GET["status"]:"" ),
				'pay_type' =>  (isset($_GET["pay_type"]) ? $_GET["pay_type"]:"" ),
				'client_type' =>  (isset($_GET["client_type"]) ? $_GET["client_type"]:"" ),
				'from_date' =>  (isset($_GET["from_date"]) ? $_GET["from_date"]:"" ),
				'to_date' =>  (isset($_GET["to_date"]) ? $_GET["to_date"]:"" )
				]
				)->links() }}
	</div>
</div>
@stop