@extends('layout.app')
@section('title', 'Roles List')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Roles <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-shopping-basket"></i> {!! $role[0]->role_name !!}
				</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-3 col-sm-2">
			<p> Current Privilege</p>
		</div>
		<div class="col-xs-3 col-sm-2">
		</div>
		<div class="clearfix visible-xs-block"></div>
		<div class="col-xs-3 col-sm-2">
			<p>Privilege List</p>
		</div>
		{!! Form::close() !!}
	</div>
	<div class="row">
		{{ Form::open(array('url' => 'roles/'.$role[0]->id.'/update/delete', 'method' => 'store','style="margin:0"')) }}
		<div class="col-xs-3 col-sm-2">
			<select name="privilege_id" multiple class="form-control">
				@foreach($has as $x)
					<option value="{{ $x->privilege_id }}">{{ $x->name}}</option>
				@endforeach				
			</select>
		</div>
		<div class="col-xs-3 col-sm-2 text-center">
		<input type="submit" value=">>"/><br/>
		{!! Form::close() !!}
		<p></p>
		{{ Form::open(array('url' => 'roles/'.$role[0]->id.'/update/add', 'method' => 'store','style="margin:0"')) }}
			<input type="submit" value="<<"/>
		</div>
		<div class="clearfix visible-xs-block"></div>
		<div class="col-xs-3 col-sm-2">
			<select name="privilege_id" multiple class="form-control">
				@foreach($hasnot as $x)
					<option value="{{ $x->privilege_id }}">{{ $x->name}}</option>
				@endforeach				
			</select>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@stop