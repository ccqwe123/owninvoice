@extends('layout.app')
@section('title', 'Add Role')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 900px;width: 100%; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Roles <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-shopping-basket" aria-hidden="true"></i> Add Roles
				</li>
			</ol>
		</div>
	</div>
	{{ Form::open(array('url' => '/roles', 'method' => 'store')) }}
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('role_name', 'Role Name') }}
			{{ Form::text('role_name','',array('class'=>'form-control span6','placeholder' => 'Role Name')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('role_name')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			<input type="submit" vlue="Submit" class="btn btn-default" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
		</div>
	</div>
	{!! Form::close() !!}
</div>
@stop