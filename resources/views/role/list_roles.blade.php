@extends('layout.app')
@section('title', 'Roles List')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Roles <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
		</div>
	</div>
	<div class="row">	
		<div class="col-lg-4">
			<a href="/roles/create"><button class="btn btn-primary">Add Role</button></a>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-shopping-basket"></i> Roles List
				</li>
			</ol>
		</div>
	</div>
	<table class="table" style="border: 1px solid black;">
		<tr style="border: 1px solid black;">
			<th>Role Name</th>
			<th></th>
		</tr>
		  	@foreach ($roles as $x)
			<tr style="border: 1px solid black;">
				<td>{{ $x->role_name }}</td>
				<td>
					<div class="dropdown">
						<button class="btn btn-primary dropdown-toggle btn-sm" type="button" data-toggle="dropdown">Actions
						<span class="caret"></span></button>
						<ul class="dropdown-menu">
							<li><a href="/roles/{{$x->id}}/privilege" title="Privilege"><i class="fa fa-eye-slash" aria-hidden="true"></i>Privilege</a></li>
							<li><a href="/roles/{{$x->id}}/edit" title="Edit"><i class="fa fa-pencil margin-right"></i>Edit</a></li>
							<li><a href="/role-delete/{{$x->id}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>Delete</a></li>
						</ul>
					</div>
				</td>
			</tr>
			@endforeach 
	</table>
	<div style="text-align: center">
		{{ $roles->links() }}
	</div>
</div>
@stop