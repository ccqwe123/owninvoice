
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=yes">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>@yield('title')</title>

	<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/sb-admin.css') }}" rel="stylesheet" media="screen">
	<link href="{{ URL::asset('css/plugins/morris.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('css/bootstrap-datetimepicker.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/datetimepicker.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/summernote.css') }}" rel="stylesheet">
	<script src="{{ URL::asset('js/angular.min.js') }}"></script>
	<link href="{{ URL::asset('css/angular-material.min.css') }}" rel="stylesheet">
	<script src="{{ URL::asset('js/angular-animate.min.js') }}"></script>
	<script src="{{ URL::asset('js/angular-aria.min.js') }}"></script>
	<script src="{{ URL::asset('js/angular-messages.min.js') }}"></script>
	<script src="{{ URL::asset('js/angular-material.min.js') }}"></script>
	<script src="{{ URL::asset('js/ui-bootstrap.js') }}"></script>
</head>

<body ng-app="app">

	<div id="wrapper"> 
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">@yield('app_name')</a>
			</div>
			<ul class="nav navbar-right top-nav">
	
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Session::get('username') }} <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li>
							<a href="/settings/password/change"><i class="fa fa-fw fa-gear"></i> Password</a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="/auth/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
						</li>
					</ul>
				</li>
			</ul>
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav side-nav">
					<li class="active">
						<a href="/dashboard"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
					</li>
					@if (Session::get('is_allow_invoice') > 0)
					<li>
						<a href="javascript:;" data-toggle="collapse" data-target="#sales"><i class="fa fa-fw fa-file-text"></i> Sales Invoice <i class="fa fa-fw fa-caret-down"></i></a>
						<ul id="sales" class="collapse">
							<li>
								<a href="/invoice">List Invoices</a>
							</li>
							<li>
								<a href="/invoice/create">Add Invoice</a>
							</li>
							@if (Session::get('is_allow_payment')>0)
							<li>
								<a href="/payments">Payments</a>
							</li>
							@endif
						</ul>
					</li>
					@endif
					@if (Session::get('is_allow_products') > 0)
					<li>
						<a href="javascript:;" data-toggle="collapse" data-target="#products"><i class="fa fa-fw fa-list"></i> Products <i class="fa fa-fw fa-caret-down"></i></a>
						<ul id="products" class="collapse">
							<li>
								<a href="/products">List Products</a>
							</li>
							<li>
								<a href="/products/create">Add Product</a>
							</li>
							<li>
								<a href="/product/import">Import Products by csv</a>
							</li>
						</ul>
					</li>
					@endif
					@if (Session::get('is_allow_clients') > 0)
					<li>
						<a href="javascript:;" data-toggle="collapse" data-target="#customers"><i class="fa fa-fw fa-users"></i> Clients <i class="fa fa-fw fa-caret-down"></i></a>
						<ul id="customers" class="collapse">
							<li>
								<a href="/clients">List Clients</a>
							</li>
							<li>
								<a href="/clients/create">Add Clients</a>
							</li>
						</ul>
					</li>
					@endif
					@if(Session::get('is_allow_reports')>0)			
					<li>
						<a href="javascript:;" data-toggle="collapse" data-target="#reports"><i class="fa fa-fw fa-bar-chart-o"></i> Reports <i class="fa fa-fw fa-caret-down"></i></a>
						<ul id="reports" class="collapse">
							<li>
								<a href="/reports/sales/new">Sales Invoice Report</a>
							</li>
							<li>
								<a href="/reports/products/new">Product Report</a>
							</li>
							<li>
								<a href="/reports/clients/new">Client Report</a>
							</li>
							<li>
								<a href="/reports/payments/new">Payment Report</a>
							</li>
						</ul>
					</li>
					@endif
					<li>
						<a href="javascript:;" data-toggle="collapse" data-target="#settings"><i class="fa fa-fw fa-users"></i> Settings <i class="fa fa-fw fa-caret-down"></i></a>
						<ul id="settings" class="collapse">
							@if(Session::get('is_allow_settings')>0)
							<li>
								<a href="/settings">Application</a>
							</li>
							@endif
							@if(Session::get('is_allow_logs')>0)
							<li>
								<a href="/logs">Activity Logs</a>
							</li>
							@endif
							@if(Session::get('is_allow_roles')>0)					
							<li>
								<a href="/roles">Roles</a>
							</li>
							@endif
							@if(Session::get('is_allow_managers')>0)		
							<li>
								<a href="/managers">Managers</a>
							</li>							
							@endif
							@if(Session::get('is_allow_banks')>0)
							<li>
								<a href="/banks">Banks</a>
							</li>
							@endif
							@if(Session::get('is_allow_user')>0)
							<li>
								<a href="/users">Users</a>
							</li>
							@endif
						</ul>
					</li>
					
					<li>
						<a href="javascript:;" data-toggle="collapse" data-target="#logged_user"><i class="fa fa-fw fa-user"></i> Hi! {{ Session::get('username') }}<i class="fa fa-fw fa-caret-down"></i></a>
						<ul id="logged_user" class="collapse">
							<li>
								<a href="/settings/password/change">Change Password</a>
							</li>
							<li>
								<a href="/auth/logout">Logout</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
		<div id="page-wrapper">
			@yield('content')
		</div>
	</div>
	<script src="{{ URL::asset('js/jquery.js') }}"></script>
	<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ URL::asset('js/plugins/morris/raphael.min.js') }}"></script>
	<script src="{{ URL::asset('js/plugins/morris/morris.min.js') }}"></script>
	<script src="{{ URL::asset('js/app.js') }}"></script>
	<script src="{{ URL::asset('js/controllers.js') }}"></script>
	<script src="{{ URL::asset('js/providers.js') }}"></script>
	<script src="{{ URL::asset('js/constants.js') }}"></script>
	<script src="{{ URL::asset('js/moment.js') }}"></script>
	<script src="{{ URL::asset('js/bootstrap/bootstrap-datetimepicker.js') }}"></script>  
	<script src="{{ URL::asset('js/summernote.js') }}"></script>  
	<script type="text/javascript">
		$('.confirmation').on('click', function () {
			return confirm('Are you sure?');
		});
	</script>
	<script type="text/javascript">
		var d = new Date();  
		$(function () {
			$('#dtp1').datetimepicker({
				format:"YYYY-MM-DD",
				defaultDate: d,
			});
		});

		$(function () {
			$('#dtp2').datetimepicker({
				format:"YYYY-MM-DD",
			});
		});

	</script>
	<script>
	$(document).ready(function() {
	  $('#summernote').summernote({
	  height: 200,
	  minHeight: null,
	  maxHeight: null
		});
	});

	$(document).ready(function() {
	  $('#summernote2').summernote({
	  height: 200,
	  minHeight: null,
	  maxHeight: null
		});
	});

	$(document).ready(function() {
	  $('#summernote3').summernote({
	  height: 200,
	  minHeight: null,
	  maxHeight: null
		});
	});
	</script>

@if(Session::has('print_now'))
<script>		
$('#myModal').modal('show');

</script>
@endif
</body>
</html>
