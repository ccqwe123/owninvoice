@extends('layout.app')
@section('title', 'User Role')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Users Role <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
			
		</div>
	</div>
	{{ Form::open(array('url' => '/users/'.$user->id.'/update-role', 'method' => 'store')) }}
	<div class="row top10">
		<div class="col-lg-4">
			{{ Form::label('role_id', 'User Role') }}
			<select class="form-control span6" name="role_id">
				<option value="0"></option>
				@foreach ($roles as $x)
					<option value="{!! $x->id !!}" @if($role_id==$x->id) selected="selected" @endif>{!! $x->role_name !!}</option>
				@endforeach
			</select>
			<span class="errors" style="color:#FF0000">{{$errors->first('role_id')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			<input type="submit" class="btn btn-default" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
		</div>
	</div>
	{!! Form::close() !!}
</div>	
@stop