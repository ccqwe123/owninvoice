@extends('layout.app')
@section('title', 'User List')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Users <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif
			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-search"></i> User Search
				</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::open(array('url' => '/users', 'method' => 'get')) }}  
			<div class="input-group margin-bottom-sm">
			    <span class="input-group-addon"><button class="fa fa-search" style="border:none; background-color:transparent;"></button></span>
			    {{ Form::text('search','',array('class'=>'form-control span6','placeholder' => 'Search')) }}
			    <span class="errors" style="color:#FF0000">{{$errors->first('search')}}</span>
			</div>
			{{ Form::close() }}
		</div>
		<div class="col-lg-4">
			<a href="/users/create"><button class="btn btn-primary">Add User</button></a>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-fw fa-users"></i> User List
				</li>
			</ol>
			
		</div>
	</div>
	<table class="table" style="border: 1px solid black;">
		<tr style="border: 1px solid black;">
			<th>Username</th>
			<th>Name</th>
			<th>Contact</th>
			<th>Role</th>
			@if (Session::get('is_allow_invoice') > 0) <th></th> @endif
		</tr>
			@foreach ($users as $x)
			<tr style="border: 1px solid black;">
				<td>{{ $x->username }}</td>
				<td>{{ $x->first_name }} {{ $x->middle_name }} {{ $x->last_name }}</td>
				<td>{{ $x->contact }}</td>
				<td>{{ $x->role_name }}</td>
				<td>
					<div class="dropdown">
						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
						<span class="caret"></span></button>
						<ul class="dropdown-menu">
							@if (Session::get('is_allow_invoice') > 0)
							<li><a href="/users/{{$x->id}}/role" title="Roles"><i class="fa fa-unlock-alt" aria-hidden="true"></i>Roles</a></li> 
							@endif
							<li><a href="/users/{{$x->id}}/edit" title="Edit"><i class="fa fa-pencil margin-right"></i>Edit</a></li>		
							<li><a href="/user-delete/{{$x->id}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>Delete</a></li>
						</ul>
					</div>
				</td>
			</tr>
			@endforeach 
	</table>
	<div style="text-align: center">
		{{ $users->links() }}
	</div>
</div>
@stop