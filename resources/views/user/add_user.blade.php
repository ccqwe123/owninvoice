@extends('layout.app')
@section('title', 'Add User')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Users <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-fw fa-users" aria-hidden="true"></i> Add User
				</li>
			</ol>
		</div>
	</div>
	{{ Form::open(array('url' => '/users', 'method' => 'store','files'=>true)) }}
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('username', 'Username') }}
			{{ Form::text('username','',array('class'=>'form-control span6','placeholder' => 'Username')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('username')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('password', 'Password') }}
			{{ Form::password('password',array('class'=>'form-control span6','placeholder' => 'Password')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('password')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			{{ Form::label('first_name', 'First Name') }}
			{{ Form::text('first_name','',array('class'=>'form-control span6','placeholder' => 'First Name')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('first_name')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			{{ Form::label('middle_name', 'Middle Name') }}
			{{ Form::text('middle_name','',array('class'=>'form-control span6','placeholder' => 'Middle Name')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('middle_name')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			{{ Form::label('last_name', 'Last Name') }}
			{{ Form::text('last_name','',array('class'=>'form-control span6','placeholder' => 'Last Name')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('last_name')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			{{ Form::label('contact', 'Contact Number') }}
			{{ Form::text('contact','',array('class'=>'form-control span6','placeholder' => 'Contact Number')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('contact')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			{{ Form::label('photo', 'Photo') }}
			{!! Form::file('photo') !!}
			<span class="errors" style="color:#FF0000">{{$errors->first('photo')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			<input type="submit" value="Submit" class="btn btn-default" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
		</div>
	</div>
	{!! Form::close() !!}
</div>
@stop