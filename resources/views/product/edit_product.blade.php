@extends('layout.app')
@section('title', 'Edit Product')
@section('app_name', Session::get('software_name'))
@section('content')



<div style=" height: 800px; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Products <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-pencil" aria-hidden="true"></i> Edit Product
				</li>
			</ol>
		</div>
	</div>
	{{ Form::open(array('url' => '/products/'.$product[0]->id, 'method' => 'PUT')) }}
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('prod_code', 'Product Code') }}
			{{ Form::text('prod_code',$product[0]->prod_code,array('class'=>'form-control span6','placeholder' => 'Product Code')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('prod_code')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			{{ Form::label('prod_name', 'Product name') }}
			{{ Form::text('prod_name',$product[0]->prod_name,array('class'=>'form-control span6','placeholder' => 'Product Name')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('prod_name')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			{{ Form::label('prod_description', 'Product Description') }}
			{{ Form::text('prod_description',$product[0]->prod_description,array('class'=>'form-control span6','placeholder' => 'Product Description')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('prod_description')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			{{ Form::label('prod_bar_code', 'Product Bar Code') }}
			{{ Form::text('prod_bar_code',$product[0]->prod_bar_code,array('class'=>'form-control span6','placeholder' => 'Product Bar Code')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('prod_bar_code')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			{{ Form::label('prod_price', 'Product Price') }}
			{{ Form::text('prod_price',$product[0]->prod_price,array('class'=>'form-control span6','placeholder' => 'Product Price')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('prod_price')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			<input type="submit" class="btn btn-default" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
		</div>
	</div>
	{!! Form::close() !!}
</div>
@stop