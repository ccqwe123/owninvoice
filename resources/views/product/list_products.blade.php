@extends('layout.app')
@section('title', 'Product List')
@section('app_name', Session::get('software_name'))
@section('content')

<div style="height: 100%; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Products <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif
			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-search"></i> Product Search
				</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-3">
			{{ Form::open(array('url' => '/products', 'method' => 'get')) }}  
			<div class="input-group margin-bottom-sm">
			    <span class="input-group-addon"><button class="fa fa-search" style="border:none; background-color:transparent;"></button></span>
			    {{ Form::text('search','',array('class'=>'form-control span6','placeholder' => 'Search')) }}
			    <span class="errors" style="color:#FF0000">{{$errors->first('search')}}</span>
			</div>
			{{ Form::close() }}
		
		</div>
		<div class="col-lg-2">
			<a href="{{ URL::to('product/export/csv') }}">
				<button class="btn dark-grey padding-large margin-top margin-bottom">
					<i class="fa fa-table margin-right"></i>Export to csv
				</button>
			</a>
		</div>
		<div class="col-lg-2">
			<a href="{{ URL::to('product/export/xls') }}">
				<button class="btn dark-grey padding-large margin-top margin-bottom">
					<i class="fa fa-file-excel-o margin-right"></i>Export to xls
				</button>
			</a>
		</div>
		<div class="col-lg-2">
			<a href="{{ URL::to('product/export/xlsx') }}">
				<button class="btn dark-grey padding-large margin-top margin-bottom">
					<i class="fa fa-file-excel-o margin-right"></i>Export to xlsx
				</button>
			</a>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-shopping-basket"></i> Product List
				</li>
			</ol>
			
		</div>
	</div>


	<table class="table" style="border: 1px solid black;overflow: scroll;white-space: nowrap;font-size: {{ Session('font_size') }}px;">
		<tr style="border: 1px solid black;">
			@if(Request::get('sortby')!='prod_code.asc')
				<th><a href="{{ URL::route('product-search',['sortby'=>'prod_code.asc']) }}">Product Code<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='prod_code.asc')
				<th><a href="{{ URL::route('product-search',['sortby'=>'prod_code.desc']) }}">Product Code<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif

			@if(Request::get('sortby')!='prod_name.asc')
				<th><a href="{{ URL::route('product-search',['sortby'=>'prod_name.asc']) }}">Product Name<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='prod_name.asc')
				<th><a href="{{ URL::route('product-search',['sortby'=>'prod_name.desc']) }}">Product Name<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif

			<th>Product Description</th>
			@if(Request::get('sortby')!='prod_price.asc')
				<th><a href="{{ URL::route('product-search',['sortby'=>'prod_price.asc']) }}">Product Price<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='prod_price.asc')
				<th><a href="{{ URL::route('product-search',['sortby'=>'prod_price.desc']) }}">Product Price<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif

			@if(Request::get('sortby')!='prod_bar_code.asc')
				<th><a href="{{ URL::route('product-search',['sortby'=>'prod_bar_code.asc']) }}">Product Bar Code<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='prod_bar_code.asc')
				<th><a href="{{ URL::route('product-search',['sortby'=>'prod_bar_code.desc']) }}">Product Bar Code<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif

			@if(Request::get('sortby')!='status.asc')
				<th><a href="{{ URL::route('product-search',['sortby'=>'status.asc']) }}">Status<i class="fa fa-caret-down" aria-hidden="true"></i></a></th>
			@else(Request::get('sortby')=='status.asc')
				<th><a href="{{ URL::route('product-search',['sortby'=>'status.desc']) }}">Status<i class="fa fa-caret-up" aria-hidden="true"></i></a></th>
			@endif
			<th></th>
		</tr>
		  	@foreach ($products as $x)
			<tr style="border: 1px solid black;white-space: normal;">
				<td>{{ $x->prod_code }}</td>
				<td>{{ $x->prod_name }}</td>
				<td>{{ $x->prod_description }}</td>
				<td>{{ number_format($x->prod_price,Session('decimal')) }}</td>
				<td>{{ $x->prod_bar_code }}</td>
				<td>
				@if ($x->status==1)
				Active 
				@else Inactive
				@endif
				</td>
				<td>
					<div class="dropdown">
						<button class="btn btn-primary dropdown-toggle btn-sm" type="button" data-toggle="dropdown">Actions
						<span class="caret"></span></button>
						<ul class="dropdown-menu dropdown-menu-right">
							@if ($x->status==1)
							<li><a href="/products/{{$x->id}}/active"><i class="fa fa-pencil margin-right"></i>Set Inactive</a></li>
							@else
							<li><a href="/products/{{$x->id}}/active"><i class="fa fa-pencil margin-right"></i>Set Active</a></li>
							@endif
							
							<li><a href="/products/{{$x->id}}/edit"><i class="fa fa-pencil margin-right"></i>Edit</a></li>
							<li><a href="/product-delete/{{$x->id}}" class="confirmation"><i class="fa fa-trash margin-right"></i>Delete</a></li>
						</ul>
					</div>
				</td>
			</tr>
			@endforeach 
	</table>
	<div style="text-align: center">
		@if(Request::get('sortby')!=null)
			{{ $products->appends(['sortby' => Request::get('sortby')])->links() }}
		@else
			{{ $products->links() }}
		@endif
		
	</div>
</div>
@stop