@extends('layout.app')
@section('title', 'Edit Manager')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 800px; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Managers <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif
			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-pencil" aria-hidden="true"></i> Edit Managers
				</li>
			</ol>
		</div>
	</div>
	{{ Form::open(array('url' => '/managers/'.$manager[0]->id, 'method' => 'PUT')) }}
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('emp_id', 'Employee ID') }}
			{{ Form::text('emp_id',$manager[0]->emp_id,array('class'=>'form-control span6','placeholder' => 'Employee ID')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('emp_id')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			{{ Form::label('first_name', 'First Name') }}
			{{ Form::text('first_name',$manager[0]->first_name,array('class'=>'form-control span6','placeholder' => 'First Name')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('first_name')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			{{ Form::label('middle_name', 'Middle Name') }}
			{{ Form::text('middle_name',$manager[0]->middle_name,array('class'=>'form-control span6','placeholder' => 'Middle Name')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('middle_name')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			{{ Form::label('last_name', 'Last Name') }}
			{{ Form::text('last_name',$manager[0]->last_name,array('class'=>'form-control span6','placeholder' => 'Last Name')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('last_name')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			{{ Form::label('user_id', 'User ID') }}
			<select class="form-control span6" name="user_id" ng-model="user_id" >
				<option value="0"></option>
				@foreach ($users as $x)
					<option value="{!! $x->id !!}" 
					<?php
						if ($manager[0]->user_id==$x->id)
						echo 'selected';
					?>
					>{!! $x->username !!}</option>
				@endforeach
			</select>
			<span class="errors" style="color:#FF0000">{{$errors->first('user_id')}}</span>
		</div>
	</div>

	<div class="row top10">
		<div class="col-lg-4">
			<input type="submit" class="btn btn-default" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
		</div>
	</div>
	{!! Form::close() !!}
</div>
@stop