@extends('layout.app')
@section('title', 'Branch List')
@section('app_name', Session::get('software_name'))
@section('content')


<div style=" height: 800px; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				{{ $client->client_name }} <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif

			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-search"></i> Branch Search
				</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::open(array('url' => '/clients/'.$client->id.'/branch', 'method' => 'get')) }}  
			{{ Form::text('search','',array('class'=>'form-control span6','placeholder' => 'Search')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('search')}}</span>{{ Form::submit('Search', array('class'=>'btn btn-default')) }}
			{{ Form::close() }}
			<button class="btn btn-default"><a href="/clients/{{$client->id}}/branch/create">Add Branch</a></button>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-shopping-basket"></i> Branch List
				</li>
			</ol>
			
		</div>
	</div>
	<table class="table" style="border: 1px solid black;white-space: nowrap;font-size: 10px;">
		<tr style="border: 1px solid black;white-space: normal;">
			<th>Branch Name</th>
			<th>Contact Person</th>
			<th>Tel No</th>
			<th>Mobile No</th>
			<th>Fax No</th>
			<th>Address</th>
			<th>Total Invoice Amount</th>
			<th>Total Balance Due</th>
			<th></th>
		</tr>
		  	@foreach ($branches as $x)
			<tr style="border: 1px solid black;">
				<td>{{ $x->branch_name }}</td>
				<td>{{ $x->contact_person }}</td>
				<td>{{ $x->tel_no }}</td>
				<td>{{ $x->mobile_no }}</td>
				<td>{{ $x->fax_no }}</td>
				<td>{{ $x->address }}</td>
				<td>{{ $x->amount_total }}</td>
				<td>{{ $x->amount_total-($x->check_amount+$x->cash_amount)}}</td>
				<td>
					<div class="dropdown">
						<button class="btn btn-primary dropdown-toggle btn-sm" type="button" data-toggle="dropdown">Actions
						<span class="caret"></span></button>
						<ul class="dropdown-menu dropdown-menu-right">						
							<li><a href="/clients/{{$client->id}}/branch/{{$x->id}}/bill" title="Branch"><i class="fa fa-money" aria-hidden="true"></i>SOA</a></li>
							<li><a href="/clients/{{$client->id}}/branch/{{$x->id}}/edit" title="Edit"><i class="fa fa-pencil margin-right"></i>Edit</a></li>
							<li><a href="/client/{{$client->id}}/branch-delete/{{$x->id}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>Delete</a></li>
						</ul>
					</div>
				</td>
				
			</tr>
			@endforeach 
	</table>
	<div style="text-align: center">
		{{ $branches->links() }}
	</div>
</div>	
@stop