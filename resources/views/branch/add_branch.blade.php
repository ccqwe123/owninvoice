@extends('layout.app')
@section('title', 'Add Branch')
@section('app_name', Session::get('software_name'))
@section('content')

<div style=" height: 900px;width: 100%; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				{{ $client->client_name }} <small></small>
			</h1>
			@if(Session::has('flash_message'))
				<div class="alert alert-success">{{Session::get('flash_message')}}</div>
			@endif
			@if(Session::has('flash_error'))
				<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
			@endif
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-shopping-basket" aria-hidden="true"></i> Add Branch
				</li>
			</ol>
		</div>
	</div>
	{{ Form::open(array('url' => '/clients/'.$client->id.'/branch', 'method' => 'store')) }}
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('branch_name', 'Branch Name') }}
			{{ Form::text('branch_name','',array('class'=>'form-control span6','placeholder' => 'Branch Name')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('branch_name')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('contact_person', 'Contact Person') }}
			{{ Form::text('contact_person','',array('class'=>'form-control span6','placeholder' => 'Contact Person')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('contact_person')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('address', 'Address') }}
			{{ Form::text('address','',array('class'=>'form-control span6','placeholder' => 'Address')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('address')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('tel_no', 'Telephone') }}
			{{ Form::text('tel_no','',array('class'=>'form-control span6','placeholder' => 'Telephone')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('tel_no')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('mobile_no', 'Mobile') }}
			{{ Form::text('mobile_no','',array('class'=>'form-control span6','placeholder' => 'Mobile')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('mobile_no')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('fax_no', 'Fax') }}
			{{ Form::text('fax_no','',array('class'=>'form-control span6','placeholder' => 'Fax')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('fax_no')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('email', 'Branch Email') }}
			{{ Form::text('email','',array('class'=>'form-control span6','placeholder' => 'Branch Email')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('email')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-2">
			{{ Form::label('personal', 'Personal') }}
			{{ Form::checkbox('personal','1') }}
			<span class="errors" style="color:#FF0000">{{$errors->first('personal')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			<input type="submit" class="btn btn-primary" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
		</div>
	</div>
	{!! Form::close() !!}
</div>
@stop