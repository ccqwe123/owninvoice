@extends('layout.app')
@section('title', 'Edit Branch')
@section('app_name', Session::get('software_name'))
@section('content')



<div style=" height: 800px; overflow: scroll;">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
				Edit Branch <small></small>
			</h1>
			<ol class="breadcrumb">
				<li class="active">
					<i class="fa fa-pencil" aria-hidden="true"></i> Edit Branch
				</li>
			</ol>
		</div>
	</div>
	<div class="row">
		@if(Session::has('flash_message'))
			<div class="alert alert-success">{{Session::get('flash_message')}}</div>
		@endif

		@if(Session::has('flash_error'))
			<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
		@endif
	</div>

	{{ Form::open(array('url' => '/clients/'.$client->id.'/branch/'.$client_branch->id, 'method' => 'PUT')) }}

	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('branch_name', 'Branch Name') }}
			{{ Form::text('branch_name',$client_branch->branch_name,array('class'=>'form-control span6','placeholder' => 'Branch Name')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('branch_name')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('address', 'Address') }}
			{{ Form::text('address',$client_branch->address,array('class'=>'form-control span6','placeholder' => 'Address')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('address')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('contact_person', 'Contact Person') }}
			{{ Form::text('contact_person',$client_branch->contact_person,array('class'=>'form-control span6','placeholder' => 'Contact Person')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('contact_person')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('tel_no', 'Tel No') }}
			{{ Form::text('tel_no',$client_branch->tel_no,array('class'=>'form-control span6','placeholder' => 'Tel No')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('tel_no')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('mobile_no', 'Mobile No') }}
			{{ Form::text('mobile_no',$client_branch->mobile_no,array('class'=>'form-control span6','placeholder' => 'Mobile No')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('mobile_no')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('fax_no', 'Fax No') }}
			{{ Form::text('fax_no',$client_branch->fax_no,array('class'=>'form-control span6','placeholder' => 'Fax No')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('fax_no')}}</span>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			{{ Form::label('email', 'Branch Email') }}
			{{ Form::text('email',$client_branch->email,array('class'=>'form-control span6','placeholder' => 'Email')) }}
			<span class="errors" style="color:#FF0000">{{$errors->first('email')}}</span>
		</div>
	</div>
	<div class="row top10">
		<div class="col-lg-4">
			<input type="submit" class="btn btn-default" onclick="this.disabled=true;this.value='Submitted, please wait...';this.form.submit();" />
		</div>
	</div>
	{!! Form::close() !!}
</div>
@stop